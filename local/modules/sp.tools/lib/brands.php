<?php

namespace SP\Tools;

/**
 * Class Brands
 * @package SP\Tools
 */
class Brands
{
    public function getFilterURL($brandID, $sectionURL = '/catalog/')
    {
        return $sectionURL . '?' . CATALOG_FILTER_NAME . '_' . CATALOG_PROPERTY_BRAND_ID . '_' . abs(crc32($brandID)) . '=Y&set_filter=Y';
    }

    public function getBrandInfo($brandID)
    {
        $result = [];
        $select = ['ID', 'NAME'];
        $filter = ['ID' => $brandID, 'IBLOCK_ID' => IBLOCK_BRANDS];
        $res = \CIBlockElement::GetList(Array(), $filter, false, false, $select);
        if ($ob = $res->Fetch()) {
            $result = $ob;
        }
        return $result;
    }

    public function getCatalogItemsByBrand($brandsID)
    {
        $result = [];
        $arSelect = Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_BRAND");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG, "PROPERTY_BRAND" => $brandsID, "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->Fetch()) {
            $result[$ob['PROPERTY_BRAND_VALUE']][$ob['IBLOCK_SECTION_ID']] = $ob['IBLOCK_SECTION_ID'];
        }
        return $result;
    }

    public function getBrandByCode($code) {
        $result = [];
        $select = ['ID'];
        $filter = ['=CODE' => $code, 'IBLOCK_ID' => IBLOCK_BRANDS];
        $res = \CIBlockElement::GetList(Array(), $filter, false, false, $select);
        if ($ob = $res->Fetch()) {
            $result = $ob;
        }
        return $result;
    }

    public function getBrandsByFilter($filter = [], $select = []) {
        $result = [];
        $res = \CIBlockElement::GetList(Array(), $filter, false, false, $select);
        while ($ob = $res->Fetch()) {
            $result[$ob['ID']] = $ob;
        }
        return $result;
    }
}