<?php
namespace SP\Tools;

use \Bitrix\Main\Service\GeoIp;
/**
 * Class GEOTools
 * @package SP\Tools
 */

class GEOTools
{
    public function getUserCityInfo() {

        $ipAddress = GeoIp\Manager::getRealIp();
        $result = GeoIp\Manager::getDataResult($ipAddress, "ru")->getGeoData();

        return $result;
    }

    public function getCitiesList() {
        $cities = [];
        $res = \Bitrix\Sale\Location\DefaultSiteTable::getList(array(
            'filter' => array(),
            'select' => array('LOCATION_CODE')
        ));
        while($item = $res->fetch())
        {
            $locations[] = $item['LOCATION_CODE'];
        }

        if ($locations) {
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'CODE' => $locations),
                'select' => array('ID', 'NAME_RU' => 'NAME.NAME')
            ));
            while($item = $res->fetch())
            {
                $cities[$item['ID']] = $item;
            }
        }

        return $cities;
    }

    public function getUserCityCodeByName($name) {
        $id = null;
        $res = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array('=NAME.NAME' => $name),
            'select' => array('ID')
        ));
        if ($item = $res->fetch())
        {
            $id = $item['ID'];
        }

        return $id;
    }
}