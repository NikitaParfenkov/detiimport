<?php

namespace SP\Tools;

/**
 * Class Shops
 * @package SP\Tools
 */
class Shops
{
    protected $iblockID = 1;

    public function getShopsCount($filter = [])
    {
        $arFilter = Array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y");
        if ($filter) {
            $arFilter = array_merge($arFilter, $filter);
        }
        $res = \CIBlockElement::GetList(Array(), $arFilter, []);
        $result = $res['ELEMENT_CNT'];

        return $result;
    }

    public function getShopsCities()
    {
        $result = [];
        $arSelect = Array("ID", "PROPERTY_CITY");
        $arFilter = Array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->Fetch()) {
            $result[$ob['PROPERTY_CITY_VALUE']] = $ob['PROPERTY_CITY_VALUE'];
        }

        return $result;
    }
}