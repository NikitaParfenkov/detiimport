<?php

namespace SP\Tools;

/**
 * Class Stores
 * @package SP\Tools
 */
class Stores
{
    public function getStoreProducts($stores = [])
    {
        $result = [];
        $arSelect = Array("PRODUCT_ID", "STORE_ID");
        $arFilter = Array("STORE_ID" => $stores, '>AMOUNT' => 0);
        $res = \CCatalogStoreProduct::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->Fetch()) {
            $result[$ob['STORE_ID']][] = $ob['PRODUCT_ID'];
        }

        return $result;
    }
}