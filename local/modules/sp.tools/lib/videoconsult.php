<?php

namespace SP\Tools;

/**
 * Class VideoConsult
 * @package SP\Tools
 */
class VideoConsult
{
    private $iblockID = 9;

    public function getMainContactInfo($select = []) {
        $filter = ["IBLOCK_ID" => $this->iblockID, "PROPERTY_MAIN_CONTACT_VALUE" => "Y", "ACTIVE" => "Y"];
        $res = \CIBlockElement::GetList(['sort' => 'asc'], $filter, false, ["nTopCount" => 1], $select);
        $ob = $res->Fetch();

        return $ob;
    }
}