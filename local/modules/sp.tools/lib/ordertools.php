<?php

namespace SP\Tools;

use \Bitrix\Main\Loader;
use \Bitrix\Sale;
use \Bitrix\Sale\Order;
use \Bitrix\Main\Context;
use \Bitrix\Sale\Delivery;
use  Bitrix\Currency\CurrencyManager;
use  Bitrix\Sale\Basket;

Loader::includeModule('sale');

/**
 * Class OrderTools
 * @package SP\Tools
 */
class OrderTools
{

    /**
     * @var array
     */
    private $data = [];
    private $errors = [];

    /**
     * OrderTools constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     * @throws \Bitrix\Main\SystemException
     */
    public function saveOrderOneClick()
    {
        global $USER;
        $this->validateFields();
        if (empty($this->errors)) {
            $order = Order::create(Context::getCurrent()->getSite(),
                $USER->isAuthorized() ? $USER->GetID() : \CSaleUser::GetAnonymousUserID());

            $order->setPersonTypeId(1);

            if ($this->data['ELEMENT_ID']) {
                $currencyCode = CurrencyManager::getBaseCurrency();
                $siteId = Context::getCurrent()->getSite();
                $basket = Basket::create($siteId);
                $item = $basket->createItem('catalog', $this->data['ELEMENT_ID']);
                $item->setFields(array(
                    'QUANTITY' => 1,
                    'CURRENCY' => $currencyCode,
                    'LID' => $siteId,
                    'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                ));
            } else {
                $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());
            }
            $order->setBasket($basket);
            $shipmentCollection = $order->getShipmentCollection();
            $shipment = $shipmentCollection->createItem();
            $service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
            $shipment->setFields([
                'DELIVERY_ID' => $service['ID'],
                'DELIVERY_NAME' => $service['NAME'],
            ]);
            $propertyCollection = $order->getPropertyCollection();
            $propertyCollection->getPhone()->setValue($this->data['phone']);
            $propertyCollection->getPayerName()->setValue($this->data['name']);
            $propertyCollection->getUserEmail()->setValue($this->data['email']);
            $order->setField('USER_DESCRIPTION', $this->data['email']);
            $order->doFinalAction(true);
            $tmlResult = $order->save();
            if ($tmlResult->isSuccess()) {
                return (int)$tmlResult->getId();
            } else {
                throw new \Exception($tmlResult->getErrors());
            }
        } else {
            throw new \Exception(implode('<br>', $this->errors));
        }
    }

    private function validateFields()
    {
        if ($this->data['privacy'] != 'on') {
            $this->errors[] = 'Установите галочку согласия с политикой конфиденциальности';
        }
        if (! check_email($this->data['email'])) {
            $this->errors[] = 'E-mail адрес не корректен';
        }
    }
}