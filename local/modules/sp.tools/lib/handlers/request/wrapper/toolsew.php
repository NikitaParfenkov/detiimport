<?php
/**
 * Created by PhpStorm.
 * User: Pavel Sidorenko
 * Date: 04.01.2018
 * Time: 10:05
 */

namespace SP\Tools\Handlers\Request\Wrapper;

use SP\Tools\Handlers\Request\EntityWrapper,\Bitrix\Main\Loader, \Bitrix\Main\Mail\Event;

class ToolsEW extends EntityWrapper
{
    private $datas;

    public function __construct($datas)
    {
        $this->datas = $datas;

        parent::__construct($this->datas);
        parent::checkRequestMethod('post');

    }


    public function get()
    {
        switch ($this->datas['method']) {
            case 'compfav':
                $_SESSION[$this->datas['add']][$this->datas['id']] = $this->datas['id'];
                $result = 'success';
                break;
            case 'compfavdelete':
                unset ($_SESSION[$this->datas['add']][$this->datas['id']]);
                $result = 'success';
                break;
        }
        return $result;
    }
}