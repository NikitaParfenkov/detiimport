<?php
/**
 * Created by PhpStorm.
 * User: Pavel Sidorenko
 * Date: 23.09.2021
 * Time: 10:05
 */

namespace SP\Tools\Handlers\Request\Wrapper;

use SP\Tools\Handlers\Request\EntityWrapper;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\Json;
use \Bitrix\Iblock\SectionTable;
use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem;

\CModule::IncludeModule('iblock');
\CModule::IncludeModule("sale");
\CModule::IncludeModule("catalog");


class BasketEW extends EntityWrapper{

    private $data;
    private $result;

    public function __construct($data)
    {
        $this->data = $data;

        parent::__construct($this->data);
        parent::checkRequestMethod('post');
    }

    public function add2basket($id = 0,$quantity = 1,$properties = []){

        if($quantity<=0){$quantity = 1;}
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        if ($item = $basket->getExistsItem('catalog', $id)) {
            $item->setField('QUANTITY', $item->getQuantity() + $quantity);
        }
        else {
            $item = $basket->createItem('catalog', $id);
            $itemFields = array(
                'QUANTITY' => $quantity,
                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            );
            if (!empty($properties)) {
                $basketPropertyCollection = $item->getPropertyCollection();
                $arParams = array("replace_space"=>"-","replace_other"=>"-");
                if (is_array($properties)) {
                    foreach ($properties as $name => $property) {
                        $basketPropertyCollection->setProperty(array(
                            array(
                                'NAME' => $name,
                                'CODE' => \Cutil::translit($name,"ru",$arParams),
                                'VALUE' => $property,
                                'SORT' => 100,
                            ),
                        ));
                    }
                } else {
                    $basketPropertyCollection->setProperty(array(
                        array(
                            'NAME' => $properties['name'],
                            'CODE' => \Cutil::translit($properties['name'],"ru",$arParams),
                            'VALUE' => $properties['value'],
                            'SORT' => 100,
                        ),
                    ));
                }
            }
            $item->setFields($itemFields);

        }
        $basket->save();
        return $item->getId();
    }

    public function delete($id) {
        $result = \CSaleBasket::Delete(intval($id));
        return $result;
    }

    public function update($id, $quantity, $properties = []) {
        $arFields = array(
            "QUANTITY" => intval($quantity),
        );
        if (!empty($properties)) {
            $arParams = array("replace_space"=>"-","replace_other"=>"-");
            foreach ($properties as $key => $property) {
                $arFields['PROPS'][] = [
                    'NAME' => $key,
                    'CODE' => \Cutil::translit($key,"ru",$arParams),
                    'VALUE' => $property,
                    'SORT' => 100,
                ];
            }

        }
        $result = \CSaleBasket::Update(intval($id), $arFields);
        if ($result) {
            return $result;
        } else {
            return 'При обновлении товара возникла ошиибка';
        }
    }

    public function setCoupon($coupon)
    {
        \Bitrix\Sale\DiscountCouponsManager::clear(true);
        Sale\DiscountCouponsManager::add($coupon);
        return true;
    }

    public function clearCoupon()
    {
        \Bitrix\Sale\DiscountCouponsManager::clear(true);
        return true;
    }

    private function clearBasket()
    {
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        $basket->getBasket()->clearCollection();
        $clearBasketResult = $basket->save();
        $clearBasket = ['status' => false];
        if (! $clearBasketResult->isSuccess()) {
            $clearBasket['message'] = $clearBasketResult->getErrors();
        } else {
            $clearBasket['status'] = true;
        }
        return $clearBasket;
    }
    public function get() {

        switch($this->data['method'])
        {
            case 'add2basket':
                global $APPLICATION;
                $this->add2basket($this->data['id'], $this->data['quantity']);
                $mxResult = \CCatalogSku::GetProductInfo($this->data['id']);
                if (is_array($mxResult))
                {
                    $productID = $mxResult['ID'];
                } else {
                    $productID = $this->data['id'];
                }
                ob_start();
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.element",
                    "basket_add",
                    Array(
                        'OFFER_ID' => $this->data['id'],
                        "ACTION_VARIABLE" => "action",
                        "ADD_DETAIL_TO_SLIDER" => "N",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_TO_BASKET_ACTION" => array("BUY"),
                        "ADD_TO_BASKET_ACTION_PRIMARY" => array("BUY"),
                        "BACKGROUND_IMAGE" => "-",
                        "BASKET_URL" => "/personal/basket.php",
                        "BRAND_USE" => "N",
                        "BROWSER_TITLE" => "-",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_SECTION_ID_VARIABLE" => "N",
                        "COMPATIBLE_MODE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_PICTURE_MODE" => array("POPUP", "MAGNIFIER"),
                        "DETAIL_URL" => "",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PREVIEW_TEXT_MODE" => "E",
                        "ELEMENT_CODE" => "",
                        "ELEMENT_ID" => $productID,
                        "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
                        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
                        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
                        "GIFTS_MESS_BTN_BUY" => "Выбрать",
                        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                        "GIFTS_SHOW_IMAGE" => "Y",
                        "GIFTS_SHOW_NAME" => "Y",
                        "GIFTS_SHOW_OLD_PRICE" => "Y",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                        "IBLOCK_ID" => "4",
                        "IBLOCK_TYPE" => "1c_catalog",
                        "IMAGE_RESOLUTION" => "16by9",
                        "LABEL_PROP" => array(),
                        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                        "LINK_IBLOCK_ID" => "",
                        "LINK_IBLOCK_TYPE" => "",
                        "LINK_PROPERTY_SID" => "",
                        "MAIN_BLOCK_PROPERTY_CODE" => array(),
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_COMMENTS_TAB" => "Комментарии",
                        "MESS_DESCRIPTION_TAB" => "Описание",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "MESS_PRICE_RANGES_TITLE" => "Цены",
                        "MESS_PROPERTIES_TAB" => "Характеристики",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "OFFERS_FIELD_CODE" => array("NAME", ""),
                        "OFFERS_LIMIT" => "0",
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "OFFER_ADD_PICT_PROP" => "-",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array("РРЦ"),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
                        "PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "SECTION_CODE" => "",
                        "SECTION_ID" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "",
                        "SEF_MODE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_CANONICAL_URL" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SET_VIEWED_IN_COMPONENT" => "N",
                        "SHOW_404" => "N",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DEACTIVATED" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_SLIDER" => "N",
                        "STRICT_SECTION_CHECK" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "USE_COMMENTS" => "N",
                        "USE_ELEMENT_COUNTER" => "Y",
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "USE_GIFTS_DETAIL" => "N",
                        "USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "USE_RATIO_IN_RANGES" => "N",
                        "USE_VOTE_RATING" => "N"
                    )
                );
                $html = ob_get_contents();
                ob_end_clean();
                $result['HTML'] = $html;
                break;
            case 'delete':
                $result = $this->data['id'];
                $this->delete($this->data['id']);
                break;
            case 'update':
                $result = $this->update($this->data['id'], $this->data['quantity'], $this->data['properties']);
                break;
            case 'clearBasket':
                $result = $this->clearBasket();
                break;
        }

        return $result;
    }
}


