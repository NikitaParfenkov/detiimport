<?php
/**
 * Created by PhpStorm.
 * User: Pavel Sidorenko
 * Date: 23.09.2021
 * Time: 10:05
 */

namespace SP\Tools\Handlers\Request\Wrapper;

use SP\Tools\Handlers\Request\EntityWrapper;
use SP\Tools\OrderTools;
use SP\Tools\Subscribe;

/**
 * Class FormsEW
 * @package SP\Tools\Handlers\Request\Wrapper
 */
class FormsEW extends EntityWrapper
{
    /**
     * @var
     */
    private $data;
    /**
     * @var array
     */
    private $def_params = [
    ];


    /**
     * FormsEW constructor.
     * @param $data
     * @throws \Exception
     */
    public function __construct($data)
    {
        $this->data = $data;
        parent::__construct($this->data);
        parent::checkRequestMethod('post');
        parent::checkDataParams($this->def_params, $this->data);
    }


    /**
     * @return array|int|string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     * @throws \Bitrix\Main\SystemException
     */
    public function get()
    {
        $result = [];
        switch ($this->data['method']) {
            case 'saveSubscriber':
                $subscribeClass = new Subscribe($this->data['params']);
                $result = $subscribeClass->saveSubscriber();
                break;
            case 'orderOneClick':
                $orderToolsInstance = new OrderTools($this->data['params']);
                $result = $orderToolsInstance->saveOrderOneClick();
            break;
        }
        return $result;
    }
}