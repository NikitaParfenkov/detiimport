<?php

namespace SP\Tools;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

class HLTools
{

    public function getElementsByFilter($hlBlock, $select = [], $filter = [], $order = [])
    {
        $result = [];
        $hlblock = HL\HighloadBlockTable::getById($hlBlock)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $rsData = $entity_data_class::getList(array(
            "select" => $select,
            "order" => $order,
            "filter" => $filter
        ));
        while ($arData = $rsData->Fetch()) {
            $result[$arData['UF_XML_ID']] = $arData['UF_DESCRIPTION'];
        }

        return $result;
    }

    public function getElementsInfoByFilter($hlBlock, $select = [], $filter = [], $order = [])
    {
        $result = [];
        $hlblock = HL\HighloadBlockTable::getById($hlBlock)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $rsData = $entity_data_class::getList(array(
            "select" => $select,
            "order" => $order,
            "filter" => $filter
        ));
        while ($arData = $rsData->Fetch()) {
            $result[$arData['UF_XML_ID']] = $arData;
        }

        return $result;
    }
}