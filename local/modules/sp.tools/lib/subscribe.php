<?php

namespace SP\Tools;

use Bitrix\Main\Loader;
use \Bitrix\Iblock\ElementTable;

/**
 * Class Subscribe
 * @package SP\Tools
 */
class Subscribe
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Subscribe constructor.
     * @param array $data
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct(array $data)
    {
        Loader::includeModule('iblock');
        $this->data = $data;
    }

    /**
     * Сохраняет подписчика
     * @return string
     * @throws \Exception
     */
    public function saveSubscriber()
    {
        $result = [];
        $this->checkEmail();
        $this->checkEmailUser();
        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "NAME" => $this->data['email'],
            "IBLOCK_ID" => $this->data['iblockID'],
            "ACTIVE" => 'Y',
        ];
        if ($el->Add($arLoadProductArray)) {
            $result = 'Подписка оформлена.';
        } else {
            throw new \Exception('Возникла техническая ошибка');
        }
        return $result;
    }


    /**
     * Проверяет , есть ли уже пользователь с ввереднным E-mail
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Exception
     */
    private function checkEmailUser()
    {
        $dbItems = ElementTable::getList([
            'select' => ['ID', 'NAME'],
            'filter' => ["IBLOCK_ID" => $this->data['iblockID'], "ACTIVE" => "Y", 'NAME' => $this->data['email']],
            'limit' => 1,
            'count_total' => 1
        ]);
        if ($dbItems->getCount() > 0) {
            throw new \Exception('Подписка для вашего email уже оформлена.');
        }
    }

    /**
     * Проверяет E-mail адрес на корректность
     * @throws \Exception
     */
    private function checkEmail()
    {
        if (!check_email($this->data['email'])) {
            throw new \Exception('Не корректный E-mail адрес');
        }
    }
}