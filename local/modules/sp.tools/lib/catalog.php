<?php

namespace SP\Tools;

/**
 * Class Section_tools
 * @package SP\Tools
 */
\CModule::IncludeModule('iblock');
\CModule::IncludeModule('catalog');
\CModule::IncludeModule('sale');

class Catalog
{
    public static function updateSectionElementsCount()
    {
        $bs = new \CIBlockSection;
        $arFilter = array('IBLOCK_ID' => IBLOCK_CATALOG, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
        $db_list = \CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, true, ['ID']);
        while ($ar_result = $db_list->Fetch()) {
            $bs->Update($ar_result['ID'], ['UF_ELEMENT_COUNT' => $ar_result['ELEMENT_CNT']]);
        }
    }

    public static function updateSectionShowsCount()
    {
        $bs = new \CIBlockSection;
        $result = \Bitrix\Iblock\ElementTable::getList(array(
            'filter' => array('IBLOCK_ID' => IBLOCK_CATALOG),
            'group' => ['IBLOCK_SECTION_ID'],
            'select' => array(
                new \Bitrix\Main\Entity\ExpressionField(
                    'COUNT',
                    'SUM(%s)',
                    ['SHOW_COUNTER']
                ),
                'IBLOCK_SECTION_ID'
            ),

        ));

        while ($res = $result->fetch()) {
            $bs->Update($res['IBLOCK_SECTION_ID'], ['UF_SHOW_COUNTER' => $res['COUNT']]);
        }
    }

    public function getSectionsName($sectionsID)
    {
        $result = [];
        $arFilter = Array('IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $sectionsID);
        $arSelect = Array('ID', 'NAME');
        $dbList = \CIBlockSection::GetList(Array('sort' => 'asc'), $arFilter, false, $arSelect);
        while ($obResult = $dbList->Fetch()) {
            $result[$obResult['ID']] = $obResult['NAME'];
        }

        return $result;
    }

    public function getSectionInfo($filter = [], $select = [], $elementCnt = false)
    {

        $result = null;
        $dbRes = \CIBlockSection::GetList(Array('sort' => 'asc'), $filter, $elementCnt, $select);
        if ($arResult = $dbRes->GetNext()) {
            $result = $arResult;
        }

        return $result;

    }

    public function getSectionsInfo($filter = [], $select = [], $elementCnt = false, $arNav = false)
    {
        $result = [];
        $dbRes = \CIBlockSection::GetList(Array('sort' => 'asc'), $filter, $elementCnt, $select, $arNav);
        while ($arResult = $dbRes->GetNext()) {
            $result[$arResult['ID']] = $arResult;
        }

        return $result;
    }

    public function getProductsSectionsByFilter($filter = [])
    {
        $result = [];
        $select = ['ID', 'IBLOCK_SECTION_ID'];
        $res = \CIBlockElement::GetList(Array(), $filter, false, false, $select);
        while ($ob = $res->Fetch()) {
            $result[$ob['ID']] = $ob['IBLOCK_SECTION_ID'];
        }
        return $result;
    }

    public function getSearchSectionsHTML($sectionsInfo)
    {
        $result = '';
        if (!empty($sectionsInfo)) {
            foreach ($sectionsInfo as $section) {
                if ($section['IBLOCK_SECTION_ID']) {
                    $filter = ['ID' => $section['IBLOCK_SECTION_ID']];
                    $select = ['ID', 'NAME'];
                    $parentSectionName = $this->getSectionInfo($filter, $select, false)['NAME'];

                    $result .= sprintf('<a class="searchList__item" href="%s"><span class="searchList__content"><span class="searchList__title">%s</span><span class="searchList__subtitle">%s</span></span></a>',
                        $section['SECTION_PAGE_URL'],
                        $section['NAME'],
                        $parentSectionName
                    );
                } else {
                    $result .= sprintf('<a class="searchList__item" href="%s"><span class="searchList__content"><span class="searchList__title">%s</span></span></a>',
                        $section['SECTION_PAGE_URL'],
                        $section['NAME']
                    );
                }
            }
        }
        return $result;
    }

    public function getElementsCount($gFilter, $arParams)
    {

        $arCatalog = \CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
        if (!empty($arCatalog)) {
            $SKU_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
            $SKU_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
        }
        $arFilter = array(
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "IBLOCK_LID" => SITE_ID,
            "IBLOCK_ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
            "MIN_PERMISSION" => "R",
            "INCLUDE_SUBSECTIONS" => ($arParams["INCLUDE_SUBSECTIONS"] != 'N' ? 'Y' : 'N'),
        );
        if (($arParams['SECTION_ID'] > 0) || ($arParams["SHOW_ALL_WO_SECTION"] !== "Y")) {
            $arFilter["SECTION_ID"] = $arParams['SECTION_ID'];
        }

        if ($arParams['HIDE_NOT_AVAILABLE'] == 'Y') {
            $arFilter['AVAILABLE'] = 'Y';
        }

        $arPriceFilter = array();
        foreach ($gFilter as $key => $value) {
            if (\CProductQueryBuilder::isPriceFilterField($key)) {
                $arPriceFilter[$key] = $value;
                unset($gFilter[$key]);
            }
        }
        if (!empty($gFilter["OFFERS"])) {
            if (empty($arPriceFilter)) {
                $arSubFilter = $gFilter["OFFERS"];
            } else {
                $arSubFilter = array_merge($gFilter["OFFERS"], $arPriceFilter);
            }

            $arSubFilter["IBLOCK_ID"] = $SKU_IBLOCK_ID;
            $arSubFilter["ACTIVE_DATE"] = "Y";
            $arSubFilter["ACTIVE"] = "Y";
            if ('Y' == $this->arParams['HIDE_NOT_AVAILABLE']) {
                $arSubFilter['AVAILABLE'] = 'Y';
            }
            $arFilter["=ID"] = \CIBlockElement::SubQuery("PROPERTY_" . $SKU_PROPERTY_ID, $arSubFilter);
        } elseif (!empty($arPriceFilter)) {
            $arSubFilter = $arPriceFilter;

            $arSubFilter["IBLOCK_ID"] = $SKU_IBLOCK_ID;
            $arSubFilter["ACTIVE_DATE"] = "Y";
            $arSubFilter["ACTIVE"] = "Y";
            $arFilter[] = array(
                "LOGIC" => "OR",
                array($arPriceFilter),
                "=ID" => \CIBlockElement::SubQuery("PROPERTY_" . $SKU_PROPERTY_ID, $arSubFilter),
            );
        }
        unset($gFilter["OFFERS"]);

        $arFilter = array_merge($gFilter, $arFilter);
        $result = \CIBlockElement::GetList([], $arFilter, []);

        return $result;
    }

}