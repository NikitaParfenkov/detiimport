<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["INPUT_PLACEHOLDER"] = array(
	"NAME" => GetMessage("TP_BSP_INPUT_PLACEHOLDER"),
	"TYPE" => "STRING",
	"DEFAULT" => "",
);
?>
