<?
namespace Arturgolubev\Smartsearch;

use \Arturgolubev\Smartsearch\Unitools as UTools;

class Tools {
	const MODULE_ID = 'arturgolubev.smartsearch';
	var $MODULE_ID = 'arturgolubev.smartsearch';
	
	// for dresscode templates. Add sku ids for correct view
	static function dwAddSkuId($ids = array()){
		if(!empty($ids) && is_array($ids) && \CModule::IncludeModule("catalog")){			
			$skuList = \CCatalogSKU::getOffersList($ids, 0, array("ACTIVE"=>"Y"), array("ID"), array());
			foreach($skuList as $k=>$v){
				if(is_array($v) && !empty($v))
					$ids = array_merge($ids, array_keys($v));
			}
		}
		
		return array_unique($ids);
	}
	
	// Get ar product ids, by mixed sku+product ids
	static function getProductIdByMixed($ids = array()){
		$result = array();
		
		if(!empty($ids) && is_array($ids) && \CModule::IncludeModule("catalog")){
			foreach($ids as $id){
				$arRs = \CCatalogSku::GetProductInfo($id);				
				if(is_array($arRs) && !empty($arRs)){
					$result[] = $arRs["ID"];
				}else{
					$result[] = $id;
				}
			}
		}
		
		return array_unique($result);
	}
	
	static function getMinWordLenght(){
		$min_length = IntVal(UTools::getSetting("min_length"));
		if($min_length <= 0) $min_length = 3;
		
		return $min_length;
	}
	
	static function getReplaceParams(){
		return array("replace_space" => "", "replace_other" => "");
	}
	
	static function dbQuery($q){
		global $DB;
		return $DB->Query($q);
	}
	
	static function array_md5($array) {
		if(is_array($array) && !empty($array)){
			ksort($array);
			
			$s = '';
			foreach($array as $k=>$v){
				$s .= $k.'_'.$v.'-';
			}
			
			return md5($s);
		}
		
		return 0;
	}
}