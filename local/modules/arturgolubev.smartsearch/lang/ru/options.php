<?
$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_OPTIONS_REINDEX"] = "Переиндексация поиска";

$MESS["ARTURGOLUBEV_SMARTSEARCH_STATISTIC_LINKS"] = "Статистика";
$MESS["ARTURGOLUBEV_SMARTSEARCH_STATISTIC_QUERY_LIST"] = "Статистика поисковых фраз";
$MESS["ARTURGOLUBEV_SMARTSEARCH_STATISTIC_PAGE_OPEN"] = "Статистика переходов на страницу поиска";



$MESS["ARTURGOLUBEV_SMARTSEARCH_DEMO_IS_EXPIRED"] = "Демонстрационный период работы решения закончился. Для дальнейшего использования необходимо приобрести полую версию решения в <a href=\"http://marketplace.1c-bitrix.ru/solutions/arturgolubev.smartsearch/\" target=\"_blank\">marketplace.1c-bitrix.ru</a>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_VISUAL_TAB_NAME"] = "Визуальные";
$MESS["ARTURGOLUBEV_SMARTSEARCH_VISUAL_TAB_TITLE"] = "Настройки стандартного шаблона решения";

$MESS["ARTURGOLUBEV_SMARTSEARCH_INPUT_SEARCH_PLACEHOLDER"] = "Текст в поле ввода поискового запроса:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_INPUT_SEARCH_PLACEHOLDER_MOVED"] = "Параметр перенесён в настройки компонентов (Текст в поле ввода поискового запроса)";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME"] = "Тема оформления:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_MY_COLOR_THEME"] = "Свой цвет оформления:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME_BLUE"] = "Синяя";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME_BLACK"] = "Тёмная";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME_YELLOW"] = "Желтая";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME_GREEN"] = "Зеленая";
$MESS["ARTURGOLUBEV_SMARTSEARCH_COLOR_THEME_RED"] = "Красная";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SHOW_PRELOADER"] = "<span data-hint='Настройка для интерактивного поиска'></span>Показать анимацию загрузки:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SHOW_PRELOADER_MOVED"] = "Параметр перенесён в настройки компонента search.title (Показать анимацию загрузки)";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SHOW_SECTION_PICTURE"] = "<span data-hint='Настройка для интерактивного поиска'></span>Показывать картинку раздела:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SHOW_SECTION_PICTURE_MOVED"] = "Параметр перенесён в настройки компонента search.title (Показывать картинку)";
$MESS["ARTURGOLUBEV_SMARTSEARCH_CLARIFY_SECTION"] = "<span data-hint='Настройка для страницы поиска. Работает только в шаблоне catalog'></span>Показывать блок уточнения раздела:";

$MESS["ARTURGOLUBEV_SMARTSEARCH_VISUAL_EXTENDED_NOTE"] = "<div style='text-align:left;'>
<b>Внимание!</b><br>
Настройки на этой вкладке относятся только к стандартным шаблонам решения. Данные настройки не активны если вы делаете установку \"С сохранением дизайна\" и используете ваши шаблоны поиска. В будущем настройки с этой вкладки переедут в настройки компонетов
</div>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_TAB_NAME"] = "Настройка поиска";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_TAB_TITLE"] = "Настройки алгоритмов поиска";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_DOP_SETTING"] = "Ситуативные доп. возможности";

$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_ALGORITMS"] = "Настройка алгоритмов поиска";
$MESS["ARTURGOLUBEV_SMARTSEARCH_MIN_LENGTH"] = "<span data-hint='Рекомендуемое значение - 4'></span> Исправлять слова длинной не менее:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_BREAK_LETTERS"] = "<span data-hint='Символы по которым происходит разбивка сложных слов на подслова, таких как бледно-розовый. Рекомендуется использовать по рекоемндации поддержки решения'></span> Символы разбивки:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_MAX_WORDS_COUNT"] = "<span data-hint='Ресурсы сайта ограничены. '></span> Максимальное количество слов в запросе !??!?!?!:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_METAPHONE_MODE"] = "<span data-hint='Система на основе метафонического анализа будет исправлять ошибки в запросах пользователей'></span> Исправление ошибок ввода:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_STITLE_MODE"] = "<span data-hint='В расширенном режиме подбор происходит по паттерну %слово%, в базовом слово%'></span> Режим работы интерактивного поиска:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SPAGE_MODE"] = "<span data-hint='В расширенном режиме осуществляется подбор вхождений, в базовом стандартный полнотекстовый поиск'></span> Режим работы страницы поиска:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_MODE_EXTENDED"] = "Расширенный";
$MESS["ARTURGOLUBEV_SMARTSEARCH_MODE_STANDART"] = "Базовый";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_MODE_DOP"] = "Расширенный режим ищет \"шире\", а базовый быстрее и точнее";
$MESS["ARTURGOLUBEV_SMARTSEARCH_GUESSPLUS_MODE"] = "<span data-hint='Включает дополнительный механизм исправления раскладки, исправляющий более сложные случаи (например артикулы), но утяжеляет поиск вцелом'></span> Усиленное исправление раскладки клавиатуры (beta):";

$MESS["ARTURGOLUBEV_SMARTSEARCH_DOP_SORTING"] = "Предварительная сортировка";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SORT_SECTION_FIRST"] = "<span data-hint='Для корректной работы на элемент не должно действовать других правил сортировки'></span> Разделы инфоблока приоритетнее чем элементы:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SORT_AVAILABLE_FIRST"] = "<span data-hint='Для корректной работы на элемент не должно действовать других правил сортировки'></span> Доступные к покупке товары приоритетнее чем недоступные:";

$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_EXTENDED"] = "Настройка области поиска";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_TITLE_TAG_SEARCH"] = "<span data-hint='Интерактивный поиск и страница поиска будут искать полю \"Теги\" с вкладки СЕО элементов инфоблоков'></span> Теги участвуют в поиске:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_TITLE_PROP_SEARCH"] = "<span data-hint='Интерактивный поиск и страница поиска будут искать по значениям Свойств элементов инфоблоков, отмеченных опцией \"Значения свойства участвуют в поиске\"'></span>Свойства участвуют в поиске:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_TITLE_ID"] = "<span data-hint='Интерактивный поиск и страница поиска будут искать по ID элементов и разделов инфоблоков'></span>ID участвует в поиске:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_PAGE_STOP_TEXT"] = "<span data-hint='При использовании страницы поиска (search.page) более не будут учитываться значения полей \"Детальное описание\" и \"Превью описание\". Интерактивный поиск не ищет по описаниям'></span> Отключить поиск по описаниям на поисковой странице:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_EXCEPTIONS_WORDS_LIST"] = "<span data-hint='Если вы не хотите исключать предлоги оставьте эту настройку пустой'></span>Список исключаемых предлогов:<br>(через запятую)";
$MESS["ARTURGOLUBEV_SMARTSEARCH_EXCEPTIONS_WORDS_LIST_DEF"] = "и, в, без, до, из, к, на, по, о, от, при, с, у, за, над, об, под, про, для";

$MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_EXTENDED_NOTE"] = "<div style='text-align:left;'>
<b>Внимание!</b><br>
После изменения настроек области поиска, сортировки или добавления свойств в поиск, необходимо выполнить полную переиндексацию данных. Сделать это просто: откройте <a target='_blank' href='/bitrix/admin/search_reindex.php?lang=ru'>Переиндексацию поиска</a>, снимите флажок \"Переиндексировать только измененные\", задайте время Шага 10 секунд и нажмите Переиндексировать. Дождитесь завершения переиндексации.
</div>";


$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_TAB_NAME"] = "Исправления";
$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_TAB_TITLE"] = "Ручная настройка исправлений";

$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_FILE_TITLE"] = "Свои правила исправлений:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_FILE_VALUE"] = "<a href=\"/bitrix/admin/fileman_file_edit.php?path=%2Fbitrix%2Ftools%2Farturgolubev.smartsearch%2Frules.txt&full_src=Y&lang=ru\" target=\"_blank\">редактировать</a>";
$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_NOFILE_VALUE"] = "Файл с правилами отсутствует. Обратитесь в поддержку решения";

$MESS["ARTURGOLUBEV_SMARTSEARCH_TERMS_INFO_TITLE"] = "Инструкция по заполнению - <a href=\"https://arturgolubev.ru/knowledge/course14/lesson111/\" target=\"_blank\">смотреть</a>";




$MESS["ARTURGOLUBEV_SMARTSEARCH_TAG_SEARCH_TITLE_NOTE"] = "";


$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_TITLE"] = 'Не оптимальные настройки у модуля поиск';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_MESSAGE_START"] = 'В настройках <a target="_blank" href="/bitrix/admin/settings.php?mid=search&lang=ru">модуля поиск</a> рекомендуется поставить следующие настройки:<br><br>';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_SIZE"] = '"Максимальный размер индексируемого документа в килобайтах" = 256';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_CNT"] = '"Максимальное количество документов в результатах поиска" = 250';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_FAST"] = '"Использовать быстрый поиск" = Включено';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_ENGINE"] = '"Полнотекстовый поиск с помощью" = Bitrix';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_USE_WORD_DISTANCE"] = '"При ранжировании результатов учитывать расстояние между словами" = Отключено';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_USE_SOCIAL_RATING"] = '"При ранжировании результатов учитывать рейтинг" = Отключено';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_AGENT_STEMMING"] = '"Отложить выполнение морфологического анализа" = Отключено';
$MESS["ARTURGOLUBEV_SMARTSEARCH_ERROS_SETTING_LETTERS"] = '"Символы, по которым не производится разделение документа" - содержит пробелы, данная опция не должа содержать пробелов';



$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_WITH_STANDART"] = "<span data-hint='При включенной опции даже при наличии результатов по стандартному поиску будет производиться анализ и подбор похожих слов. Не рекомендуется, т.к. увеличивает нагрузку от работы поиска'></span> Всегда использовать алгоритмы исправления (не рекомендуется):";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_WITH_STANDART_TITLE"] = "<span data-hint='При включенной опции даже при наличии результатов по стандартному поиску будет производиться анализ и подбор похожих слов. Не рекомендуется, т.к. увеличивает нагрузку от работы поиска'></span> Всегда использовать алгоритмы исправления в search.title (не рекомендуется):";
$MESS["ARTURGOLUBEV_SMARTSEARCH_USE_WITH_STANDART_PAGE"] = "<span data-hint='При включенной опции даже при наличии результатов по стандартному поиску будет производиться анализ и подбор похожих слов. Не рекомендуется, т.к. увеличивает нагрузку от работы поиска'></span> Всегда использовать алгоритмы исправления в search.page (не рекомендуется):";



$MESS["ARTURGOLUBEV_SMARTSEARCH_SYSTEM_TAB_NAME"] = "Системные";
$MESS["ARTURGOLUBEV_SMARTSEARCH_SYSTEM_TAB_TITLE"] = "Системные настройки";

$MESS["ARTURGOLUBEV_SMARTSEARCH_DISABLE_CACHE"] = "<span data-hint='Кешируются исправления запросов с ошибкой'></span> Отключить кеширование:";
$MESS["ARTURGOLUBEV_SMARTSEARCH_CACHE_TIME"] = "Время кеширования:";

$MESS["ARTURGOLUBEV_SMARTSEARCH_DEBUG_SETTING"] = "Отладка";
$MESS["ARTURGOLUBEV_SMARTSEARCH_DEBUG"] = "<span data-hint='Опция для разработчиков'></span> Режим отладки:";
// $MESS["ARTURGOLUBEV_SMARTSEARCH_TITLE_MAX_COUNT"] = "<span data-hint='Максимальное количество результатов в интерактивном поиске, рекомендуется не более 10'></span> Количество результатов в интерактивном поиске:";
// $MESS["ARTURGOLUBEV_SMARTSEARCH_SEARCH_MAX_COUNT"] = "<span data-hint='Максимальное количество результатов на странице поиска, рекомендуется не более 100'></span> Количество результатов на странице поиска:";

/* help tab */
$MESS["ARTURGOLUBEV_SMARTSEARCH_HELP_TAB_NAME"] = "Информация";
$MESS["ARTURGOLUBEV_SMARTSEARCH_HELP_TAB_TITLE"] = "Полезная информация";
$MESS["ARTURGOLUBEV_SMARTSEARCH_CARD_TEXT"] = "Карточка решения на Marketplace -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_CARD_TEXT_VALUE"] = "<a href='https://marketplace.1c-bitrix.ru/solutions/arturgolubev.smartsearch/#tab-about-link' target='_blank'>ссылка</a>";
$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTALL_TEXT"] = "Информация по установке -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTALL_TEXT_VALUE"] = "<a href='https://marketplace.1c-bitrix.ru/solutions/arturgolubev.smartsearch/#tab-install-link' target='_blank'>ссылка</a>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTRUCTION_TEXT"] = "Текстовая инструкция по установке -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTRUCTION_TEXT_VALUE"] = "<a href='http://arturgolubev.ru/knowledge/course14/' target='_blank'>ссылка</a>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTRUCTION_VIDEO"] = "Видео-инструкция по установке -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_INSTRUCTION_VIDEO_VALUE"] = "<a href='https://arturgolubev.ru/knowledge/course14/lesson131/' target='_blank'>ссылка</a>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_FAQ_TEXT"] = "Часто задаваемые вопросы по данному модулю -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_FAQ_TEXT_VALUE"] = "<a href='http://arturgolubev.ru/knowledge/course14/' target='_blank'>ссылка</a>";

$MESS["ARTURGOLUBEV_SMARTSEARCH_FAQ_MAIN_TEXT"] = "Вопросы по покупке, оплате, активации модуля и т.п. -";
$MESS["ARTURGOLUBEV_SMARTSEARCH_FAQ_MAIN_TEXT_VALUE"] = "<a href='http://arturgolubev.ru/learning/course/?COURSE_ID=1&INDEX=Y' target='_blank'>ссылка</a>";
?>