<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = [
    "CACHE_TIME" => [
        'NAME' => GetMessage('CACHE_TIME'),
        "DEFAULT" => 86400
    ],
    'ELEMENTS_COUNT' => [
        'NAME' => GetMessage('ELEMENTS_COUNT'),
        'TYPE' => 'STRING',
        'DEFAULT' => 15
    ],
    'INST_PROFILE_NAME' => [
        'NAME' => GetMessage('INST_PROFILE_NAME'),
        'TYPE' => 'STRING',
    ],
    'INST_LOGIN' => [
        'NAME' => GetMessage('INST_LOGIN'),
        'TYPE' => 'STRING',
    ],
    'INST_PASSWORD' => [
        'NAME' => GetMessage('INST_PASSWORD'),
        'TYPE' => 'STRING',
    ],

];
?>
