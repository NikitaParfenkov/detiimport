<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use \Bitrix\Main\Data\Cache;
use Bitrix\Main\Grid\Declension;

require($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/vendor/autoload.php');

class InstagramGallery extends \CBitrixComponent
{
    private function checkAccessData()
    {
        $errors = [];
        if (!$this->arParams['INST_LOGIN']) {
            $errors[] = GetMessage('EMPTY_INST_LOGIN');
        }
        if (!$this->arParams['INST_PASSWORD']) {
            $errors[] = GetMessage('EMPTY_INST_PASSWORD');
        }
        if (!$this->arParams['INST_PROFILE_NAME']) {
            $errors[] = GetMessage('EMPTY_INST_PROFILE_NAME');
        }
        return $errors;
    }

    private function getAccountInfo($api) {
        $result = [];
        $profile = $api->getProfile($this->arParams['INST_PROFILE_NAME']);
        $mediasInfo = $api->getMoreMediasWithProfileId($profile->getId(), $this->arParams['ELEMENTS_COUNT']);

        $postDeclension = new Declension(GetMessage('POST_FIRST_DECLENSION'),
            GetMessage('POST_SECOND_DECLENSION'), GetMessage('POST_THIRD_DECLENSION'));
        $followerDeclension = new Declension(GetMessage('FOLLOWERS_FIRST_DECLENSION'),
            GetMessage('FOLLOWERS_SECOND_DECLENSION'), GetMessage('FOLLOWERS_THIRD_DECLENSION'));

        $followers = $profile->getFollowers();

        $endingPosition = ceil(strlen($followers) / 3);
        $numberEndings = [
            1 => '',
            2 => GetMessage('THOUSANDS'),
            3 => GetMessage('MILLIONS'),
            4 => GetMessage('BILLIONS'),
        ];
        $result['FOLLOWERS_CNT'] = $profile->getFollowers();
        $result['FOLLOWERS_DECLENSION'] = $followerDeclension->get($result['FOLLOWERS_CNT']);
        $result['FOLLOWERS_CNT_FORMATTED'] = mb_substr($result['FOLLOWERS_CNT'], 0,
                strlen($followers) - ($endingPosition - 1) * 3) . ' ' . $numberEndings[$endingPosition];

        $result['POSTS_CNT'] = $profile->getMediaCount();
        $result['POSTS_DECLENSION'] = $postDeclension->get($result['POSTS_CNT']);

        $result['PROFILE_PICTURE'] = $this->__path . '/assets/' . $this->downloadMedia($profile->getProfilePicture());

        $medias = $mediasInfo->getMedias();
        foreach ($medias as $media) {
            $result['POSTS'][] = [
                'PICTURE' => $this->__path . '/assets/' . $this->downloadMedia($media->getThumbnailSrc()),
                'POST_LINK' => $media->getLink(),
            ];
        }

        return $result;
    }

    private function getApiHandler() {
        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/instApi');
        $api = new Api($cachePool);
        $api->login($this->arParams['INST_LOGIN'], $this->arParams['INST_PASSWORD']);

        return $api;
    }

    public function getPage()
    {
        $errors = $this->checkAccessData();
        if (empty($errors)) {
            $cache = Cache::createInstance();
            if ($cache->initCache($this->arParams['CACHE_TIME'], "sp_inst_gallery")) {
                $this->arResult = $cache->getVars();
            } elseif ($cache->startDataCache()) {

                $filesDir = $_SERVER['DOCUMENT_ROOT'] . '/local/components/sp/instagram.gallery/assets/';
                $files = scandir($filesDir);
                foreach ($files as $file) {
                    unlink($filesDir . $file);
                }

                if ($api = $this->getApiHandler()) {
                    $this->arResult = $this->getAccountInfo($api);
                }

                $cache->endDataCache($this->arResult);
            }
        } else {
            foreach ($errors as $error) {
                ShowMessage($error);
            }
        }
    }

    private function downloadMedia(string $url, string $folder = __DIR__ . '/assets')
    {
        $fileName = substr(str_replace('/', '-', parse_url($url, PHP_URL_PATH)), 1);
        $content = file_get_contents($url);
        file_put_contents($folder . '/' . $fileName, $content);

        return $fileName;
    }

    public function executeComponent()
    {
        $this->getPage();
        $this->includeComponentTemplate();
    }
}
