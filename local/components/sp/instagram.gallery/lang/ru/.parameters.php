<?php
$MESS['ELEMENTS_COUNT'] = 'Количество выводимых фотографий';
$MESS['CACHE_TIME'] = 'Время жизни кеша';
$MESS['INST_PROFILE_NAME'] = 'Название профиля Instagram';
$MESS['INST_LOGIN'] = 'Логин профиля Instagram';
$MESS['INST_PASSWORD'] = 'Пароль профиля Instagram';