<?php
$MESS['EMPTY_INST_LOGIN'] = 'Не указан логин профиля Instagram';
$MESS['EMPTY_INST_PASSWORD'] = 'Не указан пароль профиля Instagram';
$MESS['EMPTY_INST_PROFILE_NAME'] = 'Не указан профиль Instagram';
$MESS['POST_FIRST_DECLENSION'] = 'публикация';
$MESS['POST_SECOND_DECLENSION'] = 'публикации';
$MESS['POST_THIRD_DECLENSION'] = 'публикаций';
$MESS['FOLLOWERS_FIRST_DECLENSION'] = 'подписчик';
$MESS['FOLLOWERS_SECOND_DECLENSION'] = 'подписчика';
$MESS['FOLLOWERS_THIRD_DECLENSION'] = 'подписчиков';
$MESS['THOUSANDS'] = 'тыс.';
$MESS['MILLIONS'] = 'млн.';
$MESS['BILLIONS'] = 'млрд.';