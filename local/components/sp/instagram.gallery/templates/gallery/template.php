<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!empty($arResult['POSTS'])) {
    ?>
	<div class="b-insta section">
		<div class="section-header section-header_main section-header_insta section-header_padding-top container-fluid">
			<div class="section-header__title section-header__title_1 h1">
				<svg class="insta-logo">
					<use xlink:href="#instagram_logo"></use>
				</svg>
			</div>
			<div class="section-header__header row">
				<div class="col-md-6 col-xl-4">
					<div class="b-insta-info">
                        <? if ($arResult['PROFILE_PICTURE']) { ?>
							<div class="b-insta-info__icon">
								<img class="instagram-logo" src="<?= $arResult['PROFILE_PICTURE']; ?>" alt="">
							</div>
                        <? } ?>
						<div class="b-insta-info__content">
							<a class="b-insta-info__title"
							   href="https://www.instagram.com/<?= $arParams['INST_PROFILE_NAME'] ?>/" target="_blank">@<?= $arParams['INST_PROFILE_NAME'] ?></a>
							<div class="b-insta-info__text">
								<span class="num"><?= number_format($arResult['POSTS_CNT'], 0, '', ' '); ?></span>
								<span class="str d-none d-lg-block"><?= $arResult['POSTS_DECLENSION']; ?> /</span>
								<span class="str d-lg-none"><?= GetMessage('POST_SHORT'); ?>  /</span>
								<span class="num"><?= $arResult['FOLLOWERS_CNT_FORMATTED']; ?></span>
								<span class="str d-none d-lg-block"><?= $arResult['FOLLOWERS_DECLENSION']; ?></span>
								<span class="str d-lg-none"><?= GetMessage('FOLLOWERS_SHORT'); ?></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="section-header__navi" data-move="section-navi-get">
						<div class="swiper-pagination"></div>
						<div class="swiper-navigation swiper-navigation-parent">
							<div class="swiper-button-prev"></div>
							<div class="swiper-button-next"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="b-insta__wrapper">
			<div class="b-insta__slider swiper-container">
				<div class="swiper-wrapper">
                    <? foreach ($arResult['POSTS'] as $post) { ?>
						<a class="b-insta__item swiper-slide" href="<?= $post['POST_LINK'] ?>" target="_blank">
							<picture class="b-insta__img">
								<img src="<?= $post['PICTURE'] ?>" alt="">
							</picture>
						</a>
                    <? } ?>
				</div>
			</div>
		</div>
		<div class="b-insta__footer section-footer container-fluid">
			<div data-move="section-navi-set"></div>
			<a class="b-insta__button btn-decorate btn-decorate_lg" target="_blank"
			   href="https://www.instagram.com/<?= $arParams['INST_PROFILE_NAME'] ?>/"><?= GetMessage('SUBSCRIBE'); ?>
				<svg>
					<use xlink:href="#instagram"></use>
				</svg>
			</a>
		</div>
	</div>
<? } ?>