<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams) . $this->GetTemplateName());

$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if ($arParams["EVENT_NAME"] == '') {
    $arParams["EVENT_NAME"] = "FEEDBACK_FORM";
}
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if ($arParams["OK_TEXT"] == '') {
    $arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit_form"] <> '' && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"])) {
    $arResult["ERROR_MESSAGE"] = array();
    if (check_bitrix_sessid()) {
        if (empty($arResult["ERROR_MESSAGE"])) {
            foreach ($_POST as $keyPOST => $valuePOST) {
                $arFields[$keyPOST] = htmlspecialchars($valuePOST);
            }

            if ($arParams['FORM_ID']) {
                CModule::IncludeModule('form');
                if ($RESULT_ID = CFormResult::Add($arParams['FORM_ID'], $arFields)) {
                    $arResult['OK_MESSAGE'] = $arParams["OK_TEXT"];
                } else {
                    $arResult["ERROR_MESSAGE"][] = GetMessage("MF_UNEXPECTED_ERROR");
                }
            }

            if (!empty($arParams["EVENT_MESSAGE_ID"])) {
                foreach ($arParams["EVENT_MESSAGE_ID"] as $v) {
                    if (IntVal($v) > 0) {
                        CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
                    }
                }
            } else {
                CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
            }
            LocalRedirect($APPLICATION->GetCurPageParam("success=" . $arResult["PARAMS_HASH"], Array("success")));
        }
    } else {
        $arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
    }
} elseif ($_REQUEST["success"] == $arResult["PARAMS_HASH"]) {
    $arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

$this->IncludeComponentTemplate();