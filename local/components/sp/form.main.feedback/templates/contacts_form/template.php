<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
if (!empty($arResult["ERROR_MESSAGE"])) {
    ?>
	<script>
        new bootstrap.Modal(document.getElementById('modal-callback-error'), {}).show();
	</script>
	<?
}
?>
<? if (strlen($arResult["OK_MESSAGE"]) > 0) {
    ?>
	<script>
        new bootstrap.Modal(document.getElementById('modal-callback-ok'), {}).show();
	</script>
<? } ?>
<div class="page-inner">
	<form class="row g-0 justify-content-end js_validate" action="<?= POST_FORM_ACTION_URI ?>" method="POST">
		<div class="col-xl-11">
			<div class="title-reg"><?= GetMessage('WRITE_US') ?></div>
			<div class="row gx-6 mb-6">
				<div class="col-xl">
					<div class="mb-5 form-block">
						<input class="form-control form-control_2" id="name" required type="text" name="form_text_1" placeholder="  ">
						<label for="name"><?= GetMessage('NAME') ?></label>
						<div class="help-text"><?= GetMessage('NAME_HINT') ?></div>
					</div>
					<div class="mb-5 form-block">
						<input class="form-control form-control_2" id="email" type="email" name="form_text_2" placeholder="  ">
						<label for="email"><?= GetMessage('EMAIL') ?></label>
						<div class="help-text"><?= GetMessage('EMAIL_HINT') ?></div>
					</div>
					<div class="mb-5 form-line">
						<div class="form-block form-block_60">
							<input class="form-control form-control_2" id="phone_code" type="text" value="+7"
							       name="form_text_3" placeholder="  " disabled>
							<label for="phone_code"><?= GetMessage('CODE') ?></label>
						</div>
						<div class="form-block">
							<input class="form-control form-control_2 tel" id="phone_number" type="text" required
							       name="form_text_4" placeholder="  ">
							<label for="phone_number"><?= GetMessage('PHONE') ?></label>
						</div>
						<div class="help-text"><?= GetMessage('PHONE_HINT') ?></div>
					</div>
				</div>
				<div class="col-xl">
					<div class="mb-5 form-block">
					<textarea class="form-control form-control_2 autogrow" id="type" type="text"
					          name="form_text_5" placeholder="  "></textarea>
						<label for="type"><?= GetMessage('TEXT') ?></label>
					</div>
				</div>
			</div>
            <?= bitrix_sessid_post() ?>
			<input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
			<input type="hidden" name="submit_form" value="ok">
			<div class="hr mb-5"></div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-check">
						<input class="form-check-input" id="flexCheckDefault" type="checkbox" required>
						<label class="form-check-label" for="flexCheckDefault"><?= GetMessage('AGREEMENT',
                                ['AGREEMENT_URL' => AGREEMENT_URL]) ?></label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-6">
						<button class="btn btn-dark btn-lg float-md-right order-submit"><?= GetMessage('SUBMIT') ?></button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
