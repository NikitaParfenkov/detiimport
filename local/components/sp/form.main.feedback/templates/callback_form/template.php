<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
if (!empty($arResult["ERROR_MESSAGE"])) {
    ?>
	<script>
        new bootstrap.Modal(document.getElementById('modal-callback-error'), {}).show();
	</script>
    <?
}
?>
<? if (strlen($arResult["OK_MESSAGE"]) > 0) {
    ?>
	<script>
        new bootstrap.Modal(document.getElementById('modal-callback-ok'), {}).show();
	</script>
<? } ?>
<div class="modal fade modal-default" id="modal-callback" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('GET_CALL') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<form action="<?= POST_FORM_ACTION_URI ?>" method="POST">
				<div class="modal-body">
					<div class="form">
                        <?= bitrix_sessid_post() ?>
						<input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
						<input type="hidden" name="submit_form" value="ok">
						<div class="form-group mb-5">
							<input class="form-control form-control_2" type="text"
							       placeholder="<?= GetMessage('NAME') ?>" name="form_text_9" required>
							<div class="help-text"><?= GetMessage('NAME_HINT') ?></div>
						</div>
						<div class="mb-5">
							<div class="d-flex">
								<div class="form-group-kod">
									<input class="form-control form-control_2" type="number" placeholder="+7"
									       name="form_text_10">
								</div>
								<input class="form-control form-control_2" type="text"
								       placeholder="<?= GetMessage('PHONE') ?>" name="form_text_11" required>
							</div>
							<div class="help-text"><?= GetMessage('PHONE_HINT') ?></div>
						</div>
						<div class="form-check mb-5 text-xs">
							<input class="form-check-input" id="flexCheckDefault1" type="checkbox" value="" required>
							<label class="form-check-label" for="flexCheckDefault1">
								<span class="text-muted"><?= GetMessage('AGREE') ?></span>
								<a class="text-decoration-underline"
								   href="<?= AGREEMENT_URL; ?>"><?= GetMessage('PERSONAL_DATA') ?></a>
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="d-grid">
						<button class="btn btn-dark btn-xl button-full-mobile"
						        type="submit"><?= GetMessage('GET_CALL') ?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>