<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
$site = ($_REQUEST["site"] <> '' ? $_REQUEST["site"] : ($_REQUEST["src_site"] <> '' ? $_REQUEST["src_site"] : false));
$arFilter = ["TYPE_ID" => "FEEDBACK_FORM", "ACTIVE" => "Y"];
if ($site !== false) {
    $arFilter["LID"] = $site;
}


CModule::IncludeModule('iblock');
$iBlocks = [];
$res = CIBlock::GetList([], ['SITE_ID' => $arFilter["LID"], 'ACTIVE' => 'Y', "CNT_ACTIVE" => "Y"], true);
while ($ar_res = $res->Fetch()) {
    $iBlocks[$ar_res["ID"]] = "[" . $ar_res["ID"] . "] " . $ar_res["NAME"];
}

$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_ID" => [
            "NAME" => GetMessage('T_IBLOCK_DESC_IBLOCK_ID'),
            "TYPE" => "LIST",
            "VALUES" => $iBlocks,
            "MULTIPLE" => "N",
            "COLS" => 25,
            "PARENT" => "BASE",
        ],
    ]
];