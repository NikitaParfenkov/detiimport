'use strict';
let form = BX('sp-subscribe');
BX.bind(form, 'submit', function (e) {
    let objectParams = {};
    this.forEach(function (e) {
        let name = e.name;
        let value = e.value;
        if (name.length > 0 && value.length > 0) {
            objectParams[name] = value;
        }
    });

    BX.ajax({
        url: '/api/ajax/index.php',
        data: {action: 'forms', data: {method: 'saveSubscriber', params: objectParams}},
        method: 'POST',
        dataType: 'json',
        onsuccess: function (data) {
            let notifyBlock = form.querySelector('.notify-block');
            if (data.status) {
                notifyBlock.classList.remove('invalid-feedback');
                notifyBlock.classList.add('valid-feedback');
            } else {
                notifyBlock.classList.remove('valid-feedback');
                notifyBlock.classList.add('invalid-feedback');
            }
            notifyBlock.textContent = data.result;
            notifyBlock.style.display = 'block';
            setTimeout(() => notifyBlock.style.display = 'none', 5000);
        },
        onfailure: function () {
            console.error('Error Ajax Submiting');
        }
    });
    e.preventDefault();
});
