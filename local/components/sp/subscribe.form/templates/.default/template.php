<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

CJSCore::Init(["fx", 'ajax']);
?>
<div class="footer-middle__subscribe">
    <form class="b-subscribe" id="sp-subscribe" method="post">
        <input type="hidden" name="iblockID" value='<?= $arParams['IBLOCK_ID'] ?>'>
        <div class="b-subscribe__title"><?= GetMessage('SP_SFC_TITLE') ?>
        </div>
        <div class="b-subscribe__content">
            <div class="b-subscribe__input-wrap">
                <input class="form-control" type="email" placeholder="<?= GetMessage('SP_SFC_EMAIL_PLACEHOLDER') ?>"
                       name='email' required>
                <div class="invalid-feedback notify-block"></div>
            </div>
            <button class="btn btn-dark btn-submit" type="submit"><?= GetMessage('SP_SFC_SUBSCRIBE') ?></button>
        </div>
    </form>
</div>
