<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use SP\Tools\HLTools;

CModule::IncludeModule('sp.tools');

foreach ($arResult["ITEMS"] as $arItem) {
    if ($arItem['CODE'] === 'BAZOVYYTSVET') {
        foreach ($arItem['VALUES'] as $val) {
            $colors[] = $val['URL_ID'];
        }
    }
}
if ($colors) {
    $hlTools = new HLTools();
    $select = ['UF_XML_ID', 'UF_DESCRIPTION'];
    $filter = ["UF_XML_ID" => $colors];
    $order = ["ID" => "ASC"];

    $arResult['COLORS'] = $hlTools->getElementsByFilter(HL_COLORS, $select, $filter, $order);
}
