<?
$MESS['SHOW_MORE'] = "Показать ещё";
$MESS['HIDE'] = "Скрыть";
$MESS['PUT_QUERY'] = "Введите название";
$MESS['COST'] = "Стоимость";
$MESS['RESET'] = "Сбросить";
$MESS['FROM'] = "от";
$MESS['TO'] = "до";
$MESS['PRICE_TAG'] = "Р.";
$MESS['SUBMIT'] = "Применить";
$MESS['CLEAR'] = 'Сбросить';
