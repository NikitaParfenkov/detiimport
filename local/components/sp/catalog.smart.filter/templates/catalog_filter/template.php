<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult["ITEMS"]) {
$clearFilter = false;
$filtersCnt = 0;
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if ($arItem['PRICE']) {
        if ($arItem["VALUES"]["MAX"]["HTML_VALUE"] || $arItem["VALUES"]["MIN"]["HTML_VALUE"]) {
            $clearFilter = true;
        }
    }

    if (
        $arItem["DISPLAY_TYPE"] == "A"
        && (
            $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
        )
    ) {
        continue;
    }

    foreach ($arItem["VALUES"] as $val => $ar) {
        if ($ar["CHECKED"]) {
            $clearFilter = true;
            $filtersCnt++;
        }
    }
}
$this->SetViewTarget('filtersCnt');
echo $filtersCnt;
$this->EndViewTarget();
?>
<form class="category-aside" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
      action="<? echo $arResult["FORM_ACTION"] ?>" method="get" id="catalog-filter">
    <? //not prices
    foreach ($arResult["ITEMS"] as $key => $arItem) {
        if (
            empty($arItem["VALUES"])
            || isset($arItem["PRICE"])
        ) {
            continue;
        }

        if (
            $arItem["DISPLAY_TYPE"] == "A"
            && (
                $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
            )
        ) {
            continue;
        }
        if ($arParams['SKIP_BRAND'] === 'Y' && $arItem['CODE'] === 'BRAND') {
            continue;
        }
        $showSubmit = true;
        ?>

        <?
        $arCur = current($arItem["VALUES"]);
        switch ($arItem["DISPLAY_TYPE"]) {
        case "A":
        case "B":
        case "G":
        case "H":
        case "P":
        case "R":
        case "K":
        case "U":
        break;
        default://CHECKBOXES
        if (count($arItem["VALUES"]) === 1 && current($arItem["VALUES"])['VALUE'] === 'Y') {
        ?>
			<div class="nav-side">
				<div class="nav-side__header">
                    <? foreach ($arItem["VALUES"] as $val => $ar) { ?>
						<div class="form-check form-switch">
							<input class="form-check-input"
							       type="checkbox"
							       value="<? echo $ar["HTML_VALUE"] ?>"
							       name="<? echo $ar["CONTROL_NAME"] ?>"
							       id="<? echo $ar["CONTROL_ID"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
								   onclick="smartFilter.click(this) ">
							<label class="nav-side__title form-check-label"
							       for="<? echo $ar["CONTROL_ID"] ?>"><?= $arItem['NAME']; ?></label>
						</div>
                    <? } ?>
				</div>
			</div>
            <?
            }
        }
        ?>
    <? } ?>
    <?
    foreach ($arResult["ITEMS"] as $key => $arItem)//prices
    {
        $key = $arItem["ENCODED_ID"];
        if (isset($arItem["PRICE"])):
            if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) {
                continue;
            }
            $showSubmit = true;
            ?>
			<div class="filter-item">
				<div class="filter-item__header">
					<div class="filter-item__title"><?= GetMessage('COST') ?></div>
				</div>
				<div class="filter-item__content ui-slider-wrapper">
					<div class="filter-item__box">
						<div class="filter-range-prices">
							<div class="filter-range-prices__item"><?= GetMessage('FROM') ?>
								<div class="filter-range-prices__price">
							                <span class="ui-slider-from">
								                <?
                                                echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?: $arItem["VALUES"]["MIN"]["VALUE"] ?></span>
									<span><?= GetMessage('PRICE_TAG') ?></span>
								</div>
							</div>
							<div class="filter-range-prices__item"><?= GetMessage('FROM') ?>
								<div class="filter-range-prices__price">
									<span class="ui-slider-to"><?
                                        echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?: $arItem["VALUES"]["MAX"]["VALUE"] ?></span>
									<span><?= GetMessage('PRICE_TAG') ?></span>
								</div>
							</div>
						</div>
					</div>
					<div style="display: none">
						<input type="text"
						       class="sp-min-price"
						       name="<?
                               echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
						       id="<?
                               echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
						       value="<?
                               echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
						       onkeyup="smartFilter.keyup(this)"
						/>
						<input type="text"
						       class="sp-max-price"
						       name="<?
                               echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
						       id="<?
                               echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
						       value="<?
                               echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
						       onkeyup="smartFilter.keyup(this)"
						/>
					</div>
					<div class="ui-slider-filter" id="ui-slider" data-min="<?= $arItem["VALUES"]["MIN"]["VALUE"]; ?>"
					     data-max="<?= $arItem["VALUES"]["MAX"]["VALUE"]; ?>"
					     data-from="<?
                         echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?: $arItem["VALUES"]["MIN"]["VALUE"] ?>"
					     data-to="<?
                         echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?: $arItem["VALUES"]["MAX"]["VALUE"] ?>">
					</div>
				</div>
			</div>
        <?endif;
    }
    ?>

    <? //not prices
    foreach ($arResult["ITEMS"] as $key => $arItem) {
        if (
            empty($arItem["VALUES"])
            || isset($arItem["PRICE"])
        ) {
            continue;
        }

        if (
            $arItem["DISPLAY_TYPE"] == "A"
            && (
                $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
            )
        ) {
            continue;
        }
        if ($arParams['SKIP_BRAND'] === 'Y' && $arItem['CODE'] === 'BRAND') {
			continue;
        }
        $showSubmit = true;
        ?>

        <?
        $arCur = current($arItem["VALUES"]);
        switch ($arItem["DISPLAY_TYPE"]) {
        case "A"://NUMBERS_WITH_SLIDER
            ?>
			<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
				<i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_FROM") ?></i>
				<div class="bx-filter-input-container">
					<input
							class="min-price"
							type="text"
							name="<?
                            echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
							id="<?
                            echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
							value="<?
                            echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
							size="5"
							onkeyup="smartFilter.keyup(this)"
					/>
				</div>
			</div>
			<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
				<i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_TO") ?></i>
				<div class="bx-filter-input-container">
					<input
							class="max-price"
							type="text"
							name="<?
                            echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
							id="<?
                            echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
							value="<?
                            echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
							size="5"
							onkeyup="smartFilter.keyup(this)"
					/>
				</div>
			</div>

			<div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
				<div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
                    <?
                    $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
                    $value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
                    $value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
                    $value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
                    $value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
                    $value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                    ?>
					<div class="bx-ui-slider-part p1"><span><?= $value1 ?></span></div>
					<div class="bx-ui-slider-part p2"><span><?= $value2 ?></span></div>
					<div class="bx-ui-slider-part p3"><span><?= $value3 ?></span></div>
					<div class="bx-ui-slider-part p4"><span><?= $value4 ?></span></div>
					<div class="bx-ui-slider-part p5"><span><?= $value5 ?></span></div>

					<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;"
					     id="colorUnavailableActive_<?= $key ?>"></div>
					<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;"
					     id="colorAvailableInactive_<?= $key ?>"></div>
					<div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;"
					     id="colorAvailableActive_<?= $key ?>"></div>
					<div class="bx-ui-slider-range" id="drag_tracker_<?= $key ?>" style="left: 0;right: 0;">
						<a class="bx-ui-slider-handle left" style="left:0;" href="javascript:void(0)"
						   id="left_slider_<?= $key ?>"></a>
						<a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)"
						   id="right_slider_<?= $key ?>"></a>
					</div>
				</div>
			</div>
        <?
        $arJsParams = array(
            "leftSlider" => 'left_slider_' . $key,
            "rightSlider" => 'right_slider_' . $key,
            "tracker" => "drag_tracker_" . $key,
            "trackerWrap" => "drag_track_" . $key,
            "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
            "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
            "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
            "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
            "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
            "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
            "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
            "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
            "precision" => $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0,
            "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
            "colorAvailableActive" => 'colorAvailableActive_' . $key,
            "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
        );
        ?>
			<script type="text/javascript">
                BX.ready(function () {
                    window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                });
			</script>
        <?
        break;
        case "B"://NUMBERS
        ?>
			<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
				<i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_FROM") ?></i>
				<div class="bx-filter-input-container">
					<input
							class="min-price"
							type="text"
							name="<?
                            echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
							id="<?
                            echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
							value="<?
                            echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
							size="5"
							onkeyup="smartFilter.keyup(this)"
					/>
				</div>
			</div>
			<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
				<i class="bx-ft-sub"><?= GetMessage("CT_BCSF_FILTER_TO") ?></i>
				<div class="bx-filter-input-container">
					<input
							class="max-price"
							type="text"
							name="<?
                            echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
							id="<?
                            echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
							value="<?
                            echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
							size="5"
							onkeyup="smartFilter.keyup(this)"
					/>
				</div>
			</div>
        <?
        break;
        case "G"://CHECKBOXES_WITH_PICTURES
        ?>
			<div class="col-xs-12">
				<div class="bx-filter-param-btn-inline">
                    <?
                    foreach ($arItem["VALUES"] as $val => $ar):?>
						<input
								style="display: none"
								type="checkbox"
								name="<?= $ar["CONTROL_NAME"] ?>"
								id="<?= $ar["CONTROL_ID"] ?>"
								value="<?= $ar["HTML_VALUE"] ?>"
                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
						/>
                        <?
                        $class = "";
                        if ($ar["CHECKED"]) {
                            $class .= " bx-active";
                        }
                        if ($ar["DISABLED"]) {
                            $class .= " disabled";
                        }
                        ?>
						<label for="<?= $ar["CONTROL_ID"] ?>" data-role="label_<?= $ar["CONTROL_ID"] ?>"
						       class="bx-filter-param-label <?= $class ?>"
						       onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')); BX.toggleClass(this, 'bx-active');">
													<span class="bx-filter-param-btn bx-color-sl">
														<?
                                                        if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx-filter-btn-color-icon"
															      style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                                        <? endif ?>
													</span>
						</label>
                    <? endforeach ?>
				</div>
			</div>
        <?
        break;
        case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
        $isReset = false;
        foreach ($arItem['VALUES'] as $ar) {
            if ($ar["CHECKED"]) {
                $isReset = true;
                break;
            }
        }
        ?>
			<div class="filter-item">
				<div class="filter-item__header">
					<div class="filter-item__title"><?= $arItem['NAME']; ?></div>
					<div class="filter-item__clear" <?
                    if (!$isReset) { ?>style="display: none"<?
                    } ?>><?= GetMessage('RESET'); ?></div>
				</div>
				<div class="filter-item__content">
					<div class="filter-color">
                        <?
                        foreach ($arItem["VALUES"] as $val => $ar) { ?>
							<div class="form-check">
								<input class="form-check-input sp-filter-input"
								       type="checkbox"
								       name="<?= $ar["CONTROL_NAME"] ?>"
								       id="<?= $ar["CONTROL_ID"] ?>"
								       value="<?= $ar["HTML_VALUE"] ?>"
                                    <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>>
								<label class="form-check-label" for="<?= $ar["CONTROL_ID"] ?>"
								       style="color: <?= $arResult['COLORS'][$ar['URL_ID']]; ?>"
								       onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>'));">
									<span><?= $ar['VALUE']; ?></span>
								</label>
							</div>
                            <?
                        } ?>


					</div>
				</div>
			</div>
        <?
        break;
        case "P"://DROPDOWN
        $checkedItemExist = false;
        ?>
			<div class="col-xs-12">
				<div class="bx-filter-select-container">
					<div class="bx-filter-select-block"
					     onclick="smartFilter.showDropDownPopup(this, '<?= CUtil::JSEscape($key) ?>')">
						<div class="bx-filter-select-text" data-role="currentOption">
                            <?
                            foreach ($arItem["VALUES"] as $val => $ar) {
                                if ($ar["CHECKED"]) {
                                    echo $ar["VALUE"];
                                    $checkedItemExist = true;
                                }
                            }
                            if (!$checkedItemExist) {
                                echo GetMessage("CT_BCSF_FILTER_ALL");
                            }
                            ?>
						</div>
						<div class="bx-filter-select-arrow"></div>
						<input
								style="display: none"
								type="radio"
								name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
								id="<? echo "all_" . $arCur["CONTROL_ID"] ?>"
								value=""
						/>
                        <?
                        foreach ($arItem["VALUES"] as $val => $ar):?>
							<input
									style="display: none"
									type="radio"
									name="<?= $ar["CONTROL_NAME_ALT"] ?>"
									id="<?= $ar["CONTROL_ID"] ?>"
									value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
							/>
                        <? endforeach ?>
						<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
							<ul>
								<li>
									<label for="<?= "all_" . $arCur["CONTROL_ID"] ?>"
									       class="bx-filter-param-label"
									       data-role="label_<?= "all_" . $arCur["CONTROL_ID"] ?>"
									       onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape("all_" . $arCur["CONTROL_ID"]) ?>')">
                                        <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
									</label>
								</li>
                                <?
                                foreach ($arItem["VALUES"] as $val => $ar):
                                    $class = "";
                                    if ($ar["CHECKED"]) {
                                        $class .= " selected";
                                    }
                                    if ($ar["DISABLED"]) {
                                        $class .= " disabled";
                                    }
                                    ?>
									<li>
										<label for="<?= $ar["CONTROL_ID"] ?>"
										       class="bx-filter-param-label<?= $class ?>"
										       data-role="label_<?= $ar["CONTROL_ID"] ?>"
										       onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')"><?= $ar["VALUE"] ?></label>
									</li>
                                <? endforeach ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
        <?
        break;
        case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
        ?>
			<div class="col-xs-12">
				<div class="bx-filter-select-container">
					<div class="bx-filter-select-block"
					     onclick="smartFilter.showDropDownPopup(this, '<?= CUtil::JSEscape($key) ?>')">
						<div class="bx-filter-select-text fix" data-role="currentOption">
                            <?
                            $checkedItemExist = false;
                            foreach ($arItem["VALUES"] as $val => $ar):
                                if ($ar["CHECKED"]) {
                                    ?>
                                    <?
                                    if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
										<span class="bx-filter-btn-color-icon"
										      style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                    <? endif ?>
									<span class="bx-filter-param-text">
																	<?= $ar["VALUE"] ?>
																</span>
                                    <?
                                    $checkedItemExist = true;
                                }
                            endforeach;
                            if (!$checkedItemExist) {
                                ?><span class="bx-filter-btn-color-icon all"></span> <?
                                echo GetMessage("CT_BCSF_FILTER_ALL");
                            }
                            ?>
						</div>
						<div class="bx-filter-select-arrow"></div>
						<input
								style="display: none"
								type="radio"
								name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
								id="<? echo "all_" . $arCur["CONTROL_ID"] ?>"
								value=""
						/>
                        <?
                        foreach ($arItem["VALUES"] as $val => $ar):?>
							<input
									style="display: none"
									type="radio"
									name="<?= $ar["CONTROL_NAME_ALT"] ?>"
									id="<?= $ar["CONTROL_ID"] ?>"
									value="<?= $ar["HTML_VALUE_ALT"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
							/>
                        <? endforeach ?>
						<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
							<ul>
								<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
									<label for="<?= "all_" . $arCur["CONTROL_ID"] ?>"
									       class="bx-filter-param-label"
									       data-role="label_<?= "all_" . $arCur["CONTROL_ID"] ?>"
									       onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape("all_" . $arCur["CONTROL_ID"]) ?>')">
										<span class="bx-filter-btn-color-icon all"></span>
                                        <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
									</label>
								</li>
                                <?
                                foreach ($arItem["VALUES"] as $val => $ar):
                                    $class = "";
                                    if ($ar["CHECKED"]) {
                                        $class .= " selected";
                                    }
                                    if ($ar["DISABLED"]) {
                                        $class .= " disabled";
                                    }
                                    ?>
									<li>
										<label for="<?= $ar["CONTROL_ID"] ?>"
										       data-role="label_<?= $ar["CONTROL_ID"] ?>"
										       class="bx-filter-param-label<?= $class ?>"
										       onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')">
                                            <?
                                            if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx-filter-btn-color-icon"
												      style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                            <? endif ?>
											<span class="bx-filter-param-text">
																		<?= $ar["VALUE"] ?>
																	</span>
										</label>
									</li>
                                <? endforeach ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
        <?
        break;
        case "K"://RADIO_BUTTONS
        $isReset = false;
        foreach ($arItem['VALUES'] as $ar) {
            if ($ar["CHECKED"]) {
                $isReset = true;
                break;
            }
        }
        ?>
			<div class="filter-item">
				<div class="filter-item__header">
					<div class="filter-item__title"><?= $arItem['NAME']; ?></div>
					<div class="filter-item__clear" <?
                    if (!$isReset) { ?>style="display: none"<?
                    } ?>><?= GetMessage('RESET'); ?></div>
				</div>
				<div class="filter-item__content">
                    <?
                    foreach ($arItem["VALUES"] as $val => $ar) { ?>
						<div class="form-check">
							<input class="form-check-input sp-filter-input"
							       type="radio"
							       value="<? echo $ar["HTML_VALUE_ALT"] ?>"
							       name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
							       id="<? echo $ar["CONTROL_ID"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
								   onclick="smartFilter.click(this)">
							<label class="form-check-label"
							       for="<? echo $ar["CONTROL_ID"] ?>"><?= $ar["VALUE"]; ?></label>
						</div>
                        <?
                    } ?>
				</div>
			</div>
        <?
        break;
        case "U"://CALENDAR
        ?>
			<div class="col-xs-12">
				<div class="bx-filter-parameters-box-container-block">
					<div class="bx-filter-input-container bx-filter-calendar-container">
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.calendar',
                            '',
                            array(
                                'FORM_NAME' => $arResult["FILTER_NAME"] . "_form",
                                'SHOW_INPUT' => 'Y',
                                'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="' . FormatDate("SHORT",
                                        $arItem["VALUES"]["MIN"]["VALUE"]) . '" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
                                'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                'SHOW_TIME' => 'N',
                                'HIDE_TIMEBAR' => 'Y',
                            ),
                            null,
                            array('HIDE_ICONS' => 'Y')
                        ); ?>
					</div>
				</div>
				<div class="bx-filter-parameters-box-container-block">
					<div class="bx-filter-input-container bx-filter-calendar-container">
                        <?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.calendar',
                            '',
                            array(
                                'FORM_NAME' => $arResult["FILTER_NAME"] . "_form",
                                'SHOW_INPUT' => 'Y',
                                'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="' . FormatDate("SHORT",
                                        $arItem["VALUES"]["MAX"]["VALUE"]) . '" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
                                'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                'SHOW_TIME' => 'N',
                                'HIDE_TIMEBAR' => 'Y',
                            ),
                            null,
                            array('HIDE_ICONS' => 'Y')
                        ); ?>
					</div>
				</div>
			</div>
        <?
        break;
        default://CHECKBOXES
        if (count($arItem["VALUES"]) === 1 && current($arItem["VALUES"])['VALUE'] === 'Y') {

        } else {
            $firstPart = array_slice($arItem["VALUES"], 0, 8);
            $secondPart = array_slice($arItem["VALUES"], 8);
            $isReset = false;
            foreach ($arItem['VALUES'] as $ar) {
                if ($ar["CHECKED"]) {
                    $isReset = true;
                    break;
                }
            }
            ?>
			<div class="filter-item">
				<div class="filter-item__header">
					<div class="filter-item__title"><?= $arItem['NAME']; ?></div>
					<div class="filter-item__clear" <?
                    if (!$isReset) { ?>style="display: none"<?
                    } ?>><?= GetMessage('RESET'); ?></div>

				</div>
				<div class="filter-item__content">
                    <?
                    if ($arItem['FILTER_HINT'] === 'search') { ?>
						<div class="filter-item__box">
							<div class="b-search">
								<input class="form-control" type="text" placeholder="<?= GetMessage('PUT_QUERY'); ?>"
								       value="" name="filter_search" data-prop-id="<?= $arItem['ID']; ?>">
								<span></span>
							</div>
						</div>
                        <?
                    } ?>
					<div class="checkboxes sp-props-list">
                        <?
                        foreach ($firstPart as $val => $ar) { ?>
							<div class="form-check">
								<input class="form-check-input sp-filter-input"
								       type="checkbox"
								       value="<? echo $ar["HTML_VALUE"] ?>"
								       name="<? echo $ar["CONTROL_NAME"] ?>"
								       id="<? echo $ar["CONTROL_ID"] ?>"
                                    <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
									   onclick="smartFilter.click(this) ">
								<label class="form-check-label" for="<? echo $ar["CONTROL_ID"] ?>">
                                    <?= $ar["VALUE"]; ?>
								</label>
							</div>
                            <?
                        } ?>
                        <?
                        if ($secondPart) { ?>
							<div class="collapse" id="collapse-<?= $arItem["ID"]; ?>">
                                <?
                                foreach ($secondPart as $val => $ar) { ?>
									<div class="form-check">
										<input class="form-check-input sp-filter-input"
										       type="checkbox"
										       value="<? echo $ar["HTML_VALUE"] ?>"
										       name="<? echo $ar["CONTROL_NAME"] ?>"
										       id="<? echo $ar["CONTROL_ID"] ?>"
                                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
											   onclick="smartFilter.click(this) ">
										<label class="form-check-label" for="<? echo $ar["CONTROL_ID"] ?>">
                                            <?= $ar["VALUE"]; ?>
										</label>
									</div>
                                    <?
                                } ?>
							</div>
							<div class="btn btn-dark btn-sm rounded-pill has-arr button-more dropdown-toggle"
							     data-toggle="collapse" data-target="#collapse-<?= $arItem["ID"]; ?>"
							     data-hide-title="<?= GetMessage('HIDE'); ?>">
								<span><?= GetMessage('SHOW_MORE'); ?></span>
							</div>
                            <?
                        } ?>
					</div>
				</div>
			</div>
            <?
        }
        }
        ?>
    <? } ?>
	<div class="row" style="display: none;">
		<div class="col-xs-12 bx-filter-button-box">
			<div class="bx-filter-block">
				<div class="bx-filter-parameters-box-container">
					<input
							class="btn btn-themes"
							type="hidden"
							id="set_filter"
							name="set_filter"
							value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
					/>
				</div>
			</div>
		</div>
	</div>
	<div class="b-filter__footer">
		<div class="b-filter__clear-all btn btn-light" id="clear_filter" <? if ($clearFilter) { ?>style="display: block"
             <? } else { ?>style="display: none" <? } ?>>
			<a href="<? echo $arResult["FORM_ACTION"]; ?>">
				<svg width="12" height="10">
					<use xlink:href="#reset"></use>
				</svg>
				<span><?= GetMessage('CLEAR'); ?></span>
			</a>
		</div>
		<?if ($showSubmit) {?>
		<a href="" class="btn btn-dark" id="modef">
			<svg width="12" height="12" fill="#fff">
				<use xlink:href="#check_mark"></use>
			</svg>
			<span><?= GetMessage('SUBMIT'); ?></span>
		</a>
		<?}?>
	</div>
</form>
<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>
<?}?>