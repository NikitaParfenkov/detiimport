<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="d-flex message-404">
	<svg class="mr-3" width="24" height="24" fill="#FA002A">
		<use xlink:href="#alert-info"></use>
	</svg>
	<div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/local/include/basket/empty.php"
            )
        ); ?>
	</div>
</div>





