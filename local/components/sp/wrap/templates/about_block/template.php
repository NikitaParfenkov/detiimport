<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="b-about">
    <div class="row g-0">
        <div class="col-1 col-lg-3"></div>
        <div class="col-11 col-lg-9">
            <picture class="b-about__img b-about__img_1">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/main/aboutBlock/pictureMain.php"
                    )
                ); ?>
            </picture>
        </div>
    </div>
    <div class="b-about__inner">
        <div class="row g-0">
            <div class="col-lg-3">
                <div class="b-about__logo">
                    <svg class="icon-round-logo">
                        <use xlink:href="#logo"></use>
                    </svg>
                    <svg class="icon-round-text">
                        <use xlink:href="#round_text"></use>
                    </svg>
                </div>
            </div>
            <div class="col-1 order-first order-sm-last"></div>
            <div class="col-10 col-lg-7 order-first order-sm-last">
                <div class="b-about__content">
                    <div class="b-about__topText title-reg">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/main/aboutBlock/textMain.php"
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row g-0">
            <div class="col-10 col-lg-3 offset-1 offset-lg-0">
                <picture class="b-about__img b-about__img_2">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/include/main/aboutBlock/picture.php"
                        )
                    ); ?>
                </picture>
            </div>
            <div class="col-10 col-xl-7 col-xxl-6 col-lg-8 offset-1">
                <div class="row gx-7">

                    <div class="col-lg-6">
                        <p class="mr-3">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/main/aboutBlock/subtext1.php"
                                )
                            ); ?>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <p class="mr-3">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/include/main/aboutBlock/subtext2.php"
                                )
                            ); ?>
                        </p>
                    </div>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/main/aboutBlock/button.php"
                    )
                ); ?>
            </div>
        </div>
    </div>
</div>