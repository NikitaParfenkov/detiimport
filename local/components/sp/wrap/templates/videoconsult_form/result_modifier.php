<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use SP\Tools\VideoConsult;
CModule::IncludeModule('sp.tools');

$videoConsult = new VideoConsult();
$arResult['VIDEO_CONSULT'] = $videoConsult->getMainContactInfo(["ID", "PROPERTY_WHATSAPP", "PROPERTY_VIBER", "PROPERTY_TELEGRAM", 'DETAIL_TEXT']);