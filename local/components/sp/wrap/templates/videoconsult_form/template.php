<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="modal fade modal-default" id="modal-videoconsult" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('CONSULT') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
                <? if ($arResult['VIDEO_CONSULT']['DETAIL_TEXT']) { ?>
					<div class="text-xs">
                        <?= $arResult['VIDEO_CONSULT']['DETAIL_TEXT']; ?>
					</div>
                <? } ?>
				<div class="b-social">
                    <? if ($arResult['VIDEO_CONSULT']['PROPERTY_WHATSAPP_VALUE']) { ?>
						<div class="b-social__item" style="background-color: #67C15E">
							<a href="<?= $arResult['VIDEO_CONSULT']['PROPERTY_WHATSAPP_VALUE']; ?>">
								<svg width="26" height="22" fill="#fff">
									<use xlink:href="#whatsapp"></use>
								</svg>
							</a>
						</div>
                    <? } ?>
                    <? if ($arResult['VIDEO_CONSULT']['PROPERTY_VIBER_VALUE']) { ?>
						<div class="b-social__item" style="background-color: #7F4DA0">
							<a href="<?= $arResult['VIDEO_CONSULT']['PROPERTY_VIBER_VALUE']; ?>">
								<svg width="26" height="22" fill="#fff">
									<use xlink:href="#viber"></use>
								</svg>
							</a>
						</div>
                    <? } ?>
                    <? if ($arResult['VIDEO_CONSULT']['PROPERTY_TELEGRAM_VALUE']) { ?>
						<div class="b-social__item" style="background-color: #38A6DB">
							<a href="<?= $arResult['VIDEO_CONSULT']['PROPERTY_TELEGRAM_VALUE']; ?>">
								<svg width="26" height="22" fill="#fff">
									<use xlink:href="#twitter"></use>
								</svg>
							</a>
						</div>
                    <? } ?>
				</div>
			</div>
		</div>
	</div>
</div>