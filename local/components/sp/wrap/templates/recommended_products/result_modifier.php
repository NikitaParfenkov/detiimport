<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arSelect = Array("ID", "PROPERTY_PRODUCT");
$arFilter = Array("IBLOCK_ID"=>IBLOCK_RECOMMENDED_PRODUCTS, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch())
{
    $arResult['PRODUCTS'][$ob['PROPERTY_PRODUCT_VALUE']] = $ob['PROPERTY_PRODUCT_VALUE'];
}