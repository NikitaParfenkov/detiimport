<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if ($arResult['SECTION_INFO']['DESCRIPTION']) {
?>
<div class="catalog-info">
	<div class="row g-5">
		<?if ($arResult['SECTION_INFO']['NAME']) {?>
			<div class="col-md-3">
				<div class="catalog-info__subtitle"><?= $arResult['SECTION_INFO']['NAME'];?></div>
			</div>
		<?}?>
		<div class="col-md-9">
            <?= htmlspecialchars_decode($arResult['SECTION_INFO']['DESCRIPTION']); ?>
		</div>
	</div>
</div>
<?}?>