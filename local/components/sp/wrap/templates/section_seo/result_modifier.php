<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arParams['SECTION_INFO']) {
    CModule::IncludeModule('sp.tools');
    $catalogHandler = new \SP\Tools\Catalog();
    $filter = ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arParams['SECTION_ID']];
    $select = ['DESCRIPTION', 'NAME'];
    $arParams['SECTION_INFO'] = $catalogHandler->getSectionInfo($filter, $select, false);
}

$arResult['SECTION_INFO'] = $arParams['SECTION_INFO'];

$sectionSEO = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams['IBLOCK_ID'], $arParams['SECTION_ID']);
$seoValues = $sectionSEO->getValues();

if ($seoValues['SECTION_PAGE_TITLE']) {
    $APPLICATION->SetTitle($seoValues['SECTION_PAGE_TITLE']);
} else {
    $APPLICATION->SetTitle($arResult['SECTION_INFO']['NAME']);
}
if ($seoValues['SECTION_META_DESCRIPTION']) {
    $APPLICATION->SetPageProperty("description", $seoValues['SECTION_META_DESCRIPTION']);
}
if ($seoValues['SECTION_META_KEYWORDS']) {
    $APPLICATION->SetPageProperty("keywords", $seoValues['SECTION_META_KEYWORDS']);
}
if ($seoValues['SECTION_META_TITLE']) {
    $APPLICATION->SetPageProperty("title", $seoValues['SECTION_META_TITLE']);
}
