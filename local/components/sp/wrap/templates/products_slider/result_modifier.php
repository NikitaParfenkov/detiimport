<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if ($arParams['FILTER']) {
    CModule::IncludeModule('iblock');
    $arSelect = $arParams['SELECT'];
    $arFilter = $arParams['FILTER'];
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->Fetch())
    {
        $arResult['PRODUCTS'][] = $ob['PROPERTY_PRODUCT_VALUE'];
    }
}