<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="d-flex message-404">
	<svg class="mr-3" width="24" height="24" fill="#FA002A">
		<use xlink:href="#alert-info"></use>
	</svg>
	<div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/local/include/404/description.php"
            )
        ); ?>
	</div>
</div>
<div class="mainSearch mainSearch_static container-fluid">
	<form class="mainSearch__inner row g-0" action="/search/">
		<div class="mainSearch__control col col-md-6 offset-md-3">
			<input type="text" placeholder="<?= GetMessage('SEARCH_TITLE');?>">
			<div class="mainSearch__clear" onclick="this.previousSibling.value = '';"></div>
		</div>
		<div class="mainSearch__buttons col-md-3">
			<button class="btn btn-dark"><?= GetMessage('SEARCH');?></button>
		</div>
	</form>
</div>




