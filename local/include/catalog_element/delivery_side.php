<div class="b-delivery">
    <div class="b-delivery__top">
        <div class="b-delivery__title">Доставка</div>
    </div>
    <table>
        <thead>
        <tr>
            <th>Самовывоз</th>
            <th>Бесплатно</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Петропавловск-Камчатский, <br> ул. Красной Армии, д. 1, ТЦ Воздвиженка</td>
            <td>Сегодня</td>
        </tr>
        </tbody>
    </table>
    <table>
        <thead>
        <tr>
            <th>Курьером по Москве</th>
            <th>200 Р.</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Доставка заказов от 40 000 рублей — <b>бесплатно</b></td>
            <td>Сегодня</td>
        </tr>
        </tbody>
    </table>
    <table>
        <thead>
        <tr>
            <th>Транспортной компанией</th>
            <th>900 Р.</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>«ЖелДорЭкспедиция» <br> г. Петропавловск-Камчатский, ул. Вулканная, д. 29а</td>
            <td>3-6 дней</td>
        </tr>
        </tbody>
    </table>
    <a class="b-delivery__all" href="#">Все варианты доставки</a>
</div>