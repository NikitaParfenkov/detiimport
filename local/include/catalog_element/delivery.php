<div class="hr my-3"></div>
<div class="mb-3">
	<div class="product-content__sbl product-content__sbl_dost">
		Наличие и доставка:<span>Петропавловск-Камчатский</span>
	</div>
</div>
<div class="mb-1">
	<span class="text-success">
		<svg class="svg-icon mr-2" width="14" height="14">
			<use xlink:href="#check"></use>
		</svg>В наличии.
	</span>
	Курьер доставит завтра
</div>
<div class="mb-1">
	<div class="text-avail">
		<span class="text-info">
			<svg class="svg-icon mr-2" width="14" height="14">
				<use xlink:href="#check"></use>
			</svg>Доступно в шоурумах:
		</span>
		ул. Красной Армии, д.1, ТЦ 2 этаж. Чертаново Северное, ЮАО г. Москвы
		<a class="more" href="#home-tab-Купить-в-магазине">еще 23</a>
	</div>
</div>