<div class="b-shop">
    <picture class="b-shop__img">
        <img src="/local/templates/detimport/images/kit/shops/1.jpg">
    </picture>
    <div class="b-shop__content">
        <div class="b-shop__sity">
            <svg width="10" height="12">
                <use xlink:href="#location"></use>
            </svg>
            Москва
        </div>
        <div class="b-shop__title">Оптово-розничный магазин «Detimport» <br> на Кировоградской
        </div>
        <div class="b-shop__metro">
            <div class="b-metro">
                <div class="b-metro__metroItem">
                    <svg width="16" height="10" fill="#b4d445">
                        <use xlink:href="#metro"></use>
                    </svg>
                    Окружная
                </div>
                <div class="b-metro__metroItem">
                    <svg width="16" height="10" fill="#ffa8af">
                        <use xlink:href="#metro"></use>
                    </svg>
                    Владыкино
                </div>
            </div>
        </div>
        <div class="b-shop__address">ул. Кировоградская, д. 9, к. 1, ТЦ «Акварель», 2 этаж.
            Чертаново Северное, ЮАО г. Москвы
        </div>
        <div class="b-shop__aboveMenu">
            <div class="b-shop__social">
                <div class="social"><a class="social__telegram" href="#">
                        <svg>
                            <use xlink:href="#telegram"></use>
                        </svg>
                    </a><a class="social__viber" href="#">
                        <svg>
                            <use xlink:href="#viber"></use>
                        </svg>
                    </a><a class="social__whatsapp" href="#">
                        <svg>
                            <use xlink:href="#whatsapp"></use>
                        </svg>
                    </a></div>
            </div>
            <div class="b-shop__video-link"><a class="video-link" href="#"><span
                        class="video-link__icon"><svg><use xlink:href="#video"></use></svg></span><span>Видеоконсультации</span></a>
            </div>
        </div>
    </div>
    <div class="b-shop__side">
        <div class="b-shop__contact">+7(495) 232-97-00 <br> +7(968) 394-00-08</div>
        <div class="b-shop__days">Ежедневно: с 10:00 до 19:00</div>
    </div>
</div>
<hr>
<div class="b-shop">
    <picture class="b-shop__img"><img src="/local/templates/detimport/images/kit/shops/1.jpg">
    </picture>
    <div class="b-shop__content">
        <div class="b-shop__sity">
            <svg width="10" height="12">
                <use xlink:href="#location"></use>
            </svg>
            Москва
        </div>
        <div class="b-shop__title">Оптово-розничный магазин «Detimport» <br> на Кировоградской
        </div>
        <div class="b-shop__metro">
            <div class="b-metro">
                <div class="b-metro__metroItem">
                    <svg width="16" height="10" fill="#b4d445">
                        <use xlink:href="#metro"></use>
                    </svg>
                    Окружная
                </div>
                <div class="b-metro__metroItem">
                    <svg width="16" height="10" fill="#ffa8af">
                        <use xlink:href="#metro"></use>
                    </svg>
                    Владыкино
                </div>
            </div>
        </div>
        <div class="b-shop__address">ул. Кировоградская, д. 9, к. 1, ТЦ «Акварель», 2 этаж.
            Чертаново Северное, ЮАО г. Москвы
        </div>
        <div class="b-shop__aboveMenu">
            <div class="b-shop__social">
                <div class="social"><a class="social__telegram" href="#">
                        <svg>
                            <use xlink:href="#telegram"></use>
                        </svg>
                    </a><a class="social__viber" href="#">
                        <svg>
                            <use xlink:href="#viber"></use>
                        </svg>
                    </a><a class="social__whatsapp" href="#">
                        <svg>
                            <use xlink:href="#whatsapp"></use>
                        </svg>
                    </a></div>
            </div>
            <div class="b-shop__video-link"><a class="video-link" href="#"><span
                        class="video-link__icon"><svg><use xlink:href="#video"></use></svg></span><span>Видеоконсультации</span></a>
            </div>
        </div>
    </div>
    <div class="b-shop__side">
        <div class="b-shop__contact">+7(495) 232-97-00 <br> +7(968) 394-00-08</div>
        <div class="b-shop__days">Ежедневно: с 10:00 до 19:00</div>
    </div>
</div>