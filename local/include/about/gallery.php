<div class="page-slider-wrapper">
    <div class="page-slider swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><img src="/local/templates/detimport/images/banner_img.jpg">
                <div class="page-slider__title">Выставочный зал на Кировоградской</div>
            </div>
            <div class="swiper-slide"><img src="/local/templates/detimport/images/banner_img.jpg">
                <div class="page-slider__title">Выставочный зал на Виноградовской</div>
            </div>
        </div>
        <div class="swiper-navigation">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="page-slider-wrapper__title">На фото</div>
</div>