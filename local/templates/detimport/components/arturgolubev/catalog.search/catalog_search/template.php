<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Grid\Declension;

CModule::IncludeModule('sp.tools');
$this->setFrameMode(true);
$productDeclension = new Declension(GetMessage('FIRST_PRODUCT_DECLENSION'), GetMessage('SECOND_PRODUCT_DECLENSION'), GetMessage('THIRD_PRODUCT_DECLENSION'));
/*$bx_search_limit = COption::GetOptionString('search','max_result_size',50);*/
$arElements = $APPLICATION->IncludeComponent(
    "arturgolubev:search.page",
    "catalog",
    Array(
        "RESTART" => $arParams["RESTART"],
        "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
        "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "arrFILTER" => array("iblock_" . $arParams["IBLOCK_TYPE"]),
        "arrFILTER_iblock_" . $arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "arrWHERE" => array(),
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => '12',
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "N",
        "INPUT_PLACEHOLDER" => $arParams["INPUT_PLACEHOLDER"],
    ),
    $component,
    array('HIDE_ICONS' => 'Y')
);
if (!empty($arElements) && is_array($arElements)) {
    $sort = [
        'NAME_ASC' => 'Наименованию (А - Я)',
        'NAME_DESC' => 'Наименованию (Я - А)',
        'SHOW_COUNTER_DESC' => 'Рейтингу',
        'CATALOG_PRICE_1_ASC' => 'Цене (начиная с низкой)',
        'CATALOG_PRICE_1_DESC' => 'Цене (начиная с высокой)',
    ];

    if ($_GET['sort']) {
        $_SESSION['sort'] = $_GET['sort'];
    }
    switch ($_SESSION['sort']) {
        case 'NAME_ASC':
            $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
            $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
            break;
        case 'NAME_DESC':
            $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
            $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
            break;
        case 'SHOW_COUNTER_DESC':
            $arParams["ELEMENT_SORT_FIELD"] = 'SHOW_COUNTER';
            $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
            break;
        case 'CATALOG_PRICE_1_ASC':
            $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
            $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
            break;
        case 'CATALOG_PRICE_1_DESC':
            $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
            $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
            break;
        default:
            $_SESSION['sort'] = 'NAME_ASC';
            $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
            $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
            break;
    }

    if (!$_SESSION['HIDE_NOT_AVAILABLE']) {
        $_SESSION['HIDE_NOT_AVAILABLE'] = 'N';
    }
    if ($_GET['HIDE_NOT_AVAILABLE']) {
        $_SESSION['HIDE_NOT_AVAILABLE'] = $_GET['HIDE_NOT_AVAILABLE'];
    }
    ?>
	<div class="m-sort">
		<div class="m-sort__content">
			<div class="m-sort__subtitle"><?= GetMessage('SORT'); ?></div>
			<div class="m-sort__select-wrapper">
				<select class="m-sort__select sp-sort">
                    <? foreach ($sort as $type => $name) { ?>
						<option value="<?= $type; ?>"
                                <? if ($type === $_SESSION['sort']) { ?>selected<? } ?>><?= $name; ?></option>
                    <? } ?>
				</select>
			</div>
		</div>
		<div class="m-sort__side" data-toggle="filter">
			<div class="m-sort__icon">
				<span><? $APPLICATION->ShowViewContent('filtersCnt'); ?></span>
				<svg width="20" height="18">
					<use xlink:href="#filters"></use>
				</svg>
			</div>
		</div>
	</div>
	<div class="wrapper row g-0">
		<div class="wrapper__side col-xl-3">
			<div class="b-filter">
				<div class="b-filter__content">
					<div class="m-filter-header">
						<div class="m-filter-header__content">
							<div class="m-filter-header__subtitle"><?= GetMessage('SORT'); ?></div>
							<select class="m-filter-header__select sp-sort">
                                <? foreach ($sort as $type => $name) { ?>
									<option value="<?= $type; ?>"
                                            <? if ($type === $_SESSION['sort']) { ?>selected<? } ?>>
                                        <?= $name; ?>
									</option>
                                <? } ?>
							</select>
						</div>
						<div class="m-filter-header__side">
							<div class="m-filter-header__icon" data-toggle="filter">
								<svg width="20" height="18">
									<use xlink:href="#close"></use>
								</svg>
							</div>
						</div>
					</div>
                    <?
                    global $searchFilter;
                    $searchFilter = array(
                        "=ID" => $arElements,
                    );

                    $APPLICATION->IncludeComponent(
                        "sp:catalog.smart.filter",
                        "catalog_filter",
                        array(
                            'PREFILTER_NAME' => 'searchFilter',
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "SECTION_ID" => '0',
                            "FILTER_NAME" => 'arrFilter',
                            "PRICE_CODE" => $arParams["~PRICE_CODE"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                            "XML_EXPORT" => "N",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => 'Y',
                            "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            "SEF_MODE" => 'N',
                            "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                    ?>
				</div>
			</div>
		</div>
		<div class="wrapper__content col-xl-9">
            <?

            $elementsCount = (new \SP\Tools\Catalog())->getElementsCount($GLOBALS['arrFilter'], $arParams);

            ?>
			<!--<div class="searchText"><?/*= GetMessage('ON_REQUEST') */?>
				<span><?/*= $_GET['q'] */?></span> <?/*= substr($elementsCount, -1) == 1 ? GetMessage('FINDED') : GetMessage('FIND'); */?> <?/*= $elementsCount;*/?> <?/*= $productDeclension->get($elementsCount);*/?>
			</div>-->
			<div id="spCatalog">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "catalog_main",
                    array(
                        "HIDE_BANNER" => 'Y',
                        "HIDE_RECOMMENDED" => 'Y',
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                        "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                        "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],

                        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                        "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
                        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                        "OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
                        "OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
                        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                        "OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
                        "SECTION_URL" => $arParams["SECTION_URL"],
                        "DETAIL_URL" => $arParams["DETAIL_URL"],
                        "BASKET_URL" => $arParams["BASKET_URL"],
                        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                        "PRICE_CODE" => $arParams["~PRICE_CODE"],
                        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                        "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                        "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                        "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                        "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                        "HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                        "FILTER_NAME" => "arrFilter",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "SECTION_USER_FIELDS" => array(),
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "META_KEYWORDS" => "",
                        "META_DESCRIPTION" => "",
                        "BROWSER_TITLE" => "",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                    ),
                    $arResult["THEME_COMPONENT"],
                    array('HIDE_ICONS' => 'Y')
                );
                ?>
			</div>
		</div>
	</div>
<? } else { ?>
	<div class="wrapper row g-0">
		<div class="wrapper__side col-xl-3"></div>
		<div class="wrapper__content col-xl-9">
			<div class="d-flex">
				<svg class="mr-3" width="24" height="24" fill="#FA002A">
					<use xlink:href="#alert-info"></use>
				</svg>
				<p>
					<?= GetMessage('CT_BCSE_NOT_FOUND')?>
				</p>
			</div>
			<? $APPLICATION->IncludeComponent(
                "sp:wrap",
                "products_slider",
                array(
                    'CLASS' => 'b-items b-items_similar section b-items b-items_offsetNDown',
                    'FILTER' => ["IBLOCK_ID" => IBLOCK_RECOMMENDED_PRODUCTS, "ACTIVE" => "Y"],
                    'SELECT' => ["ID", "PROPERTY_PRODUCT"],
                    "TITLE" => 'Рекомендации для вас',
                ),
                false
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "404_slider",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "DETAIL_TEXT"),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "11",
                    "IBLOCK_TYPE" => "marketing",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("SALE", "URL", "LOGO", ""),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "NAME",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N"
                )
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "sp:wrap",
                "products_slider",
                array(
                    'CLASS' => 'b-items b-items_similar section',
                    'FILTER' => ["IBLOCK_ID" => IBLOCK_FINDED_PRODUCTS, "ACTIVE" => "Y"],
                    'SELECT' => ["ID", "PROPERTY_PRODUCT"],
                    "TITLE" => 'Какие товары чаще всего ищут',
                ),
                false
            ); ?>
		</div>
	</div>
<? } ?>
