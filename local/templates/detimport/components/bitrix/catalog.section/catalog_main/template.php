<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
ob_start();
use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
$generalParams = array(
    'HIDE_FAVCOMP_BLOCK' => $arParams['HIDE_FAVCOMP_BLOCK'],
    "CARD_CLASS" => $arParams['CARD_CLASS'],
    'GALLERY_SLIDER' => $arParams['GALLERY_SLIDER'],
    "FAVORITES" => $arParams['FAVORITES'],
    "COMPARE" => $arParams['COMPARE'],
    "COLORS" => $arResult['COLORS'],
    "SECTIONS" => $arResult['SECTIONS'],
    "IMG_SIZES" => $arParams['IMG_SIZES'],
    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
    'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
    'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
    'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
    'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
    'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
    'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
    'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
    'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
    'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
    'COMPARE_PATH' => $arParams['COMPARE_PATH'],
    'COMPARE_NAME' => $arParams['COMPARE_NAME'],
    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
    'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
    'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
    'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
    '~BASKET_URL' => $arParams['~BASKET_URL'],
    '~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
    '~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
    '~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
    '~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
    'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
    'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
    'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
    'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
    'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

if (!empty($arResult['ITEMS'])) {
    $areaIds = array();
    $firstPart = array_slice($arResult['ITEMS'], 0, 12);
    $secondPart = array_slice($arResult['ITEMS'], 12);
    ?>
	<div class="sp-list-items">
        <? if ($firstPart) { ?>
			<div class="catalog-list">
				<div class="row g-0">
                    <?
                    foreach ($firstPart as $item) {
                        $uniqueId = $item['ID'] . '_' . md5($this->randString() . $component->getAction());
                        $areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
                        $this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
                        $this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);

                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.item',
                            'catalog',
                            array(
                                'RESULT' => array(
                                    'ITEM' => $item,
                                    'AREA_ID' => $areaIds[$item['ID']],
                                    'TYPE' => 'catalog',
                                    'BIG_LABEL' => 'N',
                                    'BIG_DISCOUNT_PERCENT' => 'N',
                                    'BIG_BUTTONS' => 'N',
                                    'SCALABLE' => 'N'
                                ),
                                'PARAMS' => $generalParams
                                    + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                    } ?>
				</div>
			</div>
		<? } ?>
		<? if ($arParams['HIDE_RECOMMENDED'] !== 'Y' && $arParams['IS_FILTERED'] !== 'y') {?>
		#RECOMMENDED_PRODUCT#
		<?}?>
       <? if ($secondPart) { ?>
			<div class="catalog-list">
				<div class="row g-0">
                    <?foreach ($secondPart as $item) {
                        $uniqueId = $item['ID'] . '_' . md5($this->randString() . $component->getAction());
                        $areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
                        $this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
                        $this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);

                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.item',
                            'catalog',
                            array(
                                'RESULT' => array(
                                    'ITEM' => $item,
                                    'AREA_ID' => $areaIds[$item['ID']],
                                    'TYPE' => 'catalog',
                                    'BIG_LABEL' => 'N',
                                    'BIG_DISCOUNT_PERCENT' => 'N',
                                    'BIG_BUTTONS' => 'N',
                                    'SCALABLE' => 'N'
                                ),
                                'PARAMS' => $generalParams
                                    + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                    } ?>
                    <?
                    if ($arParams['HIDE_BANNER'] !== 'Y') {
	                    $APPLICATION->IncludeComponent(
	                        "bitrix:advertising.banner",
	                        "catalog_banner",
	                        Array(
	                            "BS_ARROW_NAV" => "Y",
	                            "BS_BULLET_NAV" => "Y",
	                            "BS_CYCLING" => "N",
	                            "BS_EFFECT" => "fade",
	                            "BS_HIDE_FOR_PHONES" => "N",
	                            "BS_HIDE_FOR_TABLETS" => "N",
	                            "BS_KEYBOARD" => "Y",
	                            "BS_WRAP" => "Y",
	                            "CACHE_TIME" => "0",
	                            "CACHE_TYPE" => "A",
	                            "DEFAULT_TEMPLATE" => "-",
	                            "NOINDEX" => "N",
	                            "QUANTITY" => "1",
	                            "TYPE" => "catalog"
	                        )
	                    );
                    }
                    ?>
				</div>
			</div>
       <? } ?>
	</div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>
		<div class="catalog-footer sp-list-pag">
            <?= $arResult["NAV_STRING"] ?>
            <? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage) { ?>
				<div class="show_more_btn">
					<div class="btn btn-light catalog-btn" data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>"
					     data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>"
					     data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">
						<svg width="12" height="12">
							<use xlink:href="#plus"></use>
						</svg>
						<span><?= GetMessage('SHOW_MORE'); ?></span>
					</div>
				</div>
            <? } ?>
		</div>
    <? } ?>
    <?
} else {
    ?>
	<div class="catalog-empty">
		<div class="catalog-empty__message">
			<div class="catalog-empty__message-head">
				<svg class="alert-icon" width="24" height="24">
					<use xlink:href="#alert"></use>
				</svg>
				<div class="catalog-empty__text">
					<?= GetMessage('EMPTY');?>
				</div>
			</div>
			<button class="catalog-empty__clear-filter btn btn-dark btn-sm rounded-pill js js_clear-catalog-filter" onclick="location.href = '<?= $arParams['SECTION_INFO']['SECTION_PAGE_URL'];?>';">
				<svg width="11" height="11" fill="#fff">
					<use xlink:href="#reset"></use>
				</svg>
				<span><?= GetMessage('CLEAR_FILTERS');?></span>
			</button>
		</div>
	</div>
    <?
}
$this->__component->arResult["RECOMMENDED_PRODUCT"] = @ob_get_contents();
ob_get_clean();