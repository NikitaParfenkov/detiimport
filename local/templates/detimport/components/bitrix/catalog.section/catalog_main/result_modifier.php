<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use SP\Tools\Catalog;
use SP\Tools\HLTools;
CModule::IncludeModule('sp.tools');
foreach ($arResult['ITEMS'] as $arItem) {
    if ($arItem['IBLOCK_SECTION_ID']) {
        $sectionsID[$arItem['IBLOCK_SECTION_ID']] = $arItem['IBLOCK_SECTION_ID'];
    }
    if ($arItem['OFFERS']) {
        foreach ($arItem['OFFERS'] as $offer) {
            $colors[$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']] = $offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'];
        }
    }
}

if ($sectionsID) {
    $sectionHandler = new Catalog();
    $arResult['SECTIONS'] = $sectionHandler->getSectionsName($sectionsID);
}

if ($colors) {
    $hlTools = new HLTools();
    $select = ['UF_XML_ID', 'UF_DESCRIPTION'];
    $filter = ["UF_XML_ID" => $colors];
    $order = ["ID" => "ASC"];

    $arResult['COLORS'] = $hlTools->getElementsByFilter(HL_COLORS, $select, $filter, $order);
}

$this->__component->SetResultCacheKeys(array("RECOMMENDED_PRODUCT"));