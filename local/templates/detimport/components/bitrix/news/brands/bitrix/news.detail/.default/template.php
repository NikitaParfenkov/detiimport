<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['~PREVIEW_TEXT']) {
    $this->SetViewTarget('brandDescription');
    ?>
	<div class="catalog-info">
		<div class="row g-5">
			<div class="col-md-3">
				<div class="catalog-info__subtitle"><?= $arResult['NAME']; ?></div>
			</div>
			<div class="col-md-9">
                <?= $arResult['~PREVIEW_TEXT']; ?>
			</div>
		</div>
	</div>
    <? $this->EndViewTarget(); ?>
<? } ?>