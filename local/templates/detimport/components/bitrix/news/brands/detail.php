<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use SP\Tools\Brands;
use SP\Tools\Catalog;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
Loader::includeModule("sp.tools");

?>
<?$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"",
	Array(
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"META_KEYWORDS" => $arParams["META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
		"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
		"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"USE_SHARE" => $arParams["USE_SHARE"],
		"SHARE_HIDE" => $arParams["SHARE_HIDE"],
		"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
		"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
		"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
		"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
	),
	$component
);?>
<?
$brandHandler = new Brands();
$catalogHandler = new Catalog();
$sectionsID = $catalogHandler->getProductsSectionsByFilter(['PROPERTY_BRAND' => $ElementID]);
if ($sectionsID) {
    $sections = $catalogHandler->getSectionsInfo(['ID' => $sectionsID, 'IBLOCK_ID' => IBLOCK_CATALOG], ['ID', 'NAME']);
}
$GLOBALS['preFilter']['PROPERTY_BRAND'] = $ElementID;
if ($_GET['section']) {
    $GLOBALS['preFilter']['IBLOCK_SECTION_ID'] = $_GET['section'];
}
$elementsCnt = $catalogHandler->getElementsCount($GLOBALS['preFilter'], ['IBLOCK_ID' => IBLOCK_CATALOG, 'INCLUDE_SUBSECTIONS' => 'Y', 'SHOW_ALL_WO_SECTION' => 'Y', 'HIDE_NOT_AVAILABLE' => 'Y']);



?>
<div class="container-fluid">
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "breadcrumbs",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    ); ?>
	<div class="b-back"><?= $_SESSION['PREVIOUS_TITLE']; ?></div>
    <? $_SESSION['PREVIOUS_TITLE'] = $APPLICATION->GetTitle(false); ?>
	<h1 class="main-title">
        <? $APPLICATION->ShowTitle(false); ?>
        <? if ($elementsCnt) { ?>
			<span class="sup"><?= $elementsCnt; ?></span>
        <? } ?>
	</h1>
<?
$sort = [
    'NAME_ASC' => 'Наименованию (А - Я)',
    'NAME_DESC' => 'Наименованию (Я - А)',
    'SHOW_COUNTER_DESC' => 'Рейтингу',
    'CATALOG_PRICE_1_ASC' => 'Цене (начиная с низкой)',
    'CATALOG_PRICE_1_DESC' => 'Цене (начиная с высокой)',
];

if ($_GET['sort']) {
    $_SESSION['sort'] = $_GET['sort'];
}
switch ($_SESSION['sort']) {
    case 'NAME_ASC':
        $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
        $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
        break;
    case 'NAME_DESC':
        $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
        $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
        break;
    case 'SHOW_COUNTER_DESC':
        $arParams["ELEMENT_SORT_FIELD"] = 'SHOW_COUNTER';
        $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
        break;
    case 'CATALOG_PRICE_1_ASC':
        $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
        $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
        break;
    case 'CATALOG_PRICE_1_DESC':
        $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
        $arParams["ELEMENT_SORT_ORDER"] = 'DESC';
        break;
    default:
        $_SESSION['sort'] = 'NAME_ASC';
        $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
        $arParams["ELEMENT_SORT_ORDER"] = 'ASC';
        break;
}

if (!$_SESSION['HIDE_NOT_AVAILABLE']) {
    $_SESSION['HIDE_NOT_AVAILABLE'] = 'N';
}
if ($_GET['HIDE_NOT_AVAILABLE']) {
    $_SESSION['HIDE_NOT_AVAILABLE'] = $_GET['HIDE_NOT_AVAILABLE'];
}
?>
<div class="m-sort">
	<div class="m-sort__content">
		<div class="m-sort__subtitle"><?= GetMessage('SORT'); ?></div>
		<div class="m-sort__select-wrapper">
			<select class="m-sort__select sp-sort">
                <? foreach ($sort as $type => $name) { ?>
					<option value="<?= $type; ?>"
                            <? if ($type === $_SESSION['sort']) { ?>selected<? } ?>><?= $name; ?></option>
                <? } ?>
			</select>
		</div>
	</div>
	<div class="m-sort__side" data-toggle="filter">
		<div class="m-sort__icon">
			<span><? $APPLICATION->ShowViewContent('filtersCnt'); ?></span>
			<svg width="20" height="18">
				<use xlink:href="#filters"></use>
			</svg>
		</div>
	</div>
</div>
<div class="wrapper row g-0">
	<div class="wrapper__side col-xl-3">
		<div class="b-filter">
			<div class="b-filter__content">
				<div class="m-filter-header">
					<div class="m-filter-header__content">
						<div class="m-filter-header__subtitle"><?= GetMessage('SORT'); ?></div>
						<select class="m-filter-header__select sp-sort">
                            <? foreach ($sort as $type => $name) { ?>
								<option value="<?= $type; ?>"
                                        <? if ($type === $_SESSION['sort']) { ?>selected<? } ?>>
                                    <?= $name; ?>
								</option>
                            <? } ?>
						</select>
					</div>
					<div class="m-filter-header__side">
						<div class="m-filter-header__icon" data-toggle="filter">
							<svg width="20" height="18">
								<use xlink:href="#close"></use>
							</svg>
						</div>
					</div>
				</div>
                <?
                if ($sections) {
                	?>
	                <div class="nav-side nav-side_cats">
		                <div class="nav-side__header">
			                <div class="nav-side__title"><?= GetMessage('CATEGORIES') ?></div>
		                </div>
		                <div class="nav-side__content">
			                <div class="nav-cats">
                                <?foreach ($sections as $section) { ?>
					                <a href="<?= $APPLICATION->GetCurPage(false);?>?section=<?= $section['ID'];?>" <?
                                    if ($section['ID'] === $_GET['section']){ ?>class="is-active"<?
                                    } ?>><?= $section['NAME']; ?></a>
                                <? } ?>
			                </div>
		                </div>
	                </div>
	                <?
                }
                ?>
				<div class="nav-side d-xl-none">
					<div class="nav-side__header">
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" id="flexSwitchCheck_available_m"
                                   <? if ($_SESSION['HIDE_NOT_AVAILABLE'] === 'Y'){ ?>checked<? } ?>
							       name="available">
							<label class="nav-side__title form-check-label"
							       for="flexSwitchCheck_available_m"><?= GetMessage('AVAILABLE'); ?></label>
						</div>
					</div>
				</div>
                <?$APPLICATION->IncludeComponent(
                    "sp:catalog.smart.filter",
                    "catalog_filter",
                    Array(
                        'SKIP_BRAND' => 'Y',
                        "PREFILTER_NAME" => "preFilter",
                        "IBLOCK_TYPE" => '',
                        "IBLOCK_ID" => IBLOCK_CATALOG,
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CONVERT_CURRENCY" => "N",
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_NOT_AVAILABLE" => $_SESSION['HIDE_NOT_AVAILABLE'],
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "PRICE_CODE" => array("РРЦ"),
                        "SAVE_IN_SESSION" => "N",
                        "SECTION_CODE" => "",
                        "SECTION_DESCRIPTION" => "-",
                        "SECTION_ID" => "0",
                        "SECTION_TITLE" => "-",
                        "SEF_MODE" => "N",
                        "XML_EXPORT" => "N",
                        "SHOW_ALL_WO_SECTION" => 'Y'
                    )
                );?>
			</div>
		</div>
	</div>
	<div class="wrapper__content col-xl-9">
		<div class="catalog-header">
			<div class="form-check form-switch">
				<input class="form-check-input" type="checkbox" id="flexSwitchCheck_available"
                       <? if ($_SESSION['HIDE_NOT_AVAILABLE'] === 'Y'){ ?>checked<? } ?> name="available">
				<label class="form-check-label"
				       for="flexSwitchCheck_available"><?= GetMessage('AVAILABLE'); ?></label>
			</div>
			<div class="form-sort"><?= GetMessage('SORT'); ?>
				<div class="dropdown">
					<button class="btn btn-dark btn-sm rounded-pill dropdown-toggle" id="dropdownMenuButton"
					        type="button" data-toggle="dropdown"
					        aria-expanded="false"><?= $sort[$_SESSION['sort']]; ?>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <? foreach ($sort as $type => $name) { ?>
							<li>
								<a class="dropdown-item sp-sort" href="javascript:void(0)"
								   data-sort="<?= $type; ?>"><?= $name; ?></a>
							</li>
                        <? } ?>
					</ul>
				</div>
			</div>
		</div>
		<div id="spCatalog">

            <?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"catalog_main",
				Array(
					"ACTION_VARIABLE" => "action",
					"ADD_PICT_PROP" => "-",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"ADD_TO_BASKET_ACTION" => "ADD",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"BACKGROUND_IMAGE" => "-",
					"BASKET_URL" => "/personal/basket.php",
					"BROWSER_TITLE" => "-",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COMPATIBLE_MODE" => "Y",
					"CONVERT_CURRENCY" => "N",
					"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
					"DETAIL_URL" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"DISPLAY_COMPARE" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_ORDER2" => "desc",
					"ENLARGE_PRODUCT" => "STRICT",
					"FILTER_NAME" => "arrFilter",
					"HIDE_NOT_AVAILABLE" => "N",
					"HIDE_NOT_AVAILABLE_OFFERS" => "N",
					"IBLOCK_ID" => IBLOCK_CATALOG,
					"IBLOCK_TYPE" => "1c_catalog",
					"INCLUDE_SUBSECTIONS" => "Y",
					"LABEL_PROP" => array(),
					"LAZY_LOAD" => "N",
					"LINE_ELEMENT_COUNT" => "3",
					"LOAD_ON_SCROLL" => "N",
					"MESSAGE_404" => "",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"META_DESCRIPTION" => "-",
					"META_KEYWORDS" => "-",
					"OFFERS_FIELD_CODE" => array("", ""),
					"OFFERS_LIMIT" => "5",
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_ORDER2" => "desc",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "Товары",
					"PAGE_ELEMENT_COUNT" => "18",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRICE_CODE" => array("РРЦ"),
					"PRICE_VAT_INCLUDE" => "Y",
					"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
					"PRODUCT_DISPLAY_MODE" => "N",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'0','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
					"PRODUCT_SUBSCRIPTION" => "Y",
					"PROPERTY_CODE_MOBILE" => array(),
					"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
					"RCM_TYPE" => "personal",
					"SECTION_CODE" => "",
					"SECTION_ID" => $_GET['section'],
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"SECTION_URL" => "",
					"SECTION_USER_FIELDS" => array("", ""),
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SHOW_ALL_WO_SECTION" => "Y",
					"SHOW_CLOSE_POPUP" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_FROM_SECTION" => "N",
					"SHOW_MAX_QUANTITY" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"SHOW_SLIDER" => "Y",
					"SLIDER_INTERVAL" => "3000",
					"SLIDER_PROGRESS" => "N",
					"TEMPLATE_THEME" => "blue",
					"USE_ENHANCED_ECOMMERCE" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"USE_PRICE_COUNT" => "N",
					"USE_PRODUCT_QUANTITY" => "N"
				)
			);?>
		</div>
	</div>
</div>
<?$APPLICATION->ShowViewContent('brandDescription');?>
</div>

