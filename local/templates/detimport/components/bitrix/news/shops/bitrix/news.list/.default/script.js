$(document).on('click', '.sp-shop-video-modal', function () {
    let modalForm = $('#modal-videoconsult-shop');
    let parentBlock = $(this).parents('.commerce-info__social');
    let telegramURL = parentBlock.find('.social__telegram').attr('href') ?? '';
    let viberURL = parentBlock.find('.social__viber').attr('href') ?? '';
    let whatsappURL = parentBlock.find('.social__whatsapp').attr('href') ?? '';
    if (telegramURL.length > 0) {
        modalForm.find('.sp-telegram').show();
        modalForm.find('.sp-telegram a').attr('href', telegramURL);
    } else {
        modalForm.find('.sp-telegram').hide();
    }
    if (viberURL.length > 0) {
        modalForm.find('.sp-viber').show();
        modalForm.find('.sp-viber a').attr('href', viberURL);
    } else {
        modalForm.find('.sp-viber').hide();
    }
    if (whatsappURL.length > 0) {
        modalForm.find('.sp-whatsapp').show();
        modalForm.find('.sp-whatsapp a').attr('href', whatsappURL);
    } else {
        modalForm.find('.sp-whatsapp').hide();
    }
    new bootstrap.Modal(document.getElementById('modal-videoconsult-shop'), {}).show();
});

$(document).on('click', '[data-show-more]', function () {
    var btn = $(this);
    var page = btn.attr('data-next-page');
    var id = btn.attr('data-show-more');
    var data = {};
    data['PAGEN_' + id] = page;

    $.ajax({
        type: "GET",
        url: window.location.href,
        data: data,
        success: function (data) {
            var list = $(data).find('.sp-list-items');
            $('.sp-list-pag').remove();
            $('.sp-list-items').append(list.html());
            $('.sp-list-items').after($(data).find('.sp-list-pag'));
        }
    });
});