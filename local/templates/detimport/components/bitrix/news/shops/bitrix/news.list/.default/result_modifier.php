<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use SP\Tools\Stores;
use SP\Tools\Catalog;

foreach ($arResult['ITEMS'] as &$arItem) {
    if ($arItem['PROPERTIES']['GALLERY']['VALUE']) {
        foreach ($arItem['PROPERTIES']['GALLERY']['VALUE'] as $picID) {
            $arItem['GALLERY'][] = [
                'SMALL' => CFile::ResizeImageGet($picID,
                    ['width' => 140, 'height' => 90],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
                'BIG' => CFile::ResizeImageGet($picID,
                    ['width' => 880, 'height' => 450],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
            ];
        }
    } else {
        $arItem['GALLERY'][] = [
            'SMALL' => CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'],
                ['width' => 140, 'height' => 90],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
            'BIG' => CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'],
                ['width' => 880, 'height' => 450],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
        ];
    }

    if ($arItem['PROPERTIES']['STORE_ID']['VALUE']) {
        $storesID[$arItem['PROPERTIES']['STORE_ID']['VALUE']] = $arItem['PROPERTIES']['STORE_ID']['VALUE'];
    }
}

if ($storesID) {

    CModule::IncludeModule('sp.tools');
    $storeHandle = new Stores();
    $storeProducts = $storeHandle->getStoreProducts($storesID);

    if ($storeProducts) {
        foreach ($storeProducts as $store) {
            foreach ($store as $productID) {
                $productsID[$productID] = $productID;
            }
        }
        if ($productsID) {
            $catalogHandle = new Catalog();
            $filter = ['IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $productsID];
            $sectionsID = $catalogHandle->getProductsSectionsByFilter($filter);
            if ($sectionsID) {
                $filter = ['IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $sectionsID];
                $select = ['ID', 'NAME', 'PICTURE', 'SECTION_PAGE_URL'];
                $sectionsInfo = $catalogHandle->getSectionsInfo($filter, $select, false);
            }
        }

        foreach ($storeProducts as $storeID => $products) {
            foreach ($products as $productID) {
                if ($sectionsID[$productID]) {
                    $arResult['STORES'][$storeID][$sectionsID[$productID]]['NAME'] = $sectionsInfo[$sectionsID[$productID]]['NAME'];
                    $arResult['STORES'][$storeID][$sectionsID[$productID]]['PICTURE'] = CFile::ResizeImageGet(
                        $sectionsInfo[$sectionsID[$productID]]['PICTURE'],
                        ['width' => 203, 'height' => 205],
                        BX_RESIZE_IMAGE_PROPORTIONAL
                    )['src'];
                    $arResult['STORES'][$storeID][$sectionsID[$productID]]['ELEMENTS_CNT']++;
                    $arResult['STORES'][$storeID][$sectionsID[$productID]]['SECTION_PAGE_URL'] = $sectionsInfo[$sectionsID[$productID]]['SECTION_PAGE_URL'];
                }
            }
        }
    }
}
