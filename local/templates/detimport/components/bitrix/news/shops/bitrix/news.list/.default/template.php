<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!empty($arResult["ITEMS"])) {
    $shops = [];
    ?>
	<div class="shops__list js js_shop-list active">
		<div class="sp-list-items">
            <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
				<div class="commerce-info" data-id="<?= $arItem['ID']; ?>"
				     id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
					<div class="commerce-info__interior-slider-wrap" id="shop<?= $arItem['ID']?>">
						<div class="commerce-info__interior-slider swiper-container">
							<div class="swiper-wrapper">
                                <? foreach ($arItem['GALLERY'] as $picture) { ?>
									<picture class="commerce-info__img swiper-slide">
										<img src="<?= $picture['BIG']; ?>">
									</picture>
                                <? } ?>
							</div>
						</div>
						<div class="commerce-info__interior-thumbs swiper-container">
							<div class="swiper-wrapper">
                                <? foreach ($arItem['GALLERY'] as $picture) { ?>
									<div class="commerce-info__thumb swiper-slide">
										<picture class="commerce-info__img">
											<img src="<?= $picture['SMALL']; ?>">
										</picture>
									</div>
                                <? } ?>
							</div>
							<div class="swiper-navigation">
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="commerce-info__content">
						<div class="info-card section">
							<div class="info-card__labels">
                                <? if ($arItem['PROPERTIES']['USER_CHOICE']['VALUE'] === 'Y') { ?>
									<div class="info-card__yandex-choice">
										<svg width="20" height="12" fill="#fff">
											<use xlink:href="#star"></use>
										</svg>
                                        <?= GetMessage('CUSTOMERS_CHOICE') ?>
									</div>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['PARTNER_SHOP']['VALUE'] === 'Y') { ?>
									<div class="info-card__partner">
										<svg width="20" height="12">
											<use xlink:href="#users"></use>
										</svg>
                                        <?= GetMessage('SHOP_PARTNER') ?>
									</div>
                                <? } ?>
							</div>
							<div class="info-card__inner">
                                <? if ($arItem['PROPERTIES']['CITY']['VALUE']) { ?>
									<div class="info-card__sity">
										<svg width="10" height="12">
											<use xlink:href="#location"></use>
										</svg>
                                        <?= $arItem['PROPERTIES']['CITY']['VALUE']; ?>
									</div>
                                <? } ?>
								<a class="info-card__title"
								   href="javascript:void(0)"><?= htmlspecialchars_decode($arItem['NAME']); ?></a>
                                <? if ($arItem['PROPERTIES']['METRO']['VALUE']) {
                                    $metroes = [];
                                    foreach ($arItem['PROPERTIES']['METRO']['VALUE'] as $key => $metro) {
                                        $metroes[] = [
                                            'title' => $metro,
                                            'color' => $arItem['PROPERTIES']['METRO']['DESCRIPTION'][$key] ?: '#a1a2a3',
                                        ];
                                        ?>
										<div class="info-card__metro">
											<div class="info-card__metroItem">
												<svg width="16" height="10"
												     fill="<?= $arItem['PROPERTIES']['METRO']['DESCRIPTION'][$key] ?: '#a1a2a3'; ?>">
													<use xlink:href="#metro"></use>
												</svg>
                                                <?= $metro; ?>
											</div>
										</div>
                                    <? } ?>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['ADDRESS']['VALUE']) { ?>
									<div class="info-card__address"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE']; ?></div>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['CONTACTS']['VALUE']) { ?>
									<div class="info-card__contact">
                                        <?= htmlspecialchars_decode(implode(' <br> ',
                                            $arItem['PROPERTIES']['CONTACTS']['VALUE'])); ?>
									</div>
                                <? } ?>
								<div class="social commerce-info__social">
                                    <? if ($arItem['PROPERTIES']['WHATSAPP']['VALUE']) { ?>
										<a class="social__whatsapp"
										   href="<?= $arItem['PROPERTIES']['WHATSAPP']['VALUE']; ?>">
											<svg>
												<use xlink:href="#whatsapp"></use>
											</svg>
										</a>
                                    <? } ?>
                                    <? if ($arItem['PROPERTIES']['VIBER']['VALUE']) { ?>
										<a class="social__viber" href="<?= $arItem['PROPERTIES']['VIBER']['VALUE']; ?>">
											<svg>
												<use xlink:href="#viber"></use>
											</svg>
										</a>
                                    <? } ?>
                                    <? if ($arItem['PROPERTIES']['TELEGRAM']['VALUE']) { ?>
										<a class="social__telegram"
										   href="<?= $arItem['PROPERTIES']['TELEGRAM']['VALUE']; ?>">
											<svg>
												<use xlink:href="#telegram"></use>
											</svg>
										</a>
                                    <? } ?>
                                    <? if ($arItem['PROPERTIES']['VIDEOCONSULT']['VALUE'] === 'Y') { ?>
										<div class="social__video-link">
											<a class="video-link sp-shop-video-modal" href="javascript:void(0)">
											    <span class="video-link__icon">
												    <svg><use xlink:href="#video"></use></svg>
											    </span>
												<span><?= $arItem['PROPERTIES']['VIDEOCONSULT']['NAME']; ?></span>
											</a>
										</div>
                                    <? } ?>
								</div>
                                <? if ($arItem['PROPERTIES']['WORK_TIME']['VALUE']) { ?>
									<div class="info-card__days">
										<span><?= $arItem['PROPERTIES']['WORK_TIME']['NAME']; ?>:</span>
                                        <?= htmlspecialchars_decode(implode(' <br> ',
                                            $arItem['PROPERTIES']['WORK_TIME']['VALUE'])); ?>
									</div>
                                <? } ?>
                                <?
                                $coordinates = [];
                                if ($arItem['PROPERTIES']['COORDINATES']['VALUE']) {
                                    $coordinates = explode(',', $arItem['PROPERTIES']['COORDINATES']['VALUE']);
                                    ?>
									<a class="info-card__how-get js js_show-on-map btn btn-dark btn-sm rounded-pill"
									   data-shop-id="<?= $arItem['ID']; ?>">
										<svg fill="#fff" width="16" height="16">
											<use xlink:href="#location"></use>
										</svg>
										<span><?= GetMessage('HOW_TO_GET') ?></span>
									</a>
                                <? } ?>
							</div>
						</div>
					</div>
                    <? if ($arResult['STORES'][$arItem['PROPERTIES']['STORE_ID']['VALUE']]) {
                        $assortment = [];
                        ?>
						<div class="commerce-info__assorment section">
							<div class="b-items b-items_similar section">
								<div class="section-header section-header_shops section-header_size-md">
									<div class="section-header__header row">
										<div class="col-md-6 col-xl-6">
											<div class="section-header__title section-header__title_2 h1">
                                                <?= GetMessage('SHOP_ASSORTMENT'); ?>
											</div>
										</div>
										<div class="col">
											<div class="section-header__navi" data-move="section-navi-get">
												<div class="swiper-pagination"></div>
												<div class="swiper-navigation swiper-navigation-parent">
													<div class="swiper-button-prev"></div>
													<div class="swiper-button-next"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="b-items__slider b-items__slider_shops swiper-container"
								     data-slidesperview="[1.5,2.5,3,5,6,6]">
									<div class="swiper-wrapper">
                                        <? foreach ($arResult['STORES'][$arItem['PROPERTIES']['STORE_ID']['VALUE']] as $section) {
                                            $assortment[] = [
                                                'title' => $section['NAME'],
                                                'num' => $section['ELEMENTS_CNT'],
                                            ];
                                            ?>
											<div class="swiper-slide col-6 col-xl-2">
												<a class="itemPlate"
												   href="<?= $section['SECTION_PAGE_URL']; ?>?store_id=<?= $arItem['PROPERTIES']['STORE_ID']['VALUE']; ?>">
											<span class="itemPlate__title">
												<?= $section['NAME'] ?>
												<span><?= $section['ELEMENTS_CNT'] ?></span>
											</span>
                                                    <? if ($section['PICTURE']) { ?>
														<img class="itemPlate__img" src="<?= $section['PICTURE']; ?>"
														     alt="" role="presentation"/>
                                                    <? } ?>
												</a>
											</div>
                                        <? } ?>
									</div>
								</div>
							</div>
						</div>
                    <? } ?>
				</div>
                <?
                $shops[] = [
                    'id' => $arItem['ID'],
                    'sity' => $arItem['PROPERTIES']['CITY']['VALUE'] ?: '',
                    'title' => $arItem['NAME'],
                    'metro' => $metroes,
                    'address' => $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?: '',
                    'coords' => [(float)$coordinates[0], (float)$coordinates[1]],
                    'contact' => $arItem['PROPERTIES']['CONTACTS']['VALUE'] ? implode(' <br> ',
                        $arItem['PROPERTIES']['CONTACTS']['VALUE']) : '',
                    'days' => $arItem['PROPERTIES']['WORK_TIME']['VALUE'] ? implode(' <br> ',
                        $arItem['PROPERTIES']['WORK_TIME']['VALUE']) : '',
                    'partner' => $arItem['PROPERTIES']['PARTNER_SHOP']['VALUE'] === 'Y' ? true : false,
                    'yandexChoice' => $arItem['PROPERTIES']['USER_CHOICE']['VALUE'] === 'Y' ? true : false,
                    'image' => $arItem['GALLERY'][0]['BIG'],
                    'assortment' => $assortment,
                    'socials' => [
                        [
                            'name' => 'whatsApp',
                            'value' => $arItem['PROPERTIES']['WHATSAPP']['VALUE'] ?: '',
                        ],
                        [
                            'name' => 'viber',
                            'value' => $arItem['PROPERTIES']['VIBER']['VALUE'] ?: '',
                        ],
                        [
                            'name' => 'telegram',
                            'value' => $arItem['PROPERTIES']['TELEGRAM']['VALUE'] ?: '',
                        ],
                        [
                            'name' => 'video',
                            'value' => $arItem['PROPERTIES']['VIDEOCONSULT']['VALUE'] === 'Y' ? "#video" : '',
                        ],
                    ],
                    'howGet' => $arItem['PROPERTIES']['HOW_TO_GET']['~VALUE']['TEXT'] ?: ''
                ];
            } ?>
		</div>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
			<div class="shops__pagination sp-list-pag">
                <?= $arResult["NAV_STRING"] ?>
                <? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage): ?>
					<div class="show_more_btn">
						<div class="btn btn-light catalog-btn" data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>"
						     data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>"
						     data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">
							<svg width="12" height="12">
								<use xlink:href="#plus"></use>
							</svg>
							<span><?= GetMessage('SHOW_MORE'); ?></span>
						</div>
					</div>
                <? endif ?>
			</div>
        <? endif; ?>
	</div>
	<script>
        if (typeof shops === 'undefined') {
            var shops = [];
        }
        var pageShops = <?=CUtil::PhpToJSObject($shops, false, false, true);?>;
        shops = shops.concat(pageShops);
	</script>
<? } ?>