<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CModule::IncludeModule('sp.tools');

use SP\Tools\Shops;

$shopsHandler = new Shops();

$shopsCount = $shopsHandler->getShopsCount();
$cities = $shopsHandler->getShopsCities();
?>
	<div class="container-fluid shops__wrapper">
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumbs",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        ); ?>
		<div class="b-back"><?= $_SESSION['PREVIOUS_TITLE']; ?></div>
        <? $_SESSION['PREVIOUS_TITLE'] = $APPLICATION->GetTitle(false); ?>
		<h1 class="main-title"> <? $APPLICATION->ShowTitle(false); ?><span class="sup"><?= $shopsCount; ?></span></h1>
		<div class="m-compare-menu m-cities-menu">
			<div class="m-compare-menu__dropdown" data-toggle="modal" data-target="#favoritesFilterModal">
				<div class="dropdown-compare dropdown">
					<button class="btn dropdown-toggle" id="dropdownMenuCompares" type="button" data-toggle="dropdown"
					        aria-expanded="false">
						<span class="dropdown-compare__title"><?= GetMessage('CHOSE_CITY'); ?></span>
					</button>
				</div>
			</div>
		</div>
        <?
        if ($_GET['city']) {
            $GLOBALS['arrFilter']['PROPERTY_CITY_VALUE'] = $_GET['city'];
        } else {
            if ($_SESSION['USER_CITY']) {
                $usersCityShops = $shopsHandler->getShopsCount(['PROPERTY_CITY_VALUE' => $_SESSION['USER_CITY']]);
                if ($usersCityShops > 0) {
                    $GLOBALS['arrFilter']['PROPERTY_CITY_VALUE'] = $_SESSION['USER_CITY'];
                } else {
                    $GLOBALS['arrFilter']['PROPERTY_CITY_VALUE'] = 'Москва';
                }
            } else {
                $GLOBALS['arrFilter']['PROPERTY_CITY_VALUE'] = 'Москва';
            }
        }
        ?>
		<div class="wrapper row g-0">
			<div class="wrapper__content col-xl-12">
				<div class="shops">
					<div class="shops-header">
						<div class="shops-header__cities-nav">
                            <? if ($cities) { ?>
								<ul class="shops-header__cities-nav-list">
                                    <? foreach ($cities as $city) { ?>
										<li class="shops-header__cities-item <? if ($GLOBALS['arrFilter']['PROPERTY_CITY_VALUE'] === $city) { ?>active<? } ?>">
											<a class="shops-header__cities-link"
											   href="?city=<?= $city; ?>"><?= $city; ?></a>
										</li>
                                    <? } ?>
								</ul>
                            <? } ?>
						</div>
						<div class="shops-header__interactive">
							<div class="form-check form-switch form-switch_double js_shops-view">
								<input class="form-check-input" type="checkbox" id="flexSwitchCheck_available">
								<label class="form-check-label"
								       for="flexSwitchCheck_available"><?= GetMessage('LIST') ?></label>
								<label class="form-check-label"
								       for="flexSwitchCheck_available"><?= GetMessage('ON_MAP') ?></label>
							</div>
						</div>
						<div class="shops-header__mobile">
							<a class="shops-header__how-get btn btn-dark btn-sm rounded-pill js js_enable-mobile-map">
								<svg fill="#fff" width="16" height="16">
									<use xlink:href="#location"></use>
								</svg>
								<span><?= GetMessage('ON_MAP') ?></span>
							</a>
						</div>
					</div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "",
                        Array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                            "SORT_BY1" => $arParams["SORT_BY1"],
                            "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                            "SORT_BY2" => $arParams["SORT_BY2"],
                            "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                            "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "MESSAGE_404" => $arParams["MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                            "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                            "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                            "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                            "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                            "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                            "FILTER_NAME" => 'arrFilter',
                            "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                            "CHECK_DATES" => $arParams["CHECK_DATES"],
                        ),
                        $component
                    ); ?>
					<div class="shops__map js js_cities-map">
						<div class="shops__cities-map-close btn btn-dark">
							<svg width="16" height="16" fill="#fff">
								<use xlink:href="#close"></use>
							</svg>
						</div>
						<div class="shops__map-pano js js_cities-stage"></div>
						<div class="shops__map-list">
							<div class="shops__map-search js js_map-search">
								<form class="b-search">
									<span></span>
									<input class="form-control" type="text" placeholder="<?= GetMessage('FIND') ?>" value="">
								</form>
							</div>
							<div class="shops__map-items-wrap js js_map-shops-list"></div>
						</div>
					</div>

					<div class="shops">
						<div class="shops__details-map js js_shop-details-map">
							<div class="shops__details-map-close btn btn-dark">
								<svg width="16" height="16" fill="#fff">
									<use xlink:href="#close"></use>
								</svg>
							</div>
							<div class="shops__details-map-content">
								<div class="shops__m-details-expand js js_detail-expand">
									<span class="shops__m-expand-text"></span>
									<span class="shops__m-expand-icon">
									<svg width="6" height="6" fill="#fff">
										<use xlink:href="#arrow"></use>
									</svg>
								</span>
								</div>
								<div class="shops__details-info"></div>
								<a class="shops__details-link btn btn-dark btn-sm rounded-pill"
								   href="javascript:void(0)">
								<span>
									<?= GetMessage('SHOP_DETAILS') ?>
									<svg width="6" height="10" fill="#fff">
										<use xlink:href="#arrow-tr-right"></use>
									</svg>
								</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="favoritesFilterModal" tabindex="-1" aria-labelledby="favoritesFilterModalLabel"
		     aria-hidden="true">
			<div class="modal-dialog modal-fullscreen">
				<div class="modal-content">
					<div class="modal-header">
						<div class="modal-title"><?= GetMessage('CHOSE_CITY') ?></div>
						<div class="modal-close2" data-dismiss="modal">
							<svg width="16" height="16">
								<use xlink:href="#close"></use>
							</svg>
						</div>
					</div>
					<div class="modal-body">
						<ul class="nav nav-default nav-border flex-column">
							<li class="nav-item active">
								<a class="nav-link"><?= GetMessage('ALL_CITIES') ?></a>
							</li>
                            <? foreach ($cities as $city) { ?>
								<li class="nav-item">
									<a class="nav-link" href="?city=<?= $city; ?>"><?= $city; ?></a>
								</li>
                            <? } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<? $APPLICATION->IncludeComponent(
    "sp:wrap",
    "videoconsult_shops_form",
    Array()
); ?>