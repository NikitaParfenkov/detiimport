<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)) {
        return;
    }
}
?>
<nav aria-label="<?= GetMessage("navigation") ?>">
	<ul class="pagination">
        <?
        $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
        $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
        if ($arResult["bDescPageNumbering"] !== true) {
            // to show always first and last pages
            $arResult["nStartPage"] = 1;
            $arResult["nEndPage"] = $arResult["NavPageCount"];

            $sPrevHref = '';
            if ($arResult["NavPageNomer"] > 1) {
                $bPrevDisabled = false;

                if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2) {
                    $sPrevHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] - 1);
                } else {
                    $sPrevHref = $arResult["sUrlPath"] . $strNavQueryStringFull;
                }
            } else {
                $bPrevDisabled = true;
            }

            $sNextHref = '';
            if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
                $bNextDisabled = false;
                $sNextHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] + 1);
            } else {
                $bNextDisabled = true;
            }
            ?>
            <?
            if ($bPrevDisabled):?>
				<li class="page-item page-link__wrap">
					<a class="page-link page-link_arrow" aria-label="<?= GetMessage("nav_prev") ?>">
						<span aria-hidden="true">
							<svg width="12" height="10">
								<use xlink:href="#arrow-tr-left"></use>
							</svg>
						</span>
					</a>
				</li>
            <? else: ?>
				<li class="page-item page-link__wrap">
					<a class="page-link page-link_arrow" href="<?= $sPrevHref; ?>" aria-label="<?= GetMessage("nav_prev") ?>">
						<span aria-hidden="true">
							<svg width="12" height="10">
								<use xlink:href="#arrow-tr-left"></use>
							</svg>
						</span>
					</a>
				</li>
            <? endif; ?>
            <?
            $bFirst = true;
            $bPoints = false;
            do {
                if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"] - $arResult["nStartPage"] <= 1 || abs($arResult['nStartPage'] - $arResult["NavPageNomer"]) <= 2) {

                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                        ?>
						<li class="page-item page-link__wrap active">
							<a class="page-link"><?= $arResult["nStartPage"] ?></a>
						</li>
                    <?
					elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                        ?>
						<li class="page-item page-link__wrap">
							<a class="page-link"
							   href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
						</li>
                    <?
                    else:
                        ?>
						<li class="page-item page-link__wrap">
							<a class="page-link"
							   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
						</li>
                    <?
                    endif;
                    $bFirst = false;
                    $bPoints = true;
                } else {
                    if ($bPoints) {
                        ?>
						<li class="page-item page-link__wrap">
							<span class="page-link">...</span>
						</li>
                        <?
                        $bPoints = false;
                    }
                }
                $arResult["nStartPage"]++;
            } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);
            ?>
            <?
            if ($bNextDisabled):?>
				<li class="page-item page-link__wrap">
					<a class="page-link page-link_arrow" aria-label="<?= GetMessage("nav_next") ?>">
						<span aria-hidden="true">
							<svg width="12" height="10">
								<use xlink:href="#arrow-tr-right"></use>
							</svg>
						</span>
					</a>
				</li>
            <? else: ?>
				<li class="page-item page-link__wrap">
					<a href="<?= $sNextHref; ?>" class="page-link page-link_arrow"
					   aria-label="<?= GetMessage("nav_next") ?>">
						<span aria-hidden="true">
							<svg width="12" height="10">
								<use xlink:href="#arrow-tr-right"></use>
							</svg>
						</span>
					</a>
				</li>
            <? endif; ?>
            <?
        }
        ?>
	</ul>
</nav>

