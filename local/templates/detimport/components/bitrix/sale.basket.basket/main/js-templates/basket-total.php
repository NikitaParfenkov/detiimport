<? if (! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
    <div class="basket-info__header">
        <div class="basket-info__title"><?= Loc::getMessage('BASKET_INFO') ?></div>
        <div class="basket-info__clear btn btn-dark rounded-pill" id="clear-all-basket">
            <svg width="10" height="11" fill="#fff">
                <use xlink:href="#basket"></use>
            </svg>
            <span><?= Loc::getMessage('CLEAR_BASKET') ?></span>
        </div>
    </div>
    <table class="table table-characteristics mb-5">
        <tbody>
        {{#ITEMS_CNT}}
        <tr>
            <th scope="row"><?= Loc::getMessage('SBB_GOODS_H') ?></th>
            <td>{{{ITEMS_CNT}}} <?= Loc::getMessage('ST_DOT') ?></td>
        </tr>
        {{/ITEMS_CNT}}
        {{#WEIGHT_FORMATED}}
        <tr>
            <th scope="row"><?= Loc::getMessage('SBB_WEIGHT') ?></th>
            <td>{{{WEIGHT_FORMATED}}}</td>
        </tr>
        {{/WEIGHT_FORMATED}}
        {{#PRICE_WITHOUT_DISCOUNT_FORMATED}}
        <tr>
            <th scope="row"><?= Loc::getMessage('ORDER_SUMM') ?></th>
            <td>{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}</td>
        </tr>
        {{/PRICE_WITHOUT_DISCOUNT_FORMATED}}
        {{#DISCOUNT_PRICE_FORMATED}}
        <tr>
            <th scope="row"><?= Loc::getMessage('DISCOUNT') ?></th>
            <td class="text-danger">- {{{DISCOUNT_PRICE_FORMATED}}}</td>
        </tr>
        {{/DISCOUNT_PRICE_FORMATED}}
        </tbody>
    </table>
    <!--<div class="b-promo">
        <div class="b-promo__title"><?/*=Loc::getMessage('INPUT_PROMO_CODE') */?></div>
        <input type="text" value="">
        <svg width="12" height="8" fill="#7BC537">
            <use xlink:href="#check_mark"></use>
        </svg>
        <span class="b-promo__error-message"><?/*=Loc::getMessage('ERROR_CODE')*/?></span>
    </div>-->
    <!--TODO create functional next time-->
    <!--  Настроить валидацию поля, ниже - временный код-->
    <!--document.querySelector('.b-promo input').addEventListener('input', function () {
            this.parentNode.classList.remove('invalid');
            this.parentNode.classList.remove('valid');

            if (this.value == 'valid') {
                this.parentNode.classList.add('valid');
            } else if (this.value == 'invalid') {
                this.parentNode.classList.add('invalid');
            }
        })-->
    <div class="basket-info__sum">
        <div class="basket-info__sumTitle"><?= Loc::getMessage('PRICE_WO_DELIVERY') ?></div>
        <div class="basket-info__price" data-entity="basket-total-price">{{{PRICE_FORMATED}}}</div>
    </div>
    <table class="table table-characteristics">
        <tbody>
        <? $APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("/local/include/basket/delivery.php"), [], [
            "MODE" => "php",
            "SHOW_BORDER" => "true",
            "NAME" => "Блок доставки",
            "TEMPLATE" => ".default.php"
        ]);
        ?>
        </tbody>
    </table>
    <div data-entity="basket-checkout-button"
         class="basket-info__submit btn btn-dark btn-lg {{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"><?= Loc::getMessage('SBB_ORDER_2') ?></div>
</script>