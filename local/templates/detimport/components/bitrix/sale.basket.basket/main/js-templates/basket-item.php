<? if (! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */
?>
<script id="basket-item-template" type="text/html">
    {{^SHOW_RESTORE}}
    <div class="basket-list__item" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
        <div class="basket-item">
            <a class="basket-item__img" data-fslightbox="data-fslightbox"
               href="{{{ORIGINAL_PICTURE}}}{{^ORIGINAL_PICTURE}}<?= $templateFolder ?>/images/no_photo.png{{/ORIGINAL_PICTURE}}">
                <img src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?= $templateFolder ?>/images/no_photo.png{{/IMAGE_URL}}"></a>
            <div class="basket-item__content">
                <div class="basket-item__text">
                    <div class="basket-item__title">{{NAME}}</div>
                    <div class="item-options">
                        <?
                        if (! empty($arParams['PRODUCT_BLOCKS_ORDER'])) {
                            foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName) {
                                switch (trim((string)$blockName)) {
                                    case 'props':
                                        if (in_array('PROPS', $arParams['COLUMNS_LIST'])) {
                                            ?>
                                            {{#PROPS}}
                                            <div class="item-options__option"><span>{{{NAME}}}</span>{{{VALUE}}}</div>
                                            {{/PROPS}}
                                            <?
                                        }

                                        break;
                                    case 'sku':
                                        ?>
                                        {{#SKU_BLOCK_LIST}}
                                        {{#IS_IMAGE}}
                                        <div class="basket-item-property basket-item-property-scu-image"
                                             data-entity="basket-item-sku-block">
                                            <div class="basket-item-property-name">{{NAME}}</div>
                                            <div class="basket-item-property-value">
                                                <ul class="basket-item-scu-list">
                                                    {{#SKU_VALUES_LIST}}
                                                    <li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
																		{{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
                                                        title="{{NAME}}"
                                                        data-entity="basket-item-sku-field"
                                                        data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
                                                        data-value-id="{{VALUE_ID}}"
                                                        data-sku-name="{{NAME}}"
                                                        data-property="{{PROP_CODE}}">
																				<span class="basket-item-scu-item-inner"
                                                                                      style="background-image: url({{PICT}});"></span>
                                                    </li>
                                                    {{/SKU_VALUES_LIST}}
                                                </ul>
                                            </div>
                                        </div>
                                        {{/IS_IMAGE}}

                                        {{^IS_IMAGE}}
                                        <div class="basket-item-property basket-item-property-scu-text"
                                             data-entity="basket-item-sku-block">
                                            <div class="basket-item-property-name">{{NAME}}</div>
                                            <div class="basket-item-property-value">
                                                <ul class="basket-item-scu-list">
                                                    {{#SKU_VALUES_LIST}}
                                                    <li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
																		{{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
                                                        title="{{NAME}}"
                                                        data-entity="basket-item-sku-field"
                                                        data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
                                                        data-value-id="{{VALUE_ID}}"
                                                        data-sku-name="{{NAME}}"
                                                        data-property="{{PROP_CODE}}">
                                                        <span class="basket-item-scu-item-inner">{{NAME}}</span>
                                                    </li>
                                                    {{/SKU_VALUES_LIST}}
                                                </ul>
                                            </div>
                                        </div>
                                        {{/IS_IMAGE}}
                                        {{/SKU_BLOCK_LIST}}

                                        {{#HAS_SIMILAR_ITEMS}}
                                        <div class="basket-items-list-item-double"
                                             data-entity="basket-item-sku-notification">
                                            <div class="alert alert-info alert-dismissable text-center">
                                                {{#USE_FILTER}}
                                                <a href="javascript:void(0)"
                                                   class="basket-items-list-item-double-anchor"
                                                   data-entity="basket-item-show-similar-link">
                                                    {{/USE_FILTER}}
                                                    <?= Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P1') ?>
                                                    {{#USE_FILTER}}</a>{{/USE_FILTER}}
                                                <?= Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P2') ?>
                                                {{SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}
                                                <br>
                                                <a href="javascript:void(0)"
                                                   class="basket-items-list-item-double-anchor"
                                                   data-entity="basket-item-merge-sku-link">
                                                    <?= Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P3') ?>
                                                    {{TOTAL_SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}?
                                                </a>
                                            </div>
                                        </div>
                                        {{/HAS_SIMILAR_ITEMS}}
                                        <?
                                        break;
                                    case 'columns':
                                        ?>
                                        {{#COLUMN_LIST}}
                                        {{#IS_IMAGE}}
                                        <div class="basket-item-property-custom basket-item-property-custom-photo
														{{#HIDE_MOBILE}}d-none d-sm-block{{/HIDE_MOBILE}}"
                                             data-entity="basket-item-property">
                                            <div class="basket-item-property-custom-name">{{NAME}}</div>
                                            <div class="basket-item-property-custom-value">
                                                {{#VALUE}}
                                                <span>
																	<img class="basket-item-custom-block-photo-item"
                                                                         src="{{{IMAGE_SRC}}}"
                                                                         data-image-index="{{INDEX}}"
                                                                         data-column-property-code="{{CODE}}">
																</span>
                                                {{/VALUE}}
                                            </div>
                                        </div>
                                        {{/IS_IMAGE}}

                                        {{#IS_TEXT}}
                                        <div class="item-options__option"><span>{{{NAME}}}</span>{{{VALUE}}}</div>
                                        {{/IS_TEXT}}

                                        {{#IS_LINK}}
                                        <div class="item-options__option"><span>{{{NAME}}}</span>{{#VALUE}}{{{LINK}}}{{/VALUE}}
                                        </div>
                                        {{/IS_LINK}}
                                        {{/COLUMN_LIST}}
                                        <?
                                        break;
                                }
                            }
                        }
                        ?>
                        <!--<div class="item-options__option"><span>Цвет</span><span class="color"
                                                                                 style="background: conic-gradient(from 180deg at 50% 50%, #FF0000 0deg, #FF0000 1.93deg, #0036FF 90.35deg, #24FF00 179.7deg, #FFDE00 268.32deg, #FF0000 360deg);"></span>Мультиколор
                        </div>-->
                    </div>
                </div>
                <div class="basket-item__counterWrap">
                    <div class="basket-item__counter">
                        <div class="b-counter" data-entity="basket-item-quantity-block">
                            <span class="b-counter__minus" data-button="-"
                                  data-entity="basket-item-quantity-minus"></span>
                            <div class="b-counter__control">
                                <input
                                        type="number"
                                        value="{{{QUANTITY}}}"
                                        data-field="number"
                                        max="{{{AVAILABLE_QUANTITY}}}"
                                        {{#NOT_AVAILABLE}} disabled="disabled" {{/NOT_AVAILABLE}}
                                data-entity="basket-item-quantity-field"
                                id="basket-item-quantity-{{ID}}"
                                >
                            </div>
                            <span class="b-counter__plus" data-button="+"
                                  data-entity="basket-item-quantity-plus"></span></div>
                    </div>
                    <div class="basket-item__counterText">{{{QUANTITY}}} x<span id="basket-item-sum-price-{{ID}}">{{{SUM_PRICE_FORMATED}}}</span></div>
                </div>
                <div class="basket-item__price">
                    <div class="basket-item__priceNew">{{{PRICE_FORMATED}}}</div>
                    {{#DISCOUNT_PRICE_PERCENT}}
                    <div class="basket-item__discount">-{{DISCOUNT_PRICE_PERCENT_FORMATED}}</div>
                    <div class="basket-item__priceOld">{{{FULL_PRICE_FORMATED}}}</div>
                    {{/DISCOUNT_PRICE_PERCENT}}
                </div>
                <div class="basket-item__del" data-entity="basket-item-delete">
                    <svg>
                        <use xlink:href="#basket"></use>
                    </svg>
                </div>
            </div>
        </div>
    </div>
    {{/SHOW_RESTORE}}
</script>