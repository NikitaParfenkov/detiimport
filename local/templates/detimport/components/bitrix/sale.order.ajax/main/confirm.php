<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
{
	$APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}

?>
    <div class="container-fluid">
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumbs",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        ); ?>
        <h1 class="main-title"><?=$APPLICATION->GetTitle(false)?></h1></div>
<? if (!empty($arResult["ORDER"])):
    $order = \Bitrix\Sale\Order::load($arResult["ORDER"]["ACCOUNT_NUMBER"]);

?>
    <div class="order container-fluid content-middle">
        <div class="row g-0">
            <div class="order__content col-xl-9">
                <div class="content-inner">
                    <div class="order__success-block"><h2 class="order__success-block-title">Заказ № <?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>
                            оформлен!</h2>
                        <p class="order__success-block-desc">На Ваш email отправлено письмо с информацией о заказе. В
                            ближайшее время с Вами свяжется менеджер для подтверждения.</p><a
                                class="order__success-block-link btn btn-dark btn-lg" href="/catalog/">Продолжить покупки</a>
                        <?
                        if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
                        {
                            if (!empty($arResult["PAYMENT"]))
                            {
                                foreach ($arResult["PAYMENT"] as $payment)
                                {
                                    if ($payment["PAID"] != 'Y')
                                    {
                                        if (!empty($arResult['PAY_SYSTEM_LIST'])
                                            && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
                                        )
                                        {
                                            $arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

                                            if (empty($arPaySystem["ERROR"]))
                                            {
                                                ?>
                                                <br /><br />

                                                <table class="sale_order_full_table">
                                                    <tr>
                                                        <td class="ps_logo">
                                                            <div class="pay_name"><?=Loc::getMessage("SOA_PAY") ?></div>
                                                            <?=CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
                                                            <div class="paysystem_name"><?=$arPaySystem["NAME"] ?></div>
                                                            <br/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <? if ($arPaySystem["ACTION_FILE"] <> '' && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
                                                                <?
                                                                $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
                                                                $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
                                                                ?>
                                                                <script>
                                                                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
                                                                </script>
                                                            <?=Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber))?>
                                                            <? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
                                                            <br/>
                                                                <?=Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&pdf=1&DOWNLOAD=Y"))?>
                                                            <? endif ?>
                                                            <? else: ?>
                                                                <?=$arPaySystem["BUFFERED_OUTPUT"]?>
                                                            <? endif ?>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <?
                                            }
                                            else
                                            {
                                                ?>
                                                <span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
                                                <?
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            <span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
                                            <?
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ?>
                            <br /><strong><?=$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']?></strong>
                            <?
                        }
                        ?>
                    </div>

                </div>
            </div>
            <div class="order__side col-xl-3">
                <div class="order-info">
                    <div class="order-info__top">
                        <div class="order-info__title">Состав заказа</div>
                        <!--<div class="order-info__button btn btn-dark btn-sm rounded-pill">Показать</div>-->
                    </div>
                    <div class="order-info__body">
                        <div class="order-info__item"><span>Товаров</span><span><?=$order->getBasket()->count()?> шт.</span></div>
                        <div class="order-info__item"><span>Общий вес</span><span><?=$order->getBasket()->getWeight()?> г</span></div>
                        <div class="order-info__item"><span>Сумма заказа</span><span><?=CCurrencyLang::CurrencyFormat($order->getBasket()->getPrice(), 'RUB',true)?></span></div>
                        <?if($order->getBasket()->getBasePrice()-$order->getBasket()->getPrice() >0){?>
                        <div class="order-info__item"><span>Скидка</span><span class="text-danger">- <?=CCurrencyLang::CurrencyFormat($order->getBasket()->getBasePrice()-$order->getBasket()->getPrice(), 'RUB',true)?></span>
                        </div>
                        <?}?>
                        <div class="order-info__item"><span>Стоимость доставки</span><span><?=CCurrencyLang::CurrencyFormat($order->getDeliveryPrice(), 'RUB',true)?></span></div>
                    </div>
                    <div class="order-info__footer"><span>Итого к оплате</span>
                        <div class="price"><?=CCurrencyLang::CurrencyFormat($order->getPrice(), 'RUB',true)?></div>
                    </div>
                </div>
                <div data-move="order-submit-set"></div>
            </div>
        </div>
    </div>



<? else: ?>

	<b><?=Loc::getMessage("SOA_ERROR_ORDER")?></b>
	<br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])])?>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>

<? endif ?>