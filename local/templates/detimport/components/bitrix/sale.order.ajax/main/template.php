<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var SaleOrderAjax $component
 * @var string $templateFolder
 */

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();

$arParams['ALLOW_USER_PROFILES'] = $arParams['ALLOW_USER_PROFILES'] === 'Y' ? 'Y' : 'N';
$arParams['SKIP_USELESS_BLOCK'] = $arParams['SKIP_USELESS_BLOCK'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['SHOW_ORDER_BUTTON']))
{
	$arParams['SHOW_ORDER_BUTTON'] = 'final_step';
}

$arParams['HIDE_ORDER_DESCRIPTION'] = isset($arParams['HIDE_ORDER_DESCRIPTION']) && $arParams['HIDE_ORDER_DESCRIPTION'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_TOTAL_ORDER_BUTTON'] = $arParams['SHOW_TOTAL_ORDER_BUTTON'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] = $arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_PAY_SYSTEM_INFO_NAME'] = $arParams['SHOW_PAY_SYSTEM_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_LIST_NAMES'] = $arParams['SHOW_DELIVERY_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_INFO_NAME'] = $arParams['SHOW_DELIVERY_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_PARENT_NAMES'] = $arParams['SHOW_DELIVERY_PARENT_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_STORES_IMAGES'] = $arParams['SHOW_STORES_IMAGES'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['BASKET_POSITION']) || !in_array($arParams['BASKET_POSITION'], array('before', 'after')))
{
	$arParams['BASKET_POSITION'] = 'after';
}

$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['SHOW_BASKET_HEADERS'] = $arParams['SHOW_BASKET_HEADERS'] === 'Y' ? 'Y' : 'N';
$arParams['HIDE_DETAIL_PAGE_URL'] = isset($arParams['HIDE_DETAIL_PAGE_URL']) && $arParams['HIDE_DETAIL_PAGE_URL'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERY_FADE_EXTRA_SERVICES'] = $arParams['DELIVERY_FADE_EXTRA_SERVICES'] === 'Y' ? 'Y' : 'N';

$arParams['SHOW_COUPONS'] = isset($arParams['SHOW_COUPONS']) && $arParams['SHOW_COUPONS'] === 'N' ? 'N' : 'Y';

if ($arParams['SHOW_COUPONS'] === 'N')
{
	$arParams['SHOW_COUPONS_BASKET'] = 'N';
	$arParams['SHOW_COUPONS_DELIVERY'] = 'N';
	$arParams['SHOW_COUPONS_PAY_SYSTEM'] = 'N';
}
else
{
	$arParams['SHOW_COUPONS_BASKET'] = isset($arParams['SHOW_COUPONS_BASKET']) && $arParams['SHOW_COUPONS_BASKET'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_COUPONS_DELIVERY'] = isset($arParams['SHOW_COUPONS_DELIVERY']) && $arParams['SHOW_COUPONS_DELIVERY'] === 'N' ? 'N' : 'Y';
	$arParams['SHOW_COUPONS_PAY_SYSTEM'] = isset($arParams['SHOW_COUPONS_PAY_SYSTEM']) && $arParams['SHOW_COUPONS_PAY_SYSTEM'] === 'N' ? 'N' : 'Y';
}

$arParams['SHOW_NEAREST_PICKUP'] = $arParams['SHOW_NEAREST_PICKUP'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERIES_PER_PAGE'] = isset($arParams['DELIVERIES_PER_PAGE']) ? intval($arParams['DELIVERIES_PER_PAGE']) : 9;
$arParams['PAY_SYSTEMS_PER_PAGE'] = isset($arParams['PAY_SYSTEMS_PER_PAGE']) ? intval($arParams['PAY_SYSTEMS_PER_PAGE']) : 9;
$arParams['PICKUPS_PER_PAGE'] = isset($arParams['PICKUPS_PER_PAGE']) ? intval($arParams['PICKUPS_PER_PAGE']) : 5;
$arParams['SHOW_PICKUP_MAP'] = $arParams['SHOW_PICKUP_MAP'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_MAP_IN_PROPS'] = $arParams['SHOW_MAP_IN_PROPS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_YM_GOALS'] = $arParams['USE_YM_GOALS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$useDefaultMessages = !isset($arParams['USE_CUSTOM_MAIN_MESSAGES']) || $arParams['USE_CUSTOM_MAIN_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_BLOCK_NAME']))
{
	$arParams['MESS_AUTH_BLOCK_NAME'] = Loc::getMessage('AUTH_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REG_BLOCK_NAME']))
{
	$arParams['MESS_REG_BLOCK_NAME'] = Loc::getMessage('REG_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BASKET_BLOCK_NAME']))
{
	$arParams['MESS_BASKET_BLOCK_NAME'] = Loc::getMessage('BASKET_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_BLOCK_NAME']))
{
	$arParams['MESS_REGION_BLOCK_NAME'] = Loc::getMessage('REGION_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAYMENT_BLOCK_NAME']))
{
	$arParams['MESS_PAYMENT_BLOCK_NAME'] = Loc::getMessage('PAYMENT_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_BLOCK_NAME']))
{
	$arParams['MESS_DELIVERY_BLOCK_NAME'] = Loc::getMessage('DELIVERY_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BUYER_BLOCK_NAME']))
{
	$arParams['MESS_BUYER_BLOCK_NAME'] = Loc::getMessage('BUYER_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BACK']))
{
	$arParams['MESS_BACK'] = Loc::getMessage('BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FURTHER']))
{
	$arParams['MESS_FURTHER'] = Loc::getMessage('FURTHER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_EDIT']))
{
	$arParams['MESS_EDIT'] = Loc::getMessage('EDIT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER']))
{
	$arParams['MESS_ORDER'] = $arParams['~MESS_ORDER'] = Loc::getMessage('ORDER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PRICE']))
{
	$arParams['MESS_PRICE'] = Loc::getMessage('PRICE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERIOD']))
{
	$arParams['MESS_PERIOD'] = Loc::getMessage('PERIOD_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_BACK']))
{
	$arParams['MESS_NAV_BACK'] = Loc::getMessage('NAV_BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_FORWARD']))
{
	$arParams['MESS_NAV_FORWARD'] = Loc::getMessage('NAV_FORWARD_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ADDITIONAL_MESSAGES']) || $arParams['USE_CUSTOM_ADDITIONAL_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRICE_FREE']))
{
	$arParams['MESS_PRICE_FREE'] = Loc::getMessage('PRICE_FREE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ECONOMY']))
{
	$arParams['MESS_ECONOMY'] = Loc::getMessage('ECONOMY_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGISTRATION_REFERENCE']))
{
	$arParams['MESS_REGISTRATION_REFERENCE'] = Loc::getMessage('REGISTRATION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_1']))
{
	$arParams['MESS_AUTH_REFERENCE_1'] = Loc::getMessage('AUTH_REFERENCE_1_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_2']))
{
	$arParams['MESS_AUTH_REFERENCE_2'] = Loc::getMessage('AUTH_REFERENCE_2_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_3']))
{
	$arParams['MESS_AUTH_REFERENCE_3'] = Loc::getMessage('AUTH_REFERENCE_3_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ADDITIONAL_PROPS']))
{
	$arParams['MESS_ADDITIONAL_PROPS'] = Loc::getMessage('ADDITIONAL_PROPS_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_USE_COUPON']))
{
	$arParams['MESS_USE_COUPON'] = Loc::getMessage('USE_COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_COUPON']))
{
	$arParams['MESS_COUPON'] = Loc::getMessage('COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERSON_TYPE']))
{
	$arParams['MESS_PERSON_TYPE'] = Loc::getMessage('PERSON_TYPE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PROFILE']))
{
	$arParams['MESS_SELECT_PROFILE'] = Loc::getMessage('SELECT_PROFILE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_REFERENCE']))
{
	$arParams['MESS_REGION_REFERENCE'] = Loc::getMessage('REGION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PICKUP_LIST']))
{
	$arParams['MESS_PICKUP_LIST'] = Loc::getMessage('PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NEAREST_PICKUP_LIST']))
{
	$arParams['MESS_NEAREST_PICKUP_LIST'] = Loc::getMessage('NEAREST_PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PICKUP']))
{
	$arParams['MESS_SELECT_PICKUP'] = Loc::getMessage('SELECT_PICKUP_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_INNER_PS_BALANCE']))
{
	$arParams['MESS_INNER_PS_BALANCE'] = Loc::getMessage('INNER_PS_BALANCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER_DESC']))
{
	$arParams['MESS_ORDER_DESC'] = Loc::getMessage('ORDER_DESC_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ERROR_MESSAGES']) || $arParams['USE_CUSTOM_ERROR_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRELOAD_ORDER_TITLE']))
{
	$arParams['MESS_PRELOAD_ORDER_TITLE'] = Loc::getMessage('PRELOAD_ORDER_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SUCCESS_PRELOAD_TEXT']))
{
	$arParams['MESS_SUCCESS_PRELOAD_TEXT'] = Loc::getMessage('SUCCESS_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FAIL_PRELOAD_TEXT']))
{
	$arParams['MESS_FAIL_PRELOAD_TEXT'] = Loc::getMessage('FAIL_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TITLE']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TITLE'] = Loc::getMessage('DELIVERY_CALC_ERROR_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TEXT']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TEXT'] = Loc::getMessage('DELIVERY_CALC_ERROR_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']))
{
	$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] = Loc::getMessage('PAY_SYSTEM_PAYABLE_ERROR_DEFAULT');
}

$scheme = $request->isHttps() ? 'https' : 'http';

switch (LANGUAGE_ID)
{
	case 'ru':
		$locale = 'ru-RU'; break;
	case 'ua':
		$locale = 'ru-UA'; break;
	case 'tk':
		$locale = 'tr-TR'; break;
	default:
		$locale = 'en-US'; break;
}
$this->addExternalJs($templateFolder.'/order_ajax.js');
\Bitrix\Sale\PropertyValueCollection::initJs();
$this->addExternalJs($templateFolder.'/script.js');
?>
	<NOSCRIPT>
		<div style="color:red"><?=Loc::getMessage('SOA_NO_JS')?></div>
	</NOSCRIPT>
<?

if ($request->get('ORDER_ID') <> '')
{
	include(Main\Application::getDocumentRoot().$templateFolder.'/confirm.php');
}
elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET'])
{
	include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
}
else
{
	Main\UI\Extension::load('phone_auth');

	$hideDelivery = empty($arResult['DELIVERY']);
	?>
    <div class="container-fluid">
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumbs",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        ); ?>
        <h1 class="main-title"><?=$APPLICATION->GetTitle(false)?></h1></div>
    <form class="order container-fluid content-middle" action="<?=POST_FORM_ACTION_URI?>" method="POST" name="ORDER_FORM" id="bx-soa-order-form" enctype="multipart/form-data">
        <?
        echo bitrix_sessid_post();

        if ($arResult['PREPAY_ADIT_FIELDS'] <> '')
        {
            echo $arResult['PREPAY_ADIT_FIELDS'];
        }
        ?>
        <input type="hidden" name="<?=$arParams['ACTION_VARIABLE']?>" value="saveOrderAjax">
        <input type="hidden" name="location_type" value="code">
        <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult['BUYER_STORE']?>">
        <div id="bx-soa-order" class="row g-0" style="opacity: 0">
            <!--	MAIN BLOCK	-->
            <div class="order__content col-xl-9">
                <div class="content-inner">
                    <div id="bx-soa-main-notifications" style="display: none">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div data-type="informer" style="display:none"></div>
                    </div>
                    <!--	AUTH BLOCK	-->
                    <div id="bx-soa-auth" class="bx-soa-section bx-soa-auth" style="display:none">
                        <div class="bx-soa-section-title-container">
                            <h2 class="bx-soa-section-title col-sm-9">
                                <span class="bx-soa-section-title-count"></span><?=$arParams['MESS_AUTH_BLOCK_NAME']?>
                            </h2>
                        </div>
                        <div class="bx-soa-section-content container-fluid"></div>
                    </div>

                    <!--	DUPLICATE MOBILE ORDER SAVE BLOCK	-->
                    <div id="bx-soa-total-mobile" style="margin-bottom: 6px;"></div>
                    <div style="display: none;">
                        <div id='bx-soa-basket-hidden' class="bx-soa-section"></div>
                        <div id='bx-soa-region-hidden' class="bx-soa-section"></div>
                        <div id='bx-soa-paysystem-hidden' class="bx-soa-section"></div>
                        <div id='bx-soa-delivery-hidden' class="bx-soa-section"></div>
                        <div id='bx-soa-pickup-hidden' class="bx-soa-section"></div>
                        <div id="bx-soa-properties-hidden" class="bx-soa-section"></div>
                        <div id="bx-soa-auth-hidden" class="bx-soa-section">
                            <div class="bx-soa-section-content container-fluid reg"></div>
                        </div>
                    </div>

                    <? if ($arParams['BASKET_POSITION'] === 'before'): ?>
                        <!--	BASKET ITEMS BLOCK	-->
                        <div id="bx-soa-basket" data-visited="false" class="bx-soa-section bx-active">
                            <div class="bx-soa-section-title-container">
                                <h2 class="bx-soa-section-title col-sm-9">
                                    <span class="bx-soa-section-title-count"></span><?=$arParams['MESS_BASKET_BLOCK_NAME']?>
                                </h2>
                                <div class="col-xs-12 col-sm-3 text-right"><a href="javascript:void(0)" class="bx-soa-editstep"><?=$arParams['MESS_EDIT']?></a></div>
                            </div>
                            <div class="bx-soa-section-content container-fluid"></div>
                        </div>
                    <? endif ?>

                    <!--	REGION BLOCK	-->
                    <div id="bx-soa-region" class="bx-soa-section bx-active order__city-choose">
                            <div class="sity-choose">
                                <div class="sity-choose__title"><?=$arParams['MESS_REGION_BLOCK_NAME']?></div>
                                <div class="sity-choose__select dropdown">
                                    <div class="dropdown-toggle" id="dropdownSityMenuButton" href="#" role="button"
                                         data-toggle="dropdown" aria-expanded="false">Петропавловск-Камчатский
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-block" aria-labelledby="dropdownSityMenuButton">
                                        <form class="b-search"><input class="form-control" type="text"
                                                                      placeholder="Введите название"><span></span></form>
                                        <ul>
                                            <li><a class="dropdown-item" href="#">Москва</a></li>
                                            <li><a class="dropdown-item" href="#">Санкт-Петербург</a></li>
                                            <li><a class="dropdown-item" href="#">Иваново</a></li>
                                            <li><a class="dropdown-item" href="#">Махачкала</a></li>
                                            <li><a class="dropdown-item" href="#">Нижний Новгород</a></li>
                                            <li><a class="dropdown-item" href="#">Нижневартовск</a></li>
                                            <li><a class="dropdown-item" href="#">Петропавловск-Камчатский</a></li>
                                            <li><a class="dropdown-item" href="#">Сургут</a></li>
                                            <li><a class="dropdown-item" href="#">Челябинск</a></li>
                                            <li><a class="dropdown-item" href="#">Ярославль</a></li>
                                            <li><a class="dropdown-item" href="#">Москва</a></li>
                                            <li><a class="dropdown-item" href="#">Санкт-Петербург</a></li>
                                            <li><a class="dropdown-item" href="#">Иваново</a></li>
                                            <li><a class="dropdown-item" href="#">Махачкала</a></li>
                                            <li><a class="dropdown-item" href="#">Нижний Новгород</a></li>
                                            <li><a class="dropdown-item" href="#">Нижневартовск</a></li>
                                            <li><a class="dropdown-item" href="#">Петропавловск-Камчатский</a></li>
                                            <li><a class="dropdown-item" href="#">Сургут</a></li>
                                            <li><a class="dropdown-item" href="#">Челябинск</a></li>
                                            <li><a class="dropdown-item" href="#">Ярославль</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!--	DELIVERY BLOCK	-->
                    <div id="bx-soa-delivery" class=" order-item bx-soa-section bx-active">

                        <div class="order-item">
                            <div class="hr mb-3"></div>
                            <h2 class="mb-5">Способ доставки</h2>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="delivery-company" role="tabpanel"
                                     aria-labelledby="delivery-company-tab">
                                    <div class="order-filter-top">
                                        <div class="order-filter-top__item">
                                            <div class="order-filter-top__title">Найдено <b>26 варинтов</b></div>
                                            <div class="text-sm text-muted">Для продолжения выберите предпочтительный
                                                вариант из списка
                                            </div>
                                        </div>
                                        <div class="order-filter-top__item">
                                            <div class="form-order-switch d-flex"><span class="is-active" data-price="high">Быстрее</span>
                                                <div class="form-check form-switch"><input class="form-check-input"
                                                                                           id="orderDeliverySwitchCheck"
                                                                                           type="checkbox"><label
                                                            class="form-check-label" for="orderDeliverySwitchCheck"></label>
                                                </div>
                                                <span data-price="low">Дешевле</span></div>
                                            <div class="btn-wrap">
                                                <div class="btn btn-dark btn-sm has-icon rounded-pill">
                                                    <svg width="9" height="11" fill="#fff">
                                                        <use xlink:href="#location"></use>
                                                    </svg>
                                                    <span class="js_open-deliveries-map">Выбрать на карте</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-delivery2 b-delivery2_bordered b-delivery2_post">
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-0"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-0"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAD+1JREFUeJztXQtwVNUZvqWorVP7mGlrW6d2LKJgadX6rIjWosVqsVi14ltqERSqUkothYpUEUEhgJAQnpIQkmwgEAmvhESeIYEk5CEJSSDvzWvz3M2+d/P1/8+5d7NZApKQTO907p35yGbvOeee+3/3fN//n5sZFKfTaSZYDegCZoX+gQH9QFGZ+Z9PxICA1SBEXzAI0RkMQnQGgxCdwSBEZzAI0RkMQnQGgxCdwSBEZzAI0RkMQnQGgxCdwSBEZzAI0RkMQnQGgxCdwSBEZzAI0RmCCXHA6XDADcBDcA8StLGdDjscdD34+DcfwXsR8MHvdYt+Po/7gm3dLhe1677Zi+/j6BEkHqPL10u/Lo88HxpU7u/x9Tc+QYS4PHC5XbDmRqMjIwq2gjjY8gYY+XGwZsagsyKDJu2FiwLAN+elIPNNfxm4na3TLm4cXdTP76EfPdswYV30vZfuJTio8lp+eLld6PWovdvlRIeNx3YFCPR7uI88vC5noL3PLcfiw6WOHyDE7RY/baW7YTtFKO4TgglxC5baP1+AmqcV1E0jvDXwqJmooDX1HfV2gMlrMjHsuXjcOCP5wvhbMq6bGIsVu4tEv4h9pzGMfg9td8O0JNwwPQmFlRZq1SWC46Jg8rEo+VTvfV7fgWFvfIaK+lYRaK09H/tyanD/23sw7LXtgfbX0/ijXk1Eyhdm0cbt7l6NHMPOMwdgnkr3+1fCjD4hRLLcHiEpbdtfQu2zCszT1UEHCOZXCa8p6KzJFTdiszugPBkD5daVUH6z7sL49Voot69CekGt6PvL2XuhDF8KZWxIu/siody5Cntzq0U7IYtEjLnZCuV3G8UYPfrw55tX4JuTEuD3+wJE5J5txqPz90O5l8a7OwLKg+uD+tDnO1aJz0m5NaK9h0mhOHL8WhMnofoxea99RIip0+RdXRBojp4AS9htaF5z94DBsuwutG75PVweeeNbj5XLGxu3Acojn/YODuKjn2LIbzfganqSObiNbZ3y3P1rzm3LY1GgdhyvlFIjfAP49fxUKDcSgb/f1LP9H6KgjF6Nv244LtqVmNswcfFB8Z1yUxiUhzb07KNhfJSc+72rYcqsFPNiyXf5/OgsS0f7oXBYs9fDeqJP6CXLYlJIT1glXT5pwl7VjAcCHB6HywuPx4N20uy8onrkV1iQX947imtbUVDVgssocGPfS5NBphs/XtJwTr9iCuaJ0kYKcjQ+SvpCPupdXvEjo5iuU9qzT7G5AwmZFVDGRCLmYJloZ26xIj6tBIdJjvKrms8/N/q+uNGGiR8dgPKzZcg50yQl0u0Vsbt0U+8BmW05rRY0bxqH5k+fQkvCRLSYLg3N6x9HZ2mqlAYK1MzNOZi69BCmrs3qxvrjeGlVBp6k7xkTVxzF43zTJBvhe4tF0CJTTmPKB+mYui6rR99p9JRPWZOFr9AqeW9rvio+Pjy38ij+vOiAGLtn+2w8+m4qhv5pCzod9HSTjz655BCmhGf0Op9J/H3QGNx/JK3aoY9FoaHVKq5mNxegYf5VaHj3KjQu6DMuUIeQn1BOQXr4stDD2j8T/nIJeIkM/RlFZBN81DV3kLyQZNz6iZQegbXiaRV4eKMESRX7x7dfNKGlo1P0vZoMVhm+JKifijGE+wh3heOd+DzRNubwWSgjl0G5Z/W57dlv6Oke85/9om0SyZxy0zIpV4ExI+WYPBf2kXvV3+9Tz/1iBR4lEsViZP/dPRNVj9D9TlJj1jdcgBCWLq9fyEzLlgmUIQ0h1r9K7PcP9bOGoGnJj8j4HGLyS3eegvLTj+QN8o2RFisTokUQZmw6AavDA0ubDU0MkpEWkjc+MkvqodxGScCd4d2BCQ4Qj0fG/fFnX5A0uiWh7B33h7Tl67HfUPv5CXI1PUyrTrnmw+7zD6wT5+eZ8sR8bpyxUyYSnGAweMzrPxbX4sPl96PjaBgsq8ejJe6p/uBLKnUmxSe9xF5XRqblF1Lm6gdEPy68aAyfz4vKhjbEpZci6UQVkgkp+fW4fc4+MspwVNC5c48uqkM8aKVVEnvoDD3NVedgd0419uXX4co/bhbSxcdGkje+Rmjb1IJ6zIzJFVKYli8zpcwzjTB9XibO8zjhlFozaYUVloAPJdK1Nx88g9V0jhG+q0isdi95otPjD3huP3ERWydq5sUBteZsQvvBSFjzoqiA7Bs6jq6Fo7FUBBV+NyL2lyAyuQhrUkuwbn8pog6WY9jU7fgm1QkV9e1krp04W9eGs/VtKCZzbWm3UQHnQ0JGBVZvK8Qa6s99GZEpEhvS5DjfmLwNl1NtMzs2H+vSy6htKSK5XWp3n5jDFXievOkquh7Lc4vVjo+2Foi2fD76UDnmRGXja8/H49jpBphb7aKglKELPbxweAkdTWiOGgfL2ofId/uDBy9yL0srGtPmofqPlC+TJ5hf70N+TfVHDdU11pNbxPQ50EIWSH+FtjPuDhfp5WUvJ+AKCsJXKFCMr5KMDZ0Yh2bVP5Rn4qAMW9LdjyXqrghZK4hxImRt83i0HFNrwxL3q4jufvz5xjBMCJP6v3JPEZRrFweNEy76DXkuDl8n/xryTCwUmg+nvHe8tRO17VJCtaqeV4atcBtqX1RkUTitX7jYzcWgonHnZHFRUey9eXEwv6KgfrYCh61V3MSc2JNQriP/eGBtT2gFIJmyzPFlIG+auUv046JQueUTad5aewr8d2lFCP9hDwkuErkNpcAKZVHK07GinhG6z99zbUEPhVb53zmHCs2fftyz/1h1DCaJ56SRdd1iXDZ+E8lenZBfl9stC8JtL6PmiUsqnvuw2yv8pEv4iTUnFrbT+2CvzYa9KutL0Vl+nH5mksaS9pHsVDW2I40ymkNFdTh0qndkU6Uce7RcBOLNjSdE0Njgd2dVIq3QLPoeKWnElfQEP7v8CNUr7difV4M0Io3B5B0rtWD6pmwRyB3UL6e6lfyiFpllFrxGY3Jw88ubZDZGvrCLCrw0qj+0MXoDj5tV0YLRvFMwIUqm8AQXSZateBdJ+ipYj5OsZ/ULfdx+V02enwbWS3vdWTg7LfT5AqBaxtFSK2SP/cNN44TtKRZmyPtR5wN7wezNuWLFpOZJ040gg14QnYNFOwqxNPkUFiYW4nKSN/ag9eln8CHVHnxOw7J9JRg1OREPz0tFdlkjPoiivlQwLt9zGvfN2o3vkzx2+aQn1DS147WIY1gQfxKLKGsKHicUy/eU4BeUej8wX6bLvDHJu9ecvFxi4dyf9yEu4Se2ApPwEta++n8Q/n4ezCTJopy8I2OFmPzJs01y+f+SUtfRkRJabj8yTKbCwz6Wkva9BVCeYt/xU5Llg0LZk6gTtPT258sxatYuEWzlIZKX21Z11zGM21cKeaugFTnq7T2ydtFSbLrWpDWZ0pIpQ7I63BjPae/3P5Becv0SOQ/uo9Ul2rj8O/lfDGVcfJyzBT8w70P6AJfUzI4D/0Hty9Lg697oHWJDcbpCq+mUmPx0qnSVHyxUjVU1Vw40GfGIvyVj7Puf4wEq1ATe+Ayzt8iNyC108yJAbOCaOZO//D06G512uzRtLjI1w6bqmT/PpRqjztIhyb6DjT1c7mGRP3BWJjNqTyBf4i2Xx/61F498+DkeXJCOMbS6RMHKm5Ji7HCxuXgZeVJpbXPA2P+3hLB0eeSeja00BZ1lR2FvKoW9ofhc1JeisyabniK3eNILK5thIskyZZQjMbMC249X45ppSWKFmC3tvaSUXfDQtdh34knKuN928p8E8oQryFjfV7dI4qkiNxES+PyJWozhnVryDn5fwdfdovWlc+MWpouir7JBXs9PhPg88t1Mb8fO3BqYqGbS+t/1z924nDIv8WLN79UBISopTrdPrBSnvYMM/jRlUY1wtAehg9BcLdp7fX60WzsRQTm+icyasZ2CmphVhe88EYPbZu3uNRjibZ3fg4RjFd1BocIt5gCtGEpFuVhzub0wHZGEbKV223PMuPaFePyB6gw+mEATXVcj654ZybiGMjM+Mk7XYzh5yQ1v7hSV+EhapSOejcO/407KCdC1Y/cVi/lq/X8+JREPvp/W7R+6IESFfCGTjrrXpXTVv616CmOW3MNqT3tXTD6FzFkZtTxo+avyQynm3e+k4L3EUyIQcxkkbWHJp8QKsTuccstb25PiPiPCcPWriWLcstoWKUcsWdq2+biNlAHxq1Y/lCdIEm9Y2t2XfGhWjJRC3nYX2yG8za6B/I3lk837iSWHhFf1qE8o9d5yeMD9Y2AIcbpc0k8yV6D2BSJlclBB+Ir83VEj09anlx2GcvUHMmj3BIENk4PJ+0g/Wij3nn68CHeQlvPBu7zKT9SijQM6WtYnvAvLR1Zpg6xbbl4h6xFqNzv+ZIj3qP7Btci4DUg5KTO3obQaRAIROh+ud65dJOfCdc9o1T+oLhk6OP4xQISIl/oemXnlxRAx62Er2UN1Cr9T3gVbUaIgjZ/0bMqwTHullmuyFQqWhLeiqHa4ZYXYKxIrwNwa8J3tJ0iuSJ7YmBcnFYrzXJ9s5XNZ1Rg+PUmsJn5Z1KXWPOw9LHk89u1cP1Bfp0u+l1+7v0TKXcic4hlH5M/guY2dl4IrXkoYDP8YIEI0UkjHeaVw6B0tdVQoeQJ1i5cKyqZWqwiwKaPivGRoN/0qPfksY1WNcpMxhgJjOlCmnq/Get70eyaO0t0GcZ53hbdRwBMzq3A5ZWz/iMlRHcgb8J6tKiE3kcyNeGunfLqpwo4j6Vm2tQDxlGCc90Hh7+n85iOVuOrxaNxL8joI/jGAhKjgTUinpRQNc2V9Yp6ioG3v21I6+KnmnP7O8J7yEAqWpJs/wYh/7hH9+B2I2MZg79HOkx8MF0HtkpJV0iDH/eFCfOsFU+D7Npv9XO8hP/hwh1xZHqqu4zMq5XsOllI+z/WNNpfR6j4Yv/OnsdmbxtEKq7HY4OPNxIElY+AJYYhNtuLPxH4Xv5Bylsqn6Z5/p8j6I9Q/egO10/xhfkKe9JZf9Tz//MqjgUxMEMKaT74TrRZrfPA+VcB7uB+/R6F2h4vqVP2X72acJG3jOU3mMYIfGCaIvWl8FF5cehg5ZZbA2KF/v6VbQrRNyPZ9M9C0eCS8Lju8XV1YllyEueuyZAZ1IcQS1mYhl/zGS08hS83ciGPnPc+opcKPs7J3RU3iFVs0/P1O8ps5ERndfTdlY962ApG18Xmer/yrFJVAKgrnUtYVaL85B+9RNvZFeTcRHrdzsMgYJEKEn7jliyk/PUlUq/DS7vtB6a7dHpCf851nyL8slAf/daLd7rhgX62NNmf5F5SeXtsGiHANKhGDSIhGiscnCJHfDfqN/L/A+GNrncEgRGcwCNEZDEJ0BoMQncEgRGcwCNEZDEJ0BoMQncEgRGcwCNEZDEJ0BoMQncEgRGcwCNEZDEJ0BoMQncFq/IcuOgMTYvyXR/qB+b9OC7xM68EjggAAAABJRU5ErkJggg==">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">EMS Почта России</div>
                                                120 пунктов выдачи
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Пункты на карте
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">2-4 дня</div>
                                                <div class="b-delivery2__price">6 012 р.</div>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-1"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-1"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAEnxJREFUeJztXQdUVNcWHZrSRYoNpQx9ANHEn/oTY01+mrGLSiwxxsQSBUVQwcSKigFpShGlg6CiFEFBBFsSa6yxt4RoUKMoMAxl/3PvYwasYa0PPxMzd7l9vPvuu/Pe2fecfc4dXIqqqqp+JZSroBT4VUR/QQXlgaiBmb/8QVTgKFcRolxQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZPh/ECL9q1/y74TmEVJZWdkMVPCjrLoez210WVpVjYqKimbO++KgRT3kWR8iN6y81UiBazdP4OjFbBSdiETO4RXIOhKEopMJOHa5EGV3Shu5qcU/iphW95CKiodAnWDc3+/cwM6jAYjIH4iF6RaYk6QHzwR1TI8X4Ys4NUyKbYPPY9vDM9kZ3+VOQPHJLaiXCffKqmX/CGJajZAKHp6quDGrq2rJCxZjaUY3zE4UYdEWLXyX3QVrcizJ8NYIzLXBqmwxlm23hf8WG3yd0hGjotTwcZg6PFP6ouRUtsJjHv5NSWlOWG81DWErmQk1azdunkLQ9n/BO0mE1TlGiCoQYz1DoRViC6zo3ArhO8UIzbXFdzl2WJFli8VbHeCX4QjPTTYYGtkW/QLVsWL7VNQ1eMuL7CmtQEgFP2ftzNU8+KfoY/EWEWJ2ixFXbI3kEiukFBNKLJHKfi6xRvweK8QQQWvzrRGcQx6TZYclmXbwzbCD1yYnTE6ywRuBIkzZMACVDyteaFJanpAKgYyzVwowNVaEgGwREvfYILlYjNR9lkhjRBTLYcHP0/ZZIIUQX2SNyJ1WWJMrxirylEWZ9pibbkchzBFTSFdeWynChJj+kElrhPD18GGzX7K2tu6pCZ1UKuVjntVqamqeOmdtbe3zE8X6ej6O3d+0ySOIMMejz1RXV9ccUppPCPsg1m7euYiInN7YWNwbkTtMEJlDHpIvwoZCLSQUmSBpT0ciqDPBnNCVPKYr0vaaE7oiYY8FIndZkL5YI2C7GAu2WGF2mhhTk+wxMdERkiUi+G/+UiH0goGeomH0LI8bTVpdTZCiWiZDTW0N6dFD3L17VzAgvy5cY2BEyZoYkxlYvgAUxqX3ZnPKZNWK+4S5a3Hv/n3FWDYP6ysvL8eDBw8UEYS1iopKPnfZ7du4fv16y3qIrLqGv9m6vH4IzDbnP9+6fQSHfw7ArmOjsXX/60jZY4u4AlPE5usTSdqIydNCdJ4mQR0xO9WwfqcGwnPbIGBrW/inaWN2ki6FKl14xBhg2Foj/CfUGHYLRNh1PEux4p5Ghrzt2pUPnzleGPrJRxjQ520M6Nsb7/Z7B/16/xturhL89ttvuPV7GXq91AN9qW9gv94c/fuwcX0x7JOPER25js9VTcZnq/jSpYsY/PGHePuNVzGwYb6BBHZ8r38f2NuK4TPXm98TExMDBzsbuLk4Y+TwEZxoefMYMwqvv9oLEnsbuDhLcODAQU4OG/M/E8KYZu3UpQL4JouwdKsIp68kNS7PWma8B7h97wJ+uXUAl3/dgfM3MnDuegrOXksgvdlI4+PoGIfTVxNw8nIijl1MxOELaTh6IQvnbnyPq7dO4+cbh7Fqxyx8tOYlWnH3UV9X/wQhrLGVP2zYELTRUINIJIK+ThsYGxnAuJ0BzIzb8T5bGys+dvXqlfyc9ZvwMfowNTaCaft2dL+IXwtYvlTxKtOmfcX7TBrm4zDS5zChe9m1xMR4PvatN17j5wxTpnyhmGPkiGGKfoaMTamCFz+fjOYTIpPWcqPHFgyDf4YI6/J1kVbsCimFkzpynKrKatRUC4Uenh7On9t+/+M8dp6Kxqq8z+CxoSf0PUWI3xspuP1jnnHr1i04OjrwF7Wy7ApdbS3o6mpDX18Xuno60NDU4Nc2b9nMx/f+92swNWkHBwdbPsZAXw/abbVgQ4S5uXVHOwMd9HrZjY9lYcvUtD2/X09PmI9BT08bOrpteH9PN2c+lnkSu9fauhsM6Hj+/Dne/9nEiXycmpoI1mJL/PzzWQUZLaYhrF0tPY2Fm0wQsqMj6YQN1uWKcPD0Qn6tkghpXMVVzwRLl+tI7OqJtPLyuzh0JgVR+WMwK8EB46LNMW6jM+Zs/gRTU4fAJ2M6qqXVqJXVNmhGHYWUerzzzlv8hcXWljDU18F8H29kZmZi8+bNSE9PR3x8PAoLC/lzHTl6BEZkUBsa262bOSLCI5Cfl4/IyLWQSBxgbWWJruYd4eriiPv3y/nnJCcnYdu2TMI2DjZ3ZuZWbCGC09LSUFoq7DT4+/nx5zAw0MYo95G8z9fXW+EVPd16opRCpmAfebnQQhrCWtGJMIr55B27xDyzStpjiugdbXCDQhRfyRXPLo6ETKgWNbIa3Ll7B+cuXMT+g/uRX5KKwyeLcL/8ocJbSsuuIPd4Moau7Ytjl4/yPvkzhIQE85eVONlDS1ONDLvuuZ43z9cXajS+nYE++vbr88g191EjoKPTlnvPG6//6xEx/rPGsqs3KVwZU9jT0W6DrKwsJCUlKsjo805vihpVCjJaNO2VSmVcwJNKvoR3ihoidlkTIVZI32vDhTp1jwTlD27RSn4y3stTwxrKTspul+H0mdPYd/AADh05gpuljeJc/uAmis8kIHjHZIyP6YVXV5qi83wN5P3UWMXfu/cHnCX2MDMzRlstdXzqMZr3k04+Ubc0NZqZqTE30ooVyx8x6gfvDoAJaYkGXZs+dQrvCw4OxuDBgzCaVrzH2NEEd46xY8Zg0KAPMX68Bx/3w48/QJfItCOBf6lnD4z71INCnTGMDHUxccKnqK0T4vbjz9UihNTK2IZhHULy3sWCdG1EUIGXUMRqDmvSlLZUbzBCyvi4ppkQi5ksq7h37x7OnD2LvQf24cjx4/jjrvCwdbVSnLyajg3F7vBM6oYh4er4MFwPH6+zwgfrnGHpr491xREKAzJh1KK4bO9gBwOK6Qf273vqS8u1pqioiMd+K0sLGBro4cTJnxRznTxxAh3N2vOMiZF14sRxXL16jf+sSUKvr6cFPdIMBn2Cgb42v9aPMjPW/PwW8HMXZyeOTh3NYGkh6NnRw4f5GJYCt0phyMS8nMQuIPtNLNhsiNBcG8QWWlPBZ8V15KeLawXDVDbWK2x1ympkuHr9GvYfOIAfDx3C3TtC3VBRdRXfn/2WEgMJfFJEmJGohhkp5piW4oRJ8U4YEeOCD4kQC389LMtdojDibM+Z0CQjdOrUAa+/1kvY2MSTqbFMJuzBDKZUWB5CRgwf3uBN9Y8YlHnIK6/04n1s9bO+Nlpq0NQUUUhshHyeot17+NhXXu6Bzh1MOBmMbGPK4FxdJDRWHbO+nsHHsOdoNULuP7iPJdvehG+6MYJybRG1U4y4onYUrizwx/1feaYlX6ksl2fZyolTJ1G8t4RW3k3BcNJSHDzjhY0FnbCC0ubl2/WxPNuGCkEHeKayal2C8RudMCzKCe9HuMLcTx9Lsr9VEDKUagZDWvGGhvoY9PH7in7mifJ0Uh4ipZQMxMREYumSRQhctRI3b95UjL9y9QpMKJOytLLgRo6NjeH90THRlP4uR1hoKMLCGML4MSQkBCtXrkROthA+i/YUEmnkHRJH8iBtzKE6KJTuYXpi3rUzz+bKyn5/6mJpEULqWVpbIcOy7X0wJ80QgTkOCM+zQQhV6LuPeXB9YVkWG8vIuH3nDg7+cBDf/3gIVQ1affxiMOKJiEjyqJhCI6wlHQrKsSeS7TEvww4ziZDJiRKMiZXgkygJ+hMhnebrY01+kMKQA/v35XWABWVLYnE3XLlyBU9rDx48fGo/axfOn6eY7wYjqi10dTRJY175022SxxurU3iiQAvjnd5v8z7meObmnRUkx8Vt4P2toiGyaqEGCcl3x5fxWgjY7oDgXDFV2yIcuyDUClVVwhYBqxFK9u+leH2G9997eB5b972J8CwR6Y4BUkrEiCqwxpodYqzIssPCLfaYm2aH6cmOFK4kGBXjTBrigv5h3dHRtx2SDiYrDPEpiSszRA83IquDMYWtV+Hr48NTza+/nobAwFXkCb/x5zh29Cj1zYCX5yz4UFrs5zcPs2ZOh72NNdob6qFLp47Q0lDHT6RprBUXF2P69Knw8prFq/C5BJ8GeHt7YcoXk2ih3eZju7s6o0vnjtzwGzduUDzfyJHDodtWk9c47qOENJiR3Sqbi6wlH/gWoyNFWJTpQOGmGwIyzXDjpiBgrD5gYaG4pBjnz13jfZdKU7E+T4/qDBFS93dDfDGRUWhFdYwNVhIZbHPRJ90eM4mQyUkSKgidMTTKBf+J6I5/B0kg9rfEwfOHFC9cWLBLqLgpm+nZszsJaQeK9Wo8/dVsqNgzMtL5WCdHe0EPKKa3oYyM6UAbLQ2Yd+lEaaqWMDY9g4+9ffs21SKdG8dTYcmgxY/qwk4AFZRME3bv3gUNdREXcGtLc/zyyw3F84WHhwo7BLZWRFgHXLx44QkvaVFCfjy/E2OjdKhgsyVDGlPW9QaFMiGjKSsrI70oxqULv/DzYxeXIjybXLdQF8kltoiV7/TusEZgti3ffp+f4QCvVDt8lSLBhHhn8g4JeYcrBpB3iBd3QP8170JWKeyfsX0m1qKio3ilzV5ciwzTVkuAXHTv/nEX2RTrBXFWp2sanAgmtuqUoeloa1KN8Bb2krbJ2+rVq/h4NlY+Xg6tBqIXLFjAx/Z++y3FZ40cPoT3yXd8f7lxA6bGhorrU6cKm6TyCr1FCWFbIlUVUsxM6YXP4gzxFYWujUVjhRVwvxJ79xfj3Pkr/HzfqflctGMKTCgbs8U6SgDC8m0QnGMjfEG1zRZ+m+0wJ9VBCFUJEowl7RgS5czFvF+oG3S8NLE4a1lDOKziq0yePZ0jHQgOWo25JKbesz3hO3c2vqSQEhy8WvDk5GReF8zzncsFl4Wfb77xR9B3q5GXl6sggpHMQkpCQhwmjB+HefN86B4WAucKoFA3a9YMLFu6hO/kMoP6zPXBDNKQCePGYd++fTw8ylN81vLy8zB50kR8NnECJQKrhGyzSbbVYoRUVQofmHIgEB+sEcEjWoTCk1EUq6qRURCAs6cFMvaeXo75lMquyTVH2A47OtpgNXnEqiwbLM1ywDeZgojP3uRIZDhwIR+7UYJh0S5Ue3Qn7XCFa4AYFvOtcLn06iMuL996eF5ruhP8Z+Me3zn+s8Yyx+fN87TvXR7/TqfFCGEbfGzntfxBOcbFOmFgsAi/3TqHzd/PR8i2r/iHH728CVPjRJQ5dSECHLhOLNtmxz1iUaYt1TAk4JtYRmWPqUTG54mO+JTIGMGEPJJCVbgreoe8DE1PDfhtXcTnZHtZz96OkSpS3qZpLzs27W967VnbOo1jn39vc+Z83vUWI4RP2PBt4e6TW9A/SAexe2ZjfFRHlN65ToVjKT7foE8hzZgyJyf4Ufbkv4VIIL1hX9XO3WQPrzRHKgCd8GWSEz6LdyARJzKiSTciJRhIntE37CWY+RnBbcnLwnZ/Qzh4liH/jmhZQiorFOIaU7gMXeeJ4JX+Nk+JF2d+hJHRWvDaJMGsNHt4kvE96fg1Q4ojpiWxwo+8IoEVf85wXy+hjMoZH611wcDw7ugb+hIsv+0Mo9kmOHLpGP8M+S/evUhoYUKEWFlfK2w/eKcOpzS4ByIK5+CDMAMKQRJMojA0mYz/RaIzptD55+QNkxKcMTFOQuHJCe4U7kZEseLPmTTDBQNIwPuE9oL5N2bQm2mInKP5fG5plfQvM9r/Yuz/OyGcFCZuDb8tuibPH+IFIrwZpA+PWBeeLY3d4ESQcLDK2329E4bHOHHhHhxJ4k1e8S5lU/3DeuDVIBfozW4Li3li7D4tpKIs/r5ooapVCRE8pZGUnSe2Y0jE67BbKMJrgUYYFOmIodHOPI0dHNUdg9Y540PyhvfXdsd74W6kFT3wcqADjOcZQs9TH+7RHrh+q7TBM6peWDJalRCBlEZNkVbKEFscitHR/eG6xAyWC9vAfrEBugeYoefKTpTKdoHNIlN0mKeH9t4GRJ4tFYKjkXs074kU8kVGqxLSlBi5t9RW16HkzG6E7gqkrGoarf5BGLx2IAZFvIcx693hne5NxG3E2Ws/K4iQf0X7VxurpQz+lxPCSaGMiNUqePT3xlArrUflwyqOx6/V/EN+wbpVCJGT0rxxwlHGvvplO9vMe+roT019k4Lpz+dpCTT3mZXos1T/pE3JoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyVCu+g9dlAyMENV/eaQ8+PW/eZ5It4tVlQAAAAAASUVORK5CYII=">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">СДЭК (экспресс - курьером до двери)</div>
                                                120 пунктов выдачи
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Указать адрес
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">3-4 дня</div>
                                                <div class="b-delivery2__price">14 800 р.</div>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-2"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-2"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAEnxJREFUeJztXQdUVNcWHZrSRYoNpQx9ANHEn/oTY01+mrGLSiwxxsQSBUVQwcSKigFpShGlg6CiFEFBBFsSa6yxt4RoUKMoMAxl/3PvYwasYa0PPxMzd7l9vPvuu/Pe2fecfc4dXIqqqqp+JZSroBT4VUR/QQXlgaiBmb/8QVTgKFcRolxQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZFARomRQEaJkUBGiZPh/ECL9q1/y74TmEVJZWdkMVPCjrLoez210WVpVjYqKimbO++KgRT3kWR8iN6y81UiBazdP4OjFbBSdiETO4RXIOhKEopMJOHa5EGV3Shu5qcU/iphW95CKiodAnWDc3+/cwM6jAYjIH4iF6RaYk6QHzwR1TI8X4Ys4NUyKbYPPY9vDM9kZ3+VOQPHJLaiXCffKqmX/CGJajZAKHp6quDGrq2rJCxZjaUY3zE4UYdEWLXyX3QVrcizJ8NYIzLXBqmwxlm23hf8WG3yd0hGjotTwcZg6PFP6ouRUtsJjHv5NSWlOWG81DWErmQk1azdunkLQ9n/BO0mE1TlGiCoQYz1DoRViC6zo3ArhO8UIzbXFdzl2WJFli8VbHeCX4QjPTTYYGtkW/QLVsWL7VNQ1eMuL7CmtQEgFP2ftzNU8+KfoY/EWEWJ2ixFXbI3kEiukFBNKLJHKfi6xRvweK8QQQWvzrRGcQx6TZYclmXbwzbCD1yYnTE6ywRuBIkzZMACVDyteaFJanpAKgYyzVwowNVaEgGwREvfYILlYjNR9lkhjRBTLYcHP0/ZZIIUQX2SNyJ1WWJMrxirylEWZ9pibbkchzBFTSFdeWynChJj+kElrhPD18GGzX7K2tu6pCZ1UKuVjntVqamqeOmdtbe3zE8X6ej6O3d+0ySOIMMejz1RXV9ccUppPCPsg1m7euYiInN7YWNwbkTtMEJlDHpIvwoZCLSQUmSBpT0ciqDPBnNCVPKYr0vaaE7oiYY8FIndZkL5YI2C7GAu2WGF2mhhTk+wxMdERkiUi+G/+UiH0goGeomH0LI8bTVpdTZCiWiZDTW0N6dFD3L17VzAgvy5cY2BEyZoYkxlYvgAUxqX3ZnPKZNWK+4S5a3Hv/n3FWDYP6ysvL8eDBw8UEYS1iopKPnfZ7du4fv16y3qIrLqGv9m6vH4IzDbnP9+6fQSHfw7ArmOjsXX/60jZY4u4AlPE5usTSdqIydNCdJ4mQR0xO9WwfqcGwnPbIGBrW/inaWN2ki6FKl14xBhg2Foj/CfUGHYLRNh1PEux4p5Ghrzt2pUPnzleGPrJRxjQ520M6Nsb7/Z7B/16/xturhL89ttvuPV7GXq91AN9qW9gv94c/fuwcX0x7JOPER25js9VTcZnq/jSpYsY/PGHePuNVzGwYb6BBHZ8r38f2NuK4TPXm98TExMDBzsbuLk4Y+TwEZxoefMYMwqvv9oLEnsbuDhLcODAQU4OG/M/E8KYZu3UpQL4JouwdKsIp68kNS7PWma8B7h97wJ+uXUAl3/dgfM3MnDuegrOXksgvdlI4+PoGIfTVxNw8nIijl1MxOELaTh6IQvnbnyPq7dO4+cbh7Fqxyx8tOYlWnH3UV9X/wQhrLGVP2zYELTRUINIJIK+ThsYGxnAuJ0BzIzb8T5bGys+dvXqlfyc9ZvwMfowNTaCaft2dL+IXwtYvlTxKtOmfcX7TBrm4zDS5zChe9m1xMR4PvatN17j5wxTpnyhmGPkiGGKfoaMTamCFz+fjOYTIpPWcqPHFgyDf4YI6/J1kVbsCimFkzpynKrKatRUC4Uenh7On9t+/+M8dp6Kxqq8z+CxoSf0PUWI3xspuP1jnnHr1i04OjrwF7Wy7ApdbS3o6mpDX18Xuno60NDU4Nc2b9nMx/f+92swNWkHBwdbPsZAXw/abbVgQ4S5uXVHOwMd9HrZjY9lYcvUtD2/X09PmI9BT08bOrpteH9PN2c+lnkSu9fauhsM6Hj+/Dne/9nEiXycmpoI1mJL/PzzWQUZLaYhrF0tPY2Fm0wQsqMj6YQN1uWKcPD0Qn6tkghpXMVVzwRLl+tI7OqJtPLyuzh0JgVR+WMwK8EB46LNMW6jM+Zs/gRTU4fAJ2M6qqXVqJXVNmhGHYWUerzzzlv8hcXWljDU18F8H29kZmZi8+bNSE9PR3x8PAoLC/lzHTl6BEZkUBsa262bOSLCI5Cfl4/IyLWQSBxgbWWJruYd4eriiPv3y/nnJCcnYdu2TMI2DjZ3ZuZWbCGC09LSUFoq7DT4+/nx5zAw0MYo95G8z9fXW+EVPd16opRCpmAfebnQQhrCWtGJMIr55B27xDyzStpjiugdbXCDQhRfyRXPLo6ETKgWNbIa3Ll7B+cuXMT+g/uRX5KKwyeLcL/8ocJbSsuuIPd4Moau7Ytjl4/yPvkzhIQE85eVONlDS1ONDLvuuZ43z9cXajS+nYE++vbr88g191EjoKPTlnvPG6//6xEx/rPGsqs3KVwZU9jT0W6DrKwsJCUlKsjo805vihpVCjJaNO2VSmVcwJNKvoR3ihoidlkTIVZI32vDhTp1jwTlD27RSn4y3stTwxrKTspul+H0mdPYd/AADh05gpuljeJc/uAmis8kIHjHZIyP6YVXV5qi83wN5P3UWMXfu/cHnCX2MDMzRlstdXzqMZr3k04+Ubc0NZqZqTE30ooVyx8x6gfvDoAJaYkGXZs+dQrvCw4OxuDBgzCaVrzH2NEEd46xY8Zg0KAPMX68Bx/3w48/QJfItCOBf6lnD4z71INCnTGMDHUxccKnqK0T4vbjz9UihNTK2IZhHULy3sWCdG1EUIGXUMRqDmvSlLZUbzBCyvi4ppkQi5ksq7h37x7OnD2LvQf24cjx4/jjrvCwdbVSnLyajg3F7vBM6oYh4er4MFwPH6+zwgfrnGHpr491xREKAzJh1KK4bO9gBwOK6Qf273vqS8u1pqioiMd+K0sLGBro4cTJnxRznTxxAh3N2vOMiZF14sRxXL16jf+sSUKvr6cFPdIMBn2Cgb42v9aPMjPW/PwW8HMXZyeOTh3NYGkh6NnRw4f5GJYCt0phyMS8nMQuIPtNLNhsiNBcG8QWWlPBZ8V15KeLawXDVDbWK2x1ympkuHr9GvYfOIAfDx3C3TtC3VBRdRXfn/2WEgMJfFJEmJGohhkp5piW4oRJ8U4YEeOCD4kQC389LMtdojDibM+Z0CQjdOrUAa+/1kvY2MSTqbFMJuzBDKZUWB5CRgwf3uBN9Y8YlHnIK6/04n1s9bO+Nlpq0NQUUUhshHyeot17+NhXXu6Bzh1MOBmMbGPK4FxdJDRWHbO+nsHHsOdoNULuP7iPJdvehG+6MYJybRG1U4y4onYUrizwx/1feaYlX6ksl2fZyolTJ1G8t4RW3k3BcNJSHDzjhY0FnbCC0ubl2/WxPNuGCkEHeKayal2C8RudMCzKCe9HuMLcTx9Lsr9VEDKUagZDWvGGhvoY9PH7in7mifJ0Uh4ipZQMxMREYumSRQhctRI3b95UjL9y9QpMKJOytLLgRo6NjeH90THRlP4uR1hoKMLCGML4MSQkBCtXrkROthA+i/YUEmnkHRJH8iBtzKE6KJTuYXpi3rUzz+bKyn5/6mJpEULqWVpbIcOy7X0wJ80QgTkOCM+zQQhV6LuPeXB9YVkWG8vIuH3nDg7+cBDf/3gIVQ1affxiMOKJiEjyqJhCI6wlHQrKsSeS7TEvww4ziZDJiRKMiZXgkygJ+hMhnebrY01+kMKQA/v35XWABWVLYnE3XLlyBU9rDx48fGo/axfOn6eY7wYjqi10dTRJY175022SxxurU3iiQAvjnd5v8z7meObmnRUkx8Vt4P2toiGyaqEGCcl3x5fxWgjY7oDgXDFV2yIcuyDUClVVwhYBqxFK9u+leH2G9997eB5b972J8CwR6Y4BUkrEiCqwxpodYqzIssPCLfaYm2aH6cmOFK4kGBXjTBrigv5h3dHRtx2SDiYrDPEpiSszRA83IquDMYWtV+Hr48NTza+/nobAwFXkCb/x5zh29Cj1zYCX5yz4UFrs5zcPs2ZOh72NNdob6qFLp47Q0lDHT6RprBUXF2P69Knw8prFq/C5BJ8GeHt7YcoXk2ih3eZju7s6o0vnjtzwGzduUDzfyJHDodtWk9c47qOENJiR3Sqbi6wlH/gWoyNFWJTpQOGmGwIyzXDjpiBgrD5gYaG4pBjnz13jfZdKU7E+T4/qDBFS93dDfDGRUWhFdYwNVhIZbHPRJ90eM4mQyUkSKgidMTTKBf+J6I5/B0kg9rfEwfOHFC9cWLBLqLgpm+nZszsJaQeK9Wo8/dVsqNgzMtL5WCdHe0EPKKa3oYyM6UAbLQ2Yd+lEaaqWMDY9g4+9ffs21SKdG8dTYcmgxY/qwk4AFZRME3bv3gUNdREXcGtLc/zyyw3F84WHhwo7BLZWRFgHXLx44QkvaVFCfjy/E2OjdKhgsyVDGlPW9QaFMiGjKSsrI70oxqULv/DzYxeXIjybXLdQF8kltoiV7/TusEZgti3ffp+f4QCvVDt8lSLBhHhn8g4JeYcrBpB3iBd3QP8170JWKeyfsX0m1qKio3ilzV5ciwzTVkuAXHTv/nEX2RTrBXFWp2sanAgmtuqUoeloa1KN8Bb2krbJ2+rVq/h4NlY+Xg6tBqIXLFjAx/Z++y3FZ40cPoT3yXd8f7lxA6bGhorrU6cKm6TyCr1FCWFbIlUVUsxM6YXP4gzxFYWujUVjhRVwvxJ79xfj3Pkr/HzfqflctGMKTCgbs8U6SgDC8m0QnGMjfEG1zRZ+m+0wJ9VBCFUJEowl7RgS5czFvF+oG3S8NLE4a1lDOKziq0yePZ0jHQgOWo25JKbesz3hO3c2vqSQEhy8WvDk5GReF8zzncsFl4Wfb77xR9B3q5GXl6sggpHMQkpCQhwmjB+HefN86B4WAucKoFA3a9YMLFu6hO/kMoP6zPXBDNKQCePGYd++fTw8ylN81vLy8zB50kR8NnECJQKrhGyzSbbVYoRUVQofmHIgEB+sEcEjWoTCk1EUq6qRURCAs6cFMvaeXo75lMquyTVH2A47OtpgNXnEqiwbLM1ywDeZgojP3uRIZDhwIR+7UYJh0S5Ue3Qn7XCFa4AYFvOtcLn06iMuL996eF5ruhP8Z+Me3zn+s8Yyx+fN87TvXR7/TqfFCGEbfGzntfxBOcbFOmFgsAi/3TqHzd/PR8i2r/iHH728CVPjRJQ5dSECHLhOLNtmxz1iUaYt1TAk4JtYRmWPqUTG54mO+JTIGMGEPJJCVbgreoe8DE1PDfhtXcTnZHtZz96OkSpS3qZpLzs27W967VnbOo1jn39vc+Z83vUWI4RP2PBt4e6TW9A/SAexe2ZjfFRHlN65ToVjKT7foE8hzZgyJyf4Ufbkv4VIIL1hX9XO3WQPrzRHKgCd8GWSEz6LdyARJzKiSTciJRhIntE37CWY+RnBbcnLwnZ/Qzh4liH/jmhZQiorFOIaU7gMXeeJ4JX+Nk+JF2d+hJHRWvDaJMGsNHt4kvE96fg1Q4ojpiWxwo+8IoEVf85wXy+hjMoZH611wcDw7ugb+hIsv+0Mo9kmOHLpGP8M+S/evUhoYUKEWFlfK2w/eKcOpzS4ByIK5+CDMAMKQRJMojA0mYz/RaIzptD55+QNkxKcMTFOQuHJCe4U7kZEseLPmTTDBQNIwPuE9oL5N2bQm2mInKP5fG5plfQvM9r/Yuz/OyGcFCZuDb8tuibPH+IFIrwZpA+PWBeeLY3d4ESQcLDK2329E4bHOHHhHhxJ4k1e8S5lU/3DeuDVIBfozW4Li3li7D4tpKIs/r5ooapVCRE8pZGUnSe2Y0jE67BbKMJrgUYYFOmIodHOPI0dHNUdg9Y540PyhvfXdsd74W6kFT3wcqADjOcZQs9TH+7RHrh+q7TBM6peWDJalRCBlEZNkVbKEFscitHR/eG6xAyWC9vAfrEBugeYoefKTpTKdoHNIlN0mKeH9t4GRJ4tFYKjkXs074kU8kVGqxLSlBi5t9RW16HkzG6E7gqkrGoarf5BGLx2IAZFvIcx693hne5NxG3E2Ws/K4iQf0X7VxurpQz+lxPCSaGMiNUqePT3xlArrUflwyqOx6/V/EN+wbpVCJGT0rxxwlHGvvplO9vMe+roT019k4Lpz+dpCTT3mZXos1T/pE3JoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyaAiRMmgIkTJoCJEyVCu+g9dlAyMENV/eaQ8+PW/eZ5It4tVlQAAAAAASUVORK5CYII=">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">СДЭК (экспресс - до пункта выдачи)</div>
                                                120 пунктов выдачи
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Изменить</div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">3-4 дня</div>
                                                <div class="b-delivery2__price">14 720 р.</div>
                                            </div>
                                            <div class="b-delivery2__info"><span class="icon"></span><span>Заказ можно оплатить наличными или <br> банковской картой при получении</span>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-3"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-3"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAFcNJREFUeJztXAmUXFWZriYJWXrf9/TeVV1dVV17VXcCEiAsIgTEgIIsozOomIgoQVQUHERhJqMIjIAsyqZBDBEDmAMMiuwCQggkXdt79eq9V0vv+5ruz//eV13d8RAHnTPdTdN1zp+qVNV779b9/uX7/ntf6xSPTVU89oEPbO4WerYMyN6WASlpYa+Vnj+Yhb32gXCrfUDy0LFu84BMzwtpktdCRuOgZ9mTNO8HsOR3teMt2m9xmcjofZf1n7AW9qzqos5m/EPmaoZKpjBza+/FHGTOD2AO9n0THW9CnJ4T/LV2HnWeLXVNGkv0/2z0u9z0W7jR72Tz8k+aTiFkybAQJi/QdRexDSwoIMu2DMhit2VAFpktA7LIbBmQRWYfTkA0dmaZ/b+H0c3Z17PftczLeD7ygMRcDAADIl4LVE8L4g11CJurECcwOu1WRDzmpCYwLfhYlzwgsqcZcS4mnZDqyhDKT4O8oQ0Rlwu+/NWIGGoge61cqMkePSQvE38fmkj5cAGieiyIu62Qq8sg5qxCcJML0Z/+CJMApgb6kfj36xBy6aGmr4BkLIbqdSLutFOK+9BEyuIDhNUCmRulI5dZm0wCIkpeL9eXoT17BQInWzF8912YhvYY6xvEYPJ/06PT6PzxTrR76iFkrkHCUAul1UbRMnMNC3+tLs76svgACXtNiDpbEHM3IbTBiC4XebilAb6c1ZAcFnTccSumkkBM9PZC/d51CBqrIdophd1xB0aSn00NA8oProXfUAk5V4e4rZlSmZMANlLKMyHcqufAx+has2AtuC0uQFh08HxPuT/uYsXbAX9FBnzr86BcdwWmJya1iBjqh3rDlQg210LIWIOwqQzxuiJ6nQ5fqxHR227B4SQwo1IM8hcvhr8gHUJ1FkULRZvTzetQzNVIBICls0UTLYsLEGYJVxOE461QDOTF6WkInHMCBt56W0tHZOqPb0bQ1Ah/9mrEDFWItlohtFIBbzVQNBnRUVVJaW0V2jc6MfDgfZh5dD7zBCJtTkTWHYuAq4bqi4N3ZiPepmVA3s943aBakXDboRQVQqTI6PrRjSlP73viWQRPshEQaeisbaCa4oDYZtRAdJLXU0SF2hgwBAprr68vhj93FQLnn4qRt17n52DxFd/xDYTzj4FUU0Tf9SRb8eYUKAvcgV54QESvma+RxAkMmTxcyFiB0CYr+ve/ySdxfHga6uWXIZivQ6SsiCKIasUGPaW1Jpo8O6UdI01qE9cecQIm5nDy91UvY2UmRPIzcagyA+r130lFS/+TT0CkuhPKX8t1DCvwM9qFnWMBa8rCAcI8McoXqujZ40LcWI9Atg7Cts/NRsW+PRCoGAezVyJm1/M0w1MM1xWaR0tU+BVzI2INJQgbS9BdQ6yKjglvoCLubkHCY0OiqQH+TB3CJ27E8NtvaISgI47IuWcgkHEM1BaqO15W9A00JkqXC0eTFzZCmHiLtlGU1FbDV5iGHqKrM4/od6/CocJVUCoLobQ1QjbQxJflUtGnZ/Jqfg4SiVGrB9GTTkT0NDvCp26CeHor5FMdiJntfAVOpGO7WMRYDfCv1NE5s5D4xa2p6ySu+hp85AiSqYbSpXOha8pCAGLh0cHSRLTNDrG8CP7qTPQ/tUfz3P4ByOeeggNpNEkkAIWqfMhVRZA+3grl2isR30iawqbVjihFl1iVDfX2mzFKx41GIxjpHETHJRdAriwg8Oyk1M0IeqooYswYvOA8RCqLEdTpoG67LKVjYvfsRLBgDeI11UQSbPzcqd7YUgdE9hi5Z6ttVgRLcuBvqsPI23/mEzO0/x1KP0YE049BdOsnEGtzoOuuW9C/92l0Pf44RkMSQub1iFnqaMKMGhGg4qxe8lkMvPMe4i8+hQlJQKiuFCFPBbrs5OlOK3wl2ejZ+xgmxyYw9NxzNAYrB0U4/WMY7e3Q0uPuJ/FexWqEa0qgbHBBam3idWqea8r8AsI4P9MX3S4X/KUrIVj1GBUFrdASLT1IkSAXZ0GtzMXwnj0YiqtI/O6XGBEVxF/5H3Q+cj/a1+jQSV4cazJQ3akjtlSCyGUXofe5ZxG9516MHnwFYYosuYUKfHM1gWKAYKxB73d/AP95J2NQaEff3r0I562BmLcSfpsBQ/53+RgG/vgcAnTtQF0e1ZQ2qE6KRq8e85jC5heQKCvGG2wIl2bwSDgsy3wien65C/7CDESri0mDEGjmOgKrFoMvvoC+118mLx6mVNaLofZ3EWzIhVRZy/WHSjQ3lL4Cfffehq5nn4a88/sY9h9AqDiXALEj4K4lJ6DUWFaIYF0u+n9xLyb6eqDs2o2DRTpyDCtCpUUQ67PR9+ZLfCyDr74MqSwbkapSxLz2ObR4yQFCHttmg1CWR8KuhlJLVIuMBx9Ce44Ocn0dOnixZtthNlChXYXEjm0Y7+7BuBpFxy9+gqHXXsXhSBgHSnIxdvDPGD8o4kB9MabiMZroQQzHJUxOjEMk0ONfv5LrjneOc6LrpmuhbP8ChH+7kF9T3vavCJdRbSJmFaP0FSsuxaGadIyQA/DU+cLLOFixBkp9AUWKfWkBwvpSzEsVpgv0FQjRxA8f8vEf3vvALkhr10JsroLcxhqABqoLRFm9pL6L1mHirjvQ/cKz6PnNo5wKC9/Zge4Dh9Bx+lno+eaXEfv2doQv3IJx+qz729dAPu9ELeKoyAdPbkP/448i1JiHod88gskhev/An9FLoIYbihC3JCPWW0/XdiJWUYBgWQ56335Vi5S9T0AozUHEql9agLDeVIQ1DK2NlNuLMfiH57XIeGw3hJzVEAx5kB2soWhAwtGCLoeB8n4jAhV5GH7q9+j/yxuQv3o++n58E9RWC8TSfHRSHfJVZ1ANWIPuzScS+9pO56E0l5eH+De+gvilWyCVF0DJySQ6a4BktqDjusuRIKCkZgNUuqa00YgYFXzG9mSXHmG3HlGKvEhtHQb2v8bH2PWze3gkKc55S1v//4BIraSkyfMTxgb4LA2YHB7k3h401UMqLUHnplZ0nXYShKZG+ItXIVS2FrHyUrTrSyGdcgJ6PF4ESo+FUMi81UBKXI8Qa4+QIo95WyA4mpAoWAXJwYQjFeLCPCrilYh73ZT6iEhsoDRoq0e4fC2U3HVIVNUh2liLQHUWwnQ9JW8dIhYC7ezNiNJYxCwdxLO0SOt//XUaD5EMh3HpAMIiRKYIiVGEBNeXYHD/W/zHBk7zIlxFOXqjFfI1VyH+yG70PvkYojfvROhC0gsnWRGoKYSvaCVNJumQ/EwEi4gZFZEXlxRCpfcUKrxiTTHCteuh1BYj1JCByPpKRAhQtSgbYvE6BAnMcEEmxJJS+Kne+JyUHk84AdLFn0b0Jz/CMNHh2K8eROxLXycRSUKybA0SW8/jY+zY9TP4izL5dtMlA0iqhpDCFouy0P3b3fzHxi+9AJGKXCRMlGosNKGUZjr2/Bajw8MpFT1MdLfzpZcw9OvfIXHf7ei88XtQdnwNse2XQryE0tInz0Ziy2mQtpwI6cwzIH/qFES2nkq6hAD96hc50F0//CESP78Tfb/dg/4XX8JoLJw6/0h3B+IP3IfYuZ+A0kRUmUUSKfn4tVdpY9x5AwIE7DwyrfljWUz5BgvXIX7rLVp+/uZVlIaytAZgazO61xdByMyEj1KP/N2rMfzefhztMT1jU2T0YnpsjJsmvacx++/7P4ZefQXKlZ+nNFkBIXs15MYKvjysEOPyEf3uuvM/+fc6t2+jaMyYT7U+vzokTN4W//o2jWHddTcV5XS+TCtRSot4tfUMpbkGPnpfoOiRP/tJDOwmhjQ2mZpMtlrIWNXh95loJN8f49878hsjAwPouv8eRM4+HYFiSn/FK0ir6Hm7P6XEnVRzSMMMPvMkP0beeiYkEqvzNT/zDgjvSW39uAbIS6/Avz4LMbt2ewLrOYmt2o6SbmI/EVcDFd1CKNnZCG4+DvHbbsEIKfe5E4/pcYqEqVTYsKgYp7CZmgPEUCCEzhtuROA4Gw4VUmEvz0TEbULC5aYxNR3ZTLRQdDZWYiKe4McKx5kgksqPLqUaMtdUYxUCXgsOT09gbLgfAXsDZHNjch2ihX54S3KjWwtibAmX3o/ZbRDr8hHKSUPITuf5zhUYTLbQZ4A5PD1KIvBwKiYYIP0vPI84icGAsQ6hTGJT9SWc4iacWnudRQVb1GLXiznNWqQaSxHc5OWC8rDoh0KEQbbX8/EsSUBkZxPEygKMvaHx/MiWUyHVlOFo6w/aKiLb8EZgsYUkw3qEs9cSWypA+PNbiZXtOyIaJsZHkHj0XsQ/dSbE8iy0F2Yi2lxPdHhm/cQ0px6YUs8MEAaQXEY17nMX8XN1PfoQAnQ8W/xasr0stkTrz1+Jntu0wp64+gpIpAOUf6CbGm51QG2hqMpfC19FFgIfa8Lgvj+iY/fDRGkb0F5KIJAWiTiIGXlsf7dby4gGW/4NtzbzDnQ4ay3it/yHNrZvfQ1BGpvksc7nDsh5Tlm8mZcD4Quf1rzw8UfhL8jgt4R90HNEPY00eUZ0u92IVxTjYHYaqfl30Pv7ffCl6RDVN0A6rpmYWz2p/kYtLR3Fw7VdLi1Uu4zosunhKy9D91uaTpLOIhpdXcJ3wCzZCGHW2VDLlTZjQqPDfaTYq6GaDbyvxCaItVnUoxzLN9GRt4ttJCbNFQjkrULvM8+nUlbPfT+FkK6DQAB3kpKPuNmOlGYOyvudT+ab5kwIE8NL0LjCrS5MsHH1dSCgNyDW0oC4u4EvBS9RQIjeksgKlKZj8IUX+SSKF54BqaKIF/W/B0bKoym1yFToA2tWoOPOnyQL+wSmpjSWFduxA8JqHWKeJmJuLYg7rH+zI34OIB5242kzX0dvL1kLlQQnB3bXLoTy16RAm0eHne+UZaFJsiGYfyx6vn01//F9t99CuXoVB0MrsNaj7sXlE0gpxp+jQ/ir2+YwLeJYkxP8NWNI6jmb0V60EtJGB1+3P9p42Ool27mYsHvgL9Rh4LFf83NEL/8c/BUrESEiEXU6MI+bHuYZEDfbjdgMtbYckc1ezpAOR1T4G0v4zg/GaNgWnrkTIM85li1uiTmknD++gWJCI7lTJNVZZLD/j05rnGu8vwdh1l5n3dsNliPuHTkyAi2U0iiKmqoQtjaR2qdzTgzhoNsA2VjLb91e4mvqrBVvJI80QyAm1PvmC9yjlXPPglpSQAXUxPdHzYJBE8h1A2mEjfReeT7abVUYT3RrYExNEBizinx6imJlSgNl4N03iYWlI15bjUgbuz2BtfftvCswAwrTPRECJFSwAsr2y/hxnXt+iWBWBk9jM/ezL+Gi3qIVWKK/wVyarB1f4pPQ++ADeDd/Bd+0Fj2C91tIuTdqtyA01iBYkI7hN1/jETE1NdtO6dv3FAZe1ZZg2WdjvLlCk7v3EfjzdJAtLOI8FJ1MmTcn93Vp+8JijhbSG+nof/UPPGLFSz+LRH4ulFZ90jnmraDPPyCqa1agKcb1CNqNlPupJFP+l1qaENNX8xtyZgDhLQsCKUKUVMjUIfHIr7Sm4vRkql50Xv99tOesgm/9WvTddSf/fBJa45E9Om68GYEMHUWak0cE8/pUGqSIFGtKIJ5yAv/uWDQGha3ru6r56iXTKEu8qCe3jrIdiy4b3ssn2vrQ3ZoQu/ZqiFmruaqevYfQghjVFGGdDp3fu0ZLU8moGHzzLwicuZFvclNaKBU2GRDKPgbKhedgTIxoyj353cSl50PMTKP05Ob0Wk7ueGfqP5SVhvg9P9Mi6qbr4afIFTbMgGaa7zoy/0Wd6YgIB8UGtSIfvi3HaxOsCnivLg8xs1Er/uTJHR47TdBKhC4894gWSffOmyGVFkIsZKmFlDurS27WNTYgnEfsSr8efff/PPX9Scpg4nFOCKVpiLa1aEu3bG+wvhLBFjNF6BhVomn46TwRfS1pGAsHLtyq/S2TJQvIDCjsmRVstvNQII/sf/GPfOLUyy+GnLOOJsuGyEby+pJ0HNpow2SyXPQfPAT5rNM12muqRbfTleo1pXZDel0I6YsRyViNrosvwqisLUiNxWQIdSUQakvR6XYi3EZahqKj6wc3aNHx8M8pLa6YE6Gzd3PN4/ws7N5emYSbXJQL6TNbNLra3k6en44wiTmxpgzh8kKMDPZpE3b7bQjVlBI7W0UphRVnB78TV/LqUzd1arTaQFFD0ULp0Z93LMRmA3oe2cXPMby/HcHsFZDYLheitSFjDSYHu/hnwiY71OoCih5Xso4tyJwsLCD8TyNRjvYVrsP4yy/ziQlfcQHEY3QQK8sxSRoFY+OIfOYc8ubVSDSWQmlzoNPm5vXIv9GYrAlzz9vMd9SzDnHMQzWjuhaH8tIgb/8Xfv6hPz0NX1EGhFU6dO28UaPIjz6EUOZqiG3mo7ZZPhKAMOtgzb3SbAS2btaiJJzAe8So1Fu/j7E/vQjJVAkx/xiaLD1RYtIRSe/tcOo5KExpa38DS1P6CqPIrJXOagQV724nW8+vI1p9LKTj3Rg68AbUL18OoagAkxPDvKcmtbI7e0vp/C1J7fGR2v0+azw10MTFHC4cyk1D/77fabXi6efRedVX+K1pB9foECrMQKiIPLg8G3JVMWL1ZVD0NVCb6hEzNSBOFmMLXeYGSkV1/DOloQxSTS5CldmQSL+E89dgP7sdoYwE6LXfQu8br/Br9RBVFtMpIjeYOLhRZ8tRe18fAUCSarmNaGZFEV97mDw8lRJ4Q0oAPc8/jZ67/xud37oGwmWXQP7UFggnnwCRvF0ggRmyNCDUXI+AiYxeB9j9hsSo1E3HQzj/E/BdfhG6iFIP08T3vfQcJpL7iXk0JhLEyEogGSrp2i5eg6K8ffMRjRCuMzgFrYfipXRUWQHlonPR86v70UcF/n97TI0PY7SnEyOdnRjq6MBITxemJob+7jET09Pof+cN9N93N8SzN0OoLiR628b/3GDUaSd1vmAFfTEAYua9LdavitPrmNcGoaqQL536GisQ2WBH+LwzELnyS+j4r5vQ9fDD6P/DPvTufxlDsoSx7j4cHh4hEEZppkdxeGgIo53dGBBF9P3lNQw9sxfdD9yL+M0/hLT9C1DOPgVBYmC+ukKECtZBqC9GotXG6wZjazGHgy9YfWRvaZtrc7k/Z1/WRiiGKkjVlMqK0/n6hI+EoL+iEO11RQgaKyCbqiHZDAg6jRCZorY2QbTUw9dUjkPV+WivyEKwYC2E/LUIl2YhUlMM1ViNqK2JX0N1W+boDMtC1o7FB8jRbKb/lbpJ1G5B3EavLY2ImmuhNlMRp0KukrENDYqZ/m9tQNROoNqtyT9EsOATvXQA+VuT+c6RmQmeW3xNR66dpGjwwo95SQOyxG0ZkEVmy4AsMlsGZJHZMiCLzJYBWWS2DMgis2VAFpktA7LIjAOy0INYtjmmU1xmVdGiZNkW2giLvwIzfgpLOMiEYQAAAABJRU5ErkJggg==">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">ЖелДорЭкспедиция (до терминала)</div>
                                                4 терминала
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Терминалы на
                                                    карте
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">24-32 дня</div>
                                                <div class="b-delivery2__price">3 389 р.</div>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-4"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-4"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAACtNJREFUeJztXQlUVNcZfm4sggiCCIqiGBPTNNZqTNqYJnps3VIVd9SA4kIURI1bajWJ0ZMY49o2a5Nq7VGPMalpTGLbEJeoRdwXoKCiso3CgICjwzLCfP3/++Y9ZwYcQIg+e9475/Mxb/573//ud//1zTlKZWVlBoJJhyZgkOgf6NAOJBszD1wRHQImnRBtQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0RgeHkLKy8sFKipkyJ8fvF4PhJDS0lLcvl2BH+OorLSI+YGqRp23tNSs6m613q5FtrT6M9vG1naw/ma+F28U+mxxAl8rJ5TVcI97JqSiogI3TDeRnJKJ5OQspKY2HMkpWTiXnIniEpPY8bm5RiQmpmHf/mT8Y/cxbNt+CB9/uhcbNu7BW29/iWVv7MSy13di7it/w4JFW8XnN978AmvWfY0PPkrA1m2H8M23J3FgfwqMBUW0gSxiEVj3oqIbOHv2ClJSHHU4ffoScui+LFPTc/NRVHwDx49fcNI/G6lpOfgu4SyuXSuAhcaXms24eSoNpoQkmL47Ip8T6HzwBMw511B+l3vcEyF8MBmt/WPg1Xoa2raPbTB82syAW8toHEk6L+bfuu0gJGkkmrpHQWr+Ev09kRBBGEsYYzuPc4LyXYQs3yKSziMwfOQ6MWdVlWwZCd+fg9R0EvwCX3bQge+3ZOkOIcMu0N4j8FFYaELYI/PQ3CNKHRNICO4UT2OH4dnnl+P6dROsJGvONMDg2w85UnfkSr1UZEsdUBi7SlhLWe2k1IMQ2s0tiYwWXlPgSw/WUHj6TEUTtygk2gg5cSIDzT2nwCcgBkEdZ4uHbh9K6DwHHWoBy7E8j/PwmUZ6Tsely1dVt8I7mUnz9pvuoAMv6uIl2x0IsXdTfZ9/k2TC4R88Sx0TIIgMR3BILIzGEnksE3IpGwa//jA0fwZX/QeoyJa6oCBmZeMTwqbKO4SVqm2B6oIAekhvvxlIOnpB9eM9ei2Bm3d0g+ZlAiVpPLbvOKwu7N59yWjmORntQuIcZNnC2PUphLAOlZWyVcXGb6bvhyMkbK4qH/rIK5CaTUIr32nIyjaqenOcMF/JxdX2g2Hw7Iur7QaqYIspjF99/wgJ6SLvUH7YmhBI4O9ZzhUhfAwZtoYWYbCTy4oQC+wIvjaBXNRLaN32ZXSihbLXh93Y722uqD6EcCzj45NN+4QVBHaIo/lkQjp1nQdP2ix8b0VnNcZqiRBWlK83JT/bzGOyeHAV/Jmu8+KzXG2ErF6zG6GhsfjNkHcwYvQGTIh8H9HTP0Yc7db5C7cK8N9RUz7EyLEbMWDQKnTtvkD4dceFjsC4CX+sNyF8sD5MOluBonPHsHloTa5Ukkbhi11H7SzKjpCLWbT4T5KL6kTnJ1RkSh4oGP+7+0eIOy36wKHv4NDh8zjwQ6p4eAUHDqSI6+Gj15NclEtC+D4WS7ltYayo67Hry6MiwHMMUebmxKDvCyvUoH53QkZh3fpv1Lny84sRwLGF3FKozerYQgI7xAr3tW6DLGuxWBxS5fKqKpgN+TCOWQzj4NkwjlqkIq9/FIr/sB0VJFNmlzj8aITwYkRN/cjlosXP2yLcSG0x5E69UEmcVAoXcv5CLlLo3mmUZipIT8/BhQsG5OWVYMfORLSgZIADujK3JyUf3X+6GNeLSlwS4u4ZiUiytr37UrDnn2fQ/9dvCasJ7XaHDDkmDadn+KuYi0muVrfwZ9r9bCk1odHrENeETMBYm3sQub/dOItFLianzvizcCO1WYjzPQsLS9Dl0fmUfU0WbskZQRyfyDI62gVeRitKqXmH5+QYXRLCsYezOsktUliFu/dUh3jE80pNJiB81AZ1c9VURApw14Cfn5ICcVbAny11qkG0T0hBQTE60S6VmkdSDTGzGnwDKA0NmqUGXgW+FOiDQmYjLT3XJSF3EpLZDhamwK3lFPTqs1Qlw2y+VdeFvVdonBCykO5PLKJidHq9Ul/WsR1lSP8l9+aKkNrgR8S2az8LiUfkWuk+9M60TUi+sRgeXtGigBNpbjWMF8Uku6cQu7m5C9CJMqPMrHyXhHTuNh9eVCzKVf94tGk3Sw3mct0xD240zoOK4YzL11y7rP93QqooKykuNiEq+kMM+e27Io11RsSk9zDoxXepepZrHWVuL98Z6NZ9IWVNRS4JadM2RmRjM+M2YVrMJ0TITLQiguxTdA7wTNijjy+gJMNiI8VcfZ24+2ytogDuDKscRx62oG42m3Evx/ETF0VmZB8D2Gqe/uXrsNgKPVdpL9c+yrF5y35RELalNNc+LjFBfH1Y+FqbpLW6pfArAeAusD58FsJj09Ky8dVXSUhKOo+z564gI8MAg6EA16/fgMl0CyUlJphu3ISRXFlmZh7JZFOhuAnu7pMcdWoyUW0wuiZkLF5b/rltjeX2f5xomYxASNgdK+HgHxwqp79LX/tM1VchpdxqhTk3D/mD45HXdyryB8apuNZ7NIpXb6H01ypIe2gI4SNayA0glzMd/uTPO5AbCqXd2e3xhaKueIwCPJ+7PraAXNQc0exkhDp1AXih51DtUxdClEpdeU4+nnpmmVyPOKXA/kEzhaXs+CxRyCkNSVGpZ2QjV/o5VephVKH3UJEpeaNg4tL7V6k3JiHcCuH5AoJj4Uf+nGsErimYIE/b4vOZAzF/x3PIfbK5DruZi9D33v93vQhRmot85OQWwLtVNFp4RDnEE/67pc9UYYGnzlxWidRUL6vBhByTCTEaixBGRWBL3/qluM7gmqKp+2QcPJRab0KE+ymXn3fPv07T96PFxlAKT87kOnPHl4rFIIozxcU35bEPgpAztCPkl0EjnVLPfniu3wohw68zayLkxeFrSe4Fp3GjhU/+4aC8cLt3H6fPQ8WLJG7BMzFsHdzNbUPFHxPIG6IdBW9eWLYi/yD5PYWPf4yQb2FLkQMp67pllyTwm0RJGmR7oWWvw3OYNXuTAyHcMFT0Xr5yF8kMtKXFduOacTf6eSo+Y5GTUyi/oLqYRS6L3VRXOvdGbpOnxAuqTMkLBeNebVxCOAW9THn4yDEbMIyCJVuEgv79V5LifxcyFqcWAfei+PrqtV+T3AqHceFj1mMoEZWSmi1kvt1zEn2eXopf0Xw9qTru9pOFosvKi9+aKnK2Jg9yF9w45N3OL6G43mgbHCvcFMv3Jt//s56vYs06pQlYIeY+cfISBgxehVHjNjrpvhyfbt4nZJzfGFptQT5+7hYMpLH24zjlHj/xT3iyx2L8hcZbuXGYXwhj+ALk9YmU0XMi8p+NRt4vIlDyweeN21ysy48c7lYw1fZDAeVHDkpjsarKgpu3zMKFcSaVnp6Nk6cyRKw5/J80JHx/VrigxCPpOHb8onhPzpuFMy/71Fm5N0Pp+tZHd1nvSpfjxH1KZVnuWylpLmdTpWa6RvdVrzVmHaL4Vo4RzuAFdbYMZ/D3LFfTeHvfLZPS0F+fVAnLVN5VyHOX13h/vlZRSyrqSnf7Z+dfqYgCEI41SBlZWh3S3foTouO+QCdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGnRCNQSdEY9AJ0Rh0QjQGk/4fumgMTIj+Xx5pB4b/AVyh2cnE4nFJAAAAAElFTkSuQmCC">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">ПЭК (до терминала)</div>
                                                1 терминал
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Терминалы на
                                                    карте
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">31 день</div>
                                                <div class="b-delivery2__price">2 053 р.</div>
                                            </div>
                                            <div class="b-delivery2__info"><span class="icon"></span><span>Доставка по предоплате</span>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-5"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-5"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAFdxJREFUeJztXQd4VFXavizqLuJvY1WURV0RsT0iK4q/bXEFV5fyICoqFgQhIuDSkU5CDR1C7xBaqAGEkA6BEAi9hxBCQgkpk8xM5k7NZOb9v3POnZk7k5kElF8Gntzn+cjMuad933u+ds4ZlSwWSx6RoZqCgvIk+gfVFDwkKcjc8olUEydDNSDBRdWABBlVAxJkFKyAmBXylJnN/svvMAo+QMxm83XQrZ9nNSB+6A7UluACxFvgJjeZTGYi13d/wNz6ud9xgKgFzIQfmAJpy63n4Y4BxBuMqgAJrCl3ADDBCIha+EY3GY3GAODcUaAELyBGo4mDoCYBzg2YsdsPnOAFhINiMMBUDpic4J+9wblObbm9QAleQJiGmKw2lF5Kh1ySAxMIFJMFRlm+YVBuIzMW3ICYCQRd7DAU9pVguJDIQRHa4gHFGMCMmf2Ac6v5vK0BMTEf4ihH6ZUjKCJACr+WoI3+CXK5nQNlNNsgV6EtvhpjqdKE3XLQghcQIyMmcPIhRqYp8aNQ8IMEzeC7UHpqo9AWBoxsUGmKH8dfSYhc+S5ANSA+JsvItcCgL4F233TyI7mQDXnQLm2Nwu8JmPnNyXRdFaDYnB5gmLb40ZhqQH63hgihMu3QRn2Hgm4S9AmjIBdmoPRcHEpm/C+K+knQxQz1aAsDRAGmoqaoBB4gTPZNOKsB8Qp72WqXYbI7IdvKoAmvj8J25Et6kS/Z2AVyZiz0qQtRPOt9FEe8Cf2xKA6eyeEdIleV9Vfpd4INkN9if38PeeUhfMUb+eo3aHKhT5oM/cau0Iz4Mwp7SCiZ2wTahGHQbvoZxWs7Q79/Loy6azDanVWGx791a+b/gyzm6wTkjwbDFxABiixWPIEiE+lPboThYjoMOXuhjx9BoDSDdslH0K79krSFANo5DMYyi8eMWR3kUxRwXMGCzw6AP9BuBe9mizkwIOZbBIh/UEhgFhsJUw9t0hiUhL8I/aovKNraAsPl45DPx0OXNgv6ffPJz0yANn4M9OlLIV9IITBLKHwWpoxn/T5kNFs92qTyO7eId/+AVCWgP56EszZZRA5SmpWI4kl/R2EXCUU9yXTNeAna7b0hH4xE6aEVKE2dQ85+JHTJE2DITCRhl8Ik62EyFMNYqoFRmwfDlRMwZKVA1hVRv9bf5XNujCrKs1JAKoARFICIqIutZFnxKcyE6TMTULLua2jGPsqBKehMFCKhmBLJ4pH3QDPuYXr3AIpDa6NoRE0UjbybPt+L4on1Ubz0Y5TumQq5lECy2f8gMAKTX0BuTDOEihuVz5XVq/x9oDY+gKhtPjMxirZwX6HXUtSVCH3aPOh2DIRuQ2doV38OXWRblCz7CCVLWtL3T3mmryMQ9GdjYNDmi6jMWiaiuQpj+iaaARJP94Lx0PXx59dvBQakMoHISpxvcR+xVnSSrlzArDrPqJgfqL77ae/po6Iz5u24IKncLoBxA6Qis0IWn/fM2XuAEGOYK+GD86qeg5onIiYHi48s1GCK/r359uO3PIBUph0uYRgo2nGUMbb8PE4bf++qB9i8XstUxvaeZKPsvz1/HNS2lNdhfwG7zzsDSokY8+qHlYv6DnddoLyScahGuSXgOwYM69NqMfl976AoToxp8JmjNx+VPw63vCoAUikYCtpicMakE1E7z6LTmHi0C92JjmE7MWLhPmonw263inpOB7KvFqPH+Hj8OGoH5m89ybiAnRhx2i0IX30Q3ccnoOvU3eg0KRndpyZj2JwUnMvVcAZlWYBx/Hw+uoclICQ0Fst3nkJ5eRnKqA+2socvSUPIgG2ISs7gc7LbbViy4zRC+m3ByrjTSD97Bd3HJaJnxF70mrWHaC+nn2akoE/EHpzMzMPolTSPKcnoOi0F3aYkoeekRGzed57maoXNJgCL35+NkIlJaD8yFt+GxWHuhsNcmFaLhf/VEd+hC/big77RaD00Bj9NSkL2lQL+bsKawzSHOPRkY9OYvSLEHH4kfpZuO0ZzLuN7bZVqiD9TZTDIJGMmDCs+HLQV0mOjIT0wAlL9cZAeHwupxgD8I2Q9CaycC4w9HwzfAanWUEj3D0edT5fB6RQao9GWQnprNrWjPp6bCKnBeEjPTKC+xuChtktQUKznY7HntZ6bIN07BNJ9Q9Ho+yguePacv1wEqfFUejcMUvM5FKKLlfxUx9WQpF7E9G58R4tBkrrSXMMgvTod0itTID08ksr6QHpzJrbtOQ+pyQwan3ioN0bM52maS6NwJB7O4f11nbGL3tMc/4d4fSwUUt0wPs8uk3bx91c1etRvtwTSU+Nx38eLIL04mfrvh7+2X4IzuSWQGlJbqS+kZ6nf12kOzxKf9w7l83q3x0beh6UqQNSguOykzSrM1CejY7lwavxrHtYmnUVRsQGfhcWiCQn8S9KYUkVNkw5nk7DDIf1zDtWdi1qtl+LKNS1/l3IshwtEajwN65LO8LJvSFuk56n+a9NxIiuPl0UlnxOM/HMupLfnoO5XK6FX+t+0JwNS0xn4U6ulVCecNPAEL6/dlr43mIBFMacRvfcCBkxL5vO76/35fC6fjIrBgOm7sTk1Gytiz0J6YQr+RPM7m1WAw5nXUKPFfC70tYkZiEvPEQD+YxpGRaYh5kAu3vh5k1gIb0RAqzOiz9w9kGoPxZMd10CvN2LPyVy06b4ezX7egh1pmQhddgCDF6bhlW7rUfO1mXj082UYtCAVvcOTsPNgNspJqwM6db+AKOaKrcwDZy7xybEVMyv6GBeAgdA9knEVe45fwpWCYjjLbdzUNOwcBemJseg6ZRca0spmgk48lMvbzNl8nAtCem82Fvx6HLuOX0XTHsToPYPwXu/NwrpSP3XaL+P1es3chQc/W8G16jCZIfaMjjwgVnxzAqvxdBLURuw5RvNjgn99BmL2X3Bb6pVxGVyT72k5n3yDwV0eQmBJ9Ubj8Q6RnNdVSRlCk5pOQ+yhi3ghZB2kR8Pw1dhEd5utqVlCC96Ygct5GvQmM8u154P5eK7TanSlRbkmKYObK7WPerv3FgJ3FD6nxeF5yDTTAjP+NkCAUcv3Q3pyLO7+eDF9K0NGdgGkR2gFfTAPtd6ag3f+G83rLY45SWZqBFoM2c6/v9R1PTcJC5RV3GPabhImrbIW80ilSSteIgbfjED3yYlkroTAQlelc9XuRKuZPY9/HslX/obkTP69w2gyR8+Eo1GnNajz+XK++ht8t5oDdG8bMntFJXA52/Zk96W6oXi+y1oqsvG5s3fv9CEhNZvJ20rPkSl5aCQ3R1M2HsMx8l1SE1p8r01D/MEctwinbzwuTOy7s1Gu+Jhu4xNFP0+MFiaKLEMoyUo8Dly4okFNkhkza6OWp/NSEWH5zUVcgPh36q7QjT0/MkHWH48HyGayh4W+Q2btwyMdVnDVHr/qEC+/7z9kUxtNQtNem9Bj+i7cz2wsmZVfFqTx9+/02Uy2ehzaDo/BkEX7SOtoVTaZjug0saqvFJSgJjNTL0/B2+QoQ6buQm0SMgNv4loxxovdNtDqDUVk7GlMXXeQ989NGwmwWY8NPOJjgneWW9GIgGBmyLM67dCU6PDgpzTvlyfTPDei/4L96E9+J3p3Bq+xjmlLIxLu6xE4k53vBuS1n2jch4fjvX7RuFygRWLKBRyi94WFOkTuPI3HvljJ/RDzZa4FsXVvJrcQDODNKed4mVHZWwuYGFYVYfFVyzSEOT8yNf3n78XJiyWIPXoJdZjdrjUEp7PysTT+jHCM5GOkJmRS6pETfJ8JagY6jksAi9LqfhnJHeTkqCO839qtFnFHWveLSP69+3QyJc+ME30wTSKzIrVcwIEbMG8vCbkMNT5eyAOC1JNXeBAhNYtADTIbzJx8PzFZEZ8TWZeKcPdHVPfv4zF6xUG3YFOO55IJnMXHjdp1Dr5PNAlOakRa03Qm2oyMwbb9ufh2coLQaLISmZc0CI2khSANIG2JoDmIoOKJz4i3vwxF79kp7r7Grz4knPm/F1IUWcS1psJdAPXWCfsncP4hAGHhZiGZk6e+pBXwBIHyyCiu3tL9FOW8MAmdp6dg025ioj4Jr/YIDJy/D6cvFuPEBQ03KyzyePqrSHK057ipkWoNdvuhVkO3Q7prEJmuqWjHIjMWedHqH0eh8WkC/URmAep1WMn7aEqrfwWtRG63icmj50UA0GoEtfvLYBp7GMatOewWxtzoE1xAUp1RtDoz3eXMsUrSQAJqAvafvqoyI0YeSdrLytBi4FbBI/kRqQ6Zs/uI1yZTMXbdUV5/x4GLxPtkEcWRpknPi3m//d/NkM0WHuazp3HIBh753UcLlzlxFlJXspGpBiTA/hHLzGUDX3HFpQaEETOdJ8RTZBSPgXNSkXIkhw+8jeL3Lr9sx7DFaXBQPuJ6NpPK9qXobDKZm5i0LPSjeHzArBRaZQXEuBXpZ66gL+UAgyiX6UwxfHf6HBbJbK0nsdyQfBZ9KHafu/U4tqedR78JCVQnjcJxHcpsNpy/VIj+UxPRn3zOmYuFXKCMdqZnoy/NcxBp9NV8DWxlNj7muqTT6DMmFsNJ60v0pVRu9c63HDbiwYJptLq7hifyEHow5RDHzgvwRGJqw87DF9BzcjLaUz72BeVby349oZiqcgpnZZRZjZhCfPel6G7pTsrFnGUw+9nEDACIv8MhA9+25rc8Kii257H6fHcq9f3nuZ6nspzdrryvrI4twHt/YzuVur58lCtlri0WUxVjmpX3lfHmOrvx7ceg1yvbTqqFb74OQNx7MWUUmmWnQrdlMHRxoYLiw1CaIEhPn3VxYbxMvFMoTrzj75W/enrv+s7buNrFe+qKej7v47zr+LZ1E+tfPYbSl1c7VXv1O/VcXfN3j6vwyj5r1XMj0irtuDzU7d2ktCP5lZ7YTPIsq7B77rVLYgmkIS5ACNXSI6ugCauP4hmvoGRm44oUwehVQTNVFKG8q9DGVce3j8aB20f41A00B3/9evWv+ux3bgH4injVu12lbV+t8F0zqh4BOFJsbKrkrAZDuRPmAcTr4MTo8R3lDqdb5diWnezwmCjXX+a+DA7xF6p3slPcNFQ/rEz2qetUlTvUZQ5BavPATv+MTrhvMZp82gGeMl+zYlL6dM9FqcPGdm1Fsv5lZc4WFY+szOjDi6s/9Tam1afMxT83wbIcUDvcZ+puDTF5myzemO/MliG/2EgRjQaXCqnMakd+iQmFesFublEpcvJL+RErE+OlAhlF/J2Dog1yjuz2odmK81e0Yq+LbagpG3OFOguOZpXgYr6Bn9oJGBxIP3UNOtnMx5ItwrlnXCpBBvXhcFLYaClzExuDnSSWOxy4mKeD2SaglmlMNrbZZsfZnGKcytFAZ7TweVrtxIPWJI6FidiZCKtbqDPTeFbIJivVpWjLYedzupiv5/2xuZlpTmdzWX8l0LP+6LuDxH/xmh7FjG+nk/d7VWOAXhZwXtaUIvdaSQU/XTkgvj5EFi5pxPIDeO7Tpfj6l1+xNiGD7+Sezc7Dm5T49Z6wC41/iMKp7EJelyVuzX5Yi4afLkc4hZ9OmtyFqxqeJLbuswmPfbYcl69pOJPztx7D058s4/3O3nyMl2VQfP8OhY2vfLUKPWcmwxWx/EyR2ctfrkJLCkXPXCwg4dndfs5qFm66KyWQ9T9chFc6rUby0Rwa24GrlGQ26rwabXtvRvdpu6A36HndNoN/RQeK2pjwZKPYHRgwPxVNf9zI29ktMp79di32Z17D8EWpaE4JqtNZjitFOr4V1K5PNN81FjvgQM8Zu9Cy5yY898lShC3bx8uiKLlsErIO4xan4skOy3Eiq4BEZ78RDfGOsMr5uYcT9Ug4f6NMeTQBk19SDFcG+lA7SggfGIY9xLzrafT9Gr7fVaP5fCzYdpKXLaRQkGXSL1M/7/TeAq1BaNZb9PnBf83BcBIEC1vZWGeyi/BI6yVo2GYxhi1Jg2t3tw1l9X99exb+M2Q7Zcg6vtflTlqddhTrdKjVejGidl/g+1mDKIFkD9uOZ5uBnUbGUrJ3gYNroDYsq996QOwMWM1i4T3Ybgla/SK2e+C0oME3a9G8z1Y80m4Z9p0S4e6cLcepP0o+KYz9dV8W1xgn5RUscX2s5SI0oAx9TeJZAs/OL088RG2lJ8KwJTVbmC+fA7pKAfENeZkwdpOwWSL3EiHcd84e2Ck2t5PZ6Umro+a7c/iWQMqxS3ywQxmXKUGait4Tk3AkqwAuK9rw29V4pv1ynnVPXieStsv5WkoEp+BpAroT1TeYZM5cn1m70bLXRp4RD5grhLouOQOtBm7D3/69AHcT45Sq8xxBACLAnbnpKGXpU9B24BY8Stp5uUBoQuOQ9XycDwn8hINCKJE7z/Adhw6DtmFbahbn8/DZq5AahCMuPdu9uJr328a3zudtO+4ue6lLFN8NaKHqb1b0UdT8aCHq0kL680eLBZ4OYaqkd+fhK75DwcAwuE9arw8Qs4+GlNuRQBP8mlbn10O2UWJHJojsbE6eBh+T6Th05ipGLk3H7C3soMWOWErYuoQnuCdvs5goabMgZGoy0jPzMGPTMb5dwgA9cz4f34yOwzfDtmMoJZLlJGDmX8LXHETrblEYS6aP7YKyrPkgCatFr2iEjI0jf3ONtKNMOTYVJ5lsnht2Z6AjgfZJ/62U2Yvs3V5mRNiKfehE2f83I2NwgpI6VncHrexOQ2PwfucoLCLtLSeekg9lI2RyAvkTSubIMrB609cfQfcJbP9LHKyVWU0IXbYf3w/fiY6kIacv5HFeFlCyOn3DYZzIyUfHsfGUaBq4Pyoq0aPr5CScyM6nOdv55YxKEsLrceoGSizVx5xOflBlULJ2Txxh4+cgDqWuzSKyXRYUsPriKXO3Ye19j0Y9uwGq0ewW5fRRnXY6+VGwizG2k8Dn41WnXHW86lT1ZxbHqg513TJqX6qYZ/XcPcfHbK4Vj4hd/RkU3hxwxY2yUVyBlY2uFNTGeQtwqeH6NMR94cAkDu/NJs/hPT/QV0Az+xzW+7skwOuoLjx4+jW6/Zb6soDrBM2rDzMrr3jL0HXWL5ir2M6kXCwwq7aBTEp/jNR8etrKXm1d340K32b3ZQfPbriLR9dfNy/q75WBob5KGnDrxD1Bz3eXYI0m1+10ZUCThzy3KlQ3LIzGCg7N/y+fXH2a3CbJ6B4v8BUbow/5v6qj/jWv66aLyWvunt+YmFRAKfPxO676pw/+rvYEuu4T8EK3z25vAIar6eaR74WSCrffzZWch5j87AB7de5DN23iVYzrWmU3LIybPb8bnEMVYHjffvcCRXHy3h1UcQHZ5PO3ijaBfmrm9ZvzgD+s8ZnrzaLrGK/qcSvy5a/udf0+5KYxVk0BqYrfzfsCogLFotD1DhIEzAY73ZRfUN1W5P6vzgXBXH4b3WGA3P5UDUiQUTUgQUbVgAQZVQMSZFQNSJBRNSBBRtWABBlVAxJkZKj+H7oEGTFAqv+XR8FDef8HezrX50QJ91EAAAAASUVORK5CYII=">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">Гарантпост</div>
                                                10 пунктов выдачи
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Указать адрес
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">3-4 дня</div>
                                                <div class="b-delivery2__price">8 767 р.</div>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-6"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-6"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAEelJREFUeJztXQlYFFe2rpjJxDzH0ScBX9yjJhrUiWNUEiMaY5KZiRlXHAVccAEVGXcRt6gJ7nFfEtCMJuC+RMSggkaDREQFRVRUQGTfRBZZeqE5c865Vd0NDXmYGbTx6/t9x6q+devWrfOf/RafUmlpaRpSoYXMgtIk/AcsZD4kycg884VYiKnQAoh5kQUQMyMLIGZGFkDMjCyAmBlZADEzsgBiZmQBxMzIAoiZkQUQMyMLIGZGFkDMjGoOSElJCR+rbVqdfozxPeXaMtOx5QCleK2ExpSW8DjjViJfU+ag8UqrvAYeKz+Xx+p+fU1mTjUHRKVS8QsmZKdB5INYuJEaD9FIUSn34E56ogmDtGqNvu9OxgMIuvkLHIkKgdB7kVAqX9Oo1KDCuXUIWvLDDLgYFwXZBXmC8SWCwRqNBnIL8yH8fgzczkjitVx6cAsiEm9B6qNsBkBVqtKDWl6mg6tJsXD81kVeq1arfdZMrh1AqOU/LoRGqx1Bmv0uSDO6gzStK0gze4K08CPo4zsT0vMfgg4ZAmVCTEOQwb2/mYZjeoA0toWgWXbQZtUIOBwbLtDSlbMG/Gn7VJDGtYY3t0wSjMV+BVTnQ2tB+scfYcg+bygvL4e3aaxLS2i5YRzoNKiB5QYV+vBf80BytIJWa5wgKS+L5zEDRv93ASmRASGpk6b9GaR59tB200To6jsDrNc4g+Rpj8xsCX12ztUzZtqpHSBNaAuSkw3YrHUGh4MrwZ6YNbc3SJM7MKCh92/w2HuZyQyq5InXJnWAtaGHK5ioBmscmcneZ/dw3/bwQJBc24E0tTP4Xw3Rjx2y90uQBv8emn45GNILHsqa9hyaLLUMyPQfvwZpVFPovM29gh0f4L8Umf86NEbJp7bk/H4E4lXWjO3hx/UaQ21PZAi8QMx3ewP6IUDU/CKDhcYt/pjvqf/FIHhcUszXzsdfR420A2n+h3A77T73FaPfaUya6tICHPYv574pgdtAGvIytPAeCjnFhXrzaQZM/u8DQr6Dmt3O2Syp68OOVgDkU/8lbFJmn9wBqblZqEXdQHLvBN9FntGPUaOdV5r9bi+QxreBt2Tz5HZsE0hTbKHhqpHwAkq3NLkjuAdu52vrQg+CNKYZtKGxtAzZOnkF7wJpYjtos9kVxh/bzGOsPv8rZBQqPqjOgfEEgGBLfZgJv1v6GUjT34GNF45AXFYShCbGwMhDa1g7pNm9IK+oENyPb0HtsEZzNlNwTmeQVAXYPt8vQk2zgUF7lvHv1zaNZ+YevREKc4J8+Vxa+DE8LnoMkwOQ2SObgMuRdTxWi4EAtYTsVJCW/A3H9Wff02DRJ3A/L1sG45kztvYAUfzHjssnhQNHKWQN8PgTSzkx638X/xVOJVzncU3RX5ApmR7kw781qBk8lxwFxWWlQr35/ZjpoQk3QIeOm/0K+iE1MvJxSRH6qD4MfK8dc1gDJHdb2HvtrN4MadQClPE/bGTT9wdc062sFAGGvGYzYG7tAKKRJXLs0fVsSqwwerH/1hOjIg/4dPd82HD+AOQWFwnmI6OaoNkhx9wdI6HKrQCZ3WWzG0gjGoMt3s9AXzrB0VerzRPR1wh75BawhU2etOBDdvQvLx8KGY+y9QxXoq8xpJ041zg0eaw92F+HwagZIGy3kdptncR22vvcPhNGUytTa/k4nCIdMmEo8cP3LYfI1LtwNzMJfK6cBKvlDiA5/AFs0PFm5Iso6IPdC9AvNQHHA6v0c+U9zod6n6M58kJNmdUTevjI5g+Dg1JZY7PzH0E97yGsab4IKmtPaZ30G08ICLbzdyPZTpOJOhd3TTAH/QFJpDKOs3KM+bMKHkH7dWNEWIqmS/J4G81PNzZt0sS28HcEIEuOgigZ5BxlRCMMAIK5T60Szn9N2BEWALrP9dhGcU02f9R8LwdhINGIzeh99CeK9tRxqgEgmHQduHYOemIuMHT/CihC81QuS6oJlQhmlaDpWoTOuee2KdBi/Vhou94FxmAechYTRX3DeS9jRm2H0dNf0PSl5+UwoAJYHfuWwRhO228cz5k5JYSkASp5Td+gVtgtHwZTMBSnRtm+GTC09gEhMi6DUGb8ayFl5boUMRaMylk6jVYfdalUhlAYiKElRnOUGRWwyk2lv1xjmFSNfq4O5hxPDogJY6pqOoOZUZhXjEkdS3LlwmIZmABm3DQyY4sr1cJMGs7LwQaZTTrqqh+qrEmpsSm+zrgptTDlnSu8j7I29VMB/dcBUeMiHqOJupmRBPEYVsZjyEqUINPNjAeQkpsJBZgvXEu5BzH4u6i4mE0aLV4nAxKPOUwYZtw3KdNGs5T+KAdi0x/wnHFId/G+7LyH+pcniS9FptzLTuOyinhuCp4nQRL2Ka0UTRitkfKfmPREozWmMFGuQv2JOek8TmnxWckQiuuhgmWuXMxU8iWlpedmw4X4aAjD0DwFfZ3SahmU6gFhCcf25U/+Iv9Y9DHmC++LHIFqV554dGkFczE7V2k00HjlcA53+3+/WAilXC7hMorrGyDN6Q0RSbdZWl9bNxbn6YtRVF8xp9cH0OTLQTDu0GrOQ6hdTLwJL2JyKGHCJ55nz8/+/ZJP4f1tU+FK8h09kzxObMfAoTuGyf3FfMp4rrG1Bq9TO3kcJY6DKaqb8Y5YEx6tcd0TDq+BZBQsakUInPvRDWIe1/Yg/bMrNPhiIAz7biFEotCVldWqr6oeEJ1so/vtWiASwKUDoBU6ZxvMQ6yRrFY7QhNk2OHon3mc+/GtII3+PwSvO0SnJXDfitBDIrqa0glO3r3CfeEJMaJuNbcXNFzhAM3WjcaM+1PRN7Y5OGDYTG3NzwdFcXLhR/xMm7VOUB/zEa40Y7TW0HsYaDXC/DTFwEGa/BbUQ8bxGnEs3dN45T/Aetnf4TZqYGZhLieQVCVuhmP6IIMpv5EcGuKc76E2JkOxuhQ6bnTlel19DKn7fr8IrL8aLd5rfGsIlt/hmQBCjdS5AcX6mKSdvRdpYldpX6NMtvWPCvPgxWWfcbg748TXsD/mAkjONszoCyl39fcsPL0LGd8M2m9y1WthdlE+9KVKMEpkUwSI2sdUH3O2BieMzkirysiMaVSwJOR7lFoMpTHbp3WmY8LIIKHG3c9KNVljuVb4wEF7vwBp6Csw/MBK/TUytZ6Y8EalxvHvBcG7OZTusGlChTkWoQYevvULn6uUysPTBEQpl+y7fp5L3L9DRl9CE5L9OI9zhwQiiv1pK0JxsNhmUMkdVVxaMkCUVlDyLqKac5ODg547ZrHWLDj9rwov7YDmShr+R3DG7JvseX3vwSz1l/G5xm37pUAGygbDcGp+kSGciDZDLUnFdZEmJKPPistJ05shMp+vYDJKlYau29zhYPQ59IHJJuC97zsTTVwraLp2FOyMCEI/d7/CdbGT+Qx8iFKZ9UBJ56SOyuUzegiajvZ39GvQco0z21NidHGxKJXTfS+T+ZllBy+iz4lBJ6xIFbUszM5fomouguUTHgg5mJHHP0wDr5DvGHhiWBIyNRadv+TRBU3QINiPQnE+MQaCE67DwrP+Yj9mbAvwjzpjkHxaI5VZyNfR+sj8OVmD/c7ZemZOoLoX+QSqIlDxEsf3whzoZJxB89eHHUbz+pZIaikRRh/0Fia5vpGn+TpFWrUIRvWAKNLczWcGm4fmuKgePtOh89Yp0AUlrD2asS/O+vEYJRmk5h6wVWgGAlJ/2UDIzs/lfgWwwzdCWWukz/8inDoRFSpHNGawt105yeM8T/oamEzOd9o7bKIo62+AfV9dDOBxRRheN5R3MNtscIHu30zjNXbGhPQN1AifiB9BpzPExL7hx2EgJqFWVG8j4KheNrMHhGG0pbQfbv4CIzEhbU2BBwFL7+P2Juy5JgTg2QCCLRnDy3oUWaEJuqaYnUrNOEx0PrCK90Q6bXaDZphd00sMlDePyrRaGbAtLIGvrHQA262ToPlXo8B2w3iYgmbqpqxNFBa32zKZNaTjFjc0YaugyaoRyJxu8Ao67USjEPRk7GUGqf6Sv+k30UxaOWXxmPlXyqc2UWmGIkbUmqM3LgjBUakrjAm5cxlewqCAzJjXqW+5T/W0AVH8h0/ECVHaRmbEoNNLz8/hmhER5RC36cMGnXjJkftX8NZpS4xqdGjGvH/aw5EQhbN3M2RGI0Pao/RSxDJffjllf8S4UR7BJX40YT/LpZbAmDCW5hdQmv3lMjy1VRRSY2TWYbs75zGpGNaKNabx+nIw0MgoyIW+O2ZDW9SkMzif8n7rUVuo1maFkV5ecSF4BvlAczSRVObPKyoQgMRdE1qCQkkfdVB76hqiZMh9d3nxJhLvc5OZoTifFkdHRytYHLwLE+YyeHerO0gDJGi+fBjkFBXKTE0RcfzQ+mC/Q9hxeiHaFSRp+1kpUJYJUDgRlBm14qe9DC6BouyhkJS3QE2iKIm2cgvlXKXVV2NEAZP2U2baGfwHmUEMXQNuhsEZjA7pXHKyEs9fij7Oq5/ow/e6jc49MTeD/RdpOPuPxZ+I/IeKm5hbbYgI5OdpVM/Ah5AkU1Y7DPOB7psmwnsYFZFN7rRN2Gbadu2MUcgtjO0PosNtuOAj6Ld9KiTnPxI8lnODWagFXTDO7/61B6gxcaQqcQf0Pf0x/i/EcLPy1yAEChUNF2FYbItBxLQTYgtXyfaPoW3vhs99HSX9F8xl6OuWd9HH2eGz7XxnQCfyHbL/6Ijr7oK5xm3586SAW+EwZPdCzFHGYlAxCGxQ650wrI6WN7UoCruUFAuj8J3f3DgBXsZ1NkIB+2znHAiOF8LDey3PqnTCTlhTxQdusrQy49VayCSnbWR1SkqUWpDBFlMRkMopDLZyL4JW3TarxqiGRcVHkw/08HmFxUVyna3qtRk/R2O0FsqZaFvY+N1M5keNpTHGtTTN0yteVu1DeIFk38kR0rEylem4AEeVWyUXqTAHFfHkazQHM5oYI5uo6hZEMT7PqZOrwqWGgp+4v5xJCT+rXaPcx4DKczCDFeHRycVCo5yCznlr2GiM8X7PUyLLt71mRhZAzIyeH0CoGqBWaWRSmzlp0JxWGbE9H4BU6eDNvCnrfu4A4UBAp4PYzAjYFuEBfteXgr9MfteX4XGZ/HsZX/OTzyuSMt7o3mvGc5jO51fpfj+T51WaD2lPtDfsjloAq8NGQUz6xao+BK/7gJD6U/v26jz4xF+Cccdeh3EBr/PRRT6no4tyPNZGP8blmIHGGVOA6W8Xo/nGBbQxnBuN0z+Pr7fRP0v0t4NRR5tDfz8JNoVPgmJVAWg1JptddR8QMlW0h//5TwPA/ce3YV5IP0HB/cAz5AMm0SfOPak/2NA3j865T/7NYz6Qzyv3ibkN10U/3xusPMPwfKXPK6Q/zD79Pow8bA37b8jfn5VVuR38HACCLTbzMkw+0RlfuncFZgpmGTFPD5Dhd5X9wYY+Txk0ZR7PKu6rML7SWK8zH+rBOPDrYDw/gJy+txvG/NBSlnSDlCrMNGFksCkI86phvMk45d4KGmh0X7Bhfq8QAqMXgvEqgrH6/wOj7gOiVZdxRr06zBmGHPgfmBTYCSadILLFc9uK50iT6Tf32xqNqeJofF2ZM9Bw3TWwI3gEdYP5aIqqA3QegjGTwDiCZip6ZU3AqPuA0AvmFT6E1RecYdm5gbDqgiOSk6BQ+Uh9+nOjPuOxJvc4GV13rHBtZehI2HzJDR2zG8xChnsG9zVoXAXNEGZqX/SKmoJR9wGhWtXj4kJ26lp1OWhU2loldalGX7jcHbWQNYYBMQGjN2rGq0+iGc8HIPytL2a85cSnWqYyo83E76IWwdhjrSv6m2DhwGexzyDNeGIw6j4gTw14o79/33F1LjvpucF99IFDZTO1N3r5bwHDAkiNwaCyDJqqryOmg8OhRjDz1HswB83SLASAQJgdbA/TT9mB4xGb36oZFkBqSmVaHWbVhbDxoisM3PcSJp9d4J9Bf4apP3YFD6Kgrtw3+ocWmPT9R2BYAKkJ0eeqDwuyIPxBINzBBPReVhTczbpagaiOdifzitis++1gWACpCYndStM/YaiqUV70H271WgAxM7IAYmZkAcTMyAKImZEFEDMjCyBmRoWW/9DFzIgAsfyXR+ZDaf8GuQGfXaY/oT8AAAAASUVORK5CYII=">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">PONY EXPRESS</div>
                                                4 пункта выдачи
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Пункты на карте
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">2-3 дня</div>
                                                <div class="b-delivery2__price">23 907 р.</div>
                                            </div>
                                        </div>
                                        <div class="b-delivery2__item b-delivery2__item_order">
                                            <div class="b-delivery2__control"><input
                                                        class="form-check-input js_delivery_radio" id="delivery-type-check-7"
                                                        type="radio" name="delivery-type-check" autocomplete="off"><label
                                                        class="b-delivery2__title"
                                                        for="delivery-type-check-7"><span></span></label></div>
                                            <div class="b-delivery2__img"><img
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABECAYAAAB3TpBiAAAAAXNSR0IB2cksfwAAELlJREFUeJztXXlUVee1p6/Na9LV9qVp8tpmaDM370WbNkmjbRKfMdYMviTGTJqpSTPHFIc44YjghDhPDCKIIioOgIDKIAgIikCYZ5AZkXm6cLncy+5v73PP5TIZ3x9v9TTrfFk7l/vd73zD/u3ht7+TteLQ09NTC+nQRRNS64B/kS7aEQcrMv/0jegi0qEDoi3RAdGY6IBoTHRANCY6IBoTHRCNiQ6IxkQHRGOiA6Ix0QHRmOiAaEx0QDQmOiAaEx0QjYkOiMZEB0RjogOiMdEB0ZjogGhMdEA0JqMD0t3dQ729RiLqp0Gt30zGf/7Gv6syHJDu7m7qM/Xa9N/c2k45pdWUXlhORZV11IsxajNax2vgIN8VGQwIK1dtcRnFNG9XCD2/wJue+Hw7PfbJFvrzrB00fbkfrT0QRSVV9VaPseig/L8AghDFym1p76A5u0LpN2+tEXnk4y009m+b6eEPNsrfY/62iW5/zZXG4rtXyHk1jl0XKL1Go3gfixF/DxuDvl6rGI3Dn1X6hz9ntHtu6Dhjz2i/jbD+KHP1qnMN+T7Sc8PnNF5zf6MC0i9gdNIzczzo1mnONMFxFz34rhuAcKepTr70hrM/TcJv97+9HsBspvFf7qCfv7ySVuyNEEjMZvOIoHBfv7mPRmvdPUq+UrzTbJ+spE+e7zMNfkb6DWTp66XRm2WQx4/czLY1vm3s0N9NyK/qPnit4fvrxpgeulbrMxmH6mwAkL6+Purp7aV5O4PpZ/+7gu5/xw1/h1JSzmXxmp4eA9U0tFBwQja9vGQv3fv2Ovqf2R70s5dWkvdJxVNGshAT5mxqaaOQhExa6XuaZm8/To47gsn90FmqqGuQ59haLBYzZRZX0BLvCFrgEUZJWSXSZwHQlXWN5LovEiE0lMLOZwv4vF8gRcHxGeTiH0lrA86SW2AsrQ88S4s9wyg5qxTPWyg5t4yW7ImgdQeV39dhHIfcg9EpZO4zKuv3KIqrqKuXfbnuj5axLC742/1QLLy3i85czKXZO0Jp4+EYamltk72x4TS3ttIq7G+h50lKQ67lfpNJMaLk7GJ5fs0BnlPZwwrowTM0UcnXrPeRAFEsAEhbTDRtqR/tOZl0TXSdvMMQ0tbSffCYW19ZSRlQpmI5vYNQ5xaJg9ww2YlunLKEbvyLE93y0gr61XQXGvvhRkrILLbNeSg6nf5t4iJyeGoebTt2ztZ/AUplI3H401woJMTW7344lm5G/x2vr6abnltKP8QaP5ziRA4PfkxuAbEyZt7uk+QwYb54No+9CXu47RVn+hXC7gyXAGqFsSE8UHH1VXr8s62yr5+/7Ez/Pnmx7Nfh6a8xtxPO1EnLfE6Rwx9ny1xsJGqbC5BuwHgeu2RPuK3fJyIZe3PFnK704xeWydp3Yq8c8u+esZZKsCa3kXOIDZR+hAIjNTS3UlNbBzW0tA+Sq82wDEs/dXZ10WLvcJoLL3p79UHaE36BujgPAVS2eA5DaihsbW+nqYt96OWlvrTU5zTNcD1Aj3+6lX77njsIwzbM2SobCzufQ799d4Pkrv1nUmwHyyqppscw/s4319Dq/VHSd7W5RZ595KMt9OxcL/pi0zH6encoPPAEvem8nyJT8mXcSr8zdO/MdfQMvPld7PPTjUE0Zb4nPTlrN/3H1BWw3hgZt/5gtAAxwXE3iMs+AB9MX205Th9tOEyO8GoTwsu6gBgBcvI8L6q60iTPRV3Kp9tfX0MT5+ym+7DOGngUt/K6q/QwDO4R5N9Jcz3ovTVYe9NR0cMfP9tGT/19FxVbidGogKjJiZtveDJQXEcvLto7TJ5f6EPTV+ynLzYfo082HsXncZq+bB/9Hrkl+lL+0EWQQ0xU39RM9jXNcljbQwCED7jvdMogQO6C4oPivrGNLa2ppz+A5SmAKAcuKK+jcV9sp7veWE3zAcTwpuQtDiV3YszjUEJ57VUrwJX0KPZ6DxT40fpD0ufiH0W/fnMtAN5MqQVl1jn6B+15EUIh73fSXE/RUz9qMgbioffdpY8B4ZDI7WxaIT2AsM8RZIHHwP5WwUD+c9oqetrRg0q/zUN6rAnWIjHaBEvzJ4dnFsLNXHEoO7H7ft/MtUj+60V+/OIyuKMLZZZU2Rbi+ZjpyCZTC2g+qLRX6AVahDzx2CdbxXVVYhCenCteM/7L7WLNTsgni7wi6LNNQUIy7saBVUBKq67S07AyVuCMVQeg+Ghy8joFDwklFxy6s7PTBshdVkCKqq5IX0peGT366RYJJ7PgBdzWwvoZcKb4hRU10meGDixMKCwK2VjsGS7hZ8p8b/nuHZogxIatnfvusQOkqKqOHnzPDXrZQDNdAyg4MRteW0gfrj8iZ3wannhdgAyELqJOQyeSUZRYz3pJmAOy8cg5SXiOiOlcr/wdoWIJwpEjQpjfqQsCKOcTSexWr3t3dQA53PeBxHT2vpfA3u5/Z71YHrdTyXmyWXbre2FZ33tmAX0fwjnqOdRDTDSGAjJ5nif9Adb+vYkL6AfPIo6Pmy25or6p1QYIh6yJczzp9ZX+AvREhK+H/roRhGQFRSTnyDhXnJE9k8NJRnH5IEXxObg5eYXTL191pY83HIHx5CCMbkce3CThbuaq/Qhdq22AsGedAOGYgD0yyLfhDDf+ZTE9CiOchBD7fwbEbKWqJ89nks+pFAqMSR8kfggz8ZklYAvJ9A6S4yyELz7cXvT7RFwE4wJzA9MQcK0WFobfF0L5u0OShBQ8hsPfDQ9z9j1l5yEb6HEc4FP3o8JO3ALjaKHHSSh/p4Co5hAVkDGI0+8gPnudTKadx+PJPTAGlptIXV0DHsKgswX/BgpncvBrfI5H/tkTlmwLJdcLyD3w6FdRHL+wyIdufskZ3h1OHR0dErJuh8cNAELCuP40ayf9DrXbCwt8aA68lxkq58PrBqTbbgNbguLoR88vFQt7AIdShesTZg3vQxGfbzoilsGJ9ZMNQbDU+XQ0Nk2eV4uy7p5u6jJ0kn3bfCRWqDOHDV8QAgHMmkM45gfHZ9rGVl9tghcgh7wxkEOKKuqQmHfQbYjHrta+kRoDwiSBFeMHw4pJK0aeK6C6hmbFjq2G53KdgHAI4vD58AebkLTdhaVxLTLui22DPITPPQVezWF2HIfBy0oYZFrPLOu6AeEKmduhmDShfpyUxqAq/++/utuErXjsh5vFVV9DGOBc8J5rIL240Au1xhllQziEyu+9QaHHf7mNth49R0fiMmQMx/Tf8y0AXF5NtjaWBSv0P3PRptSM4io7lqUov6K2AcrbjiJ1K72yxI+8YO37wMyY7W06HEfRqYVWRUdKznsUlLYezMy+qRX09QLCOYTD5rNfeyN0ucCQlNIgD8rmayV7QLJLqyS/PQDj/cjtsG3NhZiDafp1AcLhxWK1mEv5ZRQU+w3FphficAU2YZrHoarySgMtALsZA2CYXThuCxbFsbWoVTvH0S5DNz0BZTo8OZ9uQQL8xTRn8Qr2ulvx3Z7eHsV6N4OK3vTcEoS1RFt/Sl65hJkbUMMs8lR4Phd9768JpJ9OXS7J8ycgFD99cTmo7HJy+N3nNNPlgKIAWDUbFpOH1MLyoQqwATIfYZHn59rqUkHpiIB8Dtr6E6xxC+jxDFBrtWWBxDwAUvP9ZxfBA5Twuzs4UaILn2XN/kjb2M/ASrkmewiGXVRxZXRAmA31yRVFP0JAFDjyFbpWi0zJpf96X2FEzGLiMoqsvwxcNpqsOSQ8KRuAnaAXkKwnOHpKUn0LSTD0fJY8IVU3WmxaAU1CkubccOxchhRsvJ/8y7WIvb40Hla483giupX+WoQdrjumL/eXIo/ZDMuE2bsQEuPk+d0nEiVcvYIaKE/CRv8wQHi+HSfi5TpIGVc9aJxCcS3InZfoya94Lj+6XKPopx//lEJXr63wpydAw+XWAvNtQFXOYyd/7YXit0QZif51AdE0Duu8BWZYeaVRxo4ISK9RsYKAyIuodpeI6wbFZpChe/B9TAUq1OWgqfe+7UZPfbVLOPViD6V65ktD+8N2dQ2557H0odjspEYUmMNav8U6tl/qFuH/4Pki1qMrH+q9mP39kcXuu0Xo6kiN9zfwm32NYbHrs9sSooUyXrnBGLZldZ+D5lH3abbrN+N4vO8+GqoPjigjAiJ3MqjMx4IR3IHkOR5ocyiahvi8FEWcC9zuq60nJJHeg7zC3PsXr66i12EZvVaXHnq5yB5SXtuICr+V2ju7BMya+iYyo+rlg3JfEarVc98Ugaa20BVQ1abWDtlLNcY1NLdRbWML5V6uo5qrzVRcWStrJGeXUlFlPRkQDk1QMt+1tXca5JVAW0cX1u6WMJdXVkNlNQ1yu9DU2i6Xkcz+ChEqMooqxWNS8y9L+M0pqUGC7qQy5LNshN5EWDXfVvA5KrGXxpY2asfcufDWThgah+IO7D+vtJqyiqux/zaZu6G5XdYuBung3xvwHAPXhj3WNbZBx+2o8hvl2oRvPkyDjXgAEGOvET/2UXxGibgZK5vLfk5MXBNw0cM1Ar8XeRCJ7Q4UgLO2Hade08hgqPdYMUiubkj+PhEpkvAu5JTScfBzZ9/TdPpCDuVAaUfPpSNEpdO+iGQ6DDJxIiGbdgYnkG/YeQpNyJILw1DQ74u5JRSC77HpBRQQlUZ7MWd0Wj55hJynQ9GpQjKYOrd3dlBIYgYdwVwcsvjCcBdCEudDv1OXMHc8gKqjeBgC1wp7sW4qisXtxxJo67E45LJ01ETZFIW5D5xJIx8QBs51zM645uIrjxjk0+0gKVEpeXi2HFHjtKzBVP30xRzstQwU/Cxtw3xxWHdf5CXyRX22LyKJdoUk0DJQ/dT8StHRqJeLKsMyGAy0OegcTQXXHgN6d79cAawTyseh7IO1gRSWlGPzvNHehXB85Eu4ICjmDDZ+DAc9AxDiM4sBfBGFJmbRQWw0DDmGlZKQUSjXJLHpRXTkbCodj0uHUlMlV2QWVaDSzycfxGi+VuHiM72wAkBlQ6nfYGwaFJEn44zwkFLE+AvwpLTcy/DAAgqMvih5LymHyUoavKxEPK68roGCAXJSZiEdw3qF5XXUgkiRW1YtuYRBZGDZiMKSckFs8mWPeWVVMKhcOo1zxWcWyQVqWFIWHca+y6prqaCyjvwjU+TMbHjp8Eg/gBGL58/h7BEYqyT1UXLI0CpdVfQFHIATbGD0JUySLe6uNg473/ZiymC9cOT42Y4CqgIhQXlXT3BZKLq4Eoq5IiSC46zZGlb6+F0C8oXBup/2jk6qhqtfhjKyiysQnupk3i6DQebmWMzvKPgdAz/TjZDCc3B45FjNOaBbxprlmVJYOX/yeP6srm+US1X+nedhozRJ1FBeJvEcXYYuyQ3qc7w/DmU1qJNkDX5W1jeIl/Zz3kG/gZ9D2OIygPMYz839I+jr2v/VydAXLwO5yCQF5LWAUJkbK59FmApTaiRv9UrF0qcmRSUpq2/S+Heu8jk3Mej8NlPe81vUJG+Rw/McZnUcPpVnjDaqqkqvKFaZk+eXRN1vlt9s3y1mGWO020Ov9ZPntv9Uz8PrypmgXDmnvBE1WfeviHoWdR31uzrPqC+o/rVk5Nev3wH5VwXkOys6IBoTHRCNiQ6IxkQHRGOiA6Ix0QHRmOiAaEx0QDQmOiAaEx0QjYkOiMZEB0RjogOiMdEB0ZjogGhMdEA0JjogGhMdEI2JDojGpEP/H7poTBgQ/X95pB2p/Qc0DfDFi1fOnwAAAABJRU5ErkJggg==">
                                            </div>
                                            <div class="b-delivery2__content">
                                                <div class="b-delivery2__title">Энергия (жд - до терминала)</div>
                                                2 терминала
                                                <div class="b-delivery2__onmap js js_open-deliveries-map">Пункты на карте
                                                </div>
                                            </div>
                                            <div class="b-delivery2__dayPrice">
                                                <div class="b-delivery2__day">20-39 дней</div>
                                                <div class="b-delivery2__price">2 025 р.</div>
                                            </div>
                                        </div>
                                        <button class="b-delivery2__more btn btn-light">
                                            <svg width="12" height="12">
                                                <use xlink:href="#plus"></use>
                                            </svg>
                                            <span>Показать еще</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!--	PICKUP BLOCK	-->
                    <div id="bx-soa-pickup" data-visited="false" class="bx-soa-section" style="display:none">
                        <div class="bx-soa-section-title-container">
                            <h2 class="bx-soa-section-title col-sm-9">
                                <span class="bx-soa-section-title-count"></span>
                            </h2>
                            <div class="col-xs-12 col-sm-3 text-right"><a href="" class="bx-soa-editstep"><?=$arParams['MESS_EDIT']?></a></div>
                        </div>
                        <div class="bx-soa-section-content container-fluid"></div>
                    </div>
                    <!--	BUYER PROPS BLOCK	-->
                    <div id="bx-soa-properties" class="order-item bx-soa-section bx-active"></div>
                    <!--	PAY SYSTEMS BLOCK	-->
                    <div id="bx-soa-paysystem" class="order-item bx-soa-section bx-active"></div>
                    <div id="bx-soa-orderSave" data-move="order-submit-get">
                        <div class="hr mb-5"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <?
                                if ($arParams['USER_CONSENT'] === 'Y')
                                {
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.userconsent.request',
                                        '',
                                        array(
                                            'ID' => $arParams['USER_CONSENT_ID'],
                                            'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],
                                            'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],
                                            'AUTO_SAVE' => 'N',
                                            'SUBMIT_EVENT_NAME' => 'bx-soa-order-save',
                                            'REPLACE' => array(
                                                'button_caption' => isset($arParams['~MESS_ORDER']) ? $arParams['~MESS_ORDER'] : $arParams['MESS_ORDER'],
                                                'fields' => $arResult['USER_CONSENT_PROPERTY_DATA']
                                            )
                                        )
                                    );
                                }
                                ?>
                            </div>
                            <div class="col-md-6">

                                <div class="btn btn-dark btn-lg float-md-right order-submit" data-save-button="true">Подтвердить и перейти к
                                    оплате
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--	SIDEBAR BLOCK	-->
            <div id="bx-soa-total" class="order__side col-xl-3">
                <div class="order-info">
                </div>
                <div data-move="order-submit-set"></div>
            </div>
        </div>
    </form>
    <div class="section-footer"></div>
    <script>const deliveries = [{
            "id": "1231",
            "city": "Москва",
            "metro": [{"title": "Китай-город", "color": "#FBAA34"}],
            "address": "121609, г. Москва, Лубянский проезд, д.15, стр.4",
            "coords": [55.766721, 37.599005],
            "deliveryDate": "7 сентября",
            "price": "бесплатно",
            "logoSrc": "images/kit/delivery/modal/bb-sm.png",
            "markerSrc": "images/kit/delivery/modal/bb-marker.png",
            "info": "Пункты выдачи Boxberry <br> График работы: пн-вс: 10.00-20.00",
            "payInfo": "Заказ можно оплатить наличными или банковской картой при получении"
        }, {
            "id": "1232",
            "city": "Москва",
            "metro": [{"title": "Алма-Атинская", "color": "#029A54"}],
            "address": "121609, г. Москва,  Ключевая ул, 6 к.1",
            "coords": [55.755922, 37.60243],
            "deliveryDate": "10 сентября",
            "price": "155 р.",
            "logoSrc": "images/kit/delivery/modal/cdek-sm.png",
            "markerSrc": "images/kit/delivery/modal/cdek-marker.png",
            "info": "Пункты выдачи СДЕК <br> График работы: пн-вск: 08:00-22:00",
            "payInfo": "Доставка по предоплате"
        }, {
            "id": "1233",
            "city": "Москва",
            "metro": [{"title": "Славянский бульвар", "color": "#0252A2"}],
            "address": "121609, г. Москва,  Инициативная ул, 5 к.1, стр.2",
            "coords": [55.761734, 37.584106],
            "deliveryDate": "11 сентября",
            "price": "180 р.",
            "logoSrc": "images/kit/delivery/modal/td-sm.png",
            "markerSrc": "images/kit/delivery/modal/td-marker.png",
            "info": "Пункты выдачи Top Delivery <br> График работы: пн-вс: 10.00-20.00",
            "payInfo": "Заказ можно оплатить наличными или банковской картой при получении"
        }, {
            "id": "1234",
            "city": "Москва",
            "metro": [{"title": "Окружная", "color": "#B4D445"}, {"title": "Владыкино", "color": "#FFA8AF"}],
            "address": "Москва, ул. Кировоградская, д. 9, к. 1, ТЦ «Акварель»",
            "coords": [55.750055, 37.600331],
            "deliveryDate": "Послезавтра",
            "price": "бесплатно",
            "logoSrc": "images/kit/delivery/modal/ems-sm.png",
            "markerSrc": "images/kit/delivery/modal/ems-marker.png",
            "info": "Пункты выдачи EMS <br> График работы: пн-вс: 10.00-20.00",
            "payInfo": "Заказ можно оплатить наличными или банковской картой при получении"
        }];</script>
    <div class="delivery-modal">
        <div class="delivery-modal__map js js_delivery-map">
            <div class="delivery-modal__m-view-switch-block">
                <div class="form-check form-switch form-switch_double js_delivery-view"><input class="form-check-input"
                                                                                               type="checkbox"
                                                                                               id="flexSwitchCheck_available"><label
                            class="form-check-label" for="flexSwitchCheck_available">Списком</label><label
                            class="form-check-label" for="flexSwitchCheck_available">На карте</label></div>
            </div>
            <div class="delivery-modal__map-close btn btn-dark">
                <svg width="16" height="16" fill="#fff">
                    <use xlink:href="#close"></use>
                </svg>
            </div>
            <div class="delivery-modal__map-pano js js_deliveries-stage"></div>
            <div class="delivery-modal__map-list">
                <div class="delivery-modal__map-search js js_delivery-search">
                    <form class="b-search"><span></span><input class="form-control" type="text"
                                                               placeholder="Найти по станции метро или улице" value="">
                    </form>
                </div>
                <div class="delivery-modal__map-items-wrap js js_map-deliveries-list"></div>
            </div>
        </div>
    </div>

	<div id="bx-soa-saved-files" style="display:none"></div>
	<div id="bx-soa-soc-auth-services" style="display:none">
		<?
		$arServices = false;
		$arResult['ALLOW_SOCSERV_AUTHORIZATION'] = Main\Config\Option::get('main', 'allow_socserv_authorization', 'Y') != 'N' ? 'Y' : 'N';
		$arResult['FOR_INTRANET'] = false;

		if (Main\ModuleManager::isModuleInstalled('intranet') || Main\ModuleManager::isModuleInstalled('rest'))
			$arResult['FOR_INTRANET'] = true;

		if (Main\Loader::includeModule('socialservices') && $arResult['ALLOW_SOCSERV_AUTHORIZATION'] === 'Y')
		{
			$oAuthManager = new CSocServAuthManager();
			$arServices = $oAuthManager->GetActiveAuthServices(array(
				'BACKURL' => $this->arParams['~CURRENT_PAGE'],
				'FOR_INTRANET' => $arResult['FOR_INTRANET'],
			));

			if (!empty($arServices))
			{
				$APPLICATION->IncludeComponent(
					'bitrix:socserv.auth.form',
					'flat',
					array(
						'AUTH_SERVICES' => $arServices,
						'AUTH_URL' => $arParams['~CURRENT_PAGE'],
						'POST' => $arResult['POST'],
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
			}
		}
		?>
	</div>

	<div style="display: none">
		<?
		// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it
		$APPLICATION->IncludeComponent(
			'bitrix:sale.location.selector.steps',
			'.default',
			array(),
			false
		);
		$APPLICATION->IncludeComponent(
			'bitrix:sale.location.selector.search',
			'.default',
			array(),
			false
		);
		?>
	</div>
	<?
	$signer = new Main\Security\Sign\Signer;
	$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');
	$messages = Loc::loadLanguageFile(__FILE__);

	?>
	<script>
		BX.message(<?=CUtil::PhpToJSObject($messages)?>);
		BX.Sale.OrderAjaxComponent.init({
			result: <?=CUtil::PhpToJSObject($arResult['JS_DATA'])?>,
			locations: <?=CUtil::PhpToJSObject($arResult['LOCATIONS'])?>,
			params: <?=CUtil::PhpToJSObject($arParams)?>,
			signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
			siteID: '<?=CUtil::JSEscape($component->getSiteId())?>',
			ajaxUrl: '<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>',
			templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
			propertyValidation: true,
			showWarnings: true,
			pickUpMap: {
				defaultMapPosition: {
					lat: 55.76,
					lon: 37.64,
					zoom: 7
				},
				secureGeoLocation: false,
				geoLocationMaxTime: 5000,
				minToShowNearestBlock: 3,
				nearestPickUpsToShow: 3
			},
			propertyMap: {
				defaultMapPosition: {
					lat: 55.76,
					lon: 37.64,
					zoom: 7
				}
			},
			orderBlockId: 'bx-soa-order',
			authBlockId: 'bx-soa-auth',
			basketBlockId: 'bx-soa-basket',
			regionBlockId: 'bx-soa-region',
			paySystemBlockId: 'bx-soa-paysystem',
			deliveryBlockId: 'bx-soa-delivery',
			pickUpBlockId: 'bx-soa-pickup',
			propsBlockId: 'bx-soa-properties',
			totalBlockId: 'bx-soa-total'
		});
	</script>
	<script>
		<?
		// spike: for children of cities we place this prompt
		$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
		?>
		BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
			'source' => $component->getPath().'/get.php',
			'cityTypeId' => intval($city['ID']),
			'messages' => array(
				'otherLocation' => '--- '.Loc::getMessage('SOA_OTHER_LOCATION'),
				'moreInfoLocation' => '--- '.Loc::getMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
				'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.Loc::getMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.Loc::getMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
						'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
						'#ANCHOR_END#' => '</a>'
					)).'</div>'
			)
		))?>);
	</script>
	<?
	if ($arParams['SHOW_PICKUP_MAP'] === 'Y' || $arParams['SHOW_MAP_IN_PROPS'] === 'Y')
	{
		if ($arParams['PICKUP_MAP_TYPE'] === 'yandex')
		{
			$this->addExternalJs($templateFolder.'/scripts/yandex_maps.js');
			$apiKey = htmlspecialcharsbx(Main\Config\Option::get('fileman', 'yandex_map_api_key', ''));
			?>
			<script src="<?=$scheme?>://api-maps.yandex.ru/2.1.50/?apikey=<?=$apiKey?>&load=package.full&lang=<?=$locale?>"></script>
			<script>
				(function bx_ymaps_waiter(){
					if (typeof ymaps !== 'undefined' && BX.Sale && BX.Sale.OrderAjaxComponent)
						ymaps.ready(BX.proxy(BX.Sale.OrderAjaxComponent.initMaps, BX.Sale.OrderAjaxComponent));
					else
						setTimeout(bx_ymaps_waiter, 100);
				})();
			</script>
			<?
		}

		if ($arParams['PICKUP_MAP_TYPE'] === 'google')
		{
			$this->addExternalJs($templateFolder.'/scripts/google_maps.js');
			$apiKey = htmlspecialcharsbx(Main\Config\Option::get('fileman', 'google_map_api_key', ''));
			?>
			<script async defer
				src="<?=$scheme?>://maps.googleapis.com/maps/api/js?key=<?=$apiKey?>&callback=bx_gmaps_waiter">
			</script>
			<script>
				function bx_gmaps_waiter()
				{
					if (BX.Sale && BX.Sale.OrderAjaxComponent)
						BX.Sale.OrderAjaxComponent.initMaps();
					else
						setTimeout(bx_gmaps_waiter, 100);
				}
			</script>
			<?
		}
	}

	if ($arParams['USE_YM_GOALS'] === 'Y')
	{
		?>
		<script>
			(function bx_counter_waiter(i){
				i = i || 0;
				if (i > 50)
					return;

				if (typeof window['yaCounter<?=$arParams['YM_GOALS_COUNTER']?>'] !== 'undefined')
					BX.Sale.OrderAjaxComponent.reachGoal('initialization');
				else
					setTimeout(function(){bx_counter_waiter(++i)}, 100);
			})();
		</script>
		<?
	}
}

?>

