<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!empty($arResult['ERROR'])) {
    echo $arResult['ERROR'];
    return false;
}
?>
<? if ($arResult['rows']) { ?>
    <div class="services">
        <div class="container-fluid">
            <div class="services__slider swiper-container">
                <div class="swiper-wrapper">
                    <? foreach ($arResult["rows"] as $key => $arAdvantage) { ?>
                        <div class="swiper-slide">
                            <div class="services__item">
                                <? if ($arAdvantage['UF_IMG']) { ?>
                                    <div class="services__icon">
                                        <?= $arAdvantage['~UF_IMG'] ?>
                                    </div>
                                <? } ?>
                                <div class="services__title"><?= $arAdvantage['~UF_NAME'] ?></div>
                                <div class="services__text"><?= $arAdvantage['~UF_TEXT'] ?></div>
                            </div>
                        </div>
                    <? } ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
<? } ?>