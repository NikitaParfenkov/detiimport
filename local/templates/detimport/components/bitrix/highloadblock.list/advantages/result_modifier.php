<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if ($arResult['rows']) {
    foreach ($arResult["rows"] as $key => &$arAdvantage) {
        if ($arAdvantage['UF_IMG']) {
            $arAdvantage['~UF_IMG'] = htmlspecialchars_decode(htmlspecialcharsBack($arAdvantage['UF_IMG']));
        }
        if ($arAdvantage['UF_NAME']) {
            $arAdvantage['~UF_NAME'] = htmlspecialchars_decode(htmlspecialcharsBack($arAdvantage['UF_NAME']));
        }
        if ($arAdvantage['UF_TEXT']) {
            $arAdvantage['~UF_TEXT'] = htmlspecialchars_decode(htmlspecialcharsBack($arAdvantage['UF_TEXT']));
        }
    }
    unset($arAdvantage);
}