<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<a class="header-icons__cart" href="/basket/">
	<svg>
		<use xlink:href="#cart"></use>
	</svg>
	<span class="count"><?= $arResult['NUM_PRODUCTS'] ?: 0;?></span>
</a>
