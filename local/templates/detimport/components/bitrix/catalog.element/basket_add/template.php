<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

?>
<div class="order-list">
	<div class="order-list__item">
		<div class="order-list__img">
			<img src="<?= $arResult['CURRENT_ITEM']['PICTURE']; ?>">
		</div>
		<div class="order-list__content">
			<div class="order-list__inner">
				<div class="order-list__title"><?= $arResult['CURRENT_ITEM']['NAME'] ?></div>
				<div class="item-options">
                    <? if ($arResult['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']) { ?>
						<div class="item-options__option">
							<span><?= GetMessage('COLOR') ?></span>
							<span class="color"
							      style="background-color: <?= $arResult['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']; ?>"></span>
                            <?= $arResult['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_NAME']; ?>
						</div>
                    <? } ?>
                    <? if ($arResult['BRAND']) { ?>
						<div class="item-options__option">
							<span><?= GetMessage('BRAND') ?></span>
                            <?= $arResult['BRAND']['NAME']; ?>
						</div>
                    <? } ?>
                    <? if ($arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE']) { ?>
						<div class="item-options__option">
							<span> <?= $arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['NAME'] ?></span>
                            <?= $arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE'] ?>
						</div>
                    <? } ?>
				</div>
			</div>
			<div class="order-list__price">
				<div class="order-list__priceNew"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
                <? if ($arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
					<div class="order-list__discount">
						-<?= $arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%
					</div>
					<div class="order-list__priceOld"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_VALUE']; ?></div>
                <? } ?>
			</div>
		</div>
	</div>
</div>