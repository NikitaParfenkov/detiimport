<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use SP\Tools\HLTools;
use SP\Tools\Brands;
CModule::IncludeModule('sp.tools');

if ($arResult['OFFERS']) {
    $colors = [];
    foreach ($arResult['OFFERS'] as $offer) {
        if ($arParams['OFFER_ID'] == $offer['ID']) {
            if ($offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']) {
                $colors[$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']] = $offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'];
            }
            $arResult['CURRENT_ITEM'] = $offer;
            $arResult['CURRENT_ITEM']['PICTURE'] = CFile::ResizeImageGet(
                $offer['PREVIEW_PICTURE']['ID'],
                ['width' => 120, 'height' => 99],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'];
        }
    }

    if ($colors) {
        $hlTools = new HLTools();
        $select = ['UF_XML_ID', 'UF_DESCRIPTION', 'UF_NAME'];
        $filter = ["UF_XML_ID" => $colors];
        $order = ["ID" => "ASC"];

        $arResult['COLORS'] = $hlTools->getElementsInfoByFilter(HL_COLORS, $select, $filter, $order);
    }
} else {
    $arResult['CURRENT_ITEM'] = $arResult;
}

if (!$arResult['CURRENT_ITEM']['PICTURE']) {
    if ($arResult['PREVIEW_PICTURE']['ID']) {
        $arResult['CURRENT_ITEM']['PICTURE'] = CFile::ResizeImageGet(
            $arResult['PREVIEW_PICTURE']['ID'],
            ['width' => 120, 'height' => 99],
            BX_RESIZE_IMAGE_PROPORTIONAL
        )['src'];
    } else {
        $arResult['CURRENT_ITEM']['PICTURE'] = CATALOG_NO_PHOTO;
    }
}

if ($arResult['PROPERTIES']['BRAND']['VALUE']) {
    $brandHandler = new Brands();
    $arResult['BRAND'] = $brandHandler->getBrandInfo($arResult['PROPERTIES']['BRAND']['VALUE']);
}
