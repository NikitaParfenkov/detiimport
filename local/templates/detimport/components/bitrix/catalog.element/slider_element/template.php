<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$actions = [
    'FAVORITES' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('FAV_TEXT'),
    ],
    'COMPARE' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('COMP_TEXT'),
    ],
];

if (in_array($arResult['ID'], $_SESSION['FAVORITES'])) {
    $actions['FAVORITES']['ACTION'] = 'compfavdelete';
    $actions['FAVORITES']['CLASS'] = 'is-active';
    $actions['FAVORITES']['TITLE'] = '';
}

if (in_array($arResult['ID'], $_SESSION['COMPARE'])) {
    $actions['COMPARE']['ACTION'] = 'compfavdelete';
    $actions['COMPARE']['CLASS'] = 'is-active';
    $actions['COMPARE']['TITLE'] = '';
}

?>
<div class="itemsTwo__content">
    <div class="itemsTwo__contentInner">
        <div class="itemsTwo__labels">
            <div class="b-labels">
                <? if ($arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
                    <div class="b-labels__label b-labels__label_discount">Sale</div>
                <? } ?>
                <? if ($arResult['PROPERTIES']['HIT']['VALUE'] === 'Y') { ?>
		            <div class="b-labels__label b-labels__label_hit">Hit</div>
                <? } ?>
                <? if ($arResult['PROPERTIES']['NEW']['VALUE'] === 'Y') { ?>
		            <div class="b-labels__label b-labels__label_new">New</div>
                <? } ?>
            </div>
        </div>
        <a class="itemsTwo__title" href="<?= $arResult['DETAIL_PAGE_URL']; ?>"><?= $arResult['NAME']; ?></a>
        <? if ($arResult['COLORS']) { ?>
            <div class="itemsTwo__colors">
                <div class="b-colors b-colors_itemsTwo">
                    <div class="b-colors__list">
                        <? foreach ($arResult['COLORS'] as $colorCode) { ?>
                            <div class="b-colors__item" style="background-color: <?= $colorCode; ?>"></div>
                        <? } ?>
                    </div>
                    <?
                    $colorsLeft = count($arResult['COLORS']) - 5;
                    if ($colorsLeft > 0) { ?>
                        <div class="b-colors__more">+<?= $colorsLeft; ?></div>
                    <? } ?>
                </div>
            </div>
        <? } ?>
        <div class="itemsTwo__bottom">
            <div class="itemsTwo__price">
                <div class="itemsTwo__priceNew"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
                <? if ($arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
                    <div class="itemsTwo__priceOld"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_VALUE']; ?></div>
                <? } ?>
            </div>
            <!--<div class="itemsTwo__options">
                <div class="itemsTwo__option itemsTwo__option_favorites <?/*= $actions['FAVORITES']['CLASS']; */?>"
                     data-toggle="tooltip"
                     data-placement="bottom" title="<?/*= $actions['FAVORITES']['TITLE']; */?>" data-color="custom"
                     data-active-title="<?/*= GetMessage('FAV_ALREADY') */?>" data-type="option"
                     data-original-title="<?/*= GetMessage('FAV_TEXT'); */?>"
                     data-class="tools" data-method="<?/*= $actions['FAVORITES']['ACTION']; */?>" data-add="FAVORITES"
                     data-id="<?/*= $arResult['ID'] */?>"
                >
                    <svg>
                        <use xlink:href="#favorites"></use>
                    </svg>
                </div>
                <div class="itemsTwo__option itemsTwo__option_compare <?/*= $actions['COMPARE']['CLASS']; */?>"
                     data-toggle="tooltip"
                     data-placement="bottom" title="<?/*= $actions['COMPARE']['TITLE']; */?>" data-color="custom"
                     data-active-title="<?/*= GetMessage('COMP_ALREADY') */?>" data-type="option"
                     data-original-title="<?/*= GetMessage('COMP_TEXT'); */?>"
                     data-class="tools" data-method="<?/*= $actions['COMPARE']['ACTION']; */?>" data-add="COMPARE"
                     data-id="<?/*= $arResult['ID'] */?>"
                >
                    <svg>
                        <use xlink:href="#compare"></use>
                    </svg>
                </div>
            </div>-->
        </div>
    </div>
</div>
