<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use SP\Tools\HLTools;
CModule::IncludeModule('sp.tools');

if ($arResult['OFFERS']) {
    $arResult['CURRENT_ITEM'] = $arResult['OFFERS'][0];

    $colors = [];
    foreach ($arResult['OFFERS'] as $offer) {
        if ($offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']) {
            $colors[$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']] = $offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'];
        }
    }

    if ($colors) {
        $hlTools = new HLTools();
        $select = ['UF_XML_ID', 'UF_DESCRIPTION'];
        $filter = ["UF_XML_ID" => $colors];
        $order = ["ID" => "ASC"];

        $arResult['COLORS'] = $hlTools->getElementsByFilter(HL_COLORS, $select, $filter, $order);
    }
} else {
    $arResult['CURRENT_ITEM'] = $arResult;
}

