<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if ($arResult['RESULT']['PROPERTIES']['RELATED_PRODUCTS']['VALUE']) { ?>
	<div class="productList container-fluid">
		<div class="productList__top">
			<h3 class="productList__title"><?= GetMessage('RELATED_PRODUCTS') ?></h3>
			<div class="productList__navi" data-move="productList-nav-get">
				<div class="swiper-pagination"></div>
				<div class="swiper-navigation">
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
			</div>
		</div>
		<div class="productListSlider">
			<div class="productListSlider__inner swiper-container">
                <?
                $GLOBALS['relatedProductsFilter']['ID'] = $arResult['RESULT']['PROPERTIES']['RELATED_PRODUCTS']['VALUE'];
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "slider",
                    Array(
                        "CARD_TYPE" => 'small_slide',
                        "ACTION_VARIABLE" => "action",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BACKGROUND_IMAGE" => "-",
                        "BASKET_URL" => "/personal/basket.php",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COMPATIBLE_MODE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                        "DETAIL_URL" => "",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "ENLARGE_PRODUCT" => "STRICT",
                        "FILTER_NAME" => "relatedProductsFilter",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                        "IBLOCK_ID" => "4",
                        "IBLOCK_TYPE" => "1c_catalog",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "LABEL_PROP" => array(),
                        "LAZY_LOAD" => "N",
                        "LINE_ELEMENT_COUNT" => "3",
                        "LOAD_ON_SCROLL" => "N",
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "OFFERS_FIELD_CODE" => array("", ""),
                        "OFFERS_LIMIT" => "0",
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "18",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array("РРЦ"),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                        "PRODUCT_DISPLAY_MODE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                        "RCM_TYPE" => "personal",
                        "SECTION_CODE" => "",
                        "SECTION_ID" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "",
                        "SECTION_USER_FIELDS" => array("", ""),
                        "SEF_MODE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_FROM_SECTION" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_SLIDER" => "Y",
                        "SLIDER_INTERVAL" => "3000",
                        "SLIDER_PROGRESS" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N"
                    )
                ); ?>
			</div>
			<div class="productListSlider__navi" data-move="productList-nav-set"></div>
		</div>
	</div>
<? } ?>

<div class="container-fluid content-middle product-wrapper">
	<div class="row g-0">
		<div class="col-lg-8 col-xl-9">
			<div class="content-inner">
				<div class="slider-tabs-titles swiper-container">
					<ul class="nav nav-tabs swiper-wrapper" id="myTab" role="tablist">
						<li class="nav-item swiper-slide" role="presentation">
							<a class="nav-link active"
							   id="home-tab-<?= GetMessage('BUY_IN_SHOP'); ?>"
							   data-toggle="tab"
							   href="#tab1" role="tab"
							   aria-selected="true"><?= GetMessage('BUY_IN_SHOP'); ?></a>
						</li>
						<li class="nav-item swiper-slide" role="presentation">
							<a class="nav-link"
							   id="home-tab-<?= GetMessage('CHARS'); ?>"
							   data-toggle="tab"
							   href="#tab2" role="tab"
							   aria-selected="false"><?= GetMessage('CHARS'); ?></a>
						</li>
						<li class="nav-item swiper-slide" role="presentation">
							<a class="nav-link"
							   id="home-tab-<?= GetMessage('DELIVERY_OPTIONS'); ?>"
							   data-toggle="tab"
							   href="#tab3" role="tab"
							   aria-selected="false"><?= GetMessage('DELIVERY_OPTIONS'); ?></a>
						</li>
					</ul>
					<div class="swiper-scrollbar"></div>
				</div>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="tab1" role="tabpanel">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/catalog_element/buy_in_shop.php"
                            )
                        ); ?>
					</div>
					<div class="tab-pane fade" id="tab2" role="tabpanel">
						<div class="row gx-xl-5 gx-xxl-0">
                            <? if ($arResult['RESULT']['~DETAIL_TEXT']) { ?>
								<div class="col-xl">
									<div class="text-md mb-5"><?= GetMessage('DESCRIPTION') ?></div>
                                    <?= $arResult['RESULT']['~DETAIL_TEXT']; ?>
								</div>
								<div class="col-1 d-none d-xxl-block"></div>
                            <? } ?>
                            <? if ($arResult['RESULT']['PROPERTIES']['EQUIPMENT']['~VALUE']['TEXT']) { ?>
								<div class="col-xl">
									<div class="text-md mb-5"><?= $arResult['RESULT']['PROPERTIES']['EQUIPMENT']['NAME']; ?></div>
                                    <?= $arResult['RESULT']['PROPERTIES']['EQUIPMENT']['~VALUE']['TEXT']; ?>
								</div>
                            <? } ?>
						</div>
                        <?
                        if ($arParams['DISPLAY_PROPS']) {
                            $properties = [];
                            foreach ($arParams['DISPLAY_PROPS'] as $propCode) {
                                if ($arResult['RESULT']['PROPERTIES'][$propCode]['VALUE']) {
                                    $properties[] = $arResult['RESULT']['PROPERTIES'][$propCode];
                                }
                            }
                            if ($properties) {
                                $chunkedProps = array_chunk($properties, round(count($properties) / 2));
                            }
                            if ($chunkedProps) {
                                ?>
	                            <hr class="mb-4">
								<div class="general-characteristics">
									<div class="text-md mb-5"><?= GetMessage('COMMON_CHARS') ?></div>
									<div class="row gx-xl-5 gx-xxl-0">
                                        <? foreach ($chunkedProps as $key => $group) { ?>
											<div class="col-xl">
												<table class="table table-characteristics">
													<tbody>
                                                    <? foreach ($group as $property) { ?>
														<tr>
															<th scope="row"><?= $property['NAME']; ?></th>
															<td><?= $property['VALUE']; ?></td>
														</tr>
                                                    <? } ?>
													</tbody>
												</table>
											</div>
                                            <? if ($key == 0) { ?>
												<div class="col-1 d-none d-xxl-block"></div>
                                            <? } ?>
                                        <? } ?>
									</div>
								</div>
                            <? } ?>
                        <? } ?>
						<?
						if ($arResult['RESULT']['PROPERTIES']['COMPONENTS']['VALUE']) {
							$GLOBALS['componentsFilter']['ID'] = $arResult['RESULT']['PROPERTIES']['COMPONENTS']['VALUE'];
							$APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"catalog_components",
								Array(
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"ADD_SECTIONS_CHAIN" => "N",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"CACHE_TIME" => "36000000",
									"CACHE_TYPE" => "A",
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"FIELD_CODE" => array("NAME", ""),
									"FILTER_NAME" => "componentsFilter",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"IBLOCK_ID" => "13",
									"IBLOCK_TYPE" => "1c_catalog",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"INCLUDE_SUBSECTIONS" => "Y",
									"MESSAGE_404" => "",
									"NEWS_COUNT" => "50",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => ".default",
									"PAGER_TITLE" => "Новости",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"PREVIEW_TRUNCATE_LEN" => "",
									"PROPERTY_CODE" => array("IMAGES", ""),
									"SET_BROWSER_TITLE" => "N",
									"SET_LAST_MODIFIED" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_STATUS_404" => "N",
									"SET_TITLE" => "N",
									"SHOW_404" => "N",
									"SORT_BY1" => "ACTIVE_FROM",
									"SORT_BY2" => "SORT",
									"SORT_ORDER1" => "DESC",
									"SORT_ORDER2" => "ASC",
									"STRICT_SECTION_CHECK" => "N"
								)
							);
						}
						?>
					</div>
					<div class="tab-pane fade" id="tab3" role="tabpanel">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/include/catalog_element/delivery_main.php"
                            )
                        ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-xl-3 product-side d-none d-lg-block">
			<div class="stick-me">
				<div class="itemMd sp-element-side-info">
					<div class="itemMd__top">
						<div class="itemMd__img">
							<img src="<?= $arResult['RESULT']['SIDE_PICTURE'] ?>">
						</div>
						<div class="itemMd__content">
							<!--<div class="itemMd__stars">
								<div class="svg-stars svg-stars_4"></div>
								<span>3.2k</span>
							</div>-->
							<div class="itemMd__title">
                                <?= $arResult['RESULT']['CURRENT_ITEM']['NAME']; ?>
							</div>
                            <? if ($arResult['RESULT']['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE']) { ?>
								<div class="itemMd__sbl">
                                    <?= $arResult['RESULT']['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['NAME']; ?>
									<span><?= $arResult['RESULT']['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE']; ?></span>
								</div>
                            <? } ?>
                            <? if ($arResult['RESULT']['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']) { ?>
								<div class="itemMd__sbl"><?= GetMessage('COLORS'); ?>
									<div class="itemMd__color"
									     style="background-color: <?= $arResult['RESULT']['COLORS'][$arResult['RESULT']['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']; ?>"></div>
								</div>
                            <? } ?>
						</div>
					</div>
					<div class="itemMd__footer">
						<div class="itemMd__price">
							<div class="itemMd__priceNew"><?= $arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
                            <? if ($arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
								<div class="itemMd__priceOld"><?= $arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['PRINT_VALUE']; ?></div>
                            <? } ?>
						</div>
						<div class="itemMd__oneclickbuy btn btn-sm btn-dark bye-one-click" data-id="<?= $arResult['RESULT']['CURRENT_ITEM']['ID'];?>"><?= GetMessage('BUY_ONE_CLICK') ?></div>
					</div>
					<?if ($arResult['RESULT']['CURRENT_ITEM']['ID']) {?>
					<div class="itemMd__targets">
						<div class="itemMd__button itemMd__button_cart" data-class="basket" data-method="add2basket"
						     data-id="<?= $arResult['RESULT']['CURRENT_ITEM']['ID']; ?>">
							<svg>
								<use xlink:href="#cart"></use>
							</svg>
                            <?= GetMessage('ADD_TO_BASKET') ?>
						</div>
					</div>
					<?}?>
				</div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_RECURSIVE" => "Y",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/catalog_element/delivery_side.php"
                    )
                ); ?>
			</div>
		</div>
	</div>
</div>
<?
$GLOBALS['similarFilter']['!ID'] = $arResult['RESULT']['ID'];
$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "similar_products",
    Array(
        'HIDE_FAVCOMP_BLOCK' => 'Y',
        'CARD_CLASS' => 'item item_list js-item-list has-more-images',
        "TITLE" => 'Похожие товары',
        "CARD_TYPE" => 'small_slide',
        "ACTION_VARIABLE" => "action",
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BACKGROUND_IMAGE" => "-",
        "BASKET_URL" => "/personal/basket.php",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMPATIBLE_MODE" => "Y",
        "CONVERT_CURRENCY" => "N",
        "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
        "DETAIL_URL" => "",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_COMPARE" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_ORDER2" => "desc",
        "ENLARGE_PRODUCT" => "STRICT",
        "FILTER_NAME" => "similarFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
        "IBLOCK_ID" => "4",
        "IBLOCK_TYPE" => "1c_catalog",
        "INCLUDE_SUBSECTIONS" => "Y",
        "LABEL_PROP" => array(),
        "LAZY_LOAD" => "N",
        "LINE_ELEMENT_COUNT" => "3",
        "LOAD_ON_SCROLL" => "N",
        "MESSAGE_404" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "OFFERS_FIELD_CODE" => array("", ""),
        "OFFERS_LIMIT" => "0",
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_ORDER2" => "desc",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Товары",
        "PAGE_ELEMENT_COUNT" => "10",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array("РРЦ"),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
        "PRODUCT_DISPLAY_MODE" => "N",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        "RCM_TYPE" => "personal",
        "SECTION_CODE" => "",
        "SECTION_ID" => $arResult['RESULT']['IBLOCK_SECTION_ID'],
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => array("", ""),
        "SEF_MODE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SHOW_ALL_WO_SECTION" => "Y",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_FROM_SECTION" => "N",
        "SHOW_MAX_QUANTITY" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_SLIDER" => "Y",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "N",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "N"
    )
);
?>
<div class="price-button-fixed sp-element-footer-mobile">
	<div class="price-button-fixed__price">
		<div class="price-button-fixed__priceNew"><?= $arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
        <? if ($arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
			<div class="price-button-fixed__priceOld"><?= $arResult['RESULT']['CURRENT_ITEM']['MIN_PRICE']['PRINT_VALUE']; ?></div>
        <? } ?>
	</div>
	<div class="price-button-fixed__button price-button-fixed__button_cart">
		<svg>
			<use xlink:href="#cart"></use>
		</svg>
        <?= GetMessage('ADD_TO_BASKET'); ?>
	</div>
</div>

<div class="modal fade" id="modal-product" tabindex="-1" aria-labelledby="modal-productLabel" aria-hidden="true">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-close" type="button" data-dismiss="modal" aria-label="Close"></div>
			</div>
			<div class="modal-body">

			</div>
		</div>
	</div>
</div>