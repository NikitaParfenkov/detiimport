<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$actions = [
    'FAVORITES' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('FAV_TEXT'),
    ],
    'COMPARE' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('COMP_TEXT'),
    ],
];

if (in_array($arResult['ID'], $arParams['FAVORITES'])) {
    $actions['FAVORITES']['ACTION'] = 'compfavdelete';
    $actions['FAVORITES']['CLASS'] = 'is-active';
    $actions['FAVORITES']['TITLE'] = '';
}

if (in_array($arResult['ID'], $arParams['COMPARE'])) {
    $actions['COMPARE']['ACTION'] = 'compfavdelete';
    $actions['COMPARE']['CLASS'] = 'is-active';
    $actions['COMPARE']['TITLE'] = '';
}

?>
<div class="row g-0 sp-item-main-content">
	<div class="col-lg-7 col-xl-8">
		<div class="product-media product-media_page">
			<?if ($arResult['PROPERTIES']['VIDEO']['VALUE']['url']) {?>
			<div class="product-media__video">
				<a class="b-video" data-fslightbox="video" href="<?= $arResult['PROPERTIES']['VIDEO']['VALUE']['url'];?>">
					<?
					$picture = CFile::ResizeImageGet($arResult['PROPERTIES']['VIDEO']['VALUE']['picture'],
					            ['width' => 240, 'height' => 134],
					            BX_RESIZE_IMAGE_PROPORTIONAL
					        )['src'];
					?>
					<span class="b-video__img">
						<img src="<?= $picture;?>">
					</span>
					<span class="b-video__title"><?= $arResult['PROPERTIES']['VIDEO']['VALUE']['name'];?></span></a>
			</div>
			<?}?>
			<div class="product-media__labels">
				<div class="b-labels b-labels_product">
                    <? if ($arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
						<div class="b-labels__label b-labels__label_discount">Sale</div>
                    <? } ?>
                    <? if ($arResult['PROPERTIES']['HIT']['VALUE'] === 'Y') { ?>
						<div class="b-labels__label b-labels__label_hit">Hit</div>
                    <? } ?>
                    <? if ($arResult['PROPERTIES']['NEW']['VALUE'] === 'Y') { ?>
						<div class="b-labels__label b-labels__label_new">New</div>
                    <? } ?>
				</div>
			</div>
			<div class="sliderProduct sliderProduct_page">
				<div class="sliderProduct__inner swiper-container">
					<div class="swiper-wrapper">
                        <? foreach ($arResult['GALLERY'] as $picture) { ?>
							<div class="swiper-slide">
								<picture class="sliderProduct__img">
									<img src="<?= $picture['BIG']; ?>" alt="">
								</picture>
							</div>
                        <? } ?>
					</div>
				</div>
				<div class="sliderProduct__navi">
					<div class="swiper-navigation">
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
					<div class="swiper-pagination"></div>
					<div class="sliderProduct__zoom"></div>
				</div>
			</div>
			<div class="sliderProductThumbs sliderProductThumbs_page">
				<div class="sliderProductThumbs__inner swiper-container">
					<div class="swiper-wrapper">
                        <? foreach ($arResult['GALLERY'] as $picture) { ?>
							<div class="swiper-slide">
								<picture class="sliderProductThumbs__img">
									<img src="<?= $picture['SMALL']; ?>" alt="">
								</picture>
							</div>
                        <? } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-xl-3 offset-lg-1">
		<div class="product-content">
			<div class="product-content__top">
				<!--<div class="product-content__stars">
					<div class="svg-stars svg-stars_4"></div>
					<span>3.2k</span>
				</div>-->
				<h1 class="product-content__main-title h2"><?= $arResult['NAME']; ?></h1>
                <? if ($arResult['OFFERS_COLORS']) { ?>
					<div class="mb-4">
						<div class="product-content__sbl">
                            <?= GetMessage('COLORS') ?>
							<span class="sup"><?= count($arResult['OFFERS_COLORS']); ?></span>
						</div>
						<div class="product-content__dropdown">
							<div class="dropdown-color dropdown">
								<button class="btn dropdown-toggle" id="dropdownMenuColor" type="button"
								        data-toggle="dropdown" aria-expanded="false">
									<img src="<?= $arResult['CURRENT_ITEM']['OFFER_PICTURE']; ?>">
									<span class="dropdown-color__color"
									      style="background-color: <?= $arResult['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']; ?>">
									</span>
									<span class="dropdown-color__title">
										<?= $arResult['COLORS'][$arResult['CURRENT_ITEM']['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_NAME']; ?></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenuColor" data-simplebar
								    data-simplebar-auto-hide="false">
                                    <? foreach ($arResult['OFFERS_COLORS'] as $offer) {
                                        if (!$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']) {
                                            continue;
                                        }
                                        ?>
										<li>
											<a class="dropdown-item sp-change-offer" href="javascript:void(0)"
											   data-id="<?= $offer['ID']; ?>">
												<img src="<?= $offer['OFFER_PICTURE']; ?>">
												<span class="dropdown-color__color"
												      style="background-color: <?= $arResult['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_DESCRIPTION']; ?>"></span>
												<span class="dropdown-color__title"><?= $arResult['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]['UF_NAME']; ?></span>
											</a>
										</li>
                                    <? } ?>
								</ul>
							</div>
						</div>
					</div>
                <? } ?>
				<div class="mb-4">
                    <? if ($arResult['BRAND']) { ?>
						<div class="product-content__sbl">
                            <?= GetMessage('BRAND') ?>
							<span><?= $arResult['BRAND']['NAME']; ?></span>
						</div>
                    <? } ?>
                    <? if ($arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE']) { ?>
						<div class="product-content__sbl">
                            <?= $arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['NAME'] ?>
							<span><?= $arResult['CURRENT_ITEM']['PROPERTIES']['ARTIKUL']['VALUE'] ?></span>
						</div>
                    <? } ?>
				</div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_RECURSIVE" => "Y",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/catalog_element/delivery.php"
                    )
                ); ?>
			</div>
			<div class="product-content__footer">
				<div class="product-content__price">
					<div class="product-content__priceNew"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
                    <? if ($arResult['CURRENT_ITEM']['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
						<div class="product-content__priceOld"><?= $arResult['CURRENT_ITEM']['MIN_PRICE']['PRINT_VALUE']; ?></div>
                    <? } ?>
				</div>
				<div class="product-content__oneclickbuy btn btn-sm btn-dark bye-one-click"
				     data-id="<?= $arResult['CURRENT_ITEM']['ID']; ?>">
                    <?= GetMessage('BUY_ONE_CLICK'); ?>
				</div>
			</div>
            <? if ($arResult['CURRENT_ITEM']['ID']) { ?>
				<div class="product-content__targets">
					<div class="product-content__button product-content__button_cart" data-class="basket"
					     data-method="add2basket" data-id="<?= $arResult['CURRENT_ITEM']['ID']; ?>">
						<svg>
							<use xlink:href="#cart"></use>
						</svg>
                        <?= GetMessage('ADD_TO_BASKET') ?>
					</div>
				</div>
            <? } ?>
		</div>
	</div>
</div>

</div>



