<?php
$MESS['FAV_TEXT'] = 'Отложить';
$MESS['FAV_ALREADY'] = 'Отложено';
$MESS['COMP_TEXT'] = 'В сравнение';
$MESS['COMP_ALREADY'] = 'В сравнении';
$MESS['COLORS'] = 'Расцветка';
$MESS['BUY_ONE_CLICK'] = 'Купить в один клик';
$MESS['ADD_TO_BASKET'] = 'В корзину';
$MESS['RELATED_PRODUCTS'] = 'Сопутствующие товары';
$MESS['COLORS'] = 'Цвета';
$MESS['BUY_IN_SHOP'] = 'Купить в магазине';
$MESS['CHARS'] = 'Характеристики';
$MESS['DELIVERY_OPTIONS'] = 'Варианты доставки';
$MESS['DESCRIPTION'] = 'Описание';
$MESS['COMMON_CHARS'] = 'Общие характеристики';
