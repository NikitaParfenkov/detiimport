<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use SP\Tools\HLTools;
use SP\Tools\Brands;
CModule::IncludeModule('sp.tools');

if ($arResult['OFFERS']) {
    $colors = [];
    foreach ($arResult['OFFERS'] as &$offer) {
        $offers[$offer['ID']] = $offer;
        if ($offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']) {
            $colors[$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']] = $offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'];
            if ($offer['PREVIEW_PICTURE']['ID']) {
                $offer['OFFER_PICTURE'] = CFile::ResizeImageGet(
                    $offer['PREVIEW_PICTURE']['ID'],
                    ['width' => 70, 'height' => 58],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'];
            }
            $arResult['OFFERS_COLORS'][$offer['ID']] = $offer;
        }
    }
    if ($arParams['OFFER_ID']) {
        $arResult['CURRENT_ITEM'] = $arResult['OFFERS_COLORS'][$arParams['OFFER_ID']];
    } else {
        $arResult['CURRENT_ITEM'] = $arResult['OFFERS_COLORS'][array_keys($arResult['OFFERS_COLORS'])[0]];
    }

    $arResult['CURRENT_ITEM']['OFFER_PICTURE'] = CFile::ResizeImageGet(
        $arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID'],
        ['width' => 70, 'height' => 58],
        BX_RESIZE_IMAGE_PROPORTIONAL
    )['src'];
    if ($arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID']) {
        $arResult['GALLERY'][] = [
            'BIG' => CFile::ResizeImageGet($arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID'],
                ['width' => 368, 'height' => 303],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
            'SMALL' => CFile::ResizeImageGet($arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID'],
                ['width' => 60, 'height' => 50],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
        ];
    }
    if ($arResult['CURRENT_ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE']) {
        foreach ($arResult['CURRENT_ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'] as $picture) {
            $arResult['GALLERY'][] = [
                'BIG' => CFile::ResizeImageGet($picture,
                    ['width' => 368, 'height' => 303],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
                'SMALL' => CFile::ResizeImageGet($picture,
                    ['width' => 60, 'height' => 50],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
            ];
        }
    }

    if ($colors) {
        $hlTools = new HLTools();
        $select = ['UF_XML_ID', 'UF_DESCRIPTION', 'UF_NAME'];
        $filter = ["UF_XML_ID" => $colors];
        $order = ["ID" => "ASC"];

        $arResult['COLORS'] = $hlTools->getElementsInfoByFilter(HL_COLORS, $select, $filter, $order);
    }
} else {
    $arResult['CURRENT_ITEM'] = $arResult;
}

if ($arResult['PROPERTIES']['BRAND']['VALUE']) {
    $brandHandler = new Brands();
    $arResult['BRAND'] = $brandHandler->getBrandInfo($arResult['PROPERTIES']['BRAND']['VALUE']);
}

if ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']) {
    foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $picture) {
        $arResult['GALLERY'][] = [
            'BIG' => CFile::ResizeImageGet($picture,
                ['width' => 368, 'height' => 303],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
            'SMALL' => CFile::ResizeImageGet($picture,
                ['width' => 60, 'height' => 50],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'],
        ];
    }
} elseif ($arResult['PREVIEW_PICTURE']['ID']) {
    $arResult['GALLERY'][] = [
        'BIG' => CFile::ResizeImageGet($arResult['PREVIEW_PICTURE']['ID'],
            ['width' => 368, 'height' => 303],
            BX_RESIZE_IMAGE_PROPORTIONAL
        )['src'],
        'SMALL' => CFile::ResizeImageGet($arResult['PREVIEW_PICTURE']['ID'],
            ['width' => 60, 'height' => 50],
            BX_RESIZE_IMAGE_PROPORTIONAL
        )['src'],
    ];
}

if (empty($arResult['GALLERY'])) {
    $arResult['GALLERY'][] = [
        'BIG' => CATALOG_NO_PHOTO,
        'SMALL' => CATALOG_NO_PHOTO,
    ];
}

if ($arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID']) {
    $arResult['SIDE_PICTURE'] = CFile::ResizeImageGet($arResult['CURRENT_ITEM']['PREVIEW_PICTURE']['ID'],
        ['width' => 118, 'height' => 134],
        BX_RESIZE_IMAGE_PROPORTIONAL
    )['src'];
} else {
    $arResult['SIDE_PICTURE'] = CATALOG_NO_PHOTO;
}

$cp = $this->__component;
if (is_object($cp))
{
    $cp->arResult["RESULT"] = $arResult;
    $cp->SetResultCacheKeys(array("RESULT"));
}