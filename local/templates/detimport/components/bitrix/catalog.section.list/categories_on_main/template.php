<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['MAIN_SECTIONS']) {
    ?>
    <div class="b-catalog section">
        <div class="section-header section-header_main section-header_padding-top container-fluid">
            <?
            if ($arParams['TITLE']) { ?>
                <div class="section-header__title section-header__title_1 h1"><?= $arParams['TITLE']; ?></div>
                <?
            } ?>
            <?
            if ($arParams['DESCRIPTION']) { ?>
                <div class="section-header__header row">
                    <div class="col-md-6 col-xl-4 col-xxl-3">
                        <div class="section-header__text subtitle"><?= $arParams['DESCRIPTION']; ?></div>
                    </div>
                </div>
                <?
            } ?>
        </div>
        <div class="b-catalog__list row g-0">
            <?
            foreach ($arResult['MAIN_SECTIONS'] as $section) { ?>
                <div class="b-catalog__item col-md-6 col-lg-4 col-xl-3">
                    <div class="container-fluid">
                        <div class="b-catalog__title"><?= $section['NAME'] ?>
                            <span><?= $section['UF_ELEMENT_COUNT'] ?></span></div>
                    </div>
                    <?
                    if ($section['INDEX_PICTURE']) { ?>
                        <picture class="b-catalog__img">
                            <img src="<?= $section['INDEX_PICTURE']; ?>" alt="">
                        </picture>
                        <?
                    } ?>
                    <div class="b-catalog__content">
                        <div class="b-catalog__inner container-fluid">
                            <div class="b-catalog__title"><?= $section['NAME'] ?>
                                <span><?= $section['UF_ELEMENT_COUNT'] ?></span></div>
                            <div class="b-catalog__category"><?= GetMessage('POPULAR_CATEGORIES') ?></div>
                            <div class="b-catalog__listChildren">
                                <?
                                foreach ($arResult['SUB_SECTIONS'][$section['ID']] as $subSection) { ?>
                                    <a href="<?= $subSection['SECTION_PAGE_URL']; ?>">
                                        <?= $subSection['NAME']; ?>
                                    </a>
                                    <?
                                } ?>
                            </div>
                            <a class="b-catalog__contentButton btn-decorate btn-decorate_lg"
                               href="<?= $section['SECTION_PAGE_URL']; ?>"><?= GetMessage('ALL_CATEGORIES') ?>
                                <span><?= count($arResult['SUB_SECTIONS'][$section['ID']]); ?></span>
                            </a>
                        </div>
                    </div>
                </div>
                <?
            } ?>
        </div>
        <div class="b-catalog__footer section-footer container-fluid">
            <a class="b-catalog__button btn-decorate btn-decorate_lg"
               href="/search/"><?= GetMessage('ALL_PRODUCTS') ?></a>
        </div>
    </div>
    <?
}
?>