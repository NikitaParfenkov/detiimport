<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    'TITLE' => array(
        'PARENT' => 'BASE',
        'NAME' => GetMessage('BLOCK_TITLE'),
        'TYPE' => 'STRING',
    ),
    'DESCRIPTION' => array(
        'PARENT' => 'BASE',
        'NAME' => GetMessage('BLOCK_DESCRIPTION'),
        'TYPE' => 'STRING',
    ),
);