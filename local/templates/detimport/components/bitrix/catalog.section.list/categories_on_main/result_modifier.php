<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['SECTIONS'] as $section) {
    if ($section['IBLOCK_SECTION_ID']) {
        $arResult['SUB_SECTIONS'][$section['IBLOCK_SECTION_ID']][] = $section;
    } else {
        if ($section['UF_INDEX_PICTURE']) {
            $section['INDEX_PICTURE'] = CFile::GetPath($section['UF_INDEX_PICTURE']);
        }
        $arResult['MAIN_SECTIONS'][] = $section;
    }
}

foreach ($arResult['SUB_SECTIONS'] as $group) {
    uasort($group, function ($a, $b) {
        if ($a['UF_SHOW_COUNTER'] == $b['UF_SHOW_COUNTER']) {
            return 0;
        }
        return ($a['UF_SHOW_COUNTER'] < $b['UF_SHOW_COUNTER']) ? -1 : 1;
    });
}
?>