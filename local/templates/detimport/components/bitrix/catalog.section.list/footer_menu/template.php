<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['SECTIONS']) {
    ?>
    <div class="col-sm-6 col-lg-4 col-xl-3">
        <div class="footer-top__nav">
            <div class="footer-top__navTitle"><?= GetMessage('CATALOG'); ?></div>
            <nav class="nav-footer nav-footer_1">
                <?
                foreach ($arResult['SECTIONS'] as $section) { ?>
                    <a class="nav-footer__item" href="<?= $section['SECTION_PAGE_URL']; ?>"><?= $section['NAME']; ?></a>
                <? } ?>
            </nav>
        </div>
    </div>
    <?
}
?>