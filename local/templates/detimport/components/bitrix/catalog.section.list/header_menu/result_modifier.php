<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['SECTIONS'] as $section) {
    if ($section['IBLOCK_SECTION_ID']) {
        $arResult['SUB_SECTIONS'][$section['IBLOCK_SECTION_ID']][] = $section;
    } else {
        if ($section['PICTURE']['ID']) {
            $section['RESIZED_PICTURE'] = CFile::ResizeImageGet($section['PICTURE']['ID'],
                ['width' => 88, 'height' => 72],
                BX_RESIZE_IMAGE_PROPORTIONAL
            )['src'];
        }
        $arResult['MAIN_SECTIONS'][] = $section;
    }
}

?>