<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['MAIN_SECTIONS']) {
    ?>
    <div class="mainNav__item is-parent">
        <a class="mainNav__link dropdown-toggle" href="javascript:void(0)"
           data-href="#dropdownMainMenuButton-0"><?= GetMessage('CATALOG'); ?></a>
        <div class="dropdown-main-menu-block" id="dropdownMainMenuButton-0">
            <div class="modal-dialog">
                <div class="mainNav-content">
                    <div class="mainNav-inner">
                        <div class="mainNav-wrap">
                            <? foreach ($arResult['MAIN_SECTIONS'] as $firstSection) { ?>
                                <div class="menu-intro">
                                    <? if ($firstSection['RESIZED_PICTURE']) { ?>
                                        <div class="menu-intro__img">
                                            <img src="<?= $firstSection['RESIZED_PICTURE']; ?>">
                                        </div>
                                    <?
                                    } ?>
                                    <div class="menu-intro__content">
                                        <a class="menu-intro__title has-dropdown"
                                           href="<?= $firstSection['SECTION_PAGE_URL']; ?>">
                                            <?= $firstSection['NAME']; ?>
                                            <span><?= $firstSection['ELEMENT_CNT']; ?></span>
                                        </a>
                                        <? if ($arResult['SUB_SECTIONS'][$firstSection['ID']]) { ?>
                                            <ul>
                                                <? foreach ($arResult['SUB_SECTIONS'][$firstSection['ID']] as $secondSection) { ?>
                                                    <li>
                                                        <a class="menu-intro__subtitle"
                                                           href="<?= $secondSection['SECTION_PAGE_URL'] ?>">
                                                            <?= $secondSection['NAME']; ?>
                                                        </a>
                                                        <? if ($arResult['SUB_SECTIONS'][$secondSection['ID']]) { ?>
                                                            <ul>
                                                                <? foreach ($arResult['SUB_SECTIONS'][$firstSection['ID']] as $thirdSection) { ?>
                                                                    <li>
                                                                        <a href="<?= $thirdSection['SECTION_PAGE_URL'] ?>"><?= $thirdSection['NAME']; ?></a>
                                                                    </li>
                                                                <?
                                                                } ?>
                                                            </ul>
                                                        <?
                                                        } ?>
                                                    </li>
                                                <?
                                                } ?>
                                            </ul>
                                        <?
                                        } ?>
                                    </div>
                                </div>
                            <?
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?
}
?>