<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    'FIRST_PART' => array(
        'PARENT' => 'BASE',
        'NAME' => GetMessage('FIRST_PART'),
        'TYPE' => 'STRING',
    ),
);