<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['SECTIONS']) {
	if (!$arParams['FIRST_PART']) {
        $arParams['FIRST_PART'] = 10;
	}
	$firstPart = array_slice($arResult['SECTIONS'], 0, $arParams['FIRST_PART']);
	$secondPart = array_slice($arResult['SECTIONS'], $arParams['FIRST_PART']);
    ?>
	<div class="nav-side nav-side_cats">
		<div class="nav-side__header">
			<div class="nav-side__title"><?= GetMessage('CATEGORIES') ?></div>
		</div>
		<div class="nav-side__content">
			<div class="nav-cats">
                <?foreach ($firstPart as $section) { ?>
					<a href="<?= $section['SECTION_PAGE_URL']; ?>" <?
                    if ($arParams['CUR_PAGE'] === $section['SECTION_PAGE_URL']){ ?>class="is-active"<?
                    } ?>><?= $section['NAME']; ?></a>
                <? } ?>
			</div>
			<?if ($secondPart) {?>
			<div class="nav-cats collapse" id="collapse-cats">
                <?foreach ($secondPart as $section) { ?>
					<a href="<?= $section['SECTION_PAGE_URL']; ?>" <?
                    if ($arParams['CUR_PAGE'] === $section['SECTION_PAGE_URL']){ ?>class="is-active"<?
                    } ?>><?= $section['NAME']; ?></a>
                <? } ?>
			</div>
			<?}?>
            <? if (count($arResult['SECTIONS']) > 10) { ?>
				<div class="btn btn-dark btn-sm rounded-pill has-arr button-more dropdown-toggle" data-toggle="collapse"
				     data-target="#collapse-cats" data-hide-title="<?= GetMessage('HIDE')?>">
					<span><?= GetMessage('SHOW_MORE') ?></span>
				</div>
            <? } ?>
		</div>
	</div>
<? } ?>