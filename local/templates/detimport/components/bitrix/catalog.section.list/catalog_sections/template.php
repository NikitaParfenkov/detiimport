<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['SECTIONS']) {
    $strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
    $strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
    $arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

    $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete,
        $arSectionDeleteParams);
    ?>
	<div class="b-plates row g-0">
        <?
        foreach ($arResult['SECTIONS'] as $section) {
            $this->AddEditAction($section['ID'], $section['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($section['ID'], $section['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            if ($section['PICTURE']['ID']) {
                $picture = CFile::ResizeImageGet($section['PICTURE']['ID'],
                    ['width' => 213, 'height' => 177],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'];
            } else {
                $picture = CATALOG_NO_PHOTO;
            }
            ?>
			<div class="col-6 col-xl-2" id="<? echo $this->GetEditAreaId($section['ID']); ?>">
				<a class="itemPlate" href="<?= $section['SECTION_PAGE_URL']; ?>">
				<span class="itemPlate__title">
					<?= $section['NAME'] ?>
					<span><?= $section['ELEMENT_CNT'] ?: '0'; ?></span>
				</span>
					<img class="itemPlate__img" src="<?= $picture; ?>" alt="" role="presentation"/>
				</a>
			</div>
            <?
        } ?>
	</div>
    <?
}
?>