<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!empty($arResult)) {
    ?>
	<div class="col-xl-3">
		<div data-move="page-menu-get">
			<div class="page-menu">
                <?
                foreach ($arResult as $arItem) {
                    if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
                        continue;
                    } ?>
                    <? if ($arItem["SELECTED"]) { ?>
						<a class="page-menu__item is-active" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <? } else { ?>
						<a class="page-menu__item" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    <? } ?>
                    <?
                } ?>
			</div>
		</div>
	</div>
<? } ?>

