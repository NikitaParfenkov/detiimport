<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<? if (!empty($arResult)) {
    $chunkedMenu = array_chunk($arResult, ceil(count($arResult) / 2));
    ?>
    <div class="col-sm-6 col-lg-4 col-xl-3 col-xxl-4">
        <div class="footer-top__nav">
            <div class="footer-top__navTitle"><?= GetMessage('INFORMATION') ?></div>
            <div class="row">
                <?
                foreach ($chunkedMenu as $group) {
                    ?>
                    <div class="col-xxl-6">
                        <nav class="nav-footer">
                            <?
                            foreach ($group as $arItem) {
                                if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
                                    continue;
                                } ?>
                                <a class="nav-footer__item" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                <?
                            } ?>
                        </nav>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
<? } ?>