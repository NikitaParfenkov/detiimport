<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<? if (!empty($arResult)): ?>
    <?
    $cnt = 1;
    $previousLevel = 0;
    foreach ($arResult as $arItem):
        ?>
        <?
        if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
            <? if ($previousLevel == 3) { ?>
                <?= str_repeat("</ul></form></div>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
            <? } else { ?>
                <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
            <? } ?>
        <? endif ?>

        <? if ($arItem["IS_PARENT"]): ?>

        <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
            <div class="mainNav__item">

            <div class="mainNav__link dropdown-toggle mainNav__toggle"
                 id="dropdownMenu-toggle-right<?= $arItem["LINK"] ?>"
                 role="button" data-toggle="dropdown" aria-expanded="false"><?= $arItem["TEXT"] ?>
            </div>

            <form class="dropdown-menu dropdown-menu-block dropdown-stop-close"
            aria-labelledby="dropdownMenu-toggle-right<?= $arItem["LINK"] ?>">
            <ul>

        <? else: ?>

            <li>
            <a class="dropdown-item dropdown-toggle"
               data-toggle="collapse" href="#collapse<?= $cnt ?>"
               role="button" aria-expanded="false"
               aria-controls="collapse<?= $cnt ?>"><?= $arItem["TEXT"] ?></a>
            <ul class="collapse" id="collapse<?= $cnt ?>">
            <? $cnt++; ?>

        <? endif ?>

    <? else: ?>

        <? if ($arItem["PERMISSION"] > "D"): ?>

            <? if ($arItem["DEPTH_LEVEL"] == 1): ?>

                <div class="mainNav__item">
                    <a class="mainNav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </div>

            <? else: ?>
                <? if ($arItem["DEPTH_LEVEL"] == 2): ?>
                    <li class="dropdown-item">
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? else: ?>
                    <li>
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endif ?>
            <? endif ?>

        <? else: ?>

            <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                <div class="mainNav__item">
                    <a class="mainNav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                </div>
            <? else: ?>
                <? if ($arItem["DEPTH_LEVEL"] == 2): ?>
                    <li class="dropdown-item">
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? else: ?>
                    <li>
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endif ?>

            <? endif ?>

        <? endif ?>

    <? endif ?>

        <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

    <? endforeach ?>

    <?
    if ($previousLevel > 1):
        ?>
        <? if ($previousLevel == 3) { ?>
        <?= str_repeat("</ul></form></div>", ($previousLevel - 2)); ?>
    <? } else { ?>
        <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <? } ?>
    <? endif ?>
<? endif ?>