<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<? if (!empty($arResult)): ?>
    <div class="mainNav__item mainNav__item mainNav__item_dropdown"
         data-move="main-nav-toggle-get">
        <div class="mainNav__toggle" id="dropdownMenu-toggle-right"
             role="button" data-toggle="dropdown" aria-expanded="false">
            <svg width="22" height="22" viewBox="0 0 22 10" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <rect x="22" y="2" width="22" height="2" rx="1"
                      transform="rotate(-180 22 2)"></rect>
                <rect x="22" y="6" width="22" height="2" rx="1"
                      transform="rotate(-180 22 6)"></rect>
                <rect x="22" y="10" width="22" height="2" rx="1"
                      transform="rotate(-180 22 10)"></rect>
            </svg>
        </div>
        <form class="dropdown-menu dropdown-menu-block dropdown-stop-close"
              aria-labelledby="dropdownMenu-toggle-right">
            <ul>
                <?
                $previousLevel = 0;
                $cnt = 1;
                foreach ($arResult

                as $arItem):
                ?>
                <?
                if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                    <?= str_repeat("</ul></form></div>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
                <? endif ?>

                <? if ($arItem["IS_PARENT"]): ?>

                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                <li>
                    <a class="dropdown-item dropdown-toggle"
                       data-toggle="collapse" href="#collapseExample<?= $cnt; ?>"
                       role="button" aria-expanded="false"
                       aria-controls="collapseExample<?= $cnt; ?>"><?= $arItem["TEXT"] ?></a>
                    <ul class="collapse" id="collapseExample<?= $cnt; ?>">

                        <? $cnt++; ?>
                        <? endif ?>

                        <? else: ?>

                            <? if ($arItem["PERMISSION"] > "D"): ?>

                                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>

                                    <li>
                                        <a class="dropdown-item"
                                           href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                    </li>

                                <? else: ?>
                                    <li>
                                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                    </li>
                                <? endif ?>

                            <? else: ?>

                                <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                    <li>
                                        <a class="dropdown-item"
                                           href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                    </li>
                                <? else: ?>
                                    <li>
                                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                    </li>
                                <? endif ?>

                            <? endif ?>

                        <? endif ?>

                        <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

                        <? endforeach ?>

                        <?
                        if ($previousLevel > 1):
                            ?>
                            <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
                        <? endif ?>
                    </ul>
        </form>
    </div>
<? endif ?>
