<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) { ?>
	<div class="<?= $arParams['CLASS'] ?: 'block-img-slider block-img-slider_offseNRight swiper-container'; ?>">
		<div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                if ($arItem['PREVIEW_PICTURE']['ID']) {
                    $picture = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'],
                        ['width' => 696, 'height' => 500],
                        BX_RESIZE_IMAGE_PROPORTIONAL
                    )['src'];
                }
                ?>
				<a class="swiper-slide"
                   <? if ($arItem['PROPERTIES']['URL']['VALUE']) { ?>href="<?= $arItem['PROPERTIES']['URL']['VALUE']; ?>"<? } ?>
				   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
					<div class="block-img2">
                        <? if ($picture) { ?>
							<div class="block-img2__img">
								<img src="<?= $picture; ?>">
							</div>
                        <? } ?>
						<div class="block-img2__inner">
							<div class="block-img2__inner2">
                                <? if ($arItem['PROPERTIES']['SALE']['VALUE']) { ?>
									<div class="block-img2__discount"><?= $arItem['PROPERTIES']['SALE']['VALUE']; ?></div>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['LOGO']['VALUE']) { ?>
									<div class="block-img2__logo">
										<img width="110" height="30"
										     src="<?= CFile::GetPath($arItem['PROPERTIES']['LOGO']['VALUE']); ?>">
									</div>
                                <? } ?>
								<div class="block-img2__content">
									<div class="block-img2__title" style="color: #FF5000"><?= $arItem['NAME']; ?></div>
                                    <? if ($arItem['~PREVIEW_TEXT']) { ?>
										<div class="block-img2__text"><?= $arItem['~PREVIEW_TEXT']; ?></div>
                                    <? } ?>
                                    <? if ($arItem['~DETAIL_TEXT']) { ?>
										<div class="block-img2__promo"
										     style="background-color: #FF5000"><?= $arItem['~DETAIL_TEXT']; ?></div>
                                    <? } ?>
								</div>
							</div>
						</div>
					</div>
				</a>
            <? } ?>
		</div>
		<div class="swiper-pagination"></div>
	</div>

<? } ?>