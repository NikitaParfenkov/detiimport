<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) { ?>
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
            ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
        ?>
		<div class="col-md-6 col-lg-12" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<div class="footer-top__smitem">
				<div class="footer-top__smitemTitle"><?= $arItem['NAME'];?></div>
				<?if ($arItem['PROPERTIES']['ADDRESS']['VALUE']) {?>
					<div><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'];?></div>
				<?}?>
				<?
				$data = [];
                if ($arItem['PROPERTIES']['PHONE']['VALUE']) {
                    $data = array_merge($data, $arItem['PROPERTIES']['PHONE']['VALUE']);
                }
                if ($arItem['PROPERTIES']['EMAIL']['VALUE']) {
                    $data = array_merge($data, $arItem['PROPERTIES']['EMAIL']['VALUE']);
                }
				if ($data && $arItem['PROPERTIES']['MAIN_CONTACT']['VALUE'] !== 'Y') {
					?>
					<div><?= implode(' <br> ', $data);?></div>
				<?}?>
			</div>
			<div class="footer-top__social-phone">
				<div class="social">
                    <? if ($arItem['PROPERTIES']['TELEGRAM']['VALUE']) { ?>
						<a class="social__telegram" href="<?= $arItem['PROPERTIES']['TELEGRAM']['VALUE']; ?>" target="_blank">
							<svg>
								<use xlink:href="#telegram"></use>
							</svg>
						</a>
                    <? } ?>
                    <? if ($arItem['PROPERTIES']['VIBER']['VALUE']) { ?>
						<a class="social__viber" href="<?= $arItem['PROPERTIES']['VIBER']['VALUE']; ?>" target="_blank">
							<svg>
								<use xlink:href="#viber"></use>
							</svg>
						</a>
                    <? } ?>
                    <? if ($arItem['PROPERTIES']['WHATSAPP']['VALUE']) { ?>
						<a class="social__whatsapp" href="<?= $arItem['PROPERTIES']['WHATSAPP']['VALUE']; ?>" target="_blank">
							<svg>
								<use xlink:href="#whatsapp"></use>
							</svg>
						</a>
                    <? } ?>
				</div>
				<?if ($arItem['PROPERTIES']['MAIN_CONTACT']['VALUE'] === 'Y' && $arItem['PROPERTIES']['PHONE']['VALUE'][0]) {?>
				<a class="footer-top__phone" href="tel:+<?= preg_replace('/D', '',
                    $arItem['PROPERTIES']['PHONE']['VALUE'][0]); ?>"><?= $arItem['PROPERTIES']['PHONE']['VALUE'][0]; ?></a>
				<?}?>
			</div>
		</div>
    <? } ?>
<? } ?>