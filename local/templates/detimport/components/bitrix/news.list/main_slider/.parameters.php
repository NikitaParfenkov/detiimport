<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    "BRAND_URL_TEXT" => array(
        "NAME" => GetMessage('BRAND_URL_TEXT'),
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
);
?>
