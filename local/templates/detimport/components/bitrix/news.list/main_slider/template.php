<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
CModule::IncludeModule('sp.tools');
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$itemsCnt = count($arResult['ITEMS']);
if ($itemsCnt > 0) {
    $brandHandler = new \SP\Tools\Brands();
    ?>
	<div class="index-top container-fluid" data-equal-window-height>
	<div class="index-top__inner row g-0">
	<div class="col-xl-9">
		<div class="index-top__left">
			<div class="sliderMain">
				<div class="sliderMain__inner swiper-container">
					<div class="swiper-wrapper">
                        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                            if ($arParams['IS_MOBILE'] && $arItem['PROPERTIES']['MOBILE_IMAGE']['VALUE']) {
                                $picID = $arItem['PROPERTIES']['MOBILE_IMAGE']['VALUE'];
                            } else {
                                $picID = $arItem['PROPERTIES']['PC_IMAGE']['VALUE'];
                            }
                            $picture = CFile::GetPath($picID);

                            ?>
							<div class="swiper-slide" data-theme="white"
							     id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
								<div class="slide-inner">
									<picture class="sliderMain__img">
										<img src="<?= $picture; ?>" alt="">
									</picture>
									<div class="sliderMain__content">
										<div class="container-fluid">
                                            <? if ($arItem['PROPERTIES']['PRETITLE']['VALUE']) { ?>
												<div class="sliderMain__subtitle"><?= $arItem['PROPERTIES']['PRETITLE']['VALUE']; ?></div>
                                            <? } ?>
											<div class="sliderMain__title h1"><?= htmlspecialchars_decode($arItem['NAME']); ?></div>
                                            <? if ($arItem['PROPERTIES']['URL']['VALUE'] && $arItem['PROPERTIES']['URL_TEXT']['VALUE']) { ?>
												<div class="sliderMain__button">
													<a class="btn-decorate btn-decorate_lg"
													   href="<?= $arItem['PROPERTIES']['URL']['VALUE']; ?>"><?= $arItem['PROPERTIES']['URL_TEXT']['VALUE']; ?></a>
												</div>
                                            <? } ?>
										</div>
									</div>
								</div>
							</div>
                        <? } ?>
					</div>
                    <? if ($itemsCnt > 1) { ?>
						<div class="sliderMain__navi container-fluid">
							<div class="swiper-pagination"></div>
							<div class="swiper-navigation">
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
                    <? } ?>
				</div>
			</div>
		</div>
	</div>
<? } ?>