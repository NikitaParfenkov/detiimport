<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['ITEMS'] as $arItem) {
    if ($arItem['PROPERTIES']['PRODUCTS']['VALUE']) {
        $productsID[] = $arItem['PROPERTIES']['PRODUCTS']['VALUE'];
    }
}
$cp = $this->__component;
if (is_object($cp))
{
    $cp->arResult["PRODUCTS_ID"] = $productsID;
    $cp->arResult["ELEMENTS_COUNT"] = count($arResult['ITEMS']);
    $cp->SetResultCacheKeys(array("PRODUCTS_ID", 'ELEMENTS_COUNT'));
}
