<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (!empty($arResult['ITEMS'])) { ?>
	<div class="alphabet-menu row wrapper">
		<div class="alphabet-menu__side col-xl-3">
			<span class="alphabet-menu__side-title"><?= GetMessage('TITLE'); ?></span>
		</div>
		<div class="alphabet-menu__content col-xl-9">
            <? foreach ($arResult['LANG'] as $lang => $items) { ?>
				<div class="alphabet-menu__content-line">
					<span class="alphabet-menu__content-line-name"><?= $lang; ?></span>
					<ul class="alphabet-menu__list">
                        <? foreach ($items as $letter => $arItems) { ?>
							<li class="alphabet-menu__item">
								<a class="alphabet-menu__link" href="#letter_<?= $letter; ?>"><?= $letter; ?></a>
							</li>
                        <? } ?>
					</ul>
				</div>
            <? } ?>
		</div>
	</div>
    <? foreach ($arResult['LANG'] as $lang => $items) { ?>
		<div class="alphabet-section wrapper row g-0">
			<div class="alphabet-section__side col-xl-3">
				<span class="alphabet-section__side-title"><?= $lang; ?></span>
			</div>
			<div class="alphabet-section__content col-xl-9">
                <? foreach ($items as $letter => $arItems) { ?>
					<div class="alphabet-section__content-item" id="letter_<?= $letter; ?>">
						<span class="alphabet-section__char-title"><?= $letter; ?></span>
						<div class="dropdown-menu show alphabet-section__dropdown">
							<ul>
                                <? foreach ($arItems as $arItem) { ?>
                                    <?
                                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                                        ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                                    ?>
                                    <? if ($arResult['BRAND_SECTIONS'][$arItem['ID']]) { ?>
										<li class="text-md" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
											<a class="alphabet-section__dropdown-item dropdown-item dropdown-toggle collapsed"
											   href="#collapseExample<?= $arItem['ID']; ?>" data-toggle="collapse"
											   role="button"
											   aria-expanded="false"
											   aria-controls="collapseExample"><?= $arItem['NAME']; ?></a>
											<ul class="alphabet-section__nested-list collapse"
											    id="collapseExample<?= $arItem['ID']; ?>">
                                                <? foreach ($arResult['BRAND_SECTIONS'][$arItem['ID']] as $sectionID) { ?>
													<li class="alphabet-section__nested-item">
														<a href="<?= $arItem['DETAIL_PAGE_URL']?>?section=<?= $sectionID;?>"><?= $arResult['SECTIONS_NAME'][$sectionID]['NAME']; ?></a>
													</li>
                                                <? } ?>
											</ul>
										</li>
                                    <? } ?>
                                <? } ?>
							</ul>
						</div>
					</div>
                <? } ?>
			</div>
		</div>
    <? } ?>
<? } ?>