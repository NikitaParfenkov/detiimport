<?php

use SP\Tools\Brands;
use SP\Tools\Catalog;

CModule::IncludeModule('sp.tools');

foreach ($arResult['ITEMS'] as $arItem) {
    if (preg_match("/[а-яё]/iu", $arItem['NAME'])) {
        $arResult['LANG']['Ру'][mb_substr($arItem['NAME'], 0, 1)][] = $arItem;
    } else {
        $arResult['LANG']['En'][mb_substr($arItem['NAME'], 0, 1)][] = $arItem;
    }
    $brands[] = $arItem['ID'];
}


if ($brands) {
    $arResult['BRANDS_HANDLER'] = new Brands();
    $arResult['BRAND_SECTIONS'] = $arResult['BRANDS_HANDLER']->getCatalogItemsByBrand($brands);
    if ($arResult['BRAND_SECTIONS']) {
        $sectionsID = [];
        foreach ($arResult['BRAND_SECTIONS'] as $sections) {
            $sectionsID = array_merge($sectionsID, $sections);
        }
        if ($sectionsID) {
            $catalogHandler = new Catalog();
            $filter = ['IBLOCK_ID' => IBLOCK_CATALOG, 'ID' => $sectionsID];
            $select = ['ID', 'NAME', 'SECTION_PAGE_URL'];
            $arResult['SECTIONS_NAME'] = $catalogHandler->getSectionsInfo($sectionsID, $select);
        }
    }
    foreach ($arResult['LANG'] as $lang => $items) {
        foreach ($items as $letter => $arItems) {
            $isEmpty = true;
            foreach ($arItems as $arItem) {
                if ($arResult['BRAND_SECTIONS'][$arItem['ID']]) {
                    $isEmpty = false;
                }
            }
            if ($isEmpty) {
                unset($arResult['LANG'][$lang][$letter]);
            }
        }
    }
}

