<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['ITEMS'] as $arItem) {
    $brandsID[$arItem['ID']] = $arItem['ID'];
}

if ($brandsID) {
    $arSelect = Array("ID", 'PROPERTY_BRAND');
    $arFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG, "ACTIVE" => "Y", "PROPERTY_BRAND" => $brandsID);
    $res = CIBlockElement::GetList(Array(), $arFilter, ['PROPERTY_BRAND'], false, $arSelect);
    while ($ob = $res->Fetch()) {
        $arResult['BRAND_ITEMS'][$ob['PROPERTY_BRAND_VALUE']] = $ob['CNT'];
    }
}
