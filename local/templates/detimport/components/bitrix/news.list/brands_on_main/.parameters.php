<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    "TITLE" => array(
        "NAME" => GetMessage('TITLE'),
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
    "DESCRIPTION" => array(
        "NAME" => GetMessage('DESCRIPTION'),
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
    "ALL_BRANDS" => array(
        "NAME" => GetMessage('ALL_BRANDS'),
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
    "BACKGROUND_PICTURE" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage('BACKGROUND_PICTURE'),
        "TYPE" => "FILE",
        "FD_TARGET" => "F",
        "FD_UPLOAD" => true,
        "FD_USE_MEDIALIB" => true,
    ),
);
?>
