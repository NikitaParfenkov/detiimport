<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
CModule::IncludeModule('sp.tools');
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$itemsCnt = count($arResult['ITEMS']);
if ($itemsCnt > 0) {
    $brandHandler = new \SP\Tools\Brands();
    ?>
	<div class="b-brands section">
		<div class="section-header section-header_main section-header_brands section-header_padding-top container-fluid"
             <? if ($arParams['BACKGROUND_PICTURE']) { ?>style="background-image: url(<?= $arParams['BACKGROUND_PICTURE']; ?>);" <? } ?>>
            <? if ($arParams['TITLE']) { ?>
				<div class="section-header__title section-header__title_1 h1"><?= $arParams['TITLE']; ?></div>
            <? } ?>
			<div class="section-header__header row">
                <? if ($arParams['DESCRIPTION']) { ?>
					<div class="col-md-6 col-xl-4 col-xxl-3">
						<div class="section-header__text subtitle"><?= $arParams['DESCRIPTION']; ?></div>
					</div>
                <? } ?>
                <? if ($itemsCnt > 4) { ?>
					<div class="col">
						<div class="section-header__navi" data-move="section-navi-get">
							<div class="swiper-pagination"></div>
							<div class="swiper-navigation swiper-navigation-parent">
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
                <? } ?>
			</div>
		</div>
		<div class="b-brands__wrapper">
			<div class="b-brands__slider swiper-container">
				<div class="swiper-wrapper">
                    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                            ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                        if ($arItem['PROPERTIES']['PICTURE']['VALUE']) {
                            $picture = CFile::GetPath($arItem['PROPERTIES']['PICTURE']['VALUE']);
                        } else {
                            $picture = BRAND_NO_PHOTO;
                        }

                        ?>
						<a class="b-brands__item swiper-slide"
						   href="<?= $arItem['DETAIL_PAGE_URL']; ?>"
						   id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
							<div class="container-fluid">
								<span class="b-brands__title">
									<?= $arItem['NAME']; ?>
                                    <? if ($arResult['BRAND_ITEMS'][$arItem['ID']]) { ?>
										<span><?= $arResult['BRAND_ITEMS'][$arItem['ID']]; ?></span>
                                    <? } ?>
								</span>
							</div>
							<picture class="b-brands__img">
								<img src="<?= $picture; ?>" alt="">
							</picture>
							<span class="b-brands__content">
								<span class="b-brands__inner container-fluid">
									<span class="b-brands__title">
										<?= $arItem['NAME']; ?>
                                        <? if ($arResult['BRAND_ITEMS'][$arItem['ID']]) { ?>
											<span><?= $arResult['BRAND_ITEMS'][$arItem['ID']]; ?></span>
                                        <? } ?>
									</span>
									<span class="b-brands__text"><?= $arItem['PREVIEW_TEXT']; ?></span>
								</span>
							</span>
						</a>
                    <? } ?>
				</div>
			</div>
		</div>
		<div class="b-brands__footer section-footer container-fluid">
			<div data-move="section-navi-set"></div>
			<a class="b-brands__button btn-decorate btn-decorate_lg"
               <? if ($arParams['ALL_BRANDS']) { ?>href="<?= $arParams['ALL_BRANDS']; ?>"<? } ?>>
                <?= GetMessage('ALL_BRANDS'); ?>
			</a>
		</div>
	</div>
<? } ?>