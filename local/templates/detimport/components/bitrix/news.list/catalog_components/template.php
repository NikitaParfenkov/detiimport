<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
    $chunked = array_chunk($arResult['ITEMS'], 2); ?>
    <? foreach ($chunked as $group) { ?>
		<div class="row <? if (count($group) == 2) { ?>gx-xl-5 gx-xxl-0<? } ?> item-character-wrapper">
            <? foreach ($group as $key => $arItem) { ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                ?>
				<div class="col-xl">
					<div class="item-character">
						<div class="text-md mb-5"><?= $arItem['NAME']; ?></div>
                        <? if ($arItem['PROPERTIES']['IMAGES']['VALUE']) { ?>
							<div class="swiper-container">
								<div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $pictureID) {
                                        $picture = CFile::ResizeImageGet(
                                            $pictureID,
                                            ['width' => 268, 'height' => 221],
                                            BX_RESIZE_IMAGE_
                                        )['src'];
                                        ?>
										<div class="swiper-slide">
											<picture class="item-character__img">
												<img src="<?= $picture; ?>">
											</picture>
										</div>
                                    <? } ?>
								</div>
							</div>
                        <? } ?>
					</div>
                    <?= $arItem['~PREVIEW_TEXT']; ?>
				</div>
                <? if ($key == 0) { ?>
					<div class="col-1 d-none d-xxl-block"></div>
                <? } ?>
            <? } ?>
		</div>
    <? } ?>
<? } ?>