<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    "SLIDER_TITLE" => array(
        "NAME" => 'Заголовок слайдера',
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
    "SLIDER_URL_TEXT" => array(
        "NAME" => 'Текст ссылки перехода со слайдера',
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
    "SLIDER_URL" => array(
        "NAME" => 'Адрес ссылки перехода со слайдера',
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "PARENT" => "BASE",
    ),
);
?>
