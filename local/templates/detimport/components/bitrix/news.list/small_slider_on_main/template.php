<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
ob_start();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 1) { ?>

    <div class="itemsTwo">
        <? if ($arParams['SLIDER_TITLE']) { ?>
            <div class="container-fluid">
                <div class="itemsTwo__mainTitle"><?= $arParams['SLIDER_TITLE']; ?></div>
            </div>
        <?
        } ?>
        <div class="itemsTwo__slider swiper-container">
            <div class="swiper-wrapper">
                <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                    if ($arParams['IS_MOBILE'] && $arItem['PROPERTIES']['MOBILE_SLIDE']['VALUE']) {
                        $picID = $arItem['PROPERTIES']['MOBILE_SLIDE']['VALUE'];
                    } else {
                        $picID = $arItem['PROPERTIES']['PC_SLIDE']['VALUE'];
                    }
                    $picture = CFile::GetPath($picID);

                    ?>
                    <div class="itemsTwo__item swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <picture class="itemsTwo__img">
                            <img src="<?= $picture; ?>">
                        </picture>
                        <div class="container-fluid">
                            #CATALOG_ELEMENT_<?= $arItem['PROPERTIES']['PRODUCT']['VALUE']; ?>#
                        </div>
                    </div>
                <? } ?>
            </div>
            <? if (count($arResult['ITEMS']) > 2) { ?>
                <div class="itemsTwo__arrows container-fluid">
                    <div class="swiper-pagination"></div>
                    <div class="swiper-navigation">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            <?
            } ?>
        </div>
        <? if ($arParams['SLIDER_URL'] && $arParams['SLIDER_URL_TEXT']) { ?>
            <div class="itemsTwo__buttonWrapper container-fluid">
                <a class="itemsTwo__button btn-decorate btn-decorate_lg"
                   href="<?= $arParams['SLIDER_URL']; ?>"><?= $arParams['SLIDER_URL_TEXT']; ?></a>
            </div>
        <?
        } ?>
    </div>

<? }
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();
?>