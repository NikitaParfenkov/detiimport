<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$cp = $this->__component;

foreach ($arResult['ITEMS'] as $arItem) {
    $products[$arItem['PROPERTIES']['PRODUCT']['VALUE']] = $arItem['PROPERTIES']['PRODUCT']['VALUE'];
}

if (is_object($cp)) {
    $cp->arResult["PRODUCTS"] = $products;
    $this->__component->SetResultCacheKeys(array("CACHED_TPL", "PRODUCTS"));
}