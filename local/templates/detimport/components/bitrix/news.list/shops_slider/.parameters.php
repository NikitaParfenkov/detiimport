<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"TITLE" => Array(
		"NAME" => GetMessage("TITLE"),
		"TYPE" => "STRING",
		"PARENT" => "BASE",
	),
	"DESCRIPTION" => Array(
		"NAME" => GetMessage("DESCRIPTION"),
		"TYPE" => "STRING",
		"PARENT" => "BASE",
	),
	"BTN_TEXT" => Array(
		"NAME" => GetMessage("BTN_TEXT"),
		"TYPE" => "STRING",
		"PARENT" => "BASE",
	),
	"BTN_URL" => Array(
		"NAME" => GetMessage("BTN_URL"),
		"TYPE" => "STRING",
		"PARENT" => "BASE",
	),
);
?>
