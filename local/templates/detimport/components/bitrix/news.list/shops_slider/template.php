<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$itemsCnt = count($arResult['ITEMS']);
?>
<? if ($itemsCnt > 1) { ?>
    <div class="info-card section">
        <div class="section-header section-header_main section-header_padding-top container-fluid">
            <div class="section-header__title section-header__title_1 h1"><?= $arParams['TITLE'] ?></div>
            <div class="section-header__header row">
                <div class="col-md-6 col-xl-4 col-xxl-3">
                    <div class="section-header__text subtitle"><?= $arParams['DESCRIPTION'] ?></div>
                </div>
                <div class="col">
                    <? if ($itemsCnt > 2) { ?>
                        <div class="section-header__navi" data-move="section-navi-get">
                            <div class="swiper-pagination"></div>
                            <div class="swiper-navigation swiper-navigation-parent">
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
        <div class="info-card__wrapper">
            <div class="info-card__slider swiper-container">
                <div class="swiper-wrapper">
                    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                            array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <? if ($arItem['PREVIEW_PICTURE']) { ?>
		                        <picture class="info-card__img">
			                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>">
		                        </picture>
                            <? } ?>
	                        <div class="info-card__labels">
                            <? if ($arItem['PROPERTIES']['USER_CHOICE']['VALUE'] == 'Y') { ?>
                                <div class="info-card__yandex-choice">
                                    <svg width="20" height="12" fill="#fff">
                                        <use xlink:href="#star"></use>
                                    </svg>
                                    <?= $arItem['PROPERTIES']['USER_CHOICE']['NAME'] ?>
                                </div>
                            <? } ?>
                            <? if ($arItem['PROPERTIES']['PARTNER_SHOP']['VALUE'] == 'Y') { ?>
                                <div class="info-card__partner">
                                    <svg width="20" height="12">
                                        <use xlink:href="#users"></use>
                                    </svg>
                                    <?= $arItem['PROPERTIES']['PARTNER_SHOP']['NAME'] ?>
                                </div>
                            <? } ?>
	                        </div>
                            <a class="info-card__content match-height" href="/shops/?city=<?= $arItem['PROPERTIES']['CITY']['VALUE'];?>#shop<?= $arItem['ID'];?>">
                                <div class="container-fluid">
                                    <div class="info-card__inner">
                                        <? if ($arItem['PROPERTIES']['CITY']['VALUE']) { ?>
                                            <div class="info-card__sity">
                                                <svg width="10" height="12">
                                                    <use xlink:href="#location"></use>
                                                </svg>
                                                <?= $arItem['PROPERTIES']['CITY']['VALUE'] ?>
                                            </div>
                                        <? } ?>
                                        <div class="info-card__title"><?= $arItem['~NAME'] ?></div>
                                        <? if ($arItem['PROPERTIES']['METRO']['VALUE']) { ?>
                                            <div class="info-card__metro">
                                                <? foreach ($arItem['PROPERTIES']['METRO']['VALUE'] as $key => $underground) { ?>
                                                    <div class="info-card__metroItem">
                                                        <svg width="16" height="10"
                                                             fill="<?= ($arItem['PROPERTIES']['METRO']['DESCRIPTION'][$key]) ?: '#a1a2a3' ?>">
                                                            <use xlink:href="#metro"></use>
                                                        </svg>
                                                        <?= $underground ?>
                                                    </div>
                                                <? } ?>
                                            </div>
                                        <? } ?>
                                        <? if ($arItem['PROPERTIES']['ADDRESS']['VALUE']) { ?>
                                            <div class="info-card__address"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></div>
                                        <? } ?>
                                        <div class="info-card__contentHide">
                                            <? if ($arItem['PROPERTIES']['CONTACTS']['VALUE']) { ?>
                                                <div class="info-card__contact">
                                                    <?= implode('<br>', $arItem['PROPERTIES']['CONTACTS']['VALUE']) ?>
                                                </div>
                                            <? } ?>
                                            <? if ($arItem['PROPERTIES']['WORK_TIME']['VALUE']) { ?>
                                                <div class="info-card__days">
                                                    <span><?= $arItem['PROPERTIES']['WORK_TIME']['NAME'] ?>:</span>
                                                    <?= implode('<br>', $arItem['PROPERTIES']['WORK_TIME']['VALUE']) ?>
                                                </div>
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
        <? if ($arParams['BTN_URL']) { ?>
            <div class="info-card__footer section-footer container-fluid">
                <div data-move="section-navi-set"></div>
                <a class="info-card__button btn-decorate btn-decorate_lg"
                   href="<?= $arParams['BTN_URL'] ?>"><?= $arParams['BTN_TEXT'] ?: GetMessage('DEFAULT_BTN_NAME') ?></a>
            </div>
        <? } ?>
    </div>
<? } ?>