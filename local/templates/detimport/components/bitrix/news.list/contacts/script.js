$(function () {

    if (typeof contactsMap !== 'undefined') {
        ymaps.ready(init);
    }

    function init() {
        var myMap = new ymaps.Map("contact-map", {
            center: [parseFloat(contactsMap[0]['LAN']), parseFloat(contactsMap[0]['LOT'])],
            zoom: 9
        });

        for (let key in contactsMap) {
            let myPlacemark = new ymaps.Placemark([parseFloat(contactsMap[key]['LAN']), parseFloat(contactsMap[key]['LOT'])], {}, {
                iconLayout: "default#image",
                iconImageHref: '/local/templates/detimport/images/mapMarker.svg',
                iconImageSize: [100, 100],
                iconImageOffset: [-50, -100]
            });
            myMap.geoObjects.add(myPlacemark);
        }
    }
});