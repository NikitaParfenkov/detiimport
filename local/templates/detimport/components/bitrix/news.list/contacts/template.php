<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0) { ?>
	<div class="page-inner">
		<div class="row g-0 justify-content-end">
			<div class="col-xl-11">
				<div class="row gx-6">
                    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                            ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                        if ($arItem['PROPERTIES']['MAIN_CONTACT']['VALUE'] === 'Y') {
                            $additionalDescription = $arItem['~PREVIEW_TEXT'];
                        }
                        if ($arItem['PROPERTIES']['COORDINATES']['VALUE']) {
                            $coord = explode(',', $arItem['PROPERTIES']['COORDINATES']['VALUE']);
                            $coordinates[] = ['LAN' => $coord[0], 'LOT' => $coord[1]];
                        }
                        ?>
						<div class="col-xl mb-5" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
							<div class="title-reg"><?= $arItem['NAME']; ?></div>
							<table class="table-contact">
								<tbody>
                                <? if ($arItem['PROPERTIES']['ADDRESS']['VALUE']) { ?>
									<tr>
										<td><?= $arItem['PROPERTIES']['ADDRESS']['NAME']; ?>:</td>
										<td><?= $arItem['PROPERTIES']['ADDRESS']['VALUE']; ?></td>
									</tr>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['PHONE']['VALUE']) { ?>
									<tr>
										<td><?= $arItem['PROPERTIES']['PHONE']['NAME']; ?>:</td>
										<td><?= htmlspecialchars_decode(implode(' <br> ',
                                                $arItem['PROPERTIES']['PHONE']['VALUE'])); ?></td>
									</tr>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['EMAIL']['VALUE']) { ?>
									<tr>
										<td><?= $arItem['PROPERTIES']['EMAIL']['NAME'] ?>:</td>
										<td><?= htmlspecialchars_decode(implode(' <br> ',
                                                $arItem['PROPERTIES']['EMAIL']['VALUE'])); ?></td>
									</tr>
                                <? } ?>
								</tbody>
							</table>
                            <? if ($arItem['PROPERTIES']['SECOND_BLOCK_NAME']['VALUE']) { ?>
								<div class="mt-5">
									<div class="h3"><?= $arItem['PROPERTIES']['SECOND_BLOCK_NAME']['VALUE']; ?></div>
									<div class="col-xl">
										<table class="table-contact">
											<tbody>
                                            <? if ($arItem['PROPERTIES']['SECOND_BLOCK_PHONE']['VALUE']) { ?>
												<tr>
													<td><?= $arItem['PROPERTIES']['SECOND_BLOCK_PHONE']['NAME']; ?>:
													</td>
													<td><?= htmlspecialchars_decode(implode(' <br> ',
                                                            $arItem['PROPERTIES']['SECOND_BLOCK_PHONE']['VALUE'])); ?></td>
												</tr>
                                            <? } ?>
                                            <? if ($arItem['PROPERTIES']['SECOND_BLOCK_EMAIL']['VALUE']) { ?>
												<tr>
													<td><?= $arItem['PROPERTIES']['SECOND_BLOCK_EMAIL']['NAME']; ?>:
													</td>
													<td><?= htmlspecialchars_decode(implode(' <br> ',
                                                            $arItem['PROPERTIES']['SECOND_BLOCK_EMAIL']['VALUE'])); ?></td>
												</tr>
                                            <? } ?>
											</tbody>
										</table>
									</div>
								</div>
                            <? } ?>
						</div>
                    <? } ?>
				</div>
                <? if ($additionalDescription) { ?>
					<div class="row gx-6 mb-5">
                        <?= $additionalDescription; ?>
					</div>
                <? } ?>
			</div>
		</div>
	</div>
    <? if ($coordinates) { ?>
		<script>
            let contactsMap = <?=CUtil::PhpToJSObject($coordinates)?>;
		</script>
		<div id="contact-map"></div>
    <? } ?>
<? } ?>