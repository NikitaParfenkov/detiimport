<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */


$actions = [
    'FAVORITES' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('FAV_TEXT'),
    ],
    'COMPARE' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('COMP_TEXT'),
    ],
];

if (in_array($item['ID'], $_SESSION['FAVORITES'])) {
    $actions['FAVORITES']['ACTION'] = 'compfavdelete';
    $actions['FAVORITES']['CLASS'] = 'is-active';
    $actions['FAVORITES']['TITLE'] = '';
}

if (in_array($item['ID'], $_SESSION['COMPARE'])) {
    $actions['COMPARE']['ACTION'] = 'compfavdelete';
    $actions['COMPARE']['CLASS'] = 'is-active';
    $actions['COMPARE']['TITLE'] = '';
}

$endingPosition = ceil(strlen($item['SHOW_COUNTER']) / 3);
$numberEndings = [
    1 => '',
    2 => 'k',
    3 => 'kk',
    4 => 'kkk',
];
if ($item['SHOW_COUNTER']) {
    $num = floor(strlen($item['SHOW_COUNTER']));
    $roundedShowCounter = round($item['SHOW_COUNTER'] / floor(strlen($item['SHOW_COUNTER']) / 3),
            1) . $numberEndings[$num];
}
?>
<div class="recommend-single" id="<?= $areaId ?>">
	<div class="recommend-single__inner container-fluid">
		<div class="recommend-single__left">
			<div class="recommend-single__logo"><?= GetMessage('RECOMMENDED') ?></div>
			<div class="recommend-single__stars">
				<div class="svg-stars svg-stars_undefined"></div>
                <? if ($roundedShowCounter) { ?>
					<span><?= $roundedShowCounter; ?></span>
                <? } ?>
			</div>
			<a class="recommend-single__title" href="<?= $item['DETAIL_PAGE_URL']; ?>"><?= $item['NAME']; ?></a>
			<div class="recommend-single__contentMob" data-move="recommend-single-set"></div>
            <? if ($item['PROPERTIES']['TICKER_TEXT']['VALUE']) { ?>
				<div class="recommend-single__opts js js_marquee_wrap">
					<div class="recommend-single__opts-marquee js js_marquee">
                        <? foreach ($item['PROPERTIES']['TICKER_TEXT']['VALUE'] as $phrase) { ?>
							<span><?= $phrase; ?></span>
                        <? } ?>
					</div>
				</div>
            <? } ?>
            <? if ($item['PROPERTIES']['CML2_ARTICLE']['VALUE']) { ?>
				<div class="recommend-single__articul">
                    <?= $item['PROPERTIES']['CML2_ARTICLE']['NAME']; ?>
					<span><?= $item['PROPERTIES']['CML2_ARTICLE']['VALUE']; ?></span>
				</div>
            <? } ?>

			<div class="recommend-single__bottom">
                <? if ($item['OFFERS']) { ?>
					<div class="recommend-single__colors">
						<div class="b-colors">
							<div class="b-colors__list">
                                <?
                                $colorsCnt = 0;
                                foreach ($item['OFFERS'] as $offer) {
                                    if ($offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'] && $arParams['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]) {
                                        $colorsCnt++;
                                        ?>
										<div class="b-colors__item"
										     style="background-color: <?= $arParams['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]; ?>"></div>
                                        <?
                                    }
                                } ?>
							</div>
                            <? if ($colorsCnt > 5) { ?>
								<div class="b-colors__more">+<?= $colorsCnt - 5; ?></div>
                            <? } ?>
						</div>
					</div>
                <? } ?>
				<div class="recommend-single__price">
                    <? if ($actualItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']) { ?>
						<div class="recommend-single__priceOld"><?= $actualItem['MIN_PRICE']['PRINT_VALUE']; ?></div>
                    <? } ?>
					<div class="recommend-single__priceNew"><?= $actualItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
				</div>
			</div>
			<div class="recommend-single__footer">
				<div class="recommend-single__button recommend-single__button_fast sp-fast-view"
				     data-id="<?= $item['ID']; ?>">
					<svg>
						<use xlink:href="#eye"></use>
					</svg>
                    <?= GetMessage('FAST_VIEW'); ?>
				</div>
				<div class="recommend-single__button recommend-single__button_cart" data-class="basket"
				     data-method="add2basket"
				     data-id="<?= $actualItem['ID']; ?>">
					<svg>
						<use xlink:href="#cart"></use>
					</svg>
                    <?= GetMessage('ADD_TO_BASKET'); ?>
				</div>
			</div>
		</div>
		<div class="recommend-single__right">
			<div data-move="recommend-single-get">
				<!--<div class="recommend-single__optionsWrap container-fluid">
					<div class="recommend-single__options">
						<div class="recommend-single__option recommend-single__option_compare <? /*= $actions['COMPARE']['CLASS']; */ ?>"
						     data-toggle="tooltip"
						     data-placement="bottom"
						     title="<? /*= $actions['COMPARE']['TITLE']; */ ?>"
						     data-class="tools"
						     data-method="<? /*= $actions['COMPARE']['ACTION']; */ ?>" data-id="<? /*= $item['ID'] */ ?>"
						     data-add="COMPARE">
							<svg>
								<use xlink:href="#compare"></use>
							</svg>
						</div>
						<div class="recommend-single__option recommend-single__option_favorites <? /*= $actions['FAVORITES']['CLASS']; */ ?>"
						     data-toggle="tooltip"
						     data-placement="bottom"
						     title="<? /*= $actions['FAVORITES']['TITLE']; */ ?>"
						     data-class="tools"
						     data-method="<? /*= $actions['FAVORITES']['ACTION']; */ ?>" data-id="<? /*= $item['ID'] */ ?>"
						     data-add="FAVORITES">
							<svg>
								<use xlink:href="#favorites"></use>
							</svg>
						</div>
					</div>
				</div>-->

				<div class="recommend-single__slider swiper-container">
					<div class="swiper-wrapper">
                        <? if ($item['GALLERY']) { ?>
                            <? foreach ($item['GALLERY'] as $picture) { ?>
								<a class="swiper-slide" href="<?= $item['DETAIL_PAGE_URL']; ?>">
									<picture class="recommend-single__img">
										<img src="<?= $picture['BIG'] ?>">
									</picture>
								</a>
                            <? } ?>
                        <? } else { ?>
							<a class="swiper-slide" href="<?= $item['DETAIL_PAGE_URL']; ?>">
								<picture class="recommend-single__img">
									<img src="<?= $item['PREVIEW_IMAGE']; ?>">
								</picture>
							</a>
                        <? } ?>
					</div>
                    <? if (count($item['GALLERY']) > 1) { ?>
						<div class="container-fluid swiper-navigation-wrapper">
							<div class="swiper-pagination"></div>
							<div class="swiper-navigation">
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
                    <? } ?>
				</div>
			</div>
		</div>
	</div>
</div>