<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
if ($arResult['ITEM']['OFFERS']) {
    foreach ($arResult['ITEM']['OFFERS'] as $offer) {
        if ($offer['PREVIEW_PICTURE']['ID']) {
            $arResult['ITEM']['GALLERY'][] = [
                'BIG' => CFile::ResizeImageGet($offer['PREVIEW_PICTURE']['ID'],
                    $arParams['IMG_SIZES'],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
                'SMALL' => CFile::ResizeImageGet($offer['PREVIEW_PICTURE']['ID'],
                    ['width' => 80, 'height' => 66],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )['src'],
                'ORIGINAL' => $offer['PREVIEW_PICTURE']['SRC'],
            ];
        }
    }
    if (empty($arResult['ITEM']['GALLERY'])) {
        $arResult['ITEM']['PREVIEW_IMAGE'] = CATALOG_NO_PHOTO;
    }
} else {
    if ($arResult['ITEM']['PREVIEW_PICTURE']['ID']) {
        $arResult['ITEM']['PREVIEW_IMAGE'] = CFile::ResizeImageGet($arResult['ITEM']['PREVIEW_PICTURE']['ID'],
            $arParams['IMG_SIZES'],
            BX_RESIZE_IMAGE_PROPORTIONAL
        )['src'];
    } else {
        $arResult['ITEM']['PREVIEW_IMAGE'] = CATALOG_NO_PHOTO;
    }
}

