<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */


$actions = [
    'FAVORITES' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('FAV_TEXT'),
    ],
    'COMPARE' => [
        'ACTION' => 'compfav',
        'CLASS' => '',
        'TITLE' => GetMessage('COMP_TEXT'),
    ],
];

if (in_array($item['ID'], $_SESSION['FAVORITES'])) {
    $actions['FAVORITES']['ACTION'] = 'compfavdelete';
    $actions['FAVORITES']['CLASS'] = 'is-active';
    $actions['FAVORITES']['TITLE'] = '';
}

if (in_array($item['ID'], $_SESSION['COMPARE'])) {
    $actions['COMPARE']['ACTION'] = 'compfavdelete';
    $actions['COMPARE']['CLASS'] = 'is-active';
    $actions['COMPARE']['TITLE'] = '';
}
?>
<div class="catalog-list__item col-md-6 col-xl-4" id="<?= $areaId ?>">
	<div class="item item_popup item_catalog js-item-list <? if ($item['GALLERY']) { ?>has-more-images<? } ?>">
		<div class="item__inner">
			<div class="item__labels">
				<div class="b-labels">
                    <? if ($actualItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']) { ?>
						<div class="b-labels__label b-labels__label_discount">Sale</div>
                    <? } ?>
                    <? if ($item['PROPERTIES']['HIT']['VALUE'] === 'Y') { ?>
						<div class="b-labels__label b-labels__label_hit">Hit</div>
                    <? } ?>
                    <? if ($item['PROPERTIES']['NEW']['VALUE'] === 'Y') { ?>
						<div class="b-labels__label b-labels__label_new">New</div>
                    <? } ?>
				</div>
			</div>
			<div class="item__slider swiper-container">
				<div class="swiper-wrapper">
                    <? if ($item['GALLERY']) { ?>
                        <? foreach ($item['GALLERY'] as $picture) { ?>
							<a class="swiper-slide" href="<?= $item['DETAIL_PAGE_URL']; ?>">
								<picture class="item__img">
									<img src="<?= $picture['BIG']; ?>">
								</picture>
							</a>
                        <? } ?>
                    <? } else { ?>
						<a class="swiper-slide" href="<?= $item['DETAIL_PAGE_URL']; ?>">
							<picture class="item__img">
								<img src="<?= $item['PREVIEW_IMAGE']; ?>">
							</picture>
						</a>
                    <? } ?>
				</div>
			</div>
			<div class="item__content">
				<a class="item__title" href="<?= $item['DETAIL_PAGE_URL']; ?>">
					<span><?= $item['NAME']; ?></span>
				</a>
                <? if ($item['PROPERTIES']['CML2_ARTICLE']['VALUE']) { ?>
					<div class="item__articul">
                        <?= $item['PROPERTIES']['CML2_ARTICLE']['NAME']; ?>
						<span><?= $item['PROPERTIES']['CML2_ARTICLE']['VALUE']; ?></span>
					</div>
                <? } ?>
				<div class="item__bottom">
                    <? if ($item['OFFERS']) { ?>
						<div class="item__colors">
							<div class="b-colors">
								<div class="b-colors__list">
                                    <?
                                    $colorsCnt = 0;
                                    foreach ($item['OFFERS'] as $offer) {
                                        if ($offer['PROPERTIES']['BAZOVYYTSVET']['VALUE'] && $arParams['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]) {
                                            $colorsCnt++;
                                            ?>
											<div class="b-colors__item"
											     style="background-color: <?= $arParams['COLORS'][$offer['PROPERTIES']['BAZOVYYTSVET']['VALUE']]; ?>"></div>
                                            <?
                                        }
                                    } ?>
								</div>
                                <? if ($colorsCnt > 5) { ?>
									<div class="b-colors__more">+<?= $colorsCnt - 5; ?></div>
                                <? } ?>
							</div>
						</div>
                    <? } ?>
					<div class="item__price">
                        <? if ($actualItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']) { ?>
							<div class="item__priceOld"><?= $actualItem['MIN_PRICE']['PRINT_VALUE']; ?></div>
                        <? } ?>
						<div class="item__priceNew"><?= $actualItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
					</div>
				</div>
				<!--<div class="item__options">
					<div class="item__option item__option_favorites <? /*= $actions['FAVORITES']['CLASS']; */ ?>"
					     data-toggle="tooltip"
					     data-placement="bottom" title="<? /*= $actions['FAVORITES']['TITLE']; */ ?>"
					     data-active-title="<? /*= GetMessage('FAV_ALREADY') */ ?>" data-type="option" data-class="tools"
					     data-method="<? /*= $actions['FAVORITES']['ACTION']; */ ?>" data-id="<? /*= $item['ID'] */ ?>"
					     data-add="FAVORITES">
						<svg>
							<use xlink:href="#favorites"></use>
						</svg>
					</div>
					<div class="item__option item__option_compare <? /*= $actions['COMPARE']['CLASS']; */ ?>"
					     data-toggle="tooltip"
					     data-placement="bottom" title="<? /*= $actions['COMPARE']['TITLE']; */ ?>"
					     data-active-title="<? /*= GetMessage('COMP_ALREADY') */ ?>" data-type="option" data-class="tools"
					     data-method="<? /*= $actions['COMPARE']['ACTION']; */ ?>" data-id="<? /*= $item['ID'] */ ?>"
					     data-add="COMPARE">
						<svg>
							<use xlink:href="#compare"></use>
						</svg>
					</div>
				</div>-->
			</div>
			<div class="item__footer">
				<div class="item__footerInner">
					<div class="item__button item__button_fast sp-fast-view" data-id="<?= $item['ID']; ?>">
						<svg>
							<use xlink:href="#eye"></use>
						</svg>
                        <?= GetMessage('FAST_VIEW'); ?>
					</div>
					<div class="item__button item__button_cart" data-class="basket" data-method="add2basket"
					     data-id="<?= $actualItem['ID']; ?>">
						<svg>
							<use xlink:href="#cart"></use>
						</svg>
                        <?= GetMessage('ADD_TO_BASKET'); ?>
					</div>
				</div>
			</div>
		</div>
        <? if (count($item['GALLERY']) > 0) { ?>
			<div class="item__thumbs">
				<div class="item__thumbsInner">
                    <? if (count($item['GALLERY']) > 7) { ?>
					<div class="slider-button-prev"></div>
					<?}?>
					<div class="item__thumbsSlider swiper-container">
						<div class="swiper-wrapper">
                            <? foreach ($item['GALLERY'] as $picture) { ?>
								<div class="swiper-slide">
									<picture class="item__thumbs-img">
										<img src="<?= $picture['SMALL']; ?>">
									</picture>
								</div>
                            <? } ?>
						</div>
					</div>
                    <? if (count($item['GALLERY']) > 7) { ?>
					<div class="slider-button-next"></div>
					<?}?>
				</div>
			</div>
        <? } ?>
	</div>
</div>