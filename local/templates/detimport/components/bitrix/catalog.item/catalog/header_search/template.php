<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */


?>
<a class="searchList__item" href="<?= $item['DETAIL_PAGE_URL']; ?>">
	<span class="searchList__img">
		<img src="<?= $item['GALLERY'][0]['SMALL'] ?: $item['PREVIEW_IMAGE'] ?>">
	</span>
	<span class="searchList__content">
		<span class="searchList__title"><?= $actualItem['NAME']; ?></span>
		<span class="searchList__price">
			<?= $actualItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?>
            <? if ($actualItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
				<span class="searchList__oldprice"><?= $actualItem['MIN_PRICE']['PRINT_VALUE']; ?></span>
            <? } ?>
		</span>
	</span>
</a>