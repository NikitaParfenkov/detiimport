<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

if ($item['PREVIEW_PICTURE']['ID']) {
    $picture = CFile::ResizeImageGet(
        $item['PREVIEW_PICTURE']['ID'],
        ['width' => 120, 'height' => 100],
        BX_RESIZE_IMAGE_PROPORTIONAL
    )['src'];
} else {
    $picture = CATALOG_NO_PHOTO;
}
?>

<div class="swiper-slide" id="<?= $areaId ?>">
	<div class="productListItem">
		<div class="productListItem__img">
			<img src="<?= $picture; ?>">
		</div>
		<div class="productListItem__content">
			<a class="productListItem__title" href="<?= $item['DETAIL_PAGE_URL']; ?>"><?= $item['NAME']; ?></a>
			<div class="productListItem__footer">
				<div class="productListItem__price">
					<div class="productListItem__priceNew"><?= $actualItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
                    <? if ($actualItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] > 0) { ?>
						<div class="productListItem__priceOld"><?= $actualItem['MIN_PRICE']['PRINT_VALUE']; ?></div>
                    <? } ?>
				</div>
				<div class="productListItem__btn btn btn-sm btn-dark rounded-pill" data-class="basket"
				     data-method="add2basket" data-id="<?= $actualItem['ID']; ?>">
					<svg>
						<use xlink:href="#cart"></use>
					</svg>
					<span><?= GetMessage('ADD_TO_BASKET'); ?></span>
				</div>
			</div>
		</div>
	</div>
</div>