<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="block-img col-md-12 col-xl-8">
    <div class="block-img__inner container-fluid">
        <div class="row">
            <div class="block-img__content col-md-3">
                <?= $arResult['BANNER_PROPERTIES']['CODE'];?>
            </div>
        </div>
        <?if ($arResult['PICTURE']) {?>
            <div class="block-img__img">
                <img src="<?= $arResult['PICTURE'];?>">
            </div>
        <?}?>
    </div>
</div>
