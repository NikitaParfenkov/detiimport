<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult['BANNER_PROPERTIES']['IMAGE_ID']) {
    $arResult['PICTURE'] = CFile::GetPath($arResult['BANNER_PROPERTIES']['IMAGE_ID']);
}