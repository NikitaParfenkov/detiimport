<?php

use Bitrix\Main\Page\Asset;
use SP\Tools\GEOTools;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$page = $APPLICATION->GetCurPage(false);

CModule::IncludeModule('sp.tools');
CModule::IncludeModule('sale');
$geo = new GEOTools();
$userCityInfo = $geo->getUserCityInfo();
$userCity = $userCityInfo->cityName;
$userCityCode = $geo->getUserCityCodeByName($userCity);
$cities = $geo->getCitiesList();
if ($_GET['city']) {
    $_SESSION['USER_CITY'] = $cities[$_GET['city']]['NAME_RU'];
    $_SESSION['USER_CITY_ID'] = $_GET['city'];
}

if (empty($_SESSION['USER_CITY'])) {
    $_SESSION['USER_CITY'] = $userCity ?: 'Москва';
    $_SESSION['USER_CITY_ID'] = $userCityCode ?: 84;
}
?>
<!doctype html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <?
    Asset::getInstance()->addString('<meta content="width=device-width, initial-scale=1" name="viewport">');
    Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="ie=edge">');
    Asset::getInstance()->addString('<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/main.css');

    Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?apikey=109e4b66-0503-4da1-b84c-bc91501ea5fa&amp;lang=ru_RU"
        type="text/javascript"></script>');
    Asset::getInstance()->addString('<script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>');
    Asset::getInstance()->addString('<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>');
    Asset::getInstance()->addString('<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>');
    Asset::getInstance()->addString('<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
        crossorigin="anonymous"></script>');
    Asset::getInstance()->addString('<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>');
    Asset::getInstance()->addString('<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>');
    $APPLICATION->ShowHead();
    ?>
	<title><?= $APPLICATION->ShowTitle() ?></title>
</head>
<body <? if ($page === '/') { ?>class="index"<? } ?> <? $APPLICATION->ShowViewContent('body_class') ?>>
<?php
$APPLICATION->ShowPanel(); ?>
<div class="main">
	<div class="top" <? if ($page === '/') { ?>data-theme-color="white"<? } ?>>
		<div class="container-fluid">
			<div class="top__inner row g-0">
				<div class="col-6 col-xl-9">
					<div class="top__left">
						<div class="top__header"><!--+e.headerInner.container-fluid-->
							<header class="header">
								<div class="row g-0">
									<div class="col-4">
										<a class="header__logo" <? if ($page !== '/') { ?>href="/"<? } ?>>
											<svg>
												<use xlink:href="#logo"></use>
											</svg>
											<span><?= GetMessage('DETIMPORT')?></span>
										</a>
										<div class="header__sity-choose">
											<div class="sity-choose">
												<div class="sity-choose__title"><?= GetMessage('CITY')?></div>
												<div class="sity-choose__select dropdown">
													<div class="dropdown-toggle" id="dropdownSityMenuButton" href="#"
													     role="button" data-toggle="dropdown" aria-expanded="false">
                                                        <?= $_SESSION['USER_CITY'] ?>
													</div>
                                                    <? if ($cities) { ?>
														<div class="dropdown-menu dropdown-menu-block"
														     aria-labelledby="dropdownSityMenuButton">
															<form class="b-search">
																<input class="form-control" type="text"
																       placeholder="<?= GetMessage('PUT_NAME')?>" name="city-search">
																<span></span>
															</form>
															<ul class="sp-cities-list">
                                                                <? foreach ($cities as $city) { ?>
																	<li>
																		<a class="dropdown-item" href="?city=<?= $city['ID']?>"><?= $city['NAME_RU']; ?></a>
																	</li>
                                                                <? } ?>
															</ul>
														</div>
														<div class="dropdown-menu dropdown-menu-block ask-city-wrap">
															<div class="ask-city">
																<button class="ask-city__close"></button>
																<div class="ask-city__question"><?= GetMessage('CITY')?>
																	<span class="ask-city__geo js_geoCity"><?= $_SESSION['USER_CITY'] ?></span>?
																</div>
																<div class="ask-city__buttons">
																	<button class="ask-city__accept-btn rounded-pill btn btn-sm btn-dark js js_accept-city">
																		<?= GetMessage('CITY_APPROVE')?>
																	</button>
																	<button class="ask-city__choose-btn rounded-pill btn btn-sm js js_accept-city btn-gray js js_open-cities">
                                                                        <?= GetMessage('CITY_CHANGE')?>
																	</button>
																</div>
															</div>
														</div>
                                                    <? } ?>
												</div>
											</div>
										</div>
									</div>
									<div class="col-8">
										<div class="header__aboveMenu">
                                            <?
                                            $GLOBALS['mainContactFilter']['PROPERTY_MAIN_CONTACT_VALUE'] = 'Y';
                                            $APPLICATION->IncludeComponent(
                                                "bitrix:news.list",
                                                "header_contacts",
                                                Array(
                                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "AJAX_MODE" => "N",
                                                    "AJAX_OPTION_ADDITIONAL" => "",
                                                    "AJAX_OPTION_HISTORY" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "CACHE_FILTER" => "N",
                                                    "CACHE_GROUPS" => "Y",
                                                    "CACHE_TIME" => "36000000",
                                                    "CACHE_TYPE" => "A",
                                                    "CHECK_DATES" => "Y",
                                                    "DETAIL_URL" => "",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "DISPLAY_DATE" => "Y",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "Y",
                                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "FIELD_CODE" => array("", ""),
                                                    "FILTER_NAME" => "mainContactFilter",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "IBLOCK_ID" => "9",
                                                    "IBLOCK_TYPE" => "contacts",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "INCLUDE_SUBSECTIONS" => "Y",
                                                    "MESSAGE_404" => "",
                                                    "NEWS_COUNT" => "1",
                                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => ".default",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PARENT_SECTION" => "",
                                                    "PARENT_SECTION_CODE" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "",
                                                    "PROPERTY_CODE" => array(
                                                        "PHONE",
                                                        "WHATSAPP",
                                                        "VIBER",
                                                        "TELEGRAM",
                                                        ""
                                                    ),
                                                    "SET_BROWSER_TITLE" => "N",
                                                    "SET_LAST_MODIFIED" => "N",
                                                    "SET_META_DESCRIPTION" => "N",
                                                    "SET_META_KEYWORDS" => "N",
                                                    "SET_STATUS_404" => "N",
                                                    "SET_TITLE" => "N",
                                                    "SHOW_404" => "N",
                                                    "SORT_BY1" => "SORT",
                                                    "SORT_BY2" => "NAME",
                                                    "SORT_ORDER1" => "ASC",
                                                    "SORT_ORDER2" => "ASC",
                                                    "STRICT_SECTION_CHECK" => "N"
                                                )
                                            ); ?>
											<div class="header__video-link">
												<a class="video-link" href="#" data-toggle="modal"
												   data-target="#modal-videoconsult">
		                                            <span class="video-link__icon">
			                                            <svg>
				                                            <use xlink:href="#video"></use>
			                                            </svg>
		                                            </span>
													<span><?= GetMessage('VIDEOCONSULT')?></span>
												</a>
											</div>
										</div>
										<div class="header__nav" data-move="main-nav-get">
											<nav class="mainNav">
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:catalog.section.list",
                                                    "header_menu",
                                                    array(
                                                        "ADD_SECTIONS_CHAIN" => "N",
                                                        "CACHE_FILTER" => "N",
                                                        "CACHE_GROUPS" => "Y",
                                                        "CACHE_TIME" => "36000000",
                                                        "CACHE_TYPE" => "A",
                                                        "COUNT_ELEMENTS" => "Y",
                                                        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                                                        "FILTER_NAME" => "sectionsFilter",
                                                        "IBLOCK_ID" => "4",
                                                        "IBLOCK_TYPE" => "1c_catalog",
                                                        "SECTION_CODE" => "",
                                                        "SECTION_FIELDS" => array("NAME", "PICTURE", ""),
                                                        "SECTION_ID" => "",
                                                        "SECTION_URL" => "",
                                                        "SECTION_USER_FIELDS" => array("", ""),
                                                        "SHOW_PARENT_NAME" => "Y",
                                                        "TOP_DEPTH" => "3",
                                                        "VIEW_MODE" => "LINE"
                                                    )
                                                ); ?>
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:menu",
                                                    "header_menu",
                                                    array(
                                                        "ALLOW_MULTI_SELECT" => "N",
                                                        "CHILD_MENU_TYPE" => "left",
                                                        "DELAY" => "N",
                                                        "MAX_LEVEL" => "3",
                                                        "MENU_CACHE_GET_VARS" => array(""),
                                                        "MENU_CACHE_TIME" => "3600",
                                                        "MENU_CACHE_TYPE" => "N",
                                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                                        "ROOT_MENU_TYPE" => "top",
                                                        "USE_EXT" => "N"
                                                    )
                                                ); ?>
                                                <? $APPLICATION->IncludeComponent(
                                                    "bitrix:menu",
                                                    "additional_header_menu",
                                                    array(
                                                        "ALLOW_MULTI_SELECT" => "N",
                                                        "CHILD_MENU_TYPE" => "left",
                                                        "DELAY" => "N",
                                                        "MAX_LEVEL" => "4",
                                                        "MENU_CACHE_GET_VARS" => array(""),
                                                        "MENU_CACHE_TIME" => "3600",
                                                        "MENU_CACHE_TYPE" => "N",
                                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                                        "ROOT_MENU_TYPE" => "additional_top",
                                                        "USE_EXT" => "N"
                                                    )
                                                ); ?>
											</nav>
										</div>
									</div>
								</div>
							</header>
						</div>
					</div>
				</div>
				<div class="top__right col-6 col-xl-3">
					<div class="top__rightInner">
						<div class="header-icons">
							<div class="header-icons__search" data-toggle="collapse" data-target="#mainSearch"
							     role="button">
								<svg>
									<use xlink:href="#search"></use>
								</svg>
							</div>
							<!--<div class="header-icons__favorites">
								<svg>
									<use xlink:href="#favorites"></use>
								</svg>
							</div>
							<div class="header-icons__compare">
								<svg>
									<use xlink:href="#compare"></use>
								</svg>
							</div>-->
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "header_basket",
                                Array(
                                    "HIDE_ON_BASKET_PAGES" => "N",
                                    "PATH_TO_AUTHORIZE" => "",
                                    "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                    "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                                    "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                    "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                    "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                    "POSITION_FIXED" => "N",
                                    "SHOW_AUTHOR" => "N",
                                    "SHOW_EMPTY_VALUES" => "N",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PERSONAL_LINK" => "N",
                                    "SHOW_PRODUCTS" => "N",
                                    "SHOW_REGISTRATION" => "N",
                                    "SHOW_TOTAL_PRICE" => "N"
                                )
                            ); ?>
							<!--+e.user--><!--  svg: use(xlink:href="#user")-->
							<!--<div class="header-icons__user header-icons__user header-icons__user_photo has-alert">
								<img src="<? /*= SITE_TEMPLATE_PATH */ ?>/asset/images/kit/ava_user.png">
							</div>-->
							<div class="header-icons__toggle" data-toggle="modal" data-target="#mainMenuMobileModal">
								<svg>
									<use xlink:href="#nav-link"></use>
								</svg>
							</div>
						</div>
						<div data-move="main-nav-toggle-set"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="mainSearch container-fluid <? if ($page !== '/search/') { ?>collapse<? } ?>" id="mainSearch">
			<form class="mainSearch__inner row g-0" action="/search/">
				<div class="mainSearch__control col col-md-6 offset-md-3">
					<input type="text" placeholder="<?= GetMessage('PUT_QUERY')?>" class="sp-search-input" name="q" value="<?= $_GET['q']?>">
					<div class="mainSearch__clear" data-search-clear></div>
					<div class="mainSearch__list">
						<div class="searchList">

						</div>
					</div>
				</div>
				<div class="mainSearch__buttons col-md-3">
					<button class="btn btn-dark" typeof="submit"><?= GetMessage('FIND')?></button>
					<div class="mainSearch__close" data-toggle="collapse" data-target="#mainSearch" role="button"></div>
				</div>
			</form>
		</div>
	</div>
    <? if ($cities) { ?>
		<div class="dropdown-menu dropdown-menu-block ask-city-wrap ask-city-wrap ask-city-wrap_mob">
			<div class="ask-city ask-city ask-city_mob">
				<button class="ask-city__close"></button>
				<div class="ask-city__question">
                    <?= GetMessage('CITY')?> <span class="ask-city__geo js_geoCity">
					<?= $_SESSION['USER_CITY']; ?></span> ?
				</div>
				<div class="ask-city__buttons">
					<button class="ask-city__accept-btn rounded-pill btn btn-sm btn-dark js js_accept-city"><?= GetMessage('CITY_APPROVE');?></button>
					<button class="ask-city__choose-btn rounded-pill btn btn-sm js js_accept-city btn-gray js js_open-cities_mob">
                        <?= GetMessage('CITY_CHANGE');?>
					</button>
				</div>
			</div>
		</div>
    <? } ?>
    <? if ($page !== '/') { ?>
    <? if ($APPLICATION->GetProperty('special_body') !== 'Y') { ?>
	<div class="container-fluid">
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "breadcrumbs",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        ); ?>
		<div class="b-back"><?= $_SESSION['PREVIOUS_TITLE']; ?></div>
        <? $_SESSION['PREVIOUS_TITLE'] = $APPLICATION->GetTitle(false); ?>
        <? if ($APPLICATION->GetProperty('special_body') == 'auth') { ?>
			<div class="row g-0">
				<div class="col-xl-9">
					<div class="title-header"><h1 class="main-title"><? $APPLICATION->ShowTitle(false); ?></h1>
						<!--TODO temporary closed functional-->
						<!--<div class="ask-login">Есть учетная запись?<span>Войти</span></div>-->
					</div>
				</div>
			</div>
        <? } else { ?>
			<h1 class="main-title"><? $APPLICATION->ShowTitle(false); ?></h1>
        <? } ?>
        <? if ($APPLICATION->GetProperty('show_side_menu') === 'Y' && ERROR_404 !== 'Y') { ?>
		<div class="mobile-nav">
			<div class="mobile-nav__title" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
			     aria-controls="collapseExample"><? $APPLICATION->ShowTitle(false); ?>
			</div>
			<div class="collapse" id="collapseExample">
				<div class="mobile-nav__content">
					<div data-move="page-menu-set"></div>
				</div>
			</div>
		</div>
		<div class="row g-0">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "side_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "side",
                    "USE_EXT" => "N"
                )
            ); ?>
            <? } ?>
            <? } ?>
            <? } ?>
