function reInitSlider() {
    let mainSlider = document.querySelector('.preview-slider__main-slider');
    let thumbs = document.querySelector('.preview-slider__thumbs');

    if (document.querySelectorAll('.preview-slider__thumb').length <= 8) {
        thumbs.classList.add('no-arrows')
    }

    setTimeout(function () {
        const thumbsSlider = new window.Swiper('.preview-slider__thumbs', {  speed: 500,
            slidesPerView: (window.innerWidth < 768) ? 3 : 8,
            direction: "vertical",
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        })
        let stageSlider = new Swiper(mainSlider, {
            speed: 500,
            slidesPerView: 1,
            direction: "vertical",
            thumbs: {
                swiper: thumbsSlider
            },
        });
    }, 200)
}

function reInitDetailSlider() {

    var s = new window.Swiper(".sliderProductThumbs .swiper-container", {
        slidesPerView: 3.3,
        watchSlidesVisibility: !0,
        watchSlidesProgress: !0
    });
    new window.Swiper(".sliderProduct .swiper-container", {
        effect: window.innerWidth >= 768 ? "fade" : "slide",
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        pagination: {el: ".swiper-pagination", type: "fraction", clickable: !0},
        breakpoints: {},
        thumbs: {swiper: s}
    });
}

$(document).on('click change submit', '[data-class]', function (e) {
    let spClass = $(this).attr('data-class');
    let method = $(this).attr('data-method');
    let $this = $(this);
    let data = {};
    data['method'] = method;
    switch (e.type) {
        case 'click':
            switch (method) {
                case 'compfav':
                    data['id'] = $this.attr('data-id');
                    data['add'] = $this.attr('data-add');
                    break;
                case 'compfavdelete':
                    data['id'] = $this.attr('data-id');
                    data['add'] = $this.attr('data-add');
                    break;
                case 'add2basket':
                    data['id'] = $this.attr('data-id');
                    break;
                default:
                    return false;
            }
            break;
        default:
            return false;
    }
    $.ajax({
        url: "/api/ajax/index.php",
        type: "POST",
        dataType: 'json',
        data: {action: spClass, data: data},
        success: function (result) {
            switch (method) {
                case 'compfav':
                    $this.attr('data-method', 'compfavdelete');
                    break;
                case 'compfavdelete':
                    $this.attr('data-method', 'compfav');
                    break;
                case 'add2basket':
                    $('.sp-basket-added').html(result.result.HTML);
                    new bootstrap.Modal(document.getElementById('modal-product-added'), {}).show();
                    break;
            }
        }
    });
    return false;

});

$(document).on('change click', '.sp-sort', function () {
    let sort;
    if (typeof $(this).attr('data-sort') !== 'undefined') {
        sort = $(this).attr('data-sort');
        $('#dropdownMenuButton').text($(this).text());
    } else {
        sort = $(this).val();
        $('.sp-sort option[value="' + sort + '"]').attr('selected', true);
    }
    $.ajax({
        url: window.location.href,
        type: "GET",
        data: {sort: sort},
        success: function (result) {
            $('#spCatalog').html($(result).find('#spCatalog').html());
        }
    });
    return false;

});

$(document).on('change', '[name="available"]', function () {
    let checked;
    if ($(this).prop('checked')) {
        checked = 'Y';
    } else {
        checked = 'N';
    }
    $.ajax({
        url: window.location.href,
        type: "GET",
        data: {HIDE_NOT_AVAILABLE: checked},
        success: function (result) {
            $('#spCatalog').html($(result).find('#spCatalog').html());
        }
    });
    return false;

});

$(document).on('input', '[name="filter_search"]', function () {
    let query = $(this).val().toLowerCase();
    $(this).parents('.filter-item__content').find('.sp-props-list .form-check-label').each(function () {
        if ($(this).text().toLowerCase().indexOf(query) !== -1) {
            $(this).parents('.form-check').show();
        } else {
            $(this).parents('.form-check').hide();
        }
    });

    return false;
});

$(document).on('change', '.sp-filter-input', function () {
    let clearBtn = false;
    let parentBlock = $(this).parents('.filter-item');
    parentBlock.find('.sp-filter-input').each(function () {
        if ($(this).prop('checked')) {
            clearBtn = true;
        }
    });
    if (clearBtn) {
        parentBlock.find('.filter-item__clear').show();
    } else {
        parentBlock.find('.filter-item__clear').hide();
    }
});

$(document).on('click', '.filter-item__clear', function () {
    let parentBlock = $(this).parents('.filter-item');
    let first = true;
    let firstElement;
    parentBlock.find('.sp-filter-input').each(function () {
        if ($(this).prop('checked')) {
            if (first) {
                firstElement = $(this);
                first = false;
            } else {
                $(this).prop('checked', false);
            }
        }
    });
    firstElement.click();
    $(this).hide();
});

$(document).on('click', '.sp-fast-view', function () {
    let elementID = $(this).attr('data-id');
    $.ajax({
        url: '/local/templates/detimport/ajax/fastViewElement.php',
        type: "POST",
        data: {elementID: elementID},
        success: function (result) {
            $('#fast-view-modal').html(result);
            reInitSlider();
            let modal = new bootstrap.Modal(document.getElementById('modal-quick-view'), {}).show();
        }
    });
    return false;
});

$(document).on('click', '.sp-change-offer-modal', function () {
    let elementID = $(this).parents('.sp-item-main-content').find('[name="elementID"]').val();
    let offerID = $(this).attr('data-id');
    $.ajax({
        url: '/local/templates/detimport/ajax/fastViewElement.php',
        type: "POST",
        data: {elementID: elementID, offerID: offerID},
        success: function (result) {
            $('.sp-item-main-content').html($(result).html());
            reInitSlider();
        }
    });
    return false;
});

$(document).on('click', '.sp-change-offer', function () {
    let offerID = $(this).attr('data-id');
    $.ajax({
        url: window.location.href,
        type: "GET",
        data: {offerID: offerID},
        success: function (result) {
            $('.sp-item-main-content').html($(result).find('.sp-item-main-content').html());
            $('.sp-element-side-info').html($(result).find('.sp-element-side-info').html());
            $('.sp-element-footer-mobile').html($(result).find('.sp-element-footer-mobile').html());
            reInitDetailSlider();
        }
    });
    return false;
});

$(document).on('input', '.sp-search-input', function () {
    let query = $(this).val();
    let spClass = 'search';
    let method = 'getFastSearchHTML';
    let data = {};
    data['method'] = method;
    data['q'] = query;
    $.ajax({
        url: "/api/ajax/index.php",
        type: "POST",
        data: {action: spClass, data: data},
        success: function (result) {
            $('.searchList').html(result.result.HTML);
            if (query.length > 0) {
                $('.searchList').css('display', 'block');
            } else {
                $('.searchList').css('display', 'none');
            }
        }
    });
    return false;
});
BX.bind(BX('submit-external-form'), 'click', function (e) {
    let formID = BX.data(this, 'form');
    BX.submit(BX(formID));
});
BX.bind(BX('modal-one-click-buy-form'), 'submit', function (e) {
    let objectParams = {};
    let okModal = new bootstrap.Modal(document.getElementById('modal-one-click-buy-ok'), {});
    let errorModal = new bootstrap.Modal(document.getElementById('modal-one-click-buy-error'), {});
    this.forEach(function (e) {
        let name = e.name;
        let value = e.value;
        objectParams[name] = value;
    });
    BX.bind(BX('tryAgain'), 'click', function () {
        errorModal.hide();
    });
    BX.ajax({
        url: '/api/ajax/index.php',
        data: {action: 'forms', data: {method: 'orderOneClick', params: objectParams}},
        method: 'POST',
        dataType: 'json',
        onsuccess: function (result) {
            let modalID;
            if (result.status === false) {
                errorModal.show();
            } else {
                BX.adjust(BX('order-number'), {text: '#' + result.result});
                $('#modal-one-click-buy').find('[data-dismiss]').click();
                okModal.show();
            }
        },
        onfailure: function () {
            let modal = new bootstrap.Modal(document.getElementById('modal-one-click-buy-error'), {});
            modal.show();
        }
    });
    e.preventDefault();
});

$(document).on('input', '[name="city-search"]', function () {
    let query = $(this).val().toLowerCase();
    $(this).parents('.dropdown-menu-block').find('.sp-cities-list li a').each(function () {
        if ($(this).text().toLowerCase().indexOf(query) !== -1) {
            $(this).parents('li').show();
        } else {
            $(this).parents('li').hide();
        }
    });

    return false;
});

$(document).on('click', '.bye-one-click', function () {
    $('#modal-one-click-buy').find('[name="ELEMENT_ID"]').val($(this).attr('data-id'));
    new bootstrap.Modal(document.getElementById('modal-one-click-buy'), {}).show();
})