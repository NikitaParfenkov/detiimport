<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if ($page !== '/') { ?>
    <? if ($APPLICATION->GetProperty('special_body') !== 'Y') { ?>
        <? if ($APPLICATION->GetProperty('show_side_menu') === 'Y') { ?>
			</div>
        <? } ?>
		</div>
    <? } ?>
<? } ?>
<div class="footer-top">
	<div class="container-fluid">
		<div class="row g-0">
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "footer_menu",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "Y",
                    "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                    "FILTER_NAME" => "sectionsFilter",
                    "IBLOCK_ID" => "4",
                    "IBLOCK_TYPE" => "1c_catalog",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("NAME", "PICTURE", ""),
                    "SECTION_ID" => "",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "1",
                    "VIEW_MODE" => "LINE"
                )
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "footer_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "footer",
                    "USE_EXT" => "N"
                )
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "social",
                Array(
                    'TITLE' => 'Присоединяйтесь',
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("", ""),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "3",
                    "IBLOCK_TYPE" => "marketing",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("ICON", "URL", ""),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N"
                )
            ); ?>
			<div class="col-12 col-lg-4 col-xl-3 order-sm-first">
				<a class="footer-top__logo" <? if ($page !== '/'){ ?>href="/"<? } ?>>
					<svg>
						<use xlink:href="#logo"></use>
					</svg>
                    <?= GetMessage('DETIMPORT') ?>
				</a>
				<div class="row">
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "footer_contacts",
                        Array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("", ""),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "9",
                            "IBLOCK_TYPE" => "contacts",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "2",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(
                                "PHONE",
                                "WHATSAPP",
                                "VIBER",
                                "TELEGRAM",
                                "ADDRESS",
                                "EMAIL"
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "NAME",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N"
                        )
                    );
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-middle">
	<div class="container-fluid">
		<div class="footer-middle__inner">
			<div class="row g-0">
				<div class="col-md-3">
					<div class="footer-middle__contact">
						<div class="contact2">
							<a class="contact2__video" href="#" data-toggle="modal" data-target="#modal-videoconsult">
								<span class="contact2__icon">
									<svg width="13" height="8">
										<use xlink:href="#video"></use>
									</svg>
								</span>
                                <?= GetMessage('VIDEOCONSULT') ?>
							</a>
							<a class="contact2__phone" href="#" data-toggle="modal" data-target="#modal-callback">
								<span class="contact2__icon">
									<svg width="14" height="14">
										<use xlink:href="#phone"></use>
									</svg>
								</span>
                                <?= GetMessage('GET_CALL') ?>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-5">
                    <? $APPLICATION->IncludeComponent(
                        "sp:subscribe.form",
                        "",
                        array(
                            'IBLOCK_ID' => '2',
                        ),
                        false
                    ); ?>
				</div>
				<div class="col-md-1"></div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/footer/payment.php"
                    )
                ); ?>
			</div>
		</div>
	</div>
</div>
<div class="to-top"><span></span></div>
<div class="footer-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/include/footer/copyright.php"
                    )
                ); ?>
			</div>
			<div class="col-md-5">
				<div class="footer-bottom__politic">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/include/footer/privacy.php"
                        )
                    ); ?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="footer-bottom__develop">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/include/footer/design.php"
                        )
                    ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="mainMenuMobileModal" tabindex="-1" aria-labelledby="mainMenuMobileModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-header"><a class="logo" href="#">
					<svg width="26" height="22">
						<use xlink:href="#logo"></use>
					</svg>
				</a>
				<div class="header-icons">
					<div class="header-icons__search" data-toggle="collapse" data-target="#mainSearch"
					     role="button">
						<svg>
							<use xlink:href="#search"></use>
						</svg>
					</div>
					<!--<div class="header-icons__favorites">
						<svg>
							<use xlink:href="#favorites"></use>
						</svg>
					</div>
					<div class="header-icons__compare">
						<svg>
							<use xlink:href="#compare"></use>
						</svg>
					</div>-->
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "header_basket",
                        Array(
                            "HIDE_ON_BASKET_PAGES" => "N",
                            "PATH_TO_AUTHORIZE" => "",
                            "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                            "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                            "PATH_TO_REGISTER" => SITE_DIR . "login/",
                            "POSITION_FIXED" => "N",
                            "SHOW_AUTHOR" => "N",
                            "SHOW_EMPTY_VALUES" => "N",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_PERSONAL_LINK" => "N",
                            "SHOW_PRODUCTS" => "N",
                            "SHOW_REGISTRATION" => "N",
                            "SHOW_TOTAL_PRICE" => "N"
                        )
                    ); ?>

					<!--+e.user--><!--  svg: use(xlink:href="#user")-->
					<!--<div class="header-icons__user header-icons__user header-icons__user_photo has-alert"><img
								src="<? /*= SITE_TEMPLATE_PATH */ ?>/asset/images/kit/ava_user.png"></div>-->
					<div class="header-icons__toggle" data-dismiss="modal">
						<svg>
							<use xlink:href="#close"></use>
						</svg>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="b-sity-choose-options">
					<div class="b-sity-choose-options__inner">
						<div class="sity-choose">
							<div class="sity-choose__title"><?= GetMessage('CITY') ?></div>
							<div class="sity-choose__select dropdown">
								<div class="dropdown-toggle" id="dropdownSityMenuButton" href="#" role="button"
								     data-toggle="dropdown" aria-expanded="false"><?= $_SESSION['USER_CITY']; ?>
								</div>
                                <? if ($cities) { ?>
									<div class="dropdown-menu dropdown-menu-block"
									     aria-labelledby="dropdownSityMenuButton">
										<form class="b-search">
											<input class="form-control" type="text" name="city-search"
											       placeholder="<?= GetMessage('PUT_QUERY') ?>">
											<span></span>
										</form>
										<ul class="sp-cities-list">
                                            <? foreach ($cities as $city) { ?>
												<li>
													<a class="dropdown-item"
													   href="?city=<?= $city['ID'] ?>"><?= $city['NAME_RU'] ?></a>
												</li>
                                            <? } ?>
										</ul>
									</div>
									<div class="dropdown-menu dropdown-menu-block ask-city-wrap">
										<div class="ask-city">
											<button class="ask-city__close"></button>
											<div class="ask-city__question"><?= GetMessage('CITY') ?>
												<span class="ask-city__geo js_geoCity"><?= $_SESSION['USER_CITY']; ?></span>
												?
											</div>
											<div class="ask-city__buttons">
												<button class="ask-city__accept-btn rounded-pill btn btn-sm btn-dark js js_accept-city">
                                                    <?= GetMessage('CITY_APPROVE') ?>
												</button>
												<button class="ask-city__choose-btn rounded-pill btn btn-sm js js_accept-city btn-gray js js_open-cities">
                                                    <?= GetMessage('CITY_CHANGE') ?>
												</button>
											</div>
										</div>
									</div>
                                <? } ?>
							</div>
						</div>
					</div>
					<!--<div class="b-icons">
						<div class="b-icons__favorites">
							<svg width="14" height="14">
								<use xlink:href="#favorites"></use>
							</svg>
							<span class="count">32</span></div>
						<div class="b-icons__compare">
							<svg width="14" height="14">
								<use xlink:href="#compare"></use>
							</svg>
							<span class="count">32</span>
						</div>
					</div>-->
				</div>
				<div data-move="main-nav-set"></div>
				<div class="modal-social">
					<div class="modal-social__line">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "modal_contacts",
                            Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array("", ""),
                                "FILTER_NAME" => "mainContactFilter",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "9",
                                "IBLOCK_TYPE" => "contacts",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "1",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                    "PHONE",
                                    "WHATSAPP",
                                    "VIBER",
                                    "TELEGRAM",
                                    ""
                                ),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "SORT",
                                "SORT_BY2" => "NAME",
                                "SORT_ORDER1" => "ASC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N"
                            )
                        );
                        ?>
					</div>
					<div class="contact2">
						<a class="contact2__video" href="#" data-toggle="modal" data-target="#modal-videoconsult">
							<span class="contact2__icon">
								<svg width="13" height="8">
									<use xlink:href="#video"></use>
								</svg>
							</span><?= GetMessage('VIDEOCONSULT'); ?></a>
						<a class="contact2__phone" href="#" data-toggle="modal" data-target="#modal-callback">
							<span class="contact2__icon">
								<svg width="14" height="14">
									<use xlink:href="#phone"></use>
								</svg>
							</span><?= GetMessage('GET_CALL'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<? $APPLICATION->IncludeComponent(
    "sp:form.main.feedback",
    "callback_form",
    Array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        'FORM_ID' => '2',
        'OK_TEXT' => 'Ваше сообщение успешно отправлено',
        'EVENT_MESSAGE_ID' => [
            0 => '88'
        ]
    )
); ?>
<div class="modal fade modal-default" id="modal-callback-ok" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('GET_CALL'); ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #7BC537">
						<svg width="14" height="11">
							<use xlink:href="#check"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('THANKS'); ?></div>
					<p class="text-md"><?= GetMessage('REQUEST_SUCCESS'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-default" id="modal-getcall-ok" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('WRITE_US'); ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #7BC537">
						<svg width="14" height="11">
							<use xlink:href="#check"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('MESSAGE_DELIVERED'); ?></div>
					<p class="text-md"><?= GetMessage('MANAGER_CONTACT'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-default" id="modal-callback-error" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('GET_CALL'); ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #FA002A">
						<svg width="14" height="11">
							<use xlink:href="#close"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('REQUEST_WRONG'); ?></div>
					<p class="text-md"><?= GetMessage('REQUEST_TRY_AGAIN'); ?></p>
				</div>
			</div>
			<!--<div class="modal-footer">
				<div class="d-grid">
					<button class="btn btn-dark btn-xl button-full-mobile" type="submit">Попробовать еще раз</button>
				</div>
			</div>-->
		</div>
	</div>
</div>

<? $APPLICATION->IncludeComponent(
    "sp:wrap",
    "videoconsult_form",
    Array()
); ?>

<div class="modal fade modal-default" id="modal-videoconsult-ok" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('VIDEOCONSULT'); ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #7BC537">
						<svg width="14" height="11">
							<use xlink:href="#check"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('THANKS'); ?></div>
					<p class="text-md"><?= GetMessage('VIDEO_REQUEST_SUCCESS'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-default" id="modal-videoconsult-error" tabindex="-1" aria-labelledby="modal-productLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('VIDEOCONSULT'); ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #FA002A">
						<svg width="14" height="11">
							<use xlink:href="#close"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('REQUEST_WRONG'); ?></div>
					<p class="text-md"><?= GetMessage('REQUEST_TRY_AGAIN'); ?></p>
				</div>
			</div>
			<div class="modal-footer">
				<div class="d-grid">
					<button class="btn btn-dark btn-xl button-full-mobile"
					        type="submit"><?= GetMessage('TRY_AGAIN'); ?></button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-default modal-quick-view" id="modal-quick-view" tabindex="-1"
     aria-labelledby="modal-productLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body" id="fast-view-modal">

			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-default modal-product-added" id="modal-product-added" tabindex="-1"
     aria-labelledby="modal-productLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('PRODUCT_ADDED') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body sp-basket-added">

			</div>
			<div class="modal-footer d-flex">
				<a class="btn btn-xl button-full-mobile" type="button" href="/basket/"><?= GetMessage('ORDER') ?></a>
				<a class="btn btn-dark btn-xl button-full-mobile" data-dismiss="modal"><?= GetMessage('CONTINUE_ORDERING') ?></a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-default" id="modal-one-click-buy" tabindex="-1"
     aria-labelledby="modal-productLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('BYE_ONE_CLICK') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<form id="modal-one-click-buy-form" method="post">
					<input type="hidden" name="ELEMENT_ID">
					<div class="mb-5"><input name="name" class="form-control form-control_2" type="text"
					                         placeholder="<?= GetMessage('BYE_ONE_CLICK_NAME_WORD') ?>">
						<div class="help-text"><?= GetMessage('BYE_ONE_CLICK_NAME') ?></div>
					</div>
					<div class="mb-5"><input required name="email" class="form-control form-control_2" type="text"
					                         placeholder="<?= GetMessage('BYE_ONE_CLICK_EMAIL_WORD') ?>">
						<div class="help-text"><?= GetMessage('BYE_ONE_CLICK_EMAIL') ?></div>
					</div>
					<div class="mb-5">
						<div class="d-flex">
							<input required name="phone" class="form-control form-control_2" type="text"
							       placeholder="<?= GetMessage('BYE_ONE_CLICK_PHONE_NUM') ?>">
						</div>
						<div class="help-text"><?= GetMessage('BYE_ONE_CLICK_PHONE') ?></div>
					</div>
					<div class="mb-5"><textarea name="comment" class="form-control form-control_2 autogrow"
					                            placeholder="<?= GetMessage('BYE_ONE_CLICK_COMMENT_WORD') ?>"
					                            rows="1"></textarea></div>
					<div class="form-check mb-5"><input required checked name="privacy" class="form-check-input"
					                                    id="flexCheckDefault"
					                                    type="checkbox"><label class="form-check-label"
					                                                           for="flexCheckDefault"><?= GetMessage('BYE_ONE_CLICK_I_AGREE') ?>
							<a href="/privacy-policy/"
							   class="text-decoration-underline"><?= GetMessage('BYE_ONE_CLICK_PRIVACY') ?></a></label>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="d-grid">
					<button class="btn btn-dark btn-xl button-full-mobile" id="submit-external-form"
					        type="submit"
					        data-form="modal-one-click-buy-form"><?= GetMessage('BYE_ONE_CLICK_READY') ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-default" id="modal-one-click-buy-ok" tabindex="-1"
     aria-labelledby="modal-productLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('BYE_ONE_CLICK') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #7BC537">
						<svg width="14" height="11">
							<use xlink:href="#check"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('BYE_ONE_CLICK_SUCCESS_TITLE') ?></div>
					<p class="text-md"><?= GetMessage('BYE_ONE_CLICK_SUCCESS_TEXT') ?></p>
					<p class="text-md">
						<span class="font-weight-normal"><?= GetMessage('BYE_ONE_CLICK_SUCCESS_ORDER_NUMBER') ?></span>
						<span id="order-number"><? /*dynamic content*/ ?></span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-default" id="modal-one-click-buy-error" tabindex="-1"
     aria-labelledby="modal-productLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-fullscreen-sm-down">
		<div class="modal-content">
			<div class="modal-header modal-header_border">
				<div class="modal-title"><?= GetMessage('BYE_ONE_CLICK') ?></div>
				<div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
					<svg width="16" height="16">
						<use xlink:href="#close"></use>
					</svg>
				</div>
			</div>
			<div class="modal-body">
				<div class="one-click-buy-ok-message">
					<div class="icon-circle mb-3" style="color: #FA002A">
						<svg width="14" height="11">
							<use xlink:href="#close"></use>
						</svg>
					</div>
					<div class="h3"><?= GetMessage('BYE_ONE_CLICK_ERROR_NOTIFY') ?></div>
					<p class="text-md"><?= GetMessage('BYE_ONE_CLICK_ERROR_MSG') ?></p></div>
			</div>
			<div class="modal-footer">
				<div class="d-grid">
					<button class="btn btn-dark btn-xl button-full-mobile" type="submit"
					        id="tryAgain"><?= GetMessage('BYE_ONE_CLICK_ERROR_TRY_ONE_MORE') ?>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-default" id="modal-order-map" tabindex="-1"
     aria-labelledby="modal-productLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-fullscreen-sm-down" style="max-width: 90%">
        <div class="modal-content">
            <div class="modal-header modal-header_border">
                <div class="modal-title">Выбор точки самовывоза</div>
                <div class="modal-close2" type="button" data-dismiss="modal" aria-label="Close">
                    <svg width="16" height="16">
                        <use xlink:href="#close"></use>
                    </svg>
                </div>
            </div>
            <div class="modal-body">
                <div id="order-shop-map" style="width: 100%;height: 400px"></div>
            </div>
        </div>
    </div>
</div>
<script src="<?= SITE_TEMPLATE_PATH; ?>/main.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/js/costume.js"></script>
</body>
</html>