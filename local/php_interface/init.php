<?php
if (file_exists(dirname(__DIR__, 3) . '/vendor/autoload.php')) {
    require_once dirname(__DIR__, 3) . '/vendor/autoload.php';
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/defines.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/agents.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/events.php';
include_once($_SERVER['DOCUMENT_ROOT'] . '/local/include/Mobile_Detect.php');