<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>Detimport</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="<?= SITE_TEMPLATE_PATH; ?>/main.css" rel="stylesheet">
</head>
<body>
<div class="demo-container-visible">
    <div class="error-screen">
        <a class="error-screen__logo" href="/">
            <img class="error-screen__logo-img"
                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAaCAYAAABPY4eKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFBSURBVHgB7ZbtkYIwEIY3zBVABXAdnNeBJdxVoHagJViBdqAlYAdYgXZg6CAVgO86YQY1IAv7T9+ZHSDZ5cnma9akaVrRazmYZauq6hhFUWahHnH0DSFmh9cJLIZlxpgVx5ue8Cfhh3sMYt01CA8+eWhTDgP4jWigEDzHjy9JkiypfYCbAJgV82wMhjcGsUGCs5buv47QyWg4C1lseYoDXa4jLFaBk5/GQPu5LQD+By04a4rk79YXS7KgcPYOm3WpCaeyLOfN79txwq7Ga1ZDYTm3cd8X6ernscEfxf+Qs2rmyGgq8VeFU/hMvwdcpA9cS07irA23EmdVOO7rQuKvCucKR+RPusolzmp3O5dVRVFYSYxW5pbrORJKBV5XoyTUWDhXoQtwRRut1pg1zz3Y0kD1hTtvZwCPeO7BFN1mIV0BQ0x4jtIiYDYAAAAASUVORK5CYII="
                 alt="" role="presentation"/>
            <span>Detimport</span>
        </a>
        <div class="error-screen__content">
            <div class="error-screen__head">
                <svg class="alert-icon" width="24" height="24">
                    <use xlink:href="#alert"></use>
                </svg>
                <div class="error-screen__head-desc">Сайт временно недоступен</div>
            </div>
            <div class="error-screen__text-block">
                <p class="error-screen__text-title">В данный момент сайт находится на техническом обслуживании.</p>
                <p class="error-screen__text-title">Приносим свои извинения за временные неудобства.</p>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH; ?>/main.js"></script>
</html>

