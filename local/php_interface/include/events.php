<?php

AddEventHandler("main", "OnPageStart", "internetExplorerCheck");

function internetExplorerCheck() {
    if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== false || strpos($_SERVER['HTTP_USER_AGENT'],'rv:11.0') !== false){
        include_once($_SERVER['DOCUMENT_ROOT'] . '/local/include/pages/ie_plug.php');
        die();
    }
}