<?php
function getSectionElementsCount()
{
    if (CModule::IncludeModule("iblock") && CModule::IncludeModule("sp.tools")) {
        \SP\Tools\Catalog::updateSectionElementsCount();
    }

    return 'getSectionElementsCount();';
}

function getSectionShowsCount()
{
    if (!CModule::IncludeModule("iblock") && !CModule::IncludeModule("sp.tools")) {
        \SP\Tools\Catalog::updateSectionShowsCount();
    }

    return 'getSectionShowsCount();';
}