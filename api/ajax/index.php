<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use SP\Tools\Handlers\Request\RequestHandlers;
use \Bitrix\Main\Loader;

Loader::includeModule('sp.tools');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
echo RequestHandlers::i()->process();